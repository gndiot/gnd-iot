<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_temperature_daily_2hrs".
 *
 * @property string $device_id
 * @property string $device_date
 * @property double $00:00-2:00
 * @property double $2:00-4:00
 * @property double $4:00-6:00
 * @property double $6:00-8:00
 * @property double $8:00-10:00
 * @property double $10:00-12:00
 * @property double $12:00-14:00
 * @property double $14:00-16:00
 * @property double $16:00-18:00
 * @property double $18:00-20:00
 * @property double $20:00-22:00
 * @property double $22:00-00:00
 */
class TemperatureDailyReportTwoHrs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_temperature_daily_2hrs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_date'], 'safe'],
            [['00:00-2:00', '2:00-4:00', '4:00-6:00', '6:00-8:00', '8:00-10:00', '10:00-12:00', '12:00-14:00', '14:00-16:00', '16:00-18:00', '18:00-20:00', '20:00-22:00', '22:00-00:00'], 'number'],
            [['device_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'device_date' => 'Device Date',
            '00:00-2:00' => '00:00 2:00',
            '2:00-4:00' => '2:00 4:00',
            '4:00-6:00' => '4:00 6:00',
            '6:00-8:00' => '6:00 8:00',
            '8:00-10:00' => '8:00 10:00',
            '10:00-12:00' => '10:00 12:00',
            '12:00-14:00' => '12:00 14:00',
            '14:00-16:00' => '14:00 16:00',
            '16:00-18:00' => '16:00 18:00',
            '18:00-20:00' => '18:00 20:00',
            '20:00-22:00' => '20:00 22:00',
            '22:00-00:00' => '22:00 00:00',
        ];
    }
    
    public function getDevice(){
    	
    		 $device=Devices::find()->select(["name","display_name"])->where(["id"=>$this->device_id])->asArray()->one();
    		 
    		 if(is_null($device))
    		 {
    		 	return null;
    		 	
    		 }else{
    		 	
    		 	 return $device;
    		 }
    		
    }
    
    public function fields(){
    	
    	   $fields=parent::fields();
    	   
    	   
    	   $fields['device']=function($model){
    	   		
    	   		return $model->device;
    	   	
    	   };
    	   
    	   
    	   return $fields;
    	
    }
    
    
}
