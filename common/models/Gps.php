<?php
namespace common\models;

use Yii;


class Gps extends Devices{
	
	public function getGps(){		 
		return $this->hasOne(DeviceGpsReading::className(),["device_id"=>"id"])->select(["hardware_serial","payload_raw","l_latitude","l_longitude"])->orderBy(["id"=>SORT_DESC]);
	}
	
	public static function find(){
		
		return parent::find()->where(["type"=>2]);
	}
	
	
	public function fields(){
		
		$fields=parent::fields();
		
		
		return $fields;
		
		
	}
	
}