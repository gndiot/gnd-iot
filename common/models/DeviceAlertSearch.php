<?php
namespace common\models;

use yii;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use common\models\Constants;
use common\models\DeviceAlert;

class DeviceAlertSearch extends DeviceAlert{
	
	public $from;
	public $to;
	
	public function rules(){
		
		return [
				[['customer_id', 'group_id', 'device_id', 'sms', 'email', 'push', 'status','user_id', 'created_at','updated_at'], 'integer'],
				[['body', 'notes'], 'string'],
				[['from','to'],'string'],
				[['alert_type', 'payload'], 'string', 'max' => 255],
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	
	public function search($params){
		
		$query=DeviceAlert::find();
	
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 500],
				],
				'sort'=>['defaultOrder' => ['id'=>SORT_DESC]]
		]);
			
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$query->andFilterWhere([
				'group_id'=>$this->group_id,
				'customer_id'=>$this->customer_id,
				'alert_type'=>$this->alert_type,
				'status'=>$this->status,
		]);
		
		$begin = new \DateTime($this->from);
		$begin->modify("today");
		$end = new \DateTime($this->to);
		$end->modify('+1 day');
		$end->modify('-1 minute');
		if($this->from !="" && $this->to !="")
		{
			Yii::trace("datetime");
			Yii::trace($begin->format("Y-m-d H:i:m")." ".$end->format("Y-m-d H:i:m"));
			
			$query->andFilterWhere(['between','created_at',$begin->getTimestamp(),$end->getTimestamp()]);
		}
		
		
		  return $dataProvider;
	}
	
	public function searchGroups($params,$groups,$customer_id){
			
		$query=DeviceAlert::find()->where(['group_id'=>$groups,'customer_id'=>$customer_id]);
	
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 500],
				],
				'sort'=>['defaultOrder' => ['id'=>SORT_DESC]]
		]);
		
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$query->andFilterWhere([
				'group_id'=>$this->group_id,
				'customer_id'=>$this->customer_id,
				'alert_type'=>$this->alert_type,
				'status'=>$this->status,
		]);
		
		
		$begin = new \DateTime($this->from);
		$begin->modify("today");
		$end = new \DateTime($this->to);
		$end->modify('+1 day');
		$end->modify('-1 minute');
		if($this->from !="" && $this->to !="")
		{
			Yii::trace("datetime");
			Yii::trace($begin->format("Y-m-d H:i:m")." ".$end->format("Y-m-d H:i:m"));
			
			$query->andFilterWhere(['between','created_at',$begin->getTimestamp(),$end->getTimestamp()]);
		}
	
		
		
		
		return $dataProvider;
			
		
		
	}
	
	
}