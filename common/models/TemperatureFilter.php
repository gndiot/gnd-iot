<?php

namespace common\models;


use yii\helpers\Json;
use backend\modules\api\models\DeviceGroups;

class TemperatureFilter extends Temperature{
	
	 
	 public function fields(){
	 	
	 	    $mFields=parent::fields();
	 	
	 	   	$fields=[
	 	   			'id',
	 	   			'name',
	 	   			'display_name',
	 	   			'customer_id',
	 	   			'group_id',
	 	   			'status',
	 	   			'config_status',
	 	   			'start_value',
	 	   			'end_value',
	 	   			'tag',
	 	   			'process',
	 	   			'entity',
	 	   			'department',
	 	   			'equipement_id'
	 	   	];
	 	   	
	 	   	
	 	   	$fields["config_temp"]=function($model){
	 	   		
	 	   		return Json::decode($model->config_temp,true);
	 	   		
	 	   		
	 	   	};
	 	   	
	 	   	$fields["customer"]=function($model){
	 	   		 
	 	   		
	 	   		return DeviceGroups::find()->select(["id AS value","name AS label"])->where(["id"=>$model->customer_id])->asArray()->one();
	 	   	};
	 	   	
	 	   	
	 	   	
	 	   	$fields["group"]=function($model){
	 	   		
	 	   		return DeviceGroups::find()->select(["id AS value","name AS label"])->where(["id"=>$model->group_id])->asArray()->one();
	 	   	};
	 	   	return $fields;
	 	   
	 	   	
	 	   	
	 	
	 }
	
}