<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "haccp_device".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $name
 */
class HaccpDevice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'haccp_device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'group_id'], 'integer'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'name' => 'Name',
        ];
    }
}
