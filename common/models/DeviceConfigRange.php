<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_config_range".
 *
 * @property int $id
 * @property int $device_id
 * @property double $start
 * @property double $end
 * @property string $color
 */
class DeviceConfigRange extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_config_range';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['device_id','color'], 'required'],
            [['device_id'], 'integer'],
            [['start', 'end'], 'number'],
            [['color'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'start' => 'Start',
            'end' => 'End',
            'color' => 'Color',
        ];
    }
    
    public function fields(){
    	
    	 $fileds= parent::fields();
    	 
    	 unset($fileds['id'],$fileds['device_id']);
    	 
    	 return $fileds;
    	 
    	
    }
}
