<?php

namespace common\models;

use Yii;
use backend\modules\api\models\DeviceGroups;
use backend\models\AdminUser;

/**
 * This is the model class for table "device_alerts".
 *
 * @property int $customer_id
 * @property int $group_id
 * @property int $id
 * @property int $device_id
 * @property string $alert_type
 * @property string $body
 * @property int $sms
 * @property int $email
 * @property int $push
 * @property string $notes
 * @property string $payload
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $user_id
 */
class DeviceAlert extends \yii\db\ActiveRecord
{
	const STATUS_ACK=10;
	const STATUS_NOT_ACK=15;
	const STATUS_CLEAR=20;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_alerts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'group_id', 'device_id', 'sms', 'email', 'push', 'status','user_id', 'created_at','updated_at'], 'integer'],
            [['body', 'notes'], 'string'],
            [['alert_type', 'payload'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'id' => 'ID',
            'device_id' => 'Device ID',
            'alert_type' => 'Alert Type',
            'body' => 'Body',
            'sms' => 'Sms',
            'email' => 'Email',
            'push' => 'Push',
            'notes' => 'Notes',
            'payload' => 'Payload',
            'status' => 'Status',
            'created_at' => 'Created At',
        	'updated_at'=>'update at',
        	'user_id'=>'user_id'
        ];
    }
    
    public function getCustomer(){
    	
    	 	return $this->hasOne(DeviceGroups::className(), ['id'=>'customer_id'])->select(['id','name']);    	
    }
    
    public function getGroup(){
    	
    	return $this->hasOne(DeviceGroups::className(), ['id'=>'group_id'])->select(['id','name']);

    }
    
    public function getDevice(){
    	
    	return $this->hasOne(Devices::className(), ['id'=>'device_id'])->select(['id','name','display_name','config_new']);
    }
    
    public function getUser(){
    	
    	return $this->hasOne(AdminUser::className(), ['id'=>'user_id'])->select(['first_name','last_name']);
    	
    }
	
    public function fields(){
    	
    		$fields=parent::fields();
    		
    		$fields['id']=function($model){
    			
    				return Yii::$app->utils->encrypt($model->id);
    		};
    		$fields['customer']=function($model){
    				
    				return $model->customer;
    		};
    		
    		$fields['group']=function($model){
    			
    			return $model->group;
    		};
    		
    		$fields['device']=function($model){
    			
    			return $model->device;
    		};
    		
    		$fields['user']=function($model){
    			
    			return $model->user;
    		};
    		
    		$fields['formatted_datetime']=function($model){
    			
    			if($model->created_at=='')
    			{
    				return '';
    			}
    			
    			$datetime=Yii::$app->utils->getDateTime($model->created_at);
    			
    			return $datetime->format('Y-m-d H:i:s');
    			
    		};
    		
    		$fields['formatted_datetime_update']=function($model){
    			
    			if($model->updated_at=='')
    			{
    				return '';
    			}
    			
    			$datetime=Yii::$app->utils->getDateTime($model->updated_at);
    			
    			return $datetime->format('Y-m-d H:i:s');
    			
    		};
    		
    		return $fields;
    		
    }
    
}
