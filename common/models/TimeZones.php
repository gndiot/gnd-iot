<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "time_zones".
 *
 * @property int $id
 * @property string $gmt
 * @property string $zone
 */
class TimeZones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'time_zones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gmt', 'zone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gmt' => 'Gmt',
            'zone' => 'Zone',
        ];
    }
}
