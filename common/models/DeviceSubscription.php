<?php

namespace common\models;

use Yii;
use backend\modules\api\models\DeviceGroups;

/**
 * This is the model class for table "device_subscriptions".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property int $dev_id
 * @property int $condition_code
 * @property double $value_one
 * @property double $value_two
 * @property string $attribute
 * @property int $is_custom
 * @property int $is_email
 * @property int $is_push
 * @property int $is_sms
 * @property string $email
 * @property string $mobile_numbers
 * @property int $is_default_email
 * @property int $is_default_sms
 * @property int $is_repeat
 * @property string $intervel_type
 * @property int $intervel_mins
 * @property int $last_run_time
 * @property int $next_run_time
 * @property int $started_at
 * @property int $notify_first_time
 * @property int $notification_status
 * @property int $created_at
 * @property int $updated_at
 * @property int $job_id
 */
class DeviceSubscription extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE=10;
	const STATUS_INACTIVE=20;
	const STATUS_DELETE=0;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_subscriptions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['customer_id','group_id','dev_id','attribute','is_custom'],'required'],
        	[['condition_code'],'required','when'=>function($model){
        		  
        			 if($model->is_custom==1){
        			 	
        			 	return true;	
        			 	
        			 }else{
        			 	 
        			 	 return false;
        			 }
        	}],
        	[['value_one'],'required','when'=>function($model){
        		
        		if($model->condition_code==Constants::CODE_BETWEEN || $model->condition_code==Constants::CODE_NOT_BETWEEN)
        		{
        			return false;
        			 
        		}else{
        			 
        			return true;
        		}
        		 
        	}],
        	[['value_one','value_two'],'required','when'=>function($model){
        		
        				if($model->condition_code==Constants::CODE_BETWEEN || $model->condition_code==Constants::CODE_NOT_BETWEEN)
        				{
        					 return true;
        					
        				}else{
        					
        					 return false;
        				}
        		
        	}],
            [['customer_id', 'group_id', 'dev_id', 'condition_code', 'is_custom', 'is_email', 'is_push', 'is_sms', 'is_default_email', 'is_default_sms', 'is_repeat', 'intervel_mins', 'last_run_time', 'next_run_time', 'started_at', 'notify_first_time', 'notification_status', 'created_at', 'updated_at','job_id'], 'integer'],
            [['value_one', 'value_two'], 'number'],
        	[['is_custom'],'default','value'=>0],
            [['attribute', 'email'], 'string', 'max' => 50],
            [['mobile_numbers'], 'string', 'max' => 20],
            [['intervel_mins'],'default','value'=>10],
            [['notification_status'],'default','value'=>self::STATUS_ACTIVE],
            [['intervel_type'], 'default', 'value' =>Constants::INTERVEL_TYPE_MINUTES],
            [['intervel_type'], 'string', 'max' => 255],
            [['is_default_email','is_default_sms','is_push','is_email','is_sms'],'default','value'=>0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ',
            'group_id' => 'Group ',
            'dev_id' => 'Device',
            'condition_code' => 'Condition Code',
            'value_one' => 'Value One',
            'value_two' => 'Value Two',
            'attribute' => 'Alert Type',
            'is_custom' => 'Is Custom',
            'is_email' => 'Is Email',
            'is_push' => 'Is Push',
            'is_sms' => 'Is Sms',
            'email' => 'Email',
            'mobile_numbers' => 'Mobile Numbers',
            'is_default_email' => 'Is Default Email',
            'is_default_sms' => 'Is Default Sms',
            'is_repeat' => 'Is Repeat',
            'intervel_type' => 'Intervel Type',
            'intervel_mins' => 'Intervel Mins',
            'last_run_time' => 'Last Run Time',
            'next_run_time' => 'Next Run Time',
            'started_at' => 'Started At',
            'notify_first_time' => 'Notify First Time',
            'notification_status' => 'Notification Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        	'job_id'=>'job id'
        ];
    }
    
    
    public function getCustomer(){
    	
    	 
    	 return $this->hasOne(DeviceGroups::className(), ["id"=>"customer_id"]);
    	
    }
    
    public function getGroup(){
    	
    	
    	 return $this->hasOne(DeviceGroups::className(), ["id"=>"group_id"]);
    	 
    }
    
    public function getDevice(){
    	
    	
    	$device=Devices::find()->select(["id as value","name as label"])->where(["id"=>$this->dev_id])->asArray()->one();
    	 	
    	return $device;
    }
    
    public function fields(){
    	
    		$fields=parent::fields();
    		
    		$fields['id']=function($model){
    			 
    			 return Yii::$app->utils->encrypt($model->id);
    		};
    		
    		$fields["customer"]=function($model){
    			 
    			return $model->customer;
    		};
    		
    		$fields["group"]=function($model){
    		
    			return $model->group;
    		};
    		
    		$fields["device"]=function($model){
    		
    			return $model->device;
    		};
    		
    		return $fields;
    	
    }
    
    
}
