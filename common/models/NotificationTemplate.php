<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notification_template".
 *
 * @property int $id
 * @property string $alert_type
 * @property string $template_type
 * @property string $subject
 * @property string $content_body
 */
class NotificationTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
    	return [
    			[['alert_type', 'template_type'], 'required'],
    			[['template_type', 'content_body'], 'string'],
    			[['template_type'],'in','range'=>['sms','email']],
    			[['alert_type'], 'string', 'max' => 50],
    			[['subject'], 'string', 'max' => 100],
    	];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alert_type' => 'Alert Type',
            'template_type' => 'Template Type',
            'subject' => 'Subject',
            'content_body' => 'Content Body',
        ];
    }
}
