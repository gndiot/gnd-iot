<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_gps_readings".
 *
 * @property int $id
 * @property string $device_id
 * @property string $hardware_serial
 * @property string $payload_raw
 * @property int $battery
 * @property int $configuration_info
 * @property int $configuration_info_two
 * @property int $door
 * @property int $humidity
 * @property string $l_latitude
 * @property string $l_longitude
 * @property int $movement
 * @property int $switchone
 * @property int $switch_two
 * @property double $temperature
 * @property int $created_at
 */
class DeviceGpsReading extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_gps_readings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['hardware_serial','payload_raw','battery','configuration_info','configuration_info_two','door','humidity','l_latitude','l_longitude','movement','switchone','switch_two','temperature','created_at'],'required'],
            [['payload_raw'], 'string'],
            [['battery', 'configuration_info', 'configuration_info_two', 'door', 'humidity', 'movement', 'switchone', 'switch_two', 'created_at'], 'integer'],
            [['l_latitude', 'l_longitude', 'temperature'], 'number'],
            [['device_id'], 'string', 'max' => 11],
            [['hardware_serial'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'hardware_serial' => 'Hardware Serial',
            'payload_raw' => 'Payload Raw',
            'battery' => 'Battery',
            'configuration_info' => 'Configuration Info',
            'configuration_info_two' => 'Configuration Info Two',
            'door' => 'Door',
            'humidity' => 'Humidity',
            'l_latitude' => 'L Latitude',
            'l_longitude' => 'L Longitude',
            'movement' => 'Movement',
            'switchone' => 'Switchone',
            'switch_two' => 'Switch Two',
            'temperature' => 'Temperature',
            'created_at' => 'Created At',
        ];
    }
}
