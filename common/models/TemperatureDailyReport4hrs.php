<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_temperature_daily_4hrs".
 *
 * @property string $device_id
 * @property string $device_date
 * @property double $00:00-04:00
 * @property double $04:00-08:00
 * @property double $08:00-12:00
 * @property double $12:00-16:00
 * @property double $16:00-20:00
 * @property double $20:00-00:00
 */
class TemperatureDailyReport4hrs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_temperature_daily_4hrs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_date'], 'safe'],
            [['00:00-04:00', '04:00-08:00', '08:00-12:00', '12:00-16:00', '16:00-20:00', '20:00-00:00'], 'number'],
            [['device_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'device_date' => 'Device Date',
            '00:00-04:00' => '00:00 04:00',
            '04:00-08:00' => '04:00 08:00',
            '08:00-12:00' => '08:00 12:00',
            '12:00-16:00' => '12:00 16:00',
            '16:00-20:00' => '16:00 20:00',
            '20:00-00:00' => '20:00 00:00',
        ];
    }
    
    public function getDevice(){
    	 
    	$device=Devices::find()->select(["name","display_name"])->where(["id"=>$this->device_id])->asArray()->one();
    	 
    	if(is_null($device))
    	{
    		return null;
    
    	}else{
    
    		return $device;
    	}
    
    }
    
    public function fields(){
    	 
    	$fields=parent::fields();
    
    
    	$fields['device']=function($model){
    
    		return $model->device;
    			
    	};
    
    
    	return $fields;
    	 
    }
}
