<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_door_readings".
 *
 * @property int $id
 * @property string $device_id
 * @property string $hardware_serial
 * @property string $payload_raw
 * @property int $battery
 * @property int $status
 * @property int $created_at
 */
class DeviceDoorReading extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_door_readings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['hardware_serial','payload_raw','battery','status','created_at'],'required'],
            [['payload_raw'], 'string'],
            [['battery', 'status', 'created_at'], 'integer'],
            [['device_id'], 'string', 'max' => 11],
            [['hardware_serial'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'hardware_serial' => 'Hardware Serial',
            'payload_raw' => 'Payload Raw',
            'battery' => 'Battery',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
}
