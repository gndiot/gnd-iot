<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "uplink_request_log".
 *
 * @property int $id
 * @property string $request_url
 * @property string $request_data
 * @property string $response_data
 * @property int $created_at
 * @property int $status
 */
class UplinkRequestLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uplink_request_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_url', 'request_data', 'response_data'], 'string'],
            [['created_at', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_url' => 'Request Url',
            'request_data' => 'Request Data',
            'response_data' => 'Response Data',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }
}
