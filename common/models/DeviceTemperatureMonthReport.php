<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_temperature_month_report".
 *
 * @property string $device_id
 * @property string $device_Date
 * @property double $low_reading
 * @property double $high_reading
 * @property double $total_avg
 * @property string $total_samples
 * @property string $out_of_samples
 */
class DeviceTemperatureMonthReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_temperature_month_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_Date'], 'safe'],
            [['low_reading', 'high_reading', 'total_avg'], 'number'],
            [['total_samples', 'out_of_samples'], 'integer'],
            [['device_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'device_Date' => 'Device  Date',
            'low_reading' => 'Low Reading',
            'high_reading' => 'High Reading',
            'total_avg' => 'Total Avg',
            'total_samples' => 'Total Samples',
            'out_of_samples' => 'Out Of Samples',
        ];
    }
    
    public function getDevice(){
    	 
    	$device=Devices::find()->select(["name","display_name"])->where(["id"=>$this->device_id])->asArray()->one();
    	 
    	if(is_null($device))
    	{
    		return null;
    
    	}else{
    
    		return $device;
    	}
    
    }
    
    public function fields(){
    	 
    	$fields=parent::fields();
    
    
    	$fields['device']=function($model){
    
    		return $model->device;
    			
    	};
    
    
    	return $fields;
    	 
    }
}
