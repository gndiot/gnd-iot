<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_temperature_readings".
 *
 * @property int $id
 * @property string $device_id
 * @property string $hardware_serial
 * @property string $payload_raw
 * @property int $battery
 * @property double $temperature
 * @property int $humidity
 * @property int $created_at
 */
class DeviceTemperatureReading extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_temperature_readings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['hardware_serial','payload_raw','battery','temperature','humidity','created_at'],'required'],
            [['payload_raw'], 'string'],
            [['battery', 'humidity', 'created_at'], 'integer'],
            [['temperature'], 'number'],
            [['device_id'], 'string', 'max' => 11],
            [['hardware_serial'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'hardware_serial' => 'Hardware Serial',
            'payload_raw' => 'Payload Raw',
            'battery' => 'Battery',
            'temperature' => 'Temperature',
            'humidity' => 'Humidity',
            'created_at' => 'Created At',
        ];
    }
    
    
    public function fields(){
    	 
    	$fields= parent::fields();
    	$fields['formatted_date']=function($model){
    		 
    		$date= Yii::$app->utils->getDateTime($model->created_at);
    		
    		if(!is_null($date))
    		{
    			return $date->format('Y-m-d H:i:s');
    		}
    	 
    	};
    	
    	return $fields;
    }
}
