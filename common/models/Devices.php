<?php

namespace common\models;

use Yii;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\Json;

/**
 * This is the model class for table "devices".
 *
 * @property int $id
 * @property string $hardware_serial
 * @property int $customer_id
 * @property int $type
 * @property string $name
 * @property string $display_name
 * @property int $group_id
 * @property double $start_value
 * @property double $end_value
 * @property int $response_intervel
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $sync_time
 * @property int $sync_last_time
 * @property int $admin_id
 * @property string $config_new
 * @property string $config_temp
 * @property int $config_status
 * @property int $bc_req_status
 * @property int $bc_req_time
 * @property string $data
 * $property int $battery_level
 */
class Devices extends \yii\db\ActiveRecord
{
	
	 const TEMPERATURE='TEMPERATURE';
	 const GPS='GPS';
	 const DOOR='DOOR';
	 public $deviceRange=[];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['hardware_serial','name','response_intervel','start_value','end_value','status'],'required'],
            [['customer_id', 'type', 'group_id', 'response_intervel', 'status', 'created_at', 'updated_at', 'sync_time', 'sync_last_time', 'admin_id', 'customer_user_id','config_status','bc_req_time','bc_req_status'], 'integer'],
            [['start_value', 'end_value'], 'number'],
        	['start_value', 'default', 'value' => 0],
        	['end_value', 'default', 'value' => 80],
            [['hardware_serial', 'display_name'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 50],
        	[['config_new','config_temp'],'string'],
        	['data','string'],
        	[['equipement_id','entity','department','process'],'string'],
        	[['battery_level','tag'],'integer'],
            [['hardware_serial'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hardware_serial' => 'Hardware Serial',
            'customer_id' => 'Customer ID',
            'type' => 'Type',
            'name' => 'Name',
            'display_name' => 'Display Name',
            'group_id' => 'Group ID',
            'start_value' => 'Start Value',
            'end_value' => 'End Value',
            'response_intervel' => 'Response Intervel',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sync_time' => 'Sync Time',
            'sync_last_time' => 'Sync Last Time',
            'admin_id' => 'Admin ID',
            'customer_user_id' => 'Customer User ID',
        	'config_new'=>'config_new',
        	'config_temp'=>'config_old',
        	'config_status'=>'config_status',
        	'bc_req_status'=>'bc_req_status',
        	'bc_req_time'=>'bc_req_time',
        	'data'=>'data',
        	'battery_level'=>'battery level',
        	'entity'=>'entity',
        	'department'=>'department',
        	'process'=>'process',
        	'tag'=>'tag',
        	'equipement_id'=>'equipement_id'
        ];
    }
    
    public function getRange(){
    	
    	   return $this->hasMany(DeviceConfigTemperature::className(), ["device_id"=>"id"]);
    }
    
    public function customloadMultiple($data=[],$key=null){
    	 
    	 
    	 if(array_key_exists($key, $data))
    	 {
    	 	$scope = $key === null ? $data : $data[$key];
    	 	 
    	 	if($scope != "" && !empty($data))
    	 	{
    	 		for ($i = 0; $i < count($data[$key]); $i++) {
    	 	
    	 			$obj=new DeviceConfigRange();
    	 			$obj->load($data[$key][$i],"");
    	 			$obj->device_id=$this->id;
    	 			array_push($this->deviceRange, $obj);
    	 		}
    	 	}
    	 	
    	 }
    	
    	
    	
    
    	   	
    }
    
    public function loadMultipleValidation(){
    	
    	    $status=true;
    	    
    		$count=count($this->deviceRange);
    		
    	   for ($i = 0; $i < $count; $i++) {
    	   	
    	   	    if(!$this->deviceRange[$i]->validate())
    	   	    {
    	   	    	$status=false;
    	   	    }
    	   	     
    	   }
    	    
    	  return $status;
    	
    }
    
    public function getMultipleValidation(){
    	
    	  
    	  $errors=[];
    	  
    	  $count=count($this->deviceRange);
    	  for ($i = 0; $i < $count; $i++) {
    	  		
    	  	if(!$this->deviceRange[$i]->validate())
    	  	{
    	  		array_push($errors,$this->deviceRange[$i]->getErrors());
    	  	}else{
    	  		 
    	  		array_push($errors,[]);
    	  		
    	  	}
    	  
    	  }
    	  
    	  return $errors;
    	 
    }
    
    
    public function saveMultiple(){
    	
    	$count=count($this->deviceRange);
    	
    	 if($count >0)
    	  DeviceConfigRange::deleteAll(["device_id"=>$this->id]);
    	 
    	for ($i = 0; $i < $count; $i++) {
    		$this->deviceRange[$i]->device_id=$this->id;
    		$this->deviceRange[$i]->save(false); 
    	}
    	
    	return true;
    	
    }
   
    public function getTag(){
    	
    	   return $this->hasOne(MasterTag::className(), ['id'=>'tag']);
    	
    }
    
    public function getProcess(){
    	
    	return $this->hasOne(MasterProcess::className(), ['id'=>'process']);
    	
    }
    
    public function getDepartment(){
    	
    	return $this->hasOne(MasterDepartment::className(), ["id"=>'department']);
    	
    }
    
    public function getEntity(){
    	
    	return $this->hasOne(MasterEntity::className(), ['id'=>'entity']);
    }
    
    public function fields(){
    	
    	  $fields= parent::fields();
    	  
    	//  unset($fields['config_temp']);
    	  
    	  $fields["customer"]=function($model){
    	  	 
    	  		  return DeviceGroups::find()->select(["id AS value","name AS label"])->where(["id"=>$model->customer_id])->asArray()->one();
    	  };
    	  
    	  
    	  
    	  $fields["group"]=function($model){
    	  	  	
    	  		  return DeviceGroups::find()->select(["id AS value","name AS label"])->where(["id"=>$model->group_id])->asArray()->one();
    	  };
    	  
    
    	   $fields["sync_time"]=function($model){
    	   	 
	    	   	if($model->sync_time=="")
	    	   	{
	    	   		
	    	   		return "";
	    	   	}
	    	   	
	    	   	$date=new \DateTime();
	    	   	$date->setTimestamp($model->sync_time);
	    	   	
	    	   	return $date->format("Y-m-d H:i:s");
    	   	
    	   	
    	   };
    	   
    	   $fields["config_new"]=function($model){
    	   	
    	   	return Json::decode($model->config_new);
    	   	
    	   };
    	   
    	   $fields["time_ago"]=function($model){
    	   	
    	   	
    	   	if($model->sync_time=="")
    	   	{
    	   		return "Not Set";
    	   	}
    	   	
    	   	
    	   	$config=Json::decode($model->config_new);
    	   	$date1=new \DateTime();
    	   	
    	   	$date2=new \DateTime();
    	   	$date2->setTimestamp($model->sync_time);
    	   	$interval=$date2->diff($date1);
    	   	
    	   	$diffInSeconds=abs($date1->getTimestamp()-$date2->getTimestamp());
    	   	
    	   	return $interval->format('%i minutes %s seconds');
    	   	
    	   	
    	   	
    	   	
    	   };
    	   
    	   $fields['connection_state']=function($model){
    	   	
    	   	$config=Json::decode($model->config_new);
    	   	
    	   	$date1=new \DateTime();
    	   	
    	   	$date2=new \DateTime();
    	   	$date2->setTimestamp($model->sync_time);
    	   	
    	   	if($model->sync_time=="")
    	   	{
    	   		return "Disconnected";
    	   	}
    	   	
    	   	$diffInSeconds=$date2->getTimestamp()-$date1->getTimestamp();
    	   	
    	   	$reporting_interval=intval($config["reporting_interval"])+Yii::$app->params["no_communication"];
    	   	
    	   	$convertMints=($reporting_interval*60);
    	   	
    	   	if(abs($diffInSeconds) > $convertMints)
    	   	{
    	   		
    	   		return "Disconnected";
    	   		
    	   	}else{
    	   		
    	   		return "Connected";
    	   		
    	   	}
    	   	
    	   	
    	   };
    	   
    	   
    	  
    	  
    	  return $fields;
    	  
    	 
    	
    }
    
 
    
}
