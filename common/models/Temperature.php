<?php
namespace common\models;

use Yii;
use yii\helpers\Json;


Class Temperature extends Devices{
	
	
	public function getTemperature(){
			
		 return $this->hasOne(DeviceTemperatureReading::className(), ["device_id"=>"id"])->select(["hardware_serial","payload_raw","battery","temperature","humidity"])->orderBy(["id"=>SORT_DESC]);
		
	}
	
	public static function find(){
		
		return parent::find()->where(["type"=>1]);
		
	}
	
	
	public function getTemperatureConfig(){
		
		return $this->hasMany(DeviceConfigTemperature::className(), ["device_id"=>"id"]);
		
	}
	
	public function getHumidityConfig(){
		
		return $this->hasMany(DeviceConfigHumidity::className(), ["device_id"=>"id"]);
		
	}
	
	public function getBatteryConfig(){
		
		return $this->hasMany(DeviceConfigBattery::className(), ["device_id"=>"id"]);
		
	}
	
	public function getRecentAlert(){
			
		$device=DeviceAlert::find()->select(["id","alert_type"])->where(["device_id"=>$this->id,'alert_type'=>[Constants::ALERT_HIGH_TEMPERATURE,Constants::ALERT_LOW_TEMPERATURE],'status'=>15])->orderBy(["id"=>SORT_DESC])->asArray()->one();
		if(!empty($device))
		{
			$device["id"]= Yii::$app->utils->encrypt($device["id"]);
		}
		
		return $device;
	}
	//used for filter
	public function getAlertType($alertType){
		
		$output=["low_alert"=>"0","medium_alert"=>"0","high_alert"=>"0"];
		//disabled for performance reason
		//$last_response=DeviceTemperatureReading::find()->where(["device_id"=>$this->id])->orderBy(["id"=>SORT_DESC])->asArray()->one();
		$last_response=Json::decode($this->data);
		
		$config=Json::decode($this->config_new);
		
		if(empty($last_response))
		{
			return $output;
		}
		
		//$alerts=DeviceConfigTemperature::find()->where(["device_id"=>$this->id])->asArray()->all();
		
		if($config["temperature_range"]["low"] > $last_response["temperature"])
		{
			 if($alertType=="Low Temperature")
			 {
			 	return true;
			 }
			
		}elseif ($config["temperature_range"]["high"] < $last_response["temperature"])
		{
			
			if($alertType=="High Temperature")
			{
				return true;
			}
			
		}else{
			
			if($alertType=="Normal Temperature")
			{
				return true;
			}
			
		}
		
		return true;
		
		
		
	}
	
	public function getTemperatureAlert(){
		 
		$output=["low_alert"=>"0","medium_alert"=>"0","high_alert"=>"0"];
		//disabled for performance reason
		//$last_response=DeviceTemperatureReading::find()->where(["device_id"=>$this->id])->orderBy(["id"=>SORT_DESC])->asArray()->one();
		$last_response=Json::decode($this->data);
	
		$config=Json::decode($this->config_new);
		
		if(empty($last_response))
		{
			 return $output;
		}
		
		$alerts=DeviceConfigTemperature::find()->where(["device_id"=>$this->id])->asArray()->all();
		
		
		if($config["temperature_range"]["low"] > $last_response["temperature"])
		{
			$output["low_alert"]="1";
			
		}elseif ($config["temperature_range"]["high"] < $last_response["temperature"])
		{
			
			$output["high_alert"]="1";
			
		}else{
			
			$output["medium_alert"]="1";
		}
		
		
		
		return $output;
		 
		 
	}
	
	private function hasAlert($value,$max,$min){
		
		 	
			if($value >= $max && $value < $min)
			{
				return "1";
				
			}else{
				 
				 return "0";
			}
		  
		
		
	}
	
	
	 public function fields(){
	 	
	 	$fields=parent::fields();
	 	 
	 	$fields['alerts']=function($model){
	 	
	 		return [
	 				"temperature"=>$model->temperatureAlert,
	 				"battery"=>[],
	 				"humidity"=>[],
	 		];
	 	
	 	
	 	};
	 	
	 	$fields["config_temp"]=function($model){
	 		
	 		return Json::decode($model->config_temp,true);
	 		
	 	};
	 	
	 	$fields["config"]=function($model){
	 		   
	 		return [
	 					"temperature"=>$model->temperatureConfig,
	 					"humidity"=>$model->humidityConfig,
	 					"battery"=>$model->batteryConfig
	 			];
	 		
	 	};
	 	
	 	$fields['data']=function($model){
	 		 
	 		 return Json::decode($model->data);

	 	};
	 	$fields['recent_alert']=function($model){
	 		
	 		return $model->recentAlert;
	 		
	 	};
	 		return $fields;
	 	
	 }
	
}