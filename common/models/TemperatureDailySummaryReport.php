<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_temperature_daily_summary".
 *
 * @property string $device_id
 * @property string $device_date
 * @property double $am_min
 * @property double $am_max
 * @property double $am_avg
 * @property string $am_samples
 * @property double $pm_min
 * @property double $pm_max
 * @property double $pm_avg
 * @property string $pm_sample
 * @property double $total_min
 * @property double $total_max
 * @property double $total_avg
 * @property string $total_sample
 */
class TemperatureDailySummaryReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_temperature_daily_summary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_date'], 'safe'],
            [['am_min', 'am_max', 'am_avg', 'pm_min', 'pm_max', 'pm_avg', 'total_min', 'total_max', 'total_avg'], 'number'],
            [['am_samples', 'pm_sample', 'total_sample'], 'integer'],
            [['device_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'device_date' => 'Device Date',
            'am_min' => 'Am Min',
            'am_max' => 'Am Max',
            'am_avg' => 'Am Avg',
            'am_samples' => 'Am Samples',
            'pm_min' => 'Pm Min',
            'pm_max' => 'Pm Max',
            'pm_avg' => 'Pm Avg',
            'pm_sample' => 'Pm Sample',
            'total_min' => 'Total Min',
            'total_max' => 'Total Max',
            'total_avg' => 'Total Avg',
            'total_sample' => 'Total Sample',
        ];
    }
    
    public function getDevice(){
    	 
    	$device=Devices::find()->select(["name","display_name"])->where(["id"=>$this->device_id])->asArray()->one();
    	 
    	if(is_null($device))
    	{
    		return null;
    
    	}else{
    
    		return $device;
    	}
    
    }
    
    public function fields(){
    	 
    	$fields=parent::fields();
    
    
    	$fields['device']=function($model){
    
    		return $model->device;
    			
    	};
    
    
    	return $fields;
    	 
    }
}
