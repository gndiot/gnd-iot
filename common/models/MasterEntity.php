<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_entity".
 *
 * @property int $id
 * @property int $group_id
 * @property string $name
 */
class MasterEntity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_entity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id'], 'integer'],
        		['name', 'unique'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'name' => 'Name',
        ];
    }
}
