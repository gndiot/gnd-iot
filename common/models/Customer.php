<?php

namespace common\models;

use Yii;
use backend\modules\api\models\DeviceGroups;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "customer_login".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $email
 * @property int $status
 * @property string $timezone
 * @property string $timezone_description
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class Customer extends \yii\db\ActiveRecord implements IdentityInterface
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;
	const STATUS_DEACTIVE = 20;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'first_name','phone_number','username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['customer_id', 'group_id', 'status', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['first_name', 'last_name'], 'string', 'max' => 20],
            [['phone_number'], 'string', 'max' => 15],
            [['timezone'], 'string', 'max' => 10],
            [['timezone_description'], 'string', 'max' => 50],
            [['username'], 'unique'],
        	[['status'],'default','value'=>self::STATUS_ACTIVE],
            [['email','username'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'group_id' => 'Group',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'status' => 'Status',
            'timezone' => 'Timezone',
            'timezone_description' => 'Timezone Description',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
    	return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    	throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
    	return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
    	if (!static::isPasswordResetTokenValid($token)) {
    		return null;
    	}
    
    	return static::findOne([
    			'password_reset_token' => $token,
    			'status' => self::STATUS_ACTIVE,
    	]);
    }
    
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
    	if (empty($token)) {
    		return false;
    	}
    
    	$timestamp = (int) substr($token, strrpos($token, '_') + 1);
    	$expire = Yii::$app->params['user.passwordResetTokenExpire'];
    	return $timestamp + $expire >= time();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
    	return $this->getPrimaryKey();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
    	return $this->auth_key;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
    	return $this->getAuthKey() === $authKey;
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
    	return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
    	$this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
    	$this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
    	$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
    	$this->password_reset_token = null;
    }
    
    
    public function getGroup(){
    	
    	 
    	 return $this->hasOne(DeviceGroups::className(), ["id"=>"group_id"]);
    	
    }
    
    
    
    public function fields(){
    	
    	  $fields=parent::fields();
    	  
    	   $fields["group"]=function($model){
    	   	    
    	   		 return DeviceGroups::find()->select(["id","name"])->where(["id"=>$model->group_id])->one();
    	   };
    	   
    	   $fields["customer"]=function($model){
    	   	
    	   		return DeviceGroups::find()->select(["id","name"])->where(["id"=>$model->customer_id])->one();
    	   		
    	   };
    	   
    	   
    	  unset($fields["password_hash"],$fields["auth_key"],$fields["password_reset_token"]);
    	  
    	  
    	  return $fields;
    	  
    	
    }
    
}
