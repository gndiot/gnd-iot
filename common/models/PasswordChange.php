<?php 
namespace common\models;
use Yii;
use yii\base\Model;
use backend\models\AdminUser;

class PasswordChange extends Model{
	
	  public $current_password;
	  public $new_password;
	  public $confirm_password;
	  public $user_id;
	  
	  private $_user;

	  public function rules()
	  {
	  	return [
	  			[['current_password', 'new_password','confirm_password'], 'required'],
	  			// rememberMe must be a boolean value
	  			['confirm_password', 'compare', 'compareAttribute' => 'new_password'],
	  			[['new_password','confirm_password'],'string', 'length' => [4, 24]],
	  			['current_password', 'validatePassword'],
	  	];
	  }
	  
	  public function validatePassword($attribute, $params)
	  {
	  	if (!$this->hasErrors()) {
	  		$user = $this->getUser();
	  		if (!$user || !$user->validatePassword($this->current_password)) {
	  			$this->addError($attribute, 'Incorrect current password.');
	  		}
	  	}
	  }
	  
	  
	  public function updatePassword(){
	  	
	  	    if($this->validate())
	  	    {
	  	    	  
	  	    	$this->_user->setPassword($this->new_password);
	  	    	  return $this->_user->save(false);
	  	    }
	  		return false;
	  }
	  
	  
	  protected function getUser()
	  {
	  	if ($this->_user === null) {
	  		$this->_user =AdminUser::find()->where(['id'=>$this->user_id])->one();
	  	}
	  	
	  	return $this->_user;
	  }
	
}
