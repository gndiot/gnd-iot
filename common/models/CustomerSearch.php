<?php

namespace common\models;

use Yii;
use backend\modules\api\models\DeviceGroups;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "customer_login".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $email
 * @property int $status
 * @property string $timezone
 * @property string $timezone_description
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class CustomerSearch extends Customer 
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;
	const STATUS_DEACTIVE = 20;
	
    public function rules()
    {
        return [
            [['customer_id', 'group_id', 'status', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['first_name', 'last_name'], 'string', 'max' => 20],
            [['phone_number'], 'string', 'max' => 15],
            [['timezone'], 'string', 'max' => 10],
            [['timezone_description'], 'string', 'max' => 50],
            [['username'], 'unique'],
        	[['status'],'default','value'=>self::STATUS_ACTIVE],
            [['email','username'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'group_id' => 'Group',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'status' => 'Status',
            'timezone' => 'Timezone',
            'timezone_description' => 'Timezone Description',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    
    public function searchAdmin($params){
    	
    	  $query=Customer::find();
    	  
    	  $this->load($params,"");
    	  
    	  
    	  $query->andFilterWhere([
    	  		'id' => $this->id,
    	  		'customer_id' => $this->customer_id,
    	  		'group_id' => $this->group_id,
    	  		'username'=>$this->username,
    	  		'first_name'=>$this->first_name,
    	  		'last_name'=>$this->last_name,
    	  		'phone_number'=>$this->email,
    	  		'email'=>$this->email,
    	  		'status'=>$this->status
    	  ]);
    	  
    	  $query->andFilterWhere(['like', 'username', $this->username]);
    	
    	  return $query->all();
    	
    }
    
    
}
