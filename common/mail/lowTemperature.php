<div class="box skin-blue" style="width:500px;height:250px;background-color:#f1f1f1;">
      <div class="header header-color" style="background-color:#2980b9;padding:5px;text-align:center;color:white;">
        <p style="margin:10px;">Dear User,</p>
        <p style="margin:10px;">!! Sensor Alert Notification !!</p>
        <p style="margin:10px;"> ** ESCALATION NOTIFICATION</p>
      </div>
      <table style="margin:5px;width:100%;">
        <tr>
          <td style="text-indent:15px;height:30px;width:50%;vertical-align:bottom;"> Device Name</td>
          <td class="alert-txt-color" style="text-indent:15px;height:30px;width:50%;vertical-align:bottom;color:#2980b9;"><?= $device_id ?></td>
        </tr>
        <tr>
          <td style="text-indent:15px;height:30px;width:50%;vertical-align:bottom;">Timestamp</td>
          <td class="alert-txt-color" style="text-indent:15px;height:30px;width:50%;vertical-align:bottom;color:#2980b9;"><?= $notification_time ?></td>
        </tr>
        <tr>
          <td style="text-indent:15px;height:30px;width:50%;vertical-align:bottom;">Current Temperature</td>
          <td style="text-indent:15px;height:30px;width:50%;vertical-align:bottom;"><span class="alert-value alert-txt-color" style="font-weight:bold;font-size:20px;color:#2980b9;"><?= $value ?> </span>�C</td>
        </tr>
      </table>
</div>