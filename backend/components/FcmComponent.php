<?php 

namespace backend\components;

use yii\base\Component;

class FcmComponent extends Component{
	
	public $server_key;
	public $server_url;
	private $fields=array();
	private $output=array();
	
	public function init(){
		parent::init();
	
	}
	
	protected function _send_notification_to_devices($fields)
	{
		$result=array();
		$headers = array(
				'Authorization: key=' . $this->server_key,
				'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $this->server_url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		// Execute post
		$result = curl_exec($ch);
		if ($result === FALSE) {
	
			$result['error']=curl_error($ch);
		}
		// Close connection
		curl_close($ch);
	
		return $result;
	
	}

	  public function sendMultipleAlertMessages($device_ids,$title,$message)
    {
        
        if(is_array($device_ids))
        {        
            $this->fields=array(            
             "registration_ids"=>$device_ids,
             "priority"=> "high",
              "notification"=>array(     
                  "title"=>$title,
                  "body"=>$message,
                  "sound"=>"default",
              ),
             
         );
            
         $this->output=$this->_send_notification_to_devices($this->fields);
              
        }else{
          
            $this->output['error']="Device_ide must be array which contain atleast one value";
            
        }
        
       return $this->output; 
        
    }

	public function sendAlartMessage($device_id,$title,$message)
	{
		$this->fields=array(
				 
				"to"=>$device_id,
				"notification"=>array(
						"title"=>$title,
						"body"=>$message
				),
				 
		);
		 
		return $this->_send_notification_to_devices($this->fields);
	}
	
	public function send_notification_to_device($registration_id,$notification,$data)
	{
		 
		$fields=array(
				'to'=>$registration_id,
				//'notification'=>$notification,
				'data'=>$data
		);
		$headers = array(
				'Authorization: key=' . $this->server_key,
				'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $this->server_url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		// Execute post 
		$result = curl_exec($ch);
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
	
		// Close connection
		curl_close($ch);
	
		return $result;
	
	}

}
