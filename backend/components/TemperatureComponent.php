<?php 

namespace backend\components;

use yii\base\Component;

class TemperatureComponent extends Component{
	
	private $input;

	public function init(){
		
		parent::init();

	}
	
	private function base64strToHex($string){
		$hex = '';
		for ($i=0; $i<strlen($string); $i++){
			$ord = ord($string[$i]);
			$hexCode = dechex($ord);
			$hex .= substr('0'.$hexCode, -2);
		}
		return strToUpper($hex);
	}
	
	private function hexToStr($hex){
		$string='';
		for ($i=0; $i < strlen($hex)-1; $i+=2){
			$string .= chr(hexdec($hex[$i].$hex[$i+1]));
		}
		return $string;
	}

	public function convertHex(){
		
		
	}	
	
	public function getHexDownlinkConfig($rp_interval,$ss_interval,$tht,$tlt,$hh,$hl,$mod=2,$is_base64=false){
			
		$str1='b1'.'00'.$this->decimalToHexBattery($rp_interval).'00'.$this->decimalToHexBattery($ss_interval).$this->getTemperatureHex($tht).$this->getTemperatureHex($tlt).$this->decimalToHexBattery($hh).$this->decimalToHexBattery($hl).$this->decimalToHexBattery($mod);
		if($is_base64)
		{
			return $this->hexToStr($str1);	
		}
		return $str1;
	}
	
	
	public function decodeDownlinkConfig($hex){
		
		 
		return base64_encode($this->hexToStr($hex));	
	}
	
	public function periodicMessage(){
		
		$result=[];
		$strarr=str_split($this->input,2);
		
		if($strarr[0]!='A4')
		{
			return [];
		}
		$result["temperature"]=$this->convertHexToDecimalForTemp($strarr[1].$strarr[2]);
		$result["humidity"]=$this->getHexToDecimal($strarr[3]);
		$result["battery"]=$this->getHexToDecimal($strarr[4]);
		return $result;
	}
	
	
	public function alertMessage() {
		
		$result=[];
		$strarr=str_split($this->input,2);
		if($strarr[0]!='A3')
		{
			return [];
		}
		
		$result["alert_type"]=$this->getHexToDecimal($strarr[1]);
		$result["alarm_type"]=$this->getHexToDecimal($strarr[2]);
		$result["temperature"]=$this->convertHexToDecimalForTemp($strarr[3].$strarr[4]);
		$result["humidity"]=$this->getHexToDecimal($strarr[5]);
		$result["battery"]=$this->getHexToDecimal($strarr[6]);
		return $result;
	}
	
	//this function is used to convert decimal to hex for only battery and humdity
	
	private function decimalToHexBattery($value){
		
		$hexString=dechex($value);
		
		if(strlen($hexString)==1)
		{
			$hex='0'.$hexString;
			return $hex;
		}else{
			
			return $hexString;
		}
		
		
	}
	
	private function getTemperatureHex($value,$multiply=10){
		
		if($value > 0)
		{
			$r1=$value*$multiply;
			$hexString=dechex($r1);
			//check value is 20
			if(strlen($hexString) > 2)
			{
				if(strlen($hexString)==4)
				{
					$hex=$hexString;
				}else{
					$hex='0'.$hexString;
				}
				
			}else{
				$hex='00'.$hexString;
			}
			return $hex;
		}else{
			$r1=abs($value)*$multiply;
			$hexString=dechex($r1);
			if(strlen($hexString) > 2)
			{
				$hex='8'.$hexString;
			}else{
				$hex='80'.$hexString;
			}
			return $hex;
			
		}
	}
	
	public function getDecodeHealthCheckMsg($hex){
			
		$result=[];
		$strarr=str_split($hex,2);
		if($strarr[0]!='A1')
		{
			return [];
		}
		
		$result['model_number']=$this->getVersionNumber($strarr[7].$strarr[8]);
		$result['firmware_version']=$this->getVersionNumber($strarr[9].$strarr[10]);
		$result['product_version']=$this->getVersionNumber($strarr[11].$strarr[12]);
		$result['reporting_interval']=$this->getHexToDecimal($strarr[13].$strarr[14]);
		$result['sesnsor_sampling_interval']=$this->getHexToDecimal($strarr[15].$strarr[16]);
		$result['temperature_high_threshold']=$this->convertHexToDecimalForTemp($strarr[17].$strarr[18]);
		$result['temperature_low_threshold']=$this->convertHexToDecimalForTemp($strarr[19].$strarr[20]);
		$result['humidity_high_threshold']=$this->getHexToDecimal($strarr[21]);
		$result['humidity_low_threshold']=$this->getHexToDecimal($strarr[22]);
		$result['battery_percentage']=$this->getHexToDecimal($strarr[23]);
		$result['mode']=$this->getHexToDecimal($strarr[24]);
		return $result;
		
	}
	
	public function loadBase64Hex($str){
		
		$str= base64_decode($str);
		
		 $this->input=$this->base64strToHex($str);
	}
	
	public function loadHexStr($str){
		 
		 $this->input=$str;
		
	}
	
	public function getHextoString(){
		
		return $this->input;
		
	}
	
	public function output(){
		
		
	}
	
	private function convertDecimalToHexForTemp($value,$multiply=10){
		
		if($value > 0)
		{
			$r1=$value*$multiply;
			$hexString=dechex($r1);
			//check value is 20
			if(strlen($hexString) > 2)
			{
				if(strlen($hexString)==4)
				{
					$hex=$hexString;
				}else{
					$hex='0'.$hexString;
				}
				
			}else{
				$hex='00'.$hexString;
			}
			return $hex;
		}else{
			$r1=abs($value)*$multiply;
			$hexString=dechex($r1);
			if(strlen($hexString) > 2)
			{
				$hex='8'.$hexString;
			}else{
				$hex='80'.$hexString;
			}
			return $hex;
			
		}
		
	}
	
	private function convertHexToDecimalForTemp($hexString,$divided=10){
		
		$strarr=str_split($hexString,2);
		
		if($strarr[0]=='01')
		{
			$value=hexdec($hexString);
			return (intval($value) / $divided);
		}
		
		if($strarr[0]=='02')
		{
			$value=hexdec($hexString);
			$value=hexdec('2'.$strarr[1]);
			return (intval($value) / $divided);
		}
		if($strarr[0]=='03')
		{
			$value=hexdec('3'.$strarr[1]);
			return (intval($value) / $divided);
		}
		if($strarr[0]=='81')
		{
			
			$value=hexdec('01'.$strarr[1]);
			return '-'.(intval($value) / $divided);
		}
		
		if($strarr[0]=='00')
		{
			
			$value=hexdec($hexString);
			return floatval((intval($value) / $divided));
			
		}
		if($strarr[0]=='80')
		{
			if($strarr[1]=='0')
			{
				return 0;
			}
			$value=hexdec('00'.$strarr[1]);
			$result='-'.(intval($value) / $divided);
			return floatval($result);
		}
		
	}
	
	
	private function getHexToDecimal($value){
		
		return hexdec($value);
	}
	
	private function getVersionNumber($hex){
		
		if(strlen($hex)==4)
		{
			$strarr=str_split($hex,2);
			$one=hexdec($strarr[0]);
			$two=hexdec($strarr[1]);
			return $one.'.'.$two;
		}else{
			
			return "0.00";
		}
		
	}
	
	public function isPeriodicMsg(){
		
		 $arr=str_split($this->input,2);
		 
		  if(!empty($arr))
		  {
		  		if($arr[0]=='A4')
		  		{
		  			return true;
		  			
		  		}else{
		  			 return false;
		  		}
		  }else{
		  	
		  	 return false;
		  }
		
	}
	
	public function isAlertMsg(){
		
		$arr=str_split($this->input,2);
		
		if(!empty($arr))
		{
			if($arr[0]=='A3')
			{
				return true;
				
			}else{
				return false;
			}
		}else{
			
			return false;
		}
		 	
	}
	
	public function isBatteryCalibration(){
		
		$arr=str_split($this->input,2);
		
		if(!empty($arr))
		{
			if($arr[0]=='A5')
			{
				return true;
				
			}else{
				return false;
			}
		}else{
			
			return false;
		}
		
	}
	public function isHealthMsg(){
		
		$arr=str_split($this->input,2);
		
		if(!empty($arr))
		{
			if($arr[0]=='A1')
			{
				return true;
				
			}else{
				return false;
			}
		}else{
			
			return false;
		}
		
	}
	
	public function isConfigurationRequest(){
		
		$arr=str_split($this->input,2);
		
		if(!empty($arr))
		{
			if($arr[0]=='A2')
			{
				return true;
				
			}else{
				return false;
			}
		}else{
			
			return false;
		}
		 
	}
	
	//test code
	
	public function _periodicMessage($temperature,$humidity,$battery){
		
		$str1='a4'.$this->getTemperatureHex($temperature).$this->decimalToHexBattery($humidity).$this->decimalToHexBattery($battery);
		return $str1;
		
	}
	
	public function _alertMessage($alertType,$alarm_type,$temperature,$humidity,$battery){
		
		$str1='a3'.$this->decimalToHexBattery($alertType).$this->decimalToHexBattery($alarm_type).$this->getTemperatureHex($temperature).$this->decimalToHexBattery($humidity).$this->decimalToHexBattery($battery);
		return $str1;
		
	}
	
	
	
}
