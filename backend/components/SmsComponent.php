<?php 

namespace backend\components;

use yii;
use yii\base\Component;
use yii\data\Pagination;

class SmsComponent extends Component{

	
	private $url="http://nimbusit.net/api.php?"; //
	private $username="t4Siva";
	private $password="608021";
	private $sender="IOTGND";
	
	
	public $MaximumNumbers=100; // maximum number per request
	
	public function init(){
		parent::init();
		
	}
	
	
	
	public function sendOTP($mobileNumber,$message)
	{
		
		return $this->cUrlbuilder($mobileNumber,$message);
		
		
	}
	
	public function sendSms($mobilenumber="",$message){
		
		
		
		if(is_array($mobilenumber))
		{
			
			$mobile=implode(",", $mobilenumber);
			return $this->cUrlbuilder($mobile,$message);
		}else{
			
			$mobile=str_replace('|',',',trim($mobilenumber));
			return $this->cUrlbuilder($mobile,$message);
		}
		
		
		
		
		
	}
	
	public function sendMultipleSms($mobileNumbers,$message="",$prefix =''){
		
		 
		if(is_array($mobileNumbers))
		{
			
			$mobile=implode(",", $mobileNumbers);
			return $this->cUrlbuilder($mobile,$message);
			
		}else{
			
			$mobile=str_replace('|',',',trim($mobileNumbers));
			return $this->cUrlbuilder($mobile,$message);
		}
		 
		 
		
		
		
		
	}
	
	
	private function executeMultiRequest($pageObj,$numbers,$message){
		
		$emultiExeute=[];
		
		$next_index=0;
		
		for($k=0; $k < $pageObj->getPageCount(); $k++){
			
			$mobilenumbers="";
			
			for($n=0; $n < $pageObj->getLimit(); $n++)
			{
				
				
				if($next_index < $pageObj->totalCount)
				{
					
					
					
					$mobilenumbers .=$numbers[$next_index].",";
					
					
					
					$next_index++;
					
				}else{
					
					break;
					
				}
				
				
			}
			
			$mobilenumbers =rtrim($mobilenumbers,",");
			
			array_push($emultiExeute,$this->cUrlbuilder($mobilenumbers,$message));
			
		}
		
		return $emultiExeute;
		
	}
	
	private function cUrlbuilder($mobilenumber,$message){
		
		$params=[
				"username"=>$this->username,
				"password"=>$this->password,
				"sender"=>$this->sender,
				"sendto"=>$mobilenumber,
				"message"=>urlencode($message)
		];
		
		$parms=http_build_query($params);
		$final_url=$this->url.$parms;
		
		$serverOutput=$this->initCurl($final_url);
		
		return $serverOutput;
	}
	
	private function urlBuilder($mobilenumber,$message)
	{
		
		$parms="dest=".$mobilenumber."&msg=".urlencode($message);
		$final_url=$this->url.$parms;
		
		$serverOutput=$this->initCurl($final_url);
		
		
		return $serverOutput;
	}
	
	
	
	public function initCurl($url=""){
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$return = curl_exec ($ch);
		curl_close ($ch);
		return $return;
		
		
	}

	
}
