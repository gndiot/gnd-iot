<?php 

namespace backend\components;

use yii;
use yii\base\Component;
use backend\models\AdminUser;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ReactComponent extends Component{

	public function init(){
		parent::init();

	}
	public function getPayloads(){
		
		    $userProfile=$this->getUserInfo();
		    $userMenu=$this->getUserMenu();
		    $output=[];
		    $output["global"]['user']=$userProfile;
		    $output["global"]['menu_items']=$userMenu;
		    return Json::encode($output);
	}
	

	public function getUserInfo(){
		 
		$user= AdminUser::find()->select(['first_name','last_name','phone_number','email','timezone','timezone_description','customer_id'])->where(["id"=>Yii::$app->user->id])->asArray()->one();
		$user['timezone']=Yii::$app->user->identity->getTimezone();
		$user['timezone_description']=Yii::$app->utils->getTimezoneName($user['timezone']);
		$user['groups']=Yii::$app->user->identity->getGroups();
		$user['roles']=Yii::$app->user->identity->getRoles();
		return $user;
		 
	}
	
	public function getUserMenu(){
		 
		return  $array= [
				[
						'name'=>'Admin Dashboard',
						'url'=>"/dashboard",
						'visible'=>Yii::$app->user->can('Admin Dashboard'),
						'submenu'=>[]
				],
				[
						'name'=>'Dashboard',
						'url'=>"/",
						'visible'=>!Yii::$app->user->can('Admin Dashboard') || Yii::$app->user->identity->hasRole('Group Dashboard'),
						'submenu'=>[]
				],
				[
						'name'=>'Digital View',
						'url'=>"/sensor/digital/temperature",
						'visible'=>Yii::$app->user->can('Temperature'),
						'submenu'=>[
								
						]
				],
				[
						'name'=>'Analog View',
						'url'=>"/sensor/analog/temperature",
						'visible'=>Yii::$app->user->can('Temperature'),
						'submenu'=>[
							
						]
				],
				[
						'name'=>'Gps',
						'url'=>"/gps",
						'visible'=>Yii::$app->user->can('Temperature'),
						'submenu'=>[
								
						]
				],
				[
						'name'=>'Reports',
						'url'=>"",
						'visible'=>Yii::$app->user->can('Reports') || Yii::$app->user->can('Report Temperature Corrective'),
						'submenu'=>[
								[
										'name'=>'Temperature',
										'url'=>'/reports/temperature',
										
										'visible'=>Yii::$app->user->can('Report Temperature'),
								],
								 [
										'name'=>'Door',
										'visible'=>Yii::$app->user->can('Report Door'),
										'url'=>'/reports/door',
								],
								[
										'name'=>'Gps',
										'visible'=>Yii::$app->user->can('Report Gps'),
										'url'=>'/reports/gps',
								],
								[
										'name'=>'Humidity',
										'visible'=>Yii::$app->user->can('Report Temperature'),
										'url'=>'/reports/humidity',
								],
								[
										'name'=>'Notification',
										'visible'=>Yii::$app->user->can('Report Temperature Corrective'),
										'url'=>'/reports/notification',
								],
						]
				],
				
				[
						'name'=>'Alerts',
						'url'=>"",
						'visible'=>Yii::$app->user->can('Subscriptions'),
						'submenu'=>[
								[
										'name'=>'Subscriptions',
										'url'=>'/subscription',
										'visible'=>Yii::$app->user->can('Subscriptions'),
								],
						]
				],
				
				[
						'name'=>'Configurations',
						'url'=>"",
						'visible'=>Yii::$app->user->can('Devices'),
						'submenu'=>[
								[
										'name'=>'Grouping',
										'url'=>'/group',
										'visible'=>Yii::$app->user->can('Groups') && Yii::$app->user->can('Group Add'),
								],
								[
										'name'=>'Device',
										'visible'=>Yii::$app->user->can('Devices'),
										'url'=>'/sensor/configuration/temperature',
	
								],
								[
										'name'=>'Multiple Configuration',
										'visible'=>Yii::$app->user->can('Device Config Update Multiple'),
										'url'=>'/sensor/multiple/configuration/temperature',
										
								],
								[
										'name'=>'Notification Setup',
										'visible'=>Yii::$app->user->can('Email Alert Template Update') && Yii::$app->user->can('Sms Alert Template Update'),
										'url'=>'/configuration/notification/email',
										
								]
						]
				],
				[
						'name'=>'Settings',
						'url'=>"",
						'visible'=>true,
						'submenu'=>[
								[
										'name'=>'Users',
										'url'=>'/user',
										'visible'=>Yii::$app->user->can('Users') || Yii::$app->user->can('Group Users'),
								],
								[
										'name'=>'Change Password',
										'visible'=>Yii::$app->user->can('User Change Password'),
										'url'=>'/user/change/password',
								],
						]
				],
				[
						'name'=>'Logout',
						'url'=>"/logout",
						'visible'=>true,
						'submenu'=>[
						]
				]
					
		];
	
		 
	}
    
	
	
	
}
