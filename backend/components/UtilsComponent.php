<?php 

namespace backend\components;
use yii;
use yii\base\Component;

class UtilsComponent extends Component{

	public $secret_key="gnd";
	public $secret_iv="gnd123";
	
	public function init(){
		parent::init();

	}

	public function encrypt($string){
		
		
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash( 'sha256', $this->secret_key);
		$iv = substr( hash( 'sha256', $this->secret_iv ), 0, 16 );
		
		$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
		
		return $output;
		
	}
	
	public function decrypt($string){
		
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash( 'sha256', $this->secret_key );
		$iv = substr( hash( 'sha256', $this->secret_iv ), 0, 16 );
		$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		
		return $output;
		
	}
	
	public function AppendString($count,$append,$value){
		
		 $tmp="";

		 for ($i = 0; $i < $count; $i++) {
		 	
		 	 $tmp.=$append;
		 	
		 }
		 
		 return $tmp.$value;
		
	}
	
	public function getTimezoneName($offset){
		
		  if($offset=="")
		  {
		  	$offset='05:30';
		  	
		  }
		  
		  if($offset=="04:00" || $offset=="+04:00")
		  {
		  	return "Asia/Dubai";
		  }
		
		list($hours, $minutes) = explode(':', $offset);
		$seconds = $hours * 60 * 60 + $minutes * 60;
		$tz = timezone_name_from_abbr('', $seconds, 1);
		if($tz === false) $tz = timezone_name_from_abbr('', $seconds, 0);
		 return $tz;	
	}
	
	public function getYesterdayDateTime($timestamp=true){
		
		$yesterday_start = new \DateTime();
		$interval = new \DateInterval('P1D');
		$yesterday_start->sub($interval);
		$yesterday_start->modify('today');
		$yesterday_end= clone $yesterday_start;
		$yesterday_end->modify('tomorrow');
		$yesterday_end->modify('1 second ago');
		 
		
		
		if($timestamp)
		{
			
			
			return [
					"start"=>$yesterday_start->getTimestamp(),
					"end"=>$yesterday_end->getTimestamp(),
			];
			
			
		}else{
			
			
			
			return [
					"start"=>$yesterday_start->format('Y-m-d H:i:s'),
					"end"=>$yesterday_end->format('Y-m-d H:i:s'),
			];
			
			
		}
		
		 
	}
	
	
	public function getDateTime($timestamp,$timezone=""){
	
			$date= new \DateTime();
			$date->setTimestamp($timestamp);
			
			if($timezone!="")
			{
				$date->setTimezone(new \DateTimeZone($timezone));
				
			}
			
			return $date;
	}
	
	public function getToday($timestamp=true){
		
		$yesterday_start = new \DateTime();
		$yesterday_start->modify('today');
		$yesterday_end= clone $yesterday_start;
		$yesterday_end->modify('tomorrow');
		$yesterday_end->modify('1 second ago');

		if($timestamp)
		{
			
			return [
					"start"=>$yesterday_start->getTimestamp(),
					"end"=>$yesterday_end->getTimestamp(),
			];
			
			
		}else{
			
			return [
					"start"=>$yesterday_start->format('Y-m-d H:i:s'),
					"end"=>$yesterday_end->format('Y-m-d H:i:s'),
			];
			
			
		}
			
			
		
	}
	
	
	public function getTimestampOfThisMonth($timestamp=true){
		
			
		$start = new \DateTime("first day of this month");
		$start->modify("today");
		$end= new \DateTime("last day of this month");
		$end->modify('tomorrow');
		$end->modify('1 second ago');
	
		
		if($timestamp)
		{
			
			return [
					"start"=>$start->getTimestamp(),
					"end"=>$end->getTimestamp(),
			];
			
			
		}else{
			
			
			
			return [
					"start"=>$start->format('Y-m-d H:i:s'),
					"end"=>$end->format('Y-m-d H:i:s'),
			];
			
			
		}
		
		
	}
	
	public function getTimestampOfLastmonth($timestamp=true){
		
		$start = new \DateTime("first day of last month");
		$start->modify("today");
		$end= new \DateTime("last day of last month");
		$end->modify('tomorrow');
		$end->modify('1 second ago');
		
		
		if($timestamp)
		{
			
			return [
					"start"=>$start->getTimestamp(),
					"end"=>$end->getTimestamp(),
			];
			
			
		}else{
			
			
			
			return [
					"start"=>$start->format('Y-m-d H:i:s'),
					"end"=>$end->format('Y-m-d H:i:s'),
			];
			
			
		}
		 
		
	}
	
	
	public function getTimestampBtDate($from,$to,$timestamp=true){
		
		try {
			
			$start = new \DateTime($from);
			$start->modify('today');
			$end= new \DateTime($to);
			$end->modify('tomorrow');
			$end->modify('1 second ago');
			
			if($timestamp)
			{
					
				return [
						"start"=>$start->getTimestamp(),
						"end"=>$end->getTimestamp(),
				];
					
					
			}else{
					
					
					
				return [
						"start"=>$start->format('Y-m-d H:i:s'),
						"end"=>$end->format('Y-m-d H:i:s'),
				];
					
					
			}
			
			
		} catch (Exception $e) {
			
			return [
					"start"=>null,
					"end"=>null,
			];
				
			
		}
		
		
			
	}
	
	
	
	public function getTimeDifference($start,$endtime){

		$mstart =new \DateTime();
		$mstart->setTimestamp($start);
		$end= new \DateTime();
		$end->setTimestamp($endtime);
		$diff=$mstart->diff($end);
	      return $diff;
	}
		
	
}
