<?php
use common\models\TimeZone;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
	'vendorPath'=>realpath(__DIR__.'/../../../libs/vendor'),
	'name'=>'Gnd Solutions',
	'modules' =>[
			
			'api' => [
					
				'class' => 'backend\modules\api\Module',
			],
			'service' => [
				'class' => 'backend\modules\service\Module',
			],
			'reports' => [
			
					'class' => 'backend\modules\reports\Module',
			],
			'v1' => [

					'class' => 'backend\modules\v1\Module',
			],
			'gps' => [
					
					'class' => 'backend\modules\gps\Module',
					
			],
	],
    'components' => [
    		'temperature'  => [
    				'class'=> 'backend\components\TemperatureComponent',
    		],
    		'sms'  => [
    				'class'=> 'backend\components\SmsComponent',
    		],
    	'utils'=>[
    			 'class'=>'backend\components\UtilsComponent'
    	],
    	'react'=>[
    				'class'=>'backend\components\ReactComponent'
    	],
        'request' => [
            'csrfParam' => '_csrf-backend',
        	'parsers' => [
     
        			'application/json' => 'yii\web\JsonParser',
        	],
        ],
    	/* 'queue' => [
    				'class' => \yii\queue\amqp_interop\Queue::class,
    				'host' => 'localhost',
    				'port' => 5672,
    				'user' => 'guest',
    				'password' => 'guest',
    				'queueName' => 'queue',
    				//	'vhost'=>'backup',
    				'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
    	], */
    	'queue' => [
    				'class' => \yii\queue\db\Queue::class,
    				'db' => 'db', // DB connection component or its config
    				'tableName' => '{{%queue}}', // Table name
    				'channel' => 'default', // Queue channel key
    				'mutex' => \yii\mutex\MysqlMutex::class, // Mutex used to sync queries
    		],
        'user' =>[
            'identityClass' => 'backend\models\AdminUser',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
    	 'authManager'  => [
    		'class'=> 'yii\rbac\DbManager',
    	 ],
    	'mailer' => [
    			'class' => 'yii\swiftmailer\Mailer',
    			'useFileTransport' => YII_ENV_DEV?true:false,
    		],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                	'except' => ['yii\web\HttpException:404'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        	'enableStrictParsing' => false,
        	//'suffix' => '.html',
            'rules' => [
            		'login' => 'site/login',
            		'logout' => 'site/logout',
            		'/api'=>'api',
            		'/gps'=>'gps',
            		'/gps/subscription/update/<id:[a-zA-Z0-9-_]+>'=>'gps/subscription/update',
            		'/reports'=>'reports',
            		'/service'=>'service',
            		'alert/subscription'=>'subscription/index',
            		'user'=>'user/index',
            		'sensor/digital'=>'device/digital',
            		'sensor/digital/<type:\w+>'=>'device/digital',
            		'sensor/view/<type:\w+>/<id:\d+>'=>'device/view',
            		'sensor/analog'=>'device/analog',
            		'sensor/analog/<type:\w+>'=>'device/analog',
            		'sensor/configuration/<type:\w+>'=>'device/configuration',
            		'dashboard'=>'dashboard/index',
            		'group'=>'group/index',
            		'/' => 'site/index',
            		'/<url:[a-zA-Z0-9-_]+>' => 'site/index',
            ],
        ],
        
    ],
    'params' => $params,
	'timeZone'=>'Asia/Kolkata'
];
