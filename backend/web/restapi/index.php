<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../../../../libs/vendor/autoload.php';
require __DIR__ . '/../../../../libs/vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../../../api/config/config.php';
(new yii\web\Application($config))->run();
