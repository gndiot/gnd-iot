<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    		'//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
    		'//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
    		'css/app.css',
    		'css/login.css',
    		'css/_all-skins.css',
    
    ];
    public $js = [
    		'js/adminlte.js'
    ];
    public $depends = [
    		'yii\web\YiiAsset',
    		'yii\bootstrap\BootstrapAsset',
    		'yii\bootstrap\BootstrapPluginAsset'
    ];
    
}
