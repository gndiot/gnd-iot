<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_tokens".
 *
 * @property int $id
 * @property int $user_id
 * @property string $source
 * @property string $access_token
 * @property string $fcm_token
 * @property string $device_unique
 * @property string $device_manufacture
 * @property string $device_model
 * @property string $device_version
 * @property string $app_version
 * @property int $created_at
 */
class UserToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at'], 'integer'],
            [['fcm_token'], 'string'],
            [['source', 'access_token', 'device_unique', 'device_manufacture', 'device_model', 'device_version', 'app_version'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'source' => 'Source',
            'access_token' => 'Access Token',
            'fcm_token' => 'Fcm Token',
            'device_unique' => 'Device Unique',
            'device_manufacture' => 'Device Manufacture',
            'device_model' => 'Device Model',
            'device_version' => 'Device Version',
            'app_version' => 'App Version',
            'created_at' => 'Created At',
        ];
    }
}
