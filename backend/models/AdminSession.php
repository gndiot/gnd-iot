<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "admin_session".
 *
 * @property int $id
 * @property int $expire
 * @property resource $data
 */
class AdminSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['expire'], 'integer'],
            [['data'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'expire' => 'Expire',
            'data' => 'Data',
        ];
    }
}
