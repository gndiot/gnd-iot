<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;


class DeviceController extends Controller{

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		 
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'allow' => true,
										'roles' => ['@'],
								],
						],
				],
		];
	}

	public function actionDigital($type=''){
		
		 
		return $this->render("/site/index");
		
	}
	
	public function actionView($type='',$id=''){
		
		return $this->render("/site/index");
		
	}
	
	public function actionAnalog($type=''){
		return $this->render("/site/index");
		
	}
	
	public function actionConfiguration($type){
		
		return $this->render("/site/index");
	}
	
	
	
}