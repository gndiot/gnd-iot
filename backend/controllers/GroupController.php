<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;


class GroupController extends Controller{

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		 
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'allow' => true,
										'roles' => ['@'],
								],
						],
				],
		];
	}
	
	
	
	public function actionIndex(){
		
		
		return $this->render("/site/index");
		
	}
	
	public function actionCreate(){
	
	
		return $this->render("/site/index");
	
	}
	
	
}