<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;


class DashboardController extends Controller{

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		 
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
							
								[
										'allow' => true,
										'actions' => ['index'],
										'roles' => ['Admin Dashboard']
								],
						],
				],
		];
	}
	
	public function actionIndex(){
	
			return $this->render("/site/index");
	
	}
	
}