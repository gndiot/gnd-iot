<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

  
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($url="")
    {
    	
    
          if($url =="")
          {
          	if(Yii::$app->user->identity->hasRole('super user'))
          	{
          		return $this->redirect(['/dashboard']);
          		
          	}
          	
          	
          }
    	
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
    	$this->layout="login";
    	
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
        	
         if(Yii::$app->user->identity->hasRole('super user'))
         {
         	return $this->redirect(['/dashboard']);
         	
         }
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionError(){
    	
    	
    	if (Yii::$app->user->isGuest) {
    		
    		return $this->goHome();
    	}
    	
    	 Yii::$app->getResponse()->setStatusCode("200");
    	 
    	return $this->render('index');
    	
    }
}
