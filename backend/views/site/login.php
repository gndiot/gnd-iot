<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
  
 <div class="login-logo">
    	
    <img alt="" src="images/logo.png" style="
    width: 300px;
">            
	
</div>
           
	
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to Your Account</p>
                
					<?php $form = ActiveForm::begin(); ?>
                   
                    <div class="form-group has-feedback">
                         <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="form-group has-feedback">
                       <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                     
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                        </div>
                        <!-- /.col -->
                    </div>
                    
               <?php ActiveForm::end(); ?>
                <!-- /.social-auth-links -->

	 <!-- <a href="#">I forgot my password</a><br> -->
             </div>
           
           
           	 
</div>
