<?php

namespace backend\modules\gps\models;

use Yii;

/**
 * This is the model class for table "device_gps_recent_history".
 *
 * @property string $id
 * @property int $device_id
 * @property string $message_type
 * @property string $temperature_value
 * @property int $humidity_value
 * @property string $latitude
 * @property string $longitude
 * @property int $speed
 * @property int $movement
 * @property int $power_status
 * @property int $battery_status
 * @property int $battery_value
 * @property int $sensor_battery_value
 * @property int $updated_at
 */
class GpsRecentHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_gps_recent_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id', 'humidity_value','movement', 'power_status', 'battery_status', 'battery_value', 'sensor_battery_value', 'updated_at'], 'integer'],
        		[['temperature_value', 'latitude', 'longitude','speed'], 'number'],
            [['updated_at'], 'required'],
            [['message_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'message_type' => 'Message Type',
            'temperature_value' => 'Temperature Value',
            'humidity_value' => 'Humidity Value',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'speed' => 'Speed',
            'movement' => 'Movement',
            'power_status' => 'Power Status',
            'battery_status' => 'Battery Status',
            'battery_value' => 'Battery Value',
            'sensor_battery_value' => 'Sensor Battery Value',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function afterFind(){
    	
    	
    	  if($this->updated_at !="")
    	  {
    	  	$date=new \DateTime();
    	  	$date->setTimestamp($this->updated_at);
    	  	$this->updated_at=$date->format("Y-m-d H:i:s");
    	  	
    	  }
    	  
    	 
    	
    }
}
