<?php

namespace backend\modules\gps\models;

use Yii;

/**
 * This is the model class for table "device_gps_zone_notification".
 *
 * @property int $id
 * @property int $zone_alert_id
 * @property int $customer_id
 * @property int $group_id
 * @property int $device_id
 * @property string $alert_type
 * @property string $data
 * @property int $created_at
 */
class GpsZoneNotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_gps_zone_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zone_alert_id', 'customer_id', 'group_id', 'device_id', 'created_at'], 'integer'],
            [['data'], 'string'],
            [['alert_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'zone_alert_id' => 'Zone Alert ID',
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'device_id' => 'Device ID',
            'alert_type' => 'Alert Type',
            'data' => 'Data',
            'created_at' => 'Created At',
        ];
    }
}
