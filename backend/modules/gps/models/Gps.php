<?php
namespace backend\modules\gps\models;

use common\models\Devices;

class Gps extends Devices{
	
	public function getGps(){	
		
		return $this->hasOne(GpsRecentHistory::className(),["device_id"=>"id"])->select(["hardware_serial","payload_raw","l_latitude","l_longitude"])->orderBy(["id"=>SORT_DESC]);
	}
	
	public static function find(){
	
		return parent::find()->where(["type"=>2]);
	}
	
	public function getConfiguration(){
		
		 return $this->hasOne(GpsConfigurations::className(), ["device_id"=>"id"]);
		
	}
	
	public function getConfigurationTemp(){
		
		 return $this->hasOne(GpsConfigurationsTemp::className(), ["device_id"=>"id"]);
	}
	
	public function getRecentData(){
		
		return $this->hasOne(GpsRecentHistory::className(), ["device_id"=>"id"]);
	}
	
		
	public function fields(){
		
		return [
				'id',
				'hardware_serial',
				'customer_id',
				'type',
				'name',
				'display_name',
				'group_id',
				'status',
				'sync_time',
				'sync_last_time',
		];
		
	}
	
	public function afterFind(){
		
		if($this->sync_time!="")
		{
			$date=new \DateTime();
			$date->setTimestamp($this->sync_time);
			$this->sync_time=$date->format("Y-m-d H:i:s");
			
		}
		
	}
	
	public function extraFields(){
		
		return ['configuration','configurationTemp','recentData'];
	}
	
}