<?php
namespace backend\modules\gps\models;


use yii\base\Model;
use common\models\Devices;
use yii\data\ActiveDataProvider;
use common\models\Constants;

class GpsSearch extends Gps{
	

	
	public function rules(){
		
		return [
				[['hardware_serial','name','response_intervel','start_value','end_value','status'],'required'],
				[['customer_id', 'type', 'group_id', 'response_intervel', 'status', 'created_at', 'updated_at', 'sync_time', 'sync_last_time', 'admin_id', 'customer_user_id','config_status','bc_req_time','bc_req_status'], 'integer'],
				[['start_value', 'end_value'], 'number'],
				['start_value', 'default', 'value' => 0],
				['end_value', 'default', 'value' => 80],
				[['hardware_serial', 'display_name'], 'string', 'max' => 255],
				[['name'], 'string', 'max' => 50],
				[['config_new','config_temp'],'string'],
				['data','string'],
				['equipement_id','string'],
				[['battery_level','entity','department','process','tag'],'integer'],
				[['hardware_serial'], 'unique'],
		];
		
	}
	
	
	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	
	public function search($params,$customer_id,$group_id){
		
		$query=Gps::find()->andWhere(["customer_id"=>$customer_id,"group_id"=>$group_id]);
		
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 50],
				],
		]);
		
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		return $dataProvider;
	}
	public function Adminsearch($params){
		
		$query=Gps::find();
		
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 50],
				],
		]);
		
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		return $dataProvider;
	}
}