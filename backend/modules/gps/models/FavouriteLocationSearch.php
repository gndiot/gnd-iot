<?php

namespace backend\modules\gps\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "favourite_location".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $name
 * @property string $data
 * @property int $created_at
 */
class FavouriteLocationSearch extends FavouriteLocation
{
  
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'group_id', 'created_at'], 'integer'],
            [['data'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }
    
    public function search($params){
    	
    	$query=FavouriteLocation::find();
    	
    	
    	
    	$dataProvider=new ActiveDataProvider([
    			'query'=>$query,
    			'pagination' => [
    					'pageSizeLimit' => [0, 50],
    			],
    	]);
    	
    	$this->load($params,"");
    	
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    	
    	return $dataProvider;
    }
    
    public function searchGroup($params,$id){
    		
    	$query=FavouriteLocation::find()->where(["customer_id"=>$id]);
    	
    	
    	
    	$dataProvider=new ActiveDataProvider([
    			'query'=>$query,
    			'pagination' => [
    					'pageSizeLimit' => [0, 50],
    			],
    	]);
    	
    	$this->load($params,"");
    	
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    	
    	return $dataProvider;
    	
    }
    

}
