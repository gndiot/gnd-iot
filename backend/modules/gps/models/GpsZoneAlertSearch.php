<?php

namespace backend\modules\gps\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "device_gps_zone_alert".
 *
 * @property int $id
 * @property int $sensor_type
 * @property int $customer_id
 * @property int $group_id
 * @property string $device_ids
 * @property string $zone_type
 * @property string $zone
 * @property int $on_leave
 * @property int $on_enter
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $job_id
 */
class GpsZoneAlertSearch extends GpsZoneAlert 
{
   

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sensor_type', 'customer_id', 'group_id', 'zone_type', 'zone', 'on_leave', 'on_enter', 'status', 'created_at', 'updated_at', 'job_id'], 'integer'],
        	[['device_ids'], 'safe'],
        ];
    }
    
    
    public function search($params){
    	
    	 $query=GpsZoneAlert::find();
    	 
    	 $dataProvider= new ActiveDataProvider([
    	 		"query"=>$query
    	 ]);
    	
    	 $this->load($params,"");
    	 
    	 if(!$this->validate())
    	 {
    	 	
    	 	 return $dataProvider;
    	 }
    	 
    
    	 return $dataProvider;
    	 
    }
    
    public function searchGroups($filter,$user_groups,$customer_id){
    	 
    	
    	$query=GpsZoneAlert::find();
    	
    	$dataProvider= new ActiveDataProvider([
    			"query"=>$query
    	]);
    	
    	$this->load($filter,"");
    	
    	if(!$this->validate())
    	{
    		
    		return $dataProvider;
    	}
    	
    	
    	
    	return $dataProvider;
    	
    	
    }
    
}
