<?php

namespace backend\modules\gps\models;

use Yii;
use backend\modules\api\models\DeviceGroups;

/**
 * This is the model class for table "favourite_location".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $name
 * @property string $data
 * @property int $created_at
 */
class FavouriteLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'favourite_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[["name","data"],"required"],
            [['customer_id', 'group_id', 'created_at'], 'integer'],
            [['data'], 'onValidateArray'],
            [['name'], 'string', 'max' => 255],
        ];
    }
    
    public function onValidateArray($attribute){
    	
    	
    	if(!is_array($this->data))
    	{
    		
    		$this->addError($attributes,"{$attributes} must be array" );
    		return false;
    		
    	}
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'name' => 'Name',
            'data' => 'Data',
            'created_at' => 'Created At',
        ];
    }
    
    
    public function beforeSave($insert){
    	
    	
    	if (!parent::beforeSave($insert)) {
    		return false;
    	}
    	if($insert)
    	{
    		
    		
    	}else{
    		
    		
    	}
    	
    	$this->data=json_encode($this->data);
    
    	
    	return true;
    	
    }
    
    public function getCustomer(){
    	
    	
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"customer_id"]);
    	
    }
    
    
    public function afterSave($insert, $changedAttributes){
    	
    	
    	
    
    	$this->data=json_decode($this->data,true);
    }
    
    public function extraFields(){
    	
    	return ['customer'];
    }
    
    
    
    public function afterFind(){
    	parent::afterFind();
    	
    	$this->data=json_decode($this->data);
    	
    	
    	
    }
    
    
    
    
}
