<?php

namespace backend\modules\gps\models;

use Yii;
use common\models\Devices;
use backend\modules\api\models\DeviceGroups;

/**
 * This is the model class for table "device_gps_zone_alert".
 *
 * @property int $id
 * @property int $sensor_type
 * @property int $customer_id
 * @property int $group_id
 * @property int $device_ids
 * @property string $zone_type
 * @property string $zone
 * @property int $on_leave
 * @property int $on_enter
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $job_id
 */
class GpsZoneAlert extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_gps_zone_alert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
    	return [
    			[['sensor_type','device_ids', 'customer_id', 'group_id', 'zone_type', 'zone', 'on_leave', 'on_enter', 'status'], 'required'],
    			[['sensor_type', 'customer_id', 'group_id', 'zone_type', 'zone', 'on_leave', 'on_enter', 'status', 'created_at', 'updated_at', 'job_id'], 'integer'],
    			['device_ids','each','rule'=>['integer']],
    			['emails','each','rule'=>['email']],
    			[['mobile_numbers'], 'each','rule'=>['integer']],
    			
    	];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sensor_type' => 'Sensor Type',
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'device_ids' => 'Device',
        	'emails' => 'Emails',
        	'mobile_numbers' => 'Mobile Numbers',
            'zone_type' => 'Zone Type',
            'zone' => 'Zone',
            'on_leave' => 'On Leave',
            'on_enter' => 'On Enter',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'job_id' => 'Job ID',
        ];
    }
    
    
    public function getCustomer(){
    	
    	
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"customer_id"]);
    	
    }
    
    public function getGroup(){
    	
    	   
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"group_id"]);
    	
    }
    
    public function getZones(){
    	
    	 return GpsZone::find()->select(["name"])->where(["id"=>$this->zone])->one();
    	
    }
    
    public function getDevices(){
    	
    	if($this->device_ids =="")
    	{
    		return [];
    	}
    	
    	$devices=implode("|",$this->device_ids);
    	Yii::trace($devices);
    	return Devices::find()->where(["id"=>$this->device_ids])->all();
    	//return $this->hasMany(Devices::className(), ['id'=>'device_ids'])->where(["id"=>$devices]);
    	
    }
   
    
    public function extraFields(){
    	
    	return ['customer','group','devices','zones'];
    	
    }
    
    
    public function afterFind(){
    	
    	if($this->device_ids !="")
    	{
    		
    		$data=explode("|", $this->device_ids);
    		$this->device_ids=array_map('intval',$data);
    		
    		
    	}else{ 
    		
    		$this->device_ids=[];
    	}
    	
    	$created_at=Yii::$app->utils->getDateTime($this->created_at);
    	$updated_at=Yii::$app->utils->getDateTime($this->updated_at);
    	
    	$this->created_at=$created_at->format('Y-m-d H:i:s');
    	$this->updated_at=$updated_at->format('Y-m-d H:i:s');
    	
    	$this->emails=$this->emails!=""?explode("|", $this->emails):[];
    	$this->mobile_numbers=$this->mobile_numbers!=""?explode("|", $this->mobile_numbers):[];
    	
   	
    }
    
   
    public function beforeSave($insert){
    	
    	$this->emails=implode("|", $this->emails);
    	$this->mobile_numbers=implode("|", $this->mobile_numbers);
    	
    	$this->device_ids=implode("|", $this->device_ids);
    	return true;
    	
    }
    
}
