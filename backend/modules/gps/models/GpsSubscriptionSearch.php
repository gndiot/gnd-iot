<?php

namespace backend\modules\gps\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "device_gps_subscriptions".
 *
 * @property int $id
 * @property int $sensor_type
 * @property int $customer_id
 * @property int $group_id
 * @property int $device_ids
 * @property int $is_custom
 * @property int $condition_code
 * @property double $value_one
 * @property double $value_two
 * @property string $attribute
 * @property int $is_email
 * @property int $is_push
 * @property int $is_sms
 * @property string $emails
 * @property string $mobile_numbers
 * @property int $is_default_email
 * @property int $is_default_sms
 * @property int $is_repeat
 * @property string $intervel_type
 * @property int $intervel_mins
 * @property int $last_run_time
 * @property int $next_run_time
 * @property int $started_at
 * @property int $notify_first_time
 * @property int $notification_status
 * @property string $np_days
 * @property string $np_start_time
 * @property string $np_end_time
 * @property int $created_at
 * @property int $updated_at
 * @property int $job_id
 */
class GpsSubscriptionSearch extends GpsSubscriptions 
{
	
	const STATUS_ACTIVE=10;
	const STATUS_INACTIVE=20;
	const STATUS_DELETE=0;
    
    public function rules()
    {
        return [
            [['sensor_type', 'customer_id', 'group_id', 'device_ids', 'is_custom', 'condition_code', 'is_email', 'is_push', 'is_sms', 'is_default_email', 'is_default_sms', 'is_repeat', 'intervel_mins', 'last_run_time', 'next_run_time', 'started_at', 'notify_first_time', 'notification_status', 'created_at', 'updated_at', 'job_id'], 'integer'],
            [['value_one', 'value_two'], 'number'],
            [['mobile_numbers'], 'string'],
            [['np_start_time', 'np_end_time'], 'safe'],
            [['attribute', 'emails', 'np_days'], 'string', 'max' => 50],
            [['intervel_type'], 'string', 'max' => 255],
        ];
    }
    
    
    public function search($params){
    	
    	   $query= GpsSubscriptions::find();
    	   
    	   
    	   $dataProvider= new ActiveDataProvider([
    	   		 "query"=>$query,
    	   		 'pagination' => [
    	   				'pageSizeLimit' => [0, 50],
    	   		],
    	   		 
    	   ]);
    	   
    	   $this->load($params,"");
    	   
    	   if(!$this->validate())
    	   {
    	   	
    	   	  return $dataProvider;
    	   }
    	   
    	   
    	   return $dataProvider;
    			
    	
    }
    
    public function searchGroups($params,$group_ids,$customer_id){
    	
    	$query= GpsSubscriptions::find()->where(["group_id"=>$group_ids,"customer_id"=>$customer_id]);
    	
    	
    	$dataProvider= new ActiveDataProvider([
    			"query"=>$query,
    			'pagination' => [
    					'pageSizeLimit' => [0, 50],
    			],
    			
    	]);
    	
    	$this->load($params,"");
    	
    	if(!$this->validate())
    	{
    		
    		return $dataProvider;
    	}
    	
    	
    	return $dataProvider;
    	
    }
    
    
}
