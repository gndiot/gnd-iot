<?php

namespace backend\modules\gps\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "device_gps_zones".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $name
 * @property int $zone_type
 * @property string $zone_data
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class GpsZoneSearch extends GpsZone
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'group_id', 'zone_type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['zone_data'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }
    
    
    public function search($params){
    	
    	   $query=GpsZone::find();
    	   
    	   
    	   $dataProvider=new ActiveDataProvider([
    	   		"query"=>$query
    	   ]);
    	   
    	    $this->load($params,"");
    	    
    	    if(!$this->validate())
    	    {
    	    	return $dataProvider;
    	    }
    	   
    	   
    	   return $dataProvider;
    	
    }
    
    public function searchGroups($filter,$user_groups,$customer_id){
    	
    	$query=GpsZone::find()->where(["customer_id"=>$customer_id]);
    	
    	$dataProvider=new ActiveDataProvider([
    			"query"=>$query
    	]);
    	
    	$this->load($filter,"");
    	
    	if(!$this->validate())
    	{
    		return $dataProvider;
    	}
    	
    	
    	return $dataProvider;
    	
    }
    
    
    

}
