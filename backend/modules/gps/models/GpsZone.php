<?php

namespace backend\modules\gps\models;

use Yii;
use yii\helpers\Json;
use backend\modules\api\models\DeviceGroups;

/**
 * This is the model class for table "device_gps_zones".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $name
 * @property string $zone_type
 * @property string $zone_data
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class GpsZone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_gps_zones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['name','zone_type','zone_data'], 'required'],
            [['customer_id', 'group_id', 'zone_type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['zone_data'], 'safe'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'name' => 'Name',
            'zone_type' => 'Zone Type',
            'zone_data' => 'Zone Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    
    public function getCustomer(){
    	

    	return $this->hasOne(DeviceGroups::className(), ["id"=>"customer_id"]);
    	
    }
    
    public function afterFind(){
    	
    	  if($this->zone_data !="")
    	  {
    	  	
    	  	$this->zone_data=Json::decode($this->zone_data);
    	  	
    	  }else{
    	  	
    	  	 $this->zone_data=[];
    	  }
    	  
    	  $this->zone_type=intval($this->zone_type);
    	  
    	
    	 
    	  
    	  if($this->created_at !="")
    	  {
    	  	$created_at=Yii::$app->utils->getDateTime($this->created_at);
    	  	$this->created_at=$created_at->format('Y-m-d H:i:s');
    	  }
    	  
    	  if($this->updated_at !="")
    	  {
    	  	$updated_at=Yii::$app->utils->getDateTime($this->updated_at);
    	  	$this->updated_at=$updated_at->format('Y-m-d H:i:s');
    	  	
    	  }
    	 
    	  
    	  
    }
    
    public function beforeSave($insert){
    	 $this->zone_data=Json::encode($this->zone_data);
    	return true;
    	
    }
    
    public function extraFields(){
    	
    	return ['customer'];
    }
    
}
