<?php

namespace backend\modules\gps\models;

use Yii;
use backend\modules\api\models\DeviceGroups;
use common\models\Constants;
use common\models\Devices;

/**
 * This is the model class for table "device_gps_subscriptions".
 *
 * @property int $id
 * @property int $sensor_type
 * @property int $customer_id
 * @property int $group_id
 * @property int $device_ids
 * @property int $is_custom
 * @property int $condition_code
 * @property double $value_one
 * @property double $value_two
 * @property string $attribute
 * @property int $is_email
 * @property int $is_push
 * @property int $is_sms
 * @property string $emails
 * @property string $mobile_numbers
 * @property int $is_default_email
 * @property int $is_default_sms
 * @property int $is_repeat
 * @property string $intervel_type
 * @property int $intervel_mins
 * @property int $last_run_time
 * @property int $next_run_time
 * @property int $started_at
 * @property int $notify_first_time
 * @property int $notification_status
 * @property string $np_days
 * @property string $np_start_time
 * @property string $np_end_time
 * @property int $created_at
 * @property int $updated_at
 * @property int $job_id
 */
class GpsSubscriptions extends \yii\db\ActiveRecord
{
	const STATUS_ACTIVE=10;
	const STATUS_INACTIVE=20;
	const STATUS_DELETE=0;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
    	 
        return 'device_gps_subscriptions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sensor_type', 'customer_id', 'group_id', 'is_custom', 'condition_code', 'is_email', 'is_push', 'is_sms', 'is_default_email', 'is_default_sms', 'is_repeat', 'intervel_mins', 'last_run_time', 'next_run_time', 'started_at', 'notify_first_time', 'notification_status', 'created_at', 'updated_at', 'job_id'], 'integer'],
            [['value_one', 'value_two'], 'number'],
        	['device_ids','each','rule'=>['integer']],
        	['emails','each','rule'=>['email']],
        	[['mobile_numbers'], 'each','rule'=>['integer']],
            [['np_start_time', 'np_end_time'], 'safe'],
            [['attribute', 'np_days'], 'string', 'max' => 50],
            [['intervel_type'], 'string', 'max' => 255],
        	[['notification_status'],'default','value'=>self::STATUS_ACTIVE],
        	[['intervel_type'], 'default', 'value' =>Constants::INTERVEL_TYPE_MINUTES],
        	[['is_default_email','is_default_sms','is_push','is_email','is_sms'],'default','value'=>0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sensor_type' => 'Sensor Type',
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'device_ids' => 'Device Ids',
            'is_custom' => 'Is Custom',
            'condition_code' => 'Condition Code',
            'value_one' => 'Value One',
            'value_two' => 'Value Two',
            'attribute' => 'Attribute',
            'is_email' => 'Is Email',
            'is_push' => 'Is Push',
            'is_sms' => 'Is Sms',
            'emails' => 'Emails',
            'mobile_numbers' => 'Mobile Numbers',
            'is_default_email' => 'Is Default Email',
            'is_default_sms' => 'Is Default Sms',
            'is_repeat' => 'Is Repeat',
            'intervel_type' => 'Intervel Type',
            'intervel_mins' => 'Intervel Mins',
            'last_run_time' => 'Last Run Time',
            'next_run_time' => 'Next Run Time',
            'started_at' => 'Started At',
            'notify_first_time' => 'Notify First Time',
            'notification_status' => 'Notification Status',
            'np_days' => 'Np Days',
            'np_start_time' => 'Np Start Time',
            'np_end_time' => 'Np End Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'job_id' => 'Job ID',
        ];
    }
    
    public function getCustomer(){
    	
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"customer_id"]);
    	
    }
    
    public function getGroup(){
    	
    	 
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"group_id"]);
    	
    }
    
    public function getDevices(){
    	
    	if($this->device_ids =="")
    	{
    		return [];
    	}
    	
    	
    	
    	$devices=implode("|",$this->device_ids);
    	Yii::trace($devices);
    	return Devices::find()->where(["id"=>$this->device_ids])->all();
    	//return $this->hasMany(Devices::className(), ['id'=>'device_ids'])->where(["id"=>$devices]);
    	
    }
    
    public function getDaysTostring($glue="|"){
    	
    	if($this->np_days !=""){
    		
    		return implode($glue, $this->np_days);
    		
    	}
    	return "";
    	
    }
    
    public function getEmailsTostring($glue="|"){
    	
    	if($this->emails !=""){
    		
    		return implode($glue, $this->emails);
    		
    	}
    	return "";
    	
    }
    
    public function getNumbersTostring($glue="|"){
    	
    	if($this->mobile_numbers!=""){
    		
    		return implode($glue, $this->mobile_numbers);
    		
    	}
    	return "";
    	
    }
    
    public function extraFields(){
    	
    	return ['customer','group','devices'];
    	
    }
    
    public function afterFind(){
    	
    	if($this->device_ids!="")
    	{
    		$data=explode("|", $this->device_ids);
    		$this->device_ids=array_map('intval',$data);
    	}
    	
    	$this->condition_code="".$this->condition_code;
    	$this->id=Yii::$app->utils->encrypt($this->id);
    	$this->emails=$this->emails!=""?explode("|", $this->emails):[];
    	$this->mobile_numbers=$this->mobile_numbers!=""?explode("|", $this->mobile_numbers):[];
    	$this->np_days=$this->np_days!=""?explode("|", $this->np_days):[];
    	
    	$created_at=Yii::$app->utils->getDateTime($this->created_at);
    	$updated_at=Yii::$app->utils->getDateTime($this->updated_at);
    	
    	$this->created_at=$created_at->format('Y-m-d H:i:s');
    	$this->updated_at=$updated_at->format('Y-m-d H:i:s');
    	
    }
    
    public function beforeSave($insert){
    	
    	
    	if (!parent::beforeSave($insert)) {
    		return false;
    	}
    	if($insert)
    	{
    		
    	}else{
    		

    	}
    	Yii::trace($this->device_ids);
    	$this->id=Yii::$app->utils->decrypt($this->id);
    	$this->device_ids=implode("|", $this->device_ids);
    	$this->emails=implode("|", $this->emails);
    	$this->mobile_numbers=implode("|", $this->mobile_numbers);
    	//$this->np_days=$this->getDaysTostring();
    	 //Yii::trace($this->np_days);
    	return true;
    	
    }
}
