<?php

namespace backend\modules\gps\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Default controller for the `gps` module
 */
class SubscriptionController extends Controller
{
	
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'allow' => true,
										'roles' => ['@'],
								],
						],
				],
		];
	}
	
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	$this->layout='gps';
    	
    	return $this->render('index');
    }
    public function actionCreate()
    {
    	$this->layout='gps';
    	
    	return $this->render('index');
    }
    public function actionUpdate()
    {
    	$this->layout='gps';
    	
    	return $this->render('index');
    }
}
