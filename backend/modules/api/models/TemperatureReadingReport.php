<?php

namespace backend\modules\api\models;

use Yii;
use common\models\DeviceTemperatureReading;
use common\models\Devices;

/**
 * This is the model class for table "device_temperature_readings".
 *
 * @property int $id
 * @property string $device_id
 * @property string $hardware_serial
 * @property string $payload_raw
 * @property int $battery
 * @property double $temperature
 * @property int $humidity
 * @property int $created_at
 */
class TemperatureReadingReport extends DeviceTemperatureReading
{
   
	
	public function getTemperature(){
		
		
		return $this->hasOne(Devices::class, ["hardware_serial"=>"hardware_serial"]);
		
	}
	
}
