<?php
namespace backend\modules\api\models;

use yii;
use yii\base\Model;




class DailySummaryReport extends Model{
	
	 public $sensor;
	 public $from;
	 public $to;
	 public $devices;
	 public $group;
	 public $groups;
	 
	 public function rules(){
	 	
	 		return [
	 				[['sensor','from','to','devices'],'required'],
	 				[['from','to'], 'date', 'format' => 'php:Y-m-d'],
	 				[['group'], 'integer'],
	 				['devices', 'each', 'rule' => ['integer']],
	 				['groups', 'each', 'rule' => ['integer']],
	 				['sensor', 'in', 'range' => ['temperature','humidity','gps']],
	 		];
	 	
	 }
	
	
}