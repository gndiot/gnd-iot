<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "service_provider".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $endpoint
 * @property string $auth_type
 * @property string $username
 * @property string $password
 * @property int $created_at
 * @property int $updated_at
 */
class NetworkProvider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_provider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['name','endpoint'],'required'],
        	['name','unique'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'auth_type', 'username', 'password','endpoint'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        	'endpoint'=>'Endpoint',
            'auth_type' => 'Auth Type',
            'username' => 'Username',
            'password' => 'Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    
}
