<?php
namespace backend\modules\api\models;

use yii;
use yii\base\Model;




class DoorReport extends Model{
	
	 public $sensor;
	 public $date;
	 public $devices;
	 public $groups;
	 public $from;
	 public $to;
	 
	 
	 public function rules(){
	 	
	 		return [
	 				[['sensor','groups',"from","to"],'required'],
	 				[['date'], 'date', 'format' => 'php:Y-m-d'],
	 				['devices', 'each', 'rule' => ['integer']],
	 				['groups', 'each', 'rule' => ['integer']],
	 				['sensor', 'in', 'range' => ['door']],
	 				
	 		];
	 	
	 }
	
	
}