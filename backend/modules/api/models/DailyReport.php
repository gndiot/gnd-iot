<?php
namespace backend\modules\api\models;

use yii;
use yii\base\Model;




class DailyReport extends Model{
	
	 public $sensor;
	 public $date;
	 public $devices;
	 public $hours;
	 public $group;
	 public $groups;
	 
	 public function rules(){
	 	
	 		return [
	 				[['sensor','date','hours','group'],'required'],
	 				[['date'], 'date', 'format' => 'php:Y-m-d'],
	 				[['group'], 'integer'],
	 				[['groups'], 'each', 'rule' => ['integer']],
	 				['devices', 'each', 'rule' => ['integer']],
	 				['sensor', 'in', 'range' => ['temperature','humidity','gps']],
	 				['hours', 'in', 'range' => ['two','four']],
	 		];
	 }

}
