<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "device_groups".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $description
 * @property int $company_id
 */
class DeviceGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['name','parent_id'],'required'],
        	['name','unique','when'=>function($model){
        		  
        		 return DeviceGroups::find()->where(["parent_id"=>$model->parent_id,"name"=>$model->name])->exists();
        		 
        	}],
        	//['company_id','exist','targetAttribute'=>'id'],
            [['parent_id'], 'integer'],
            ['parent_id','default', 'value' => 0],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }
    
    
    public static function getChildrenWithParent($group_id)
    {
    	
    	$children=self::find()->where(["parent_id"=>$group_id])->asArray()->all();
    	
    		if(!empty($children))
    		{
    			return $children;
    			
    		}
    		
    		$ownGroup=self::find()->where(["id"=>$group_id])->asArray()->all();
    		
    		return $ownGroup;
    	
    	
    }
    
    public static function hasParent($group_id){
	    	
    	$children_count=self::find()->where(["id"=>$group_id,'parent_id'=>0])->count();
	    	
	    	if(intval($children_count) > 0)
	    	{
	    		return true;
	    		
	    	}else{
	    		
	    		return false;
	    	}
    	
    }
    
    public static function getParentId($child_id){
    	
    	
    		$group=self::find()->where(["id"=>$child_id])->one();
    		return $group->parent_id;
    	
    	
    }
    
    
    
    public static function getChildren($parent_id){
    	
    	  return self::find()->where(["parent_id"=>$parent_id])->asArray()->all();
    	
    }
    
    public static function getAllChildren($parent_id=0,$level=0){
    	
    	if($parent_id!=0)
    	{
    		$level++;
    	}
    	$result=[];
    	 
    	$parents=self::find()->where(["parent_id"=>$parent_id])->asArray()->all();
    	 
 	
    	
    	foreach ($parents as $id=>$p){
    		$p["level"]=$level;
    		array_push($result, $p);
    		$child=self::getAllChildren($p["id"],$level);
    	
    		for ($i = 0; $i < count($child); $i++) {
    			 
    			array_push($result, $child[$i]);
    			 
    		}
    	
   
    	
    	}
    	 
    	return $result;
    	
    }
    
    
}
