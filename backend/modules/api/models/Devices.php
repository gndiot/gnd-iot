<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "devices".
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property int $group_id
 * @property string $display_name
 * @property string $dev_description
 * @property int $dev_type
 * @property string $dev_eui
 * @property string $app_eui
 * @property string $app_key
 * @property string $activation_method
 * @property int $dev_status
 */
class Devices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'group_id', 'dev_type', 'dev_status'], 'integer'],
            [['dev_description'], 'string'],
            [['uuid'], 'string', 'max' => 15],
            [['display_name', 'dev_eui', 'app_eui', 'app_key', 'activation_method'], 'string', 'max' => 255],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'Uuid',
            'company_id' => 'Company ID',
            'group_id' => 'Group ID',
            'display_name' => 'Display Name',
            'dev_description' => 'Dev Description',
            'dev_type' => 'Dev Type',
            'dev_eui' => 'Dev Eui',
            'app_eui' => 'App Eui',
            'app_key' => 'App Key',
            'activation_method' => 'Activation Method',
            'dev_status' => 'Dev Status',
        ];
    }
}
