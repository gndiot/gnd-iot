<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
use backend\modules\api\models\DeviceGroups;
use common\models\CustomerSearch;
use common\models\Customer;
use yii\helpers\ArrayHelper;
use common\models\Devices;
use common\models\DeviceAlert;
use yii\filters\AccessControl;
use yii\helpers\Json;

class AdminController extends Controller{
	
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'actions' => ['index'],
								'roles' => ['Admin Dashboard']
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		
		
		return $behaviors;
	}
	
	
	public function actionIndex(){
				
		$groups=DeviceGroups::find()->where(["parent_id"=>0])->asArray()->all();

		 $count=count($groups);
		
		 $result=[];
		 
		for ($i = 0; $i < $count; $i++) {
			
			 
			 $temp["name"]=$groups[$i]["name"];
			 $temp["temperature"]=intval(Temperature::find()->andWhere(['customer_id'=>$groups[$i]["id"]])->count());
			 $temp["door"]=intval(Door::find()->andWhere(['customer_id'=>$groups[$i]["id"]])->count());
			 $temp["gps"]=intval(Gps::find()->andWhere(['customer_id'=>$groups[$i]["id"]])->count());
			 $no_comm_temperature=$this->getNoCommDevices(1,$groups[$i]["id"]);//Temperature::find()->andWhere(['customer_id'=>$groups[$i]["id"]])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
			 $no_comm_door=$this->getNoCommDevices(3,$groups[$i]["id"]);//Door::find()->andWhere(['customer_id'=>$groups[$i]["id"]])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
			 $no_comm_gps=$this->getNoCommDevices(2,$groups[$i]["id"]);//Gps::find()->andWhere(['customer_id'=>$groups[$i]["id"]])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
			 $temp["no_communication"]=($no_comm_temperature+$no_comm_door+$no_comm_gps);
			 $temp["battery"]=$this->getBatteryIssues($groups[$i]["id"]);
			 array_push($result, $temp);
			 
		}
		
		return $result;
		
		
		
		
		 
		
	}
	
	public function actionDevices(){
		
		$group_id=Yii::$app->user->identity->group_id;
		
		$groups=DeviceGroups::getAllChildren($group_id);
		
		
		
		
		
		if(empty($groups))
		{
			$groups=$group_id;
			
		}else{
			
			$groups= ArrayHelper::getColumn($groups, "id");
		}
		
		$temperature= Temperature::find()->andWhere(['group_id'=>$groups,'status'=>10])->count();
		$door = Door::find()->andWhere(['group_id'=>$groups,'status'=>10])->count();
		$gps= Gps::find()->andWhere(['group_id'=>$groups,'status'=>10])->count();
		$customers=DeviceGroups::find()->where(["parent_id"=>0])->count();
		$no_comm_temperature=Temperature::find()->andWhere(['group_id'=>$groups,'status'=>10])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
		$no_comm_door=Door::find()->andWhere(['group_id'=>$groups,'status'=>10])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
		$no_comm_gps=Gps::find()->andWhere(['group_id'=>$groups,'status'=>10])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
	
		$alerts_ack=DeviceAlert::find()->where(["group_id"=>$groups,"status"=>DeviceAlert::STATUS_ACK])->count();
		$alerts_not_ack=DeviceAlert::find()->where(["group_id"=>$groups,"status"=>DeviceAlert::STATUS_NOT_ACK])->count();
		
		return [
				"temperature"=>$temperature,
				"door"=>$door,
				"gps"=>$gps,
				"customers"=>1,
				"humidity"=>$temperature,
				"no_com_temperature"=>$no_comm_temperature,
				"no_com_gps"=>$gps,
				"no_com_door"=>$door,
				"alert_ack"=>$alerts_ack,
				"alert_not_ack"=>$alerts_not_ack
		];
		
	}
	
	public function actionDeviceCommunication(){
		
				
	}
	
	public function actionAlertsCount(){
		
		//Yii::$app->db->createCommand("SELECT *,TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) as time_diff FROM `devices` HAVING TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->q;			
			  
		return Devices::find()->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
		   
	}
	
	
	public function getBatteryIssues($groups){
		
		$devices=Devices::find()->where(['customer_id'=>$groups])->asArray()->all();
		$count=count($devices);
		$battery_issue=[];
		$battery_not_issue=[];
		
		for ($i = 0; $i < $count; $i++) {
			
			   $config=$devices[$i]["config_new"];
			   $config=Json::decode($config);
			   
			   if($devices[$i]["data"] =="")
			   {
			   	  continue;
			   }
			   
			    if($config["battery_range"]["low"] > $devices[$i]["battery_level"]){
			    	  $battery_issue[]=1;
			    }
		}
		
		return array_sum($battery_issue);
		
	}
	
	
	public function hasComminication($sync_time='',$reporting_interval){
		
		
		$date1=new \DateTime();
		$date2=new \DateTime();
		$date2->setTimestamp($sync_time);
		
		if($sync_time=="")
		{
			return true;
		}
		
		$diffInSeconds=$date2->getTimestamp()-$date1->getTimestamp();
		
		
		$reporting_interval=intval($reporting_interval)+Yii::$app->params["no_communication"];
		
		$convertMints=($reporting_interval*60);
		
		if(abs($diffInSeconds) > $convertMints)
		{
			
			return true;
			
		}else{
			
			return false;
			
		}
		
	}
	
	public function getNoCommDevices($type,$groups){
		
		
		$groups_data=DeviceGroups::find()->where(["parent_id"=>$groups])->asArray()->all();
		$gids=ArrayHelper::getColumn($groups_data, "id");
		
		 $devList=Devices::find()->where(["type"=>$type,"status"=>10,"group_id"=>$gids])->asArray()->all();
		$count=count($devList);
		
		$result=[];
		
		for ($i = 0; $i < $count; $i++) {
			
			$config=Json::decode($devList[$i]["config_new"]);
			
			if(empty($config))
			{
				continue;
			}
			
			$device=$devList[$i];
			
			$date1=new \DateTime();
			$date2=new \DateTime();
			$date2->setTimestamp($device['sync_time']);
			
			if($device['sync_time']=="")
			{
				
				array_push($result,intval($device["type"]));
				
				continue;
			}
			
			$diffInSeconds=$date2->getTimestamp()-$date1->getTimestamp();
			
			
			$reporting_interval=intval($config["reporting_interval"])+Yii::$app->params["no_communication"];
			
			$convertMints=($reporting_interval*60);
			
			if(abs($diffInSeconds) > $convertMints)
			{
				
				array_push($result,intval($device["type"]));
				continue;
				
			}else{
				
				
				
			}
			
			
		}
		
		
		return count($result);
		
		
	}
	
	
}