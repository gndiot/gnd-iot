<?php
namespace backend\modules\api\controllers;


use yii;
use yii\rest\Controller;
use common\models\DeviceAlert;
use yii\web\NotFoundHttpException;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use common\models\DeviceAlertSearch;

class NotificationController extends Controller{
	

	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		   
		return $behaviors;
	}
	
		protected function verbs(){
			
			return [
					'index' => ['GET','HEAD','POST'],
					'view' => ['GET', 'HEAD'],
					'status' => ['POST'],
					'update' => ['PUT', 'PATCH'],
					'delete' => ['DELETE'],
			];
			
		}
	
		public function actionIndex(){
			if(Yii::$app->user->identity->hasRole('super user'))
			{
				$filter=Yii::$app->getRequest()->getBodyParams();
				$search = new DeviceAlertSearch();
				return $search->search($filter);
				
// 				/return DeviceAlert::find()->orderBy(['id'=>SORT_DESC])->limit(100)->all();
			}else{
				
				
				$user= Yii::$app->user->identity;
				
				$customer_id=$user->customer_id;
				
				$user_groups=explode("|",$user->group_ids);
				
				$filter=Yii::$app->getRequest()->getBodyParams();
				
				  
				$search = new DeviceAlertSearch();
				return $search->searchGroups($filter, $user_groups, $customer_id);

				
				//return DeviceAlert::find()->where(['group_id'=>$user_groups,'status'=>DeviceAlert::STATUS_NOT_ACK])->orderBy(['id'=>SORT_DESC])->limit(100)->all();
			}
			
				
		}
			
		public function actionView($id){
			
			$oid= Yii::$app->utils->decrypt($id);
			
			$model = DeviceAlert::find()->where(['id'=>$oid])->one();
			
			if(is_null($model))
			{
				throw new NotFoundHttpException("Object not found: $id");
			}
			
			return $model;
		}
		
		public function actionStatus($id){
			
			   $oid= Yii::$app->utils->decrypt($id);
				   
			    $model = DeviceAlert::find()->where(['id'=>$oid])->one();
			    
			    if(is_null($model))
			    {
			    	throw new NotFoundHttpException("Object not found: $id");
			    }
			    
			    $rdata=Yii::$app->getRequest()->getBodyParams();
			    
			    unset($rdata['id']);
			    
				$model->load($rdata,'');
			    
				$model->updated_at=time();
				$model->user_id=Yii::$app->user->id;
				
				$model->save(false);
			   
				return $model;
				
			
		}
		
		public function actionDelete($id){
			
			$oid= Yii::$app->utils->decrypt($id);
			
			$model = DeviceAlert::find()->where(['id'=>$oid])->one();
			
			if(is_null($model))
			{
				throw new NotFoundHttpException("Object not found: $id");
			}
			
		}
	
	
}