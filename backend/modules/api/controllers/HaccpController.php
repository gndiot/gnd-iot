<?php

namespace backend\modules\api\controllers;

use yii\rest\Controller;
use yii;
use yii\filters\AccessControl;
use backend\models\AdminUser;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use common\models\HaccpDevice;
use yii\helpers\ArrayHelper;
/**
 * Default controller for the `api` module
 */
class HaccpController extends Controller
{
	public $enableCsrfValidation =false;
    /**
     * Renders the index view for the module
     * @return string
     */
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
    public function actionIndex()
    {
    	$devices=HaccpDevice::find()->asArray()->all();
    	
    	$count=count($devices);
    	
    	for ($i = 0; $i < $count; $i++) {
    		
    		$lastData=$this->actionView(trim($devices[$i]["name"]));
    		 if(!empty($lastData))
    		 {
    		 	$devices[$i]["last_data"]=$lastData[0];
    		 	
    		 }else{
    		 	
    		 	$devices[$i]["last_data"]=null;
    		 }
    		 
    		 
    	}
    	
    	
    	return $devices;
    	
    }
    
  	public function actionView($name=""){
  		
  		 if($name =="")
  		 {
  		 	return [];
  		 }
  		 
  		
  		 try {
  		 	
  		 	$files=FileHelper::findFiles("./haccp/".$name,['only'=>['*.pdf']]);
  		 	$result=[];
  		 	
  		 	foreach ($files as $file)
  		 	{
  		 		 $date= new \DateTime();
  		 		 $date->setTimestamp(intval(filemtime($file)));
  		 		
  		 		$split=ltrim($file,'./');
  		 		$temp["absolute_path"]=Url::to($split,true);
  		 		$temp["created_at"]=intval(filemtime($file));
  		 		$temp["formated_date"]=$date->format("Y-m-d H:i:s");
  		 		$m_result=array_merge($temp,pathinfo($file));
  		 		
  		 		unset($m_result['dirname']);
  		 		$result[]=$m_result;
  		 		
  		 	}
  		 	
  		 	
  		 	ArrayHelper::multisort($result, ['created_at'],[SORT_DESC]);
  		 	
  		 	return $result;
  		 	
  		 	
  		 	
  		 } catch (\Exception $e) {
  		 	
  		 	return [];
  		 }
  		 
  		
  	}
  	
  	
  	public function actionTest(){
  		
  	
  		    $data=[
  		    		[
  		    			'name'=>"name",
  		    			'display_name'=>"display_name",
  		    			'equipment_id'=>'Equipment Id',
  		    			'location'=>'location',
  		    			'entity'=>'entity',
  		    			'department'=>'department',
  		    			'process'=>'process',
  		    			'temperature'=>'temperature',
  		    			'humidity'=>'humidity',
  		    				
  		    			'time'=>'Time'
  		    		],
  		    		[
  		    				'name'=>"Device1",
  		    				'display_name'=>"Technician_dining_Bain_marie",
  		    				'equipment_id'=>'AC001',
  		    				'location'=>'',
  		    				'entity'=>'entity1',
  		    				'department'=>'department1',
  		    				'process'=>'process1',
  		    				'temperature'=>'30',
  		    				'humidity'=>'75',
  		    				'time'=>'15-06-2018 23:22:54'
  		    		],
  		    		[
  		    				'name'=>"Device1",
  		    				'display_name'=>"Technician_dining_Bain_marie",
  		    				'equipment_id'=>'AC001',
  		    				'location'=>'',
  		    				'entity'=>'entity1',
  		    				'department'=>'department1',
  		    				'process'=>'process1',
  		    				'temperature'=>'30',
  		    				'humidity'=>'75',
  		    				'time'=>'15-06-2018 23:22:54'
  		    		],
  		    		[
  		    				'name'=>"Device1",
  		    				'display_name'=>"Technician_dining_Bain_marie",
  		    				'equipment_id'=>'AC001',
  		    				'location'=>'',
  		    				'temperature'=>'30',
  		    				'humidity'=>'75',
  		    				'time'=>'15-06-2018 22:32:54'
  		    		],
  		    		[
  		    				'name'=>"Device1",
  		    				'display_name'=>"Technician_dining_Bain_marie",
  		    				'equipment_id'=>'AC001',
  		    				'location'=>'',
  		    				'temperature'=>'30',
  		    				'humidity'=>'75',
  		    				'time'=>'15-06-2018 22:35:24'
  		    		],
  		    		[
  		    				'name'=>"Device2",
  		    				'display_name'=>"device2",
  		    				'equipment_id'=>'AC001',
  		    				'location'=>'',
  		    				'temperature'=>'30',
  		    				'humidity'=>'75',
  		    				'time'=>'15-06-2018 22:35:24'
  		    		],
  		    		
  		    		
  		    ];
  		    
  		    
  		   // $data = array('total_stud' => 500);
  		//    return $data;
  		    // creating object of SimpleXMLElement
  		    $xml_data = new \SimpleXMLElement('<?xml version="1.0"?><rows></rows>');
  		    
  		    // function call to convert array to xml
  		    $this->array_to_xml($data,$xml_data);
  		    
  		    $folder=Yii::getAlias("@app")."/web/ajic";
  		    
  		    print $xml_data->asXML($folder."/20-00-00-23-59-59.xml");
  		    die();
  		    
  		    
  		
  	}
  	
  	public  function array_to_xml( $data, &$xml_data ) {
  		foreach( $data as $key => $value ) {
  			if( is_numeric($key) ){
  				$key = 'row'; //dealing with <0/>..<n/> issues
  			}
  			if( is_array($value) ) {
  				$subnode = $xml_data->addChild($key);
  				$this->array_to_xml($value, $subnode);
  			} else {
  				$xml_data->addChild("$key",htmlspecialchars("$value"));
  			}
  		}
  	}
    
    
}
