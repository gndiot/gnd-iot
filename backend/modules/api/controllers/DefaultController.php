<?php

namespace backend\modules\api\controllers;

use yii\rest\Controller;
use yii;
use yii\filters\AccessControl;
use backend\models\AdminUser;
/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
	public $enableCsrfValidation =false;
    /**
     * Renders the index view for the module
     * @return string
     */
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
    public function actionIndex()
    {
    	if(YII_ENV_DEV)
    	{
    		
    	return  AdminUser::find()->select(["auth_key AS token"])->asArray()->one();
    	
    	}else{
    		
    		return ["token"=>"awex09fw9skw3kjds"];
    	}
       
    }
}
