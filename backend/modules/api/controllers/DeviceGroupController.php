<?php

namespace backend\modules\api\controllers;

use yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use backend\modules\api\models\NetworkProvider;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\modules\api\models\DeviceGroups;
use backend\models\AdminUser;

/**
 * Default controller for the `api` module
 */
class DeviceGroupController extends Controller
{
	public $enableCsrfValidation =false;
    /**
     * Renders the index view for the module
     * @return string
     */
	
	public function behaviors(){
		
			$behaviors=parent::behaviors();
			 $behaviors["access"]=[
			 		'class' => AccessControl::className(),
			 		'rules' => [
			 				[
			 						'allow' => true,
			 						'roles' => ['@'],
			 				],
			 		],
			 		'denyCallback' => function ($rule, $action) {
			 			
			 			throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
			 		}
			 ];
			
		   return $behaviors;
	}
	
	protected function verbs(){
		
		return [
				'index' => ['GET','HEAD'],
				'parent' => ['GET','HEAD'],
				'children' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
		
	}
	
	
    public function actionIndex()
    {	
    	
    	if(Yii::$app->user->can('super user'))
    	{	
    		return DeviceGroups::getAllChildren();
    	
    	}else{
    		
    		$user=Yii::$app->user->identity;
    		$groups=explode("|",$user->group_ids);
    		
    		$customer=DeviceGroups::find()->where(["id"=>$user->customer_id,"parent_id"=>0])->asArray()->one();
    		$groups=DeviceGroups::find()->where(["id"=>$groups])->asArray()->all();
    		
    	    array_unshift($groups,$customer);
    	    
    	    return $groups;
    		
    		
    		 if(DeviceGroups::hasParent($group_id))
    		 {
    		 	return DeviceGroups::getAllChildren($group_id);
    		 	
    		 }else{
    		 	
    		 	return DeviceGroups::find()->where(['id'=>$group_id])->all(); 
    		 	
    		 }
    		
    		
    		
    	}
    	
    	
    
    }
    
    public function actionCreate(){
    	
    		$model = new DeviceGroups();
    		$rdata=Yii::$app->getRequest()->getBodyParams();
    		$model->load($rdata,"");
    		if($model->save())
    		{
    			 if($model->parent_id == "")
    			 {

    			 	$default=new DeviceGroups();
    			 	$default->name="Default";
    			 	$default->parent_id=$model->id;
    			 	$default->save(false);
    			 	
    			 }
    			
    			return $model;
    			
    		}else{
    			 
    			 Yii::$app->getResponse()->setStatusCode(422);
    			 return $model->getErrors();
    		}
    	
    }
    
    
    public function actionUpdate($id)
    {
    	
    		$model=DeviceGroups::find()->where(["id"=>$id])->one();
    		if(is_null($model))
    		{
    			
    			throw new NotFoundHttpException("Object not found: $id");
    		}
    		
    		$rdata=Yii::$app->getRequest()->getBodyParams();
    		unset($rdata["id"]);
    		$model->load($rdata,"");
    		
    		if($model->save())
    		{
    			return $model;
    			 
    		}else{
    		
    			Yii::$app->getResponse()->setStatusCode(422);
    			return $model->getErrors();
    		}
    		
    }
    
    
    public function actionDelete($id){
    	
    	$model=DeviceGroups::find()->where(["id"=>$id])->one();
    	if(is_null($model))
    	{
    		 
    		throw new NotFoundHttpException("Object not found: $id");
    	}
    	
    	 DeviceGroups::deleteAll(["parent_id"=>$id]);
    	
    	$model->delete();
    	
    	return $model;
    	
    }
    
    
    public function actionChildren($id){
    	 
    	$roles=AdminUser::activeRoles();
    	
    	
    	if(in_array("super user", $roles) ||  in_array("admin", $roles)){
    		
    		$list=DeviceGroups::find()->where(["parent_id"=>$id])->all();
    		
    	}else{
    		
	    		$user=Yii::$app->user->identity;
	    		
	    		if(!is_null($user))
	    		{
	    		
	    			$groups=explode("|",$user->group_ids);
	    			
	    			
	    			$data=DeviceGroups::find()->where(["id"=>$groups])->all();
	    			
	    			if(empty($data))
	    			{
	    				
	    				return DeviceGroups::find()->where(["id"=>$groups])->all();;
	    			}else{
	    				
	    				return $data;
	    				 
	    			}
	    		
	    		}else{
	    		
	    			return [];
	    		
	    		}
    		 
    	}
    	
    	
    	  return $list;
    }
    
    public function actionParent(){
    	
    	    $roles=AdminUser::activeRoles();
    	    
    	    if(in_array("super user", $roles))
    	    {
    	    	return DeviceGroups::find()->where(["parent_id"=>0])->all();
    	    }else{
    	    	
    	    	$user=Yii::$app->user->identity;
    	    	
    	    	 if(!is_null($user))
    	    	 {
    	    	 	
    	    	 	return DeviceGroups::find()->where(["id"=>$user->customer_id])->all();
    	    	 	
    	    	 }else{
    	    	 	
    	    	 	 return [];
    	    	 	
    	    	 }
    	    	
    	    }
    	    
    		   	
    }
    
    
}
