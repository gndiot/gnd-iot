<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use common\models\Devices;
use common\models\Customer;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use common\models\CustomerSearch;
//use backend\modules\api\models\Devices;


class CustomerController extends Controller{
	
	 
	public function behaviors(){
	
		$behaviors=parent::behaviors();
		$behaviors["access"]=[ 
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				 
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
			
		return $behaviors;
	}
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
	public function actionIndex()
	{
		 $params=Yii::$app->request->queryParams;
		 
		  unset($params["token"]);
		 
		
		 $customer= new CustomerSearch();
		 
		 return $customer->searchAdmin($params);
	 
	}
	
	public function actionCreate(){
		
		 $model= new Customer();
		 $rdata=Yii::$app->getRequest()->getBodyParams();
		 $model->loadDefaultValues();
		 $model->load($rdata,"");
		 $model->generateAuthKey();
		 $model->setPassword($model->password_hash);
		 $model->created_at=time();
		 $model->updated_at=time();
		 
		 if($model->group_id=="")
		 {
		 	$model->group_id=$model->customer_id;
		 }
		 
		 if($model->save())
		 {
		 	return $model;
		 	 
		 }else{
		 
		 	Yii::$app->getResponse()->setStatusCode(422);
		 	return $model->getErrors();
		 }
	}
	
	
	public function actionUpdate($id){
		
		$model=Customer::find()->where(["id"=>$id])->one();

		if(is_null($model))
		{
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(empty($rdata))
		{	 
			throw new NotFoundHttpException("resource not found");	 
		}
		
		unset($rdata["id"]);
		$model->load($rdata,"");
		$model->updated_at=time();
		if(isset($rdata["password_hash"]) && $rdata["password_hash"] != "")
		{
			$model->setPassword($rdata["password_hash"]);
			
		}
		
		if($model->group_id=="")
		{
			$model->group_id=$model->customer_id;
		}
		
		if($model->save())
		{
			return $model;
		
		}else{
		
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
		
	}
	
	
	
	public function actionDelete($id){
		
		$model=Customer::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			 
			throw new NotFoundHttpException("Object not found: $id");
		}
			 
		$model->delete();
		 
		return $model;
		
		 
	}
	
	
	
}