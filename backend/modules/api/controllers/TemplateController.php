<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use common\models\Devices;
use yii\web\NotFoundHttpException;
use common\models\TimeZone;
use yii\filters\AccessControl;
use common\models\Constants;
use common\models\NotificationTemplate;
//use backend\modules\api\models\Devices;


class TemplateController extends Controller{
	
	 
	public function behaviors(){
	
		$behaviors=parent::behaviors();
		$behaviors["access"]=[ 
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				 
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	public function actionType(){
		
		  
		if(!Yii::$app->user->identity->hasRole('super user'))
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return [];	
		}
		
		  return [
		  		[
		  				'name'=>Constants::ALERT_HIGH_TEMPERATURE,
		  				'value'=>Constants::ALERT_HIGH_TEMPERATURE
		  		],
		  		[
		  				'name'=>Constants::ALERT_LOW_TEMPERATURE,
		  				'value'=>Constants::ALERT_LOW_TEMPERATURE
		  		],
		  		[
		  				'name'=>Constants::ALERT_HIGH_HUMIDITY,
		  				'value'=>Constants::ALERT_HIGH_HUMIDITY
		  		],
		  		[
		  				'name'=>Constants::ALERT_LOW_HUMIDITY,
		  				'value'=>Constants::ALERT_LOW_HUMIDITY
		  		],
		  		[
		  				'name'=>Constants::ALERT_OBEDIENCE,
		  				'value'=>Constants::ALERT_OBEDIENCE
		  		],
		  		[
		  				'name'=>Constants::ALERT_DOOR_OPEN,
		  				'value'=>Constants::ALERT_DOOR_OPEN
		  		],
		  		[
		  				'name'=>Constants::ALERT_DOOR_CLOSE,
		  				'value'=>Constants::ALERT_DOOR_CLOSE
		  		],
		  		[
		  				'name'=>Constants::ALERT_LOW_BATTERY,
		  				'value'=>Constants::ALERT_LOW_BATTERY
		  		],
		  		[
		  				'name'=>Constants::ALERT_GPS_IDLE,
		  				'value'=>Constants::ALERT_GPS_IDLE
		  		],
		  		[
		  				'name'=>Constants::ALERT_GPS_OVERSPEED,
		  				'value'=>Constants::ALERT_GPS_OVERSPEED
		  		],
		  		[
		  				"name"=>"ZONE ENTER",
		  				"value"=>"ON_ENTER"
		  		],
		  		[
		  				"name"=>"ZONE EXIT",
		  				"value"=>"ON_EXIT"
		  		]
		  		
		  ];
		 	 
	}
	
	public function actionView($type){
		
		if(!Yii::$app->user->identity->hasRole('super user'))
		{
			 Yii::$app->getResponse()->setStatusCode(402);
			return [];
		}
		     //$model = new NotificationTemplate();
		     $model=NotificationTemplate::find()->where(["alert_type"=>$type])->all();
		     return $model;
		    
	}
	
	
	public function actionUpdate($type,$temp_type){
		
		if(!Yii::$app->user->identity->hasRole('super user'))
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return [];
		}
		
		$model=NotificationTemplate::find()->where(["alert_type"=>$type,"template_type"=>$temp_type])->one();
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(is_null($model))
		{
			$model = new NotificationTemplate();
			$model->load($rdata,"");
			
			if($model->save())
			{
				return $model;
				
			}else{
				
				 return $model->getErrors();
			}
			
		}else{
			
			$model->load($rdata,"");
			
			if($model->save())
			{
				return $model;
				
			}else{
				
				return $model->getErrors();
			}
			
		}
		
		
	}
	
	
	
}