<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use common\models\Devices;
use yii\web\NotFoundHttpException;
use common\models\DeviceConfigRange;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
use common\models\DeviceConfigTemperature;
use common\models\DeviceConfigBattery;
use common\models\DeviceConfigHumidity;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\ArrayHelper;
use common\models\DeviceTemperatureReading;
use common\models\DeviceGpsReading;
use common\models\DeviceDoorReading;
use common\models\DeviceAlert;
use yii\filters\AccessControl;
use yii\helpers\Json;
//use backend\modules\api\models\Devices;


class DeviceController extends Controller{
	
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[		'actions'=>['create'],
								'allow' => true,
								//'ips' => YII_ENV_DEV ? ['127.0.0.1']:['166.62.10.190','192.185.129.139'],
						],
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		 
		
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'config' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	public function actionIndex()
	{
		 if(Yii::$app->user->can('super user'))
		 {
		 	
		 	return Devices::find()->where(['status'=>10])->all();
		 	
		 }else{
		 	
		 	$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
		 	
		 	
		 	$gids=ArrayHelper::getColumn($groups, 'id');
		 	
		 	return Devices::find()->where(['status'=>10,'group_id'=>$gids])->all();
		 	

		 	
		 }
			
		 
		
	}
	
	
	
	public function actionCreate(){
		
		 $model= new Devices();
		 $model->loadDefaultValues();
		 $rdata=Yii::$app->getRequest()->getBodyParams();
		 $model->load($rdata,"");
		 $model->created_at=time();
		
		 if($model->save())
		 {
		 	$config=[];
		 	if(array_key_exists('configuration', $rdata))
		 	{
		 		$config=$this->device_configuration($rdata['configuration']);
		 	}else{
		 		
		 		$config=$this->device_configuration([]);
		 	}
		 	
		 	 if($model->type==1)
		 	 {
		 	 	 $this->updateDeviceConfiguration($model,$config);
		 	 	  $model->config_new=Json::encode($config);
		 	 	  $model->config_temp=Json::encode($config);
		 	 	  $model->config_status=1;
		 	 	  $model->save(false);	
		 	 }
		 	 
		 	 if($model->type==3)
		 	 {
		 	 	
		 	 	$model->config_new=Json::encode($config);
		 	 	$model->config_temp=Json::encode($config);
		 	 	$model->config_status=1;
		 	 	$model->save(false);
		 	 }
		 	 
		 	return $model;
		 }else{
		 
		 	Yii::$app->getResponse()->setStatusCode(422);
		 	return $model->getErrors();
		 }
	}
	
	
	public function actionTemperature(){
			
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			$model = Temperature::find()->andWhere(['not',['customer_id'=>null]])->andWhere(['not',['group_id'=>null]])->all();
			
			return $model;
			
		}else{
			
			$filter=[];
			
			$userObj=Yii::$app->user->identity;
			
			if(is_null($userObj))
			{
				
		      return [];
				
			}
			
			// $groups=DeviceGroups::getChildrenWithParent();
			//$gids=ArrayHelper::getColumn($groups, 'id');
			 
			 $groups=explode("|",$userObj->group_ids);
			 
			 $model = Temperature::find()->andWhere(["status"=>10,'customer_id'=>$userObj->customer_id,'group_id'=>$groups])->all();
			 
			 $count=count($model);
			 
			  for ($i = 0; $i < $count; $i++) {
			  	
			  		$filter[]=ArrayHelper::toArray($model[$i]);
			  		
			  }
			 
			  return $filter;
			 
			 
		}
		
	}
	
	public function actionDoor(){
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			$model = Door::find()->andWhere(["status"=>10])->andWhere(['not',['customer_id'=>null]])->andWhere(['not',['group_id'=>null]])->all();
			
			return $model;
			
		}else{
			
			$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
			
			$gids=ArrayHelper::getColumn($groups, 'id');
			
			$model = Door::find()->andWhere(["status"=>10,'group_id'=>$gids])->all();
			
			return $model;
			
		}
		
		  
	}
	
	
	public function actionRecentPulses($device_id,$type,$limit=10){
		
		$type=strtoupper($type);
		
		if($type==Devices::TEMPERATURE)
		{
			
			$model = DeviceTemperatureReading::find()->andWhere(['device_id'=>$device_id])->orderBy(['id'=>SORT_DESC])->limit($limit)->all();
			
			return $model;
			
		}elseif($type ==Devices::GPS){
			
			$model = DeviceGpsReading::find()->andWhere(['device_id'=>$device_id])->orderBy(['id'=>SORT_DESC])->limit($limit)->all();
			
			return $model;
			
			
		}elseif ($type==Devices::DOOR) {
			
			$model = DeviceDoorReading::find()->andWhere(['device_id'=>$device_id])->orderBy(['id'=>SORT_DESC])->limit($limit)->all();
			
			return $model;
		}
		
		
		return [];
		
	}
	
	public function actionRecentAlerts($id){
		
		return DeviceAlert::find()->where(['device_id'=>$id])->orderBy(['created_at'=>SORT_DESC])->limit(10)->all();	
		
	}
	
	
	public function actionChartData($device_id){
		
		$results=[];
		
		$rsTemperature=[];
		$rsBattery=[];
		$rsHuminity=[];
		
		$readings=DeviceTemperatureReading::find()->select(['battery','temperature','humidity','created_at'])->where(['device_id'=>$device_id])->orderBy(["created_at"=>SORT_ASC])->asArray()->all();
	
		$_count=count($readings);
		
		for ($i = 0; $i < $_count; $i++) {
			
			$readingtime=intval($readings[$i]['created_at'])*1000;
			array_push($rsTemperature, [$readingtime,floatval($readings[$i]['temperature'])]);
			array_push($rsHuminity, [$readingtime,floatval($readings[$i]['humidity'])]);
			array_push($rsBattery, [$readingtime,floatval($readings[$i]['battery'])]);
		 }
		 
		 $results["temperature"]=$rsTemperature;
		 $results["humidity"]=$rsHuminity;
		 $results["battery"]=$rsBattery;
		 
		 return $results;
	
	}
	
	
	public function actionGps(){
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			$model = Gps::find()->andWhere(["status"=>10])->andWhere(['not',['customer_id'=>null]])->andWhere(['not',['group_id'=>null]])->all();
			return $model;
			
		}else{
			
			$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
			$gids=ArrayHelper::getColumn($groups, 'id');
			$model = Gps::find()->andWhere(["status"=>10,'group_id'=>$gids])->all();
			return $model;	
		}
		
	}
	
	public function actionFilterBy($group_id,$type){
			
		
		 $user= Yii::$app->user->identity;
		 
		 $customer_id=$user->customer_id;
		 
		 $user_groups=explode("|",$user->group_ids);
		 
		 
		 if($user->hasRole('Group Admin') || $user->hasRole('super user'))
		 {
		 	
		 }else{
		 	
		 	if(!in_array($group_id, $user_groups))
		 	{
		 		return [];
		 		
		 	}
		 }
		 
		
		 
		
		  $type=strtoupper($type);
		  $groups=[];
		  if($group_id !="")
		  {
		  	$group_id=explode(",", $group_id);
		  	
		  	 $groups=DeviceGroups::find()->where(["id"=>$group_id])->all();
		  	
		  }
		  
		//	$user=Yii::$app->user->identity;
	 	//$groups=DeviceGroups::getAllChildren($group_id);
			
			if(empty($groups))
			{
				$groups=$group_id;
				
			}else{
				
				$groups= ArrayHelper::getColumn($groups, "id");
			}
			
			if($type==Devices::TEMPERATURE)
			{
				
				if(!$user->hasRole('super user'))
				{
					return Temperature::find()->andWhere(['customer_id'=>$customer_id,'group_id'=>$groups])->all();
				}else{
					
					return Temperature::find()->andWhere(['group_id'=>$groups])->all();
				}

				
			}elseif($type ==Devices::GPS){
				
				if(!$user->hasRole('super user'))
				{
					return Gps::find()->andWhere(['customer_id'=>$customer_id,'group_id'=>$groups])->all();
				}else{
					
					return Gps::find()->andWhere(['group_id'=>$groups])->all();
				}
				

				
			}elseif ($type==Devices::DOOR) {
				
				
				if(!$user->hasRole('super user'))
				{
					
				  return Door::find()->andWhere(['customer_id'=>$customer_id,'group_id'=>$groups])->all();
					
				}else{
					
				  return Door::find()->andWhere(['group_id'=>$groups])->all();
				}
				
			}elseif ($type==="ALL") {
				
				
				if(!$user->hasRole('super user'))
				{
					
					return Devices::find()->where(['customer_id'=>$customer_id,'group_id'=>$groups])->all();
					
				}else{
					
					return Devices::find()->where(['group_id'=>$groups])->all();
				}
				
			}
			
			
		
	}
	
	
	
	public function actionGroup($id){
	
		$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getAllChildren($id);
		
		if(empty($groups))
		{
			$groups=$id;
			
		}else{
			
			$groups= ArrayHelper::getColumn($groups, "id");
		}
		
		//return $groups;
		
		$list=Devices::find()->select(["id","name"])->where(["group_id"=>$groups])->asArray()->all();
		return $list;
	}
	
	public function actionConditionCodes(){
		
		 return [
[
   "key"=>"10",
   "value"=>"Grater Than"
],
[
   "key"=>"20",
   "value"=>"Less Than"
],
[
   "key"=>"30",
   "value"=>"Equal To"
],
[
   "key"=>"60",
   "value"=>"Between"
],
[
   "key"=>"70",
   "value"=>"Not Between"
],
];
		  
		
	}
	
	
	public function actionAlertTypes($id,$device_id=""){
		
		
		switch ($id)
			  {
			  	 case 1: 	  	 	
				  	 	 	return [
				[
				   "key"=>"HIGH TEMPERATURE",
				   "value"=>"High Temperature"
				],
				[
				   "key"=>"LOW TEMPERATURE",
				   "value"=>"Low Temperature"
				],
				[
				   "key"=>"HIGH HUMIDITY",
				   "value"=>"High Humidity"
				],
				[
				   "key"=>"LOW HUMIDITY",
				   "value"=>"Low Humidity"
				],
				[
				   "key"=>"LOW BATTERY",
				   "value"=>"Battery"
				]
				];
							  	 	 
			  	 	break;
			  	  case 2: //GPS
			  	  	
						  	  	 return [
	  	  	 [
	  	  	 "key"=>"OVERSPEED",
	  	  	 "value"=>"Over Speed"
	  	  	 ],
	  	  	 [
	  	  	 "key"=>"IDLE",
	  	  	 "value"=>"Idle"
	  	  	 ],
			[
			   "key"=>"HIGH TEMPERATURE",
			   "value"=>"High Temperature"
			],
			[
			   "key"=>"LOW TEMPERATURE",
			   "value"=>"Low Temperature"
			],
			[
			   "key"=>"HIGH HUMIDITY",
			   "value"=>"High Humidity"
			],
			[
			   "key"=>"LOW HUMIDITY",
			   "value"=>"Low Humidity"
			],
			[
			"key"=>"LOW BATTERY",
			"value"=>"Battery"
			]
			];
			  	  	 
			  	  break;
			  	  case 3: //Door
			  	  	
				return [
			[
			   "key"=>"DOOR OPEN",
			   "value"=>"Door Open"
			],
			[
			   "key"=>"DOOR CLOSE",
			   "value"=>"Door Close"
			],
			[
			"key"=>"LOW BATTERY",
			"value"=>"Battery"
			]
			];
			  	  
			  	  	
			  	  	break;
			  	  	default:
			  	  		return [];
			  	 
			  }
		
	}
	
	public function actionView($id="",$type=''){
		
		$model=null;
		$gids=[];
		
		 if(Yii::$app->user->identity->hasRole('super user'))
		 {
		 	
		 	
		 }else{
		 	
		 	$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
		 	$gids=ArrayHelper::getColumn($groups, 'id');
		 	
		 }

		if($type=='temperature')
		{
			
			$model = Temperature::find()->andWhere(["id"=>$id])->one();
		   
		}
		
		if($type=='gps')
		{
				
			$model = Gps::find()->andWhere(["id"=>$id])->one();
			 
		}
		
		if($type=='door')
		{
		
			$model = Door::find()->andWhere(["id"=>$id])->one();
		
		}
		
		
		if(is_null($model) )
		{
		
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		return $model;
		
	}
	
	public function actionUpdate($id){
		
		$gids=[];
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			$model = Devices::find()->where(["id"=>$id])->one();
			
		}else{
			
			$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
			$gids=ArrayHelper::getColumn($groups, 'id');
			
			$model = Devices::find()->where(["id"=>$id,'group_id'=>$gids])->one();
			
		}
		
		    if(is_null($model))
		    {
		    
		    	throw new NotFoundHttpException("Object not found: $id");
		    }
		    
		    $rdata=Yii::$app->getRequest()->getBodyParams();
		    
		    if(empty($rdata))
		    {
		    	
		    	throw new NotFoundHttpException("resource not found");
		    
		    }
		    
		    //unset($rdata["id"]);
		    
		    $model->load($rdata,"device");
		    $model->customloadMultiple($rdata, "range");
		    $model->updated_at=time();
		    
		    if($model->save() && $model->loadMultipleValidation())
		    {
		    	
		    	 
		    	 return $model;
		    	
		    	
		    }else{
		    	
		    	$error=[];
		    	if(isset($rdata["device"]))
		    	{
		    		$error["device"]=$model->getErrors();
		    		
		    	}
		    	
		    	if(isset($rdata["range"]))
		    	{
		    		$error["range"]=$model->getMultipleValidation();
		    		
		    	}
		    	
		    	Yii::$app->getResponse()->setStatusCode(422);
		    	return $error;
		    	
		    }
		
	}
	
	public function actionConfig($id){
		
		 $output=[];
		
		$model = Devices::find()->where(["id"=>$id])->one();
		$rdata=Yii::$app->getRequest()->getBodyParams();
		if(is_null($model))
		{
		
			throw new NotFoundHttpException("Object not found: $id");
		}
		if(empty($rdata))
		{
			 
		
			throw new NotFoundHttpException("resource not found");
		
		}
		
		
		if(array_key_exists("temperature", $rdata))
		{
			
			$range = new DeviceConfigTemperature();
			$range->device_id=$id;
			$range->customloadmultiple($rdata["temperature"]);
					
			if($range->customValidationMultiple())
			{
				
				 DeviceConfigTemperature::deleteAll(["device_id"=>$id]);
				 $range->saveMultiple();
				 $output["temperature"]=$range->deviceRange;	
				 
				
			}else{
				Yii::$app->getResponse()->setStatusCode(422);
				 $output["temperature"]=$range->getMultipleValidation();
				 return $output;
			}
				
		}
		
		if(array_key_exists("battery", $rdata))
		{
			
			 $config = new DeviceConfigBattery();
			 $config->device_id=$id;
			 $config->customloadmultiple($rdata["battery"]);
			 
			 if($config->customValidationMultiple())
			 {
			 	$config->saveMultiple();
			 	
			 	$output["battery"]=$config->deviceRange;
			 	
			 }else{
			 	Yii::$app->getResponse()->setStatusCode(422);
			 	$output["battery"]=$config->getMultipleValidation();
			 	
			 	return $output;		
			 }	
		}
		
		if(array_key_exists("humidity", $rdata))
		{
			  $config = new DeviceConfigHumidity();
			  $config->device_id=$id;
			  $config->customloadmultiple($rdata["humidity"]);
			  
			  if($config->customValidationMultiple())
			  {
			  	$config->saveMultiple();
			  		
			  	$output["humidity"]=$config->deviceRange;
			  	
			  	
			  		
			  }else{
			  	Yii::$app->getResponse()->setStatusCode(422);
			  	$output["humidity"]=$config->getMultipleValidation();
			  	return $output;
			  	
			  
			  }
			  	
		}
		
		return $output;
		   
		
	}
	
	
	private function device_configuration($config=[]){
		
		         if(is_null($config))
		         {
		         	 $config=[];
		         }
			 $default=[];
			 $default['mode']=1;
			 $default['reporting_interval']=2;
			 $default['sampling_inteval']=30;
			 $default["temperature_range"]=[
			 		"low"=>10,
			 		"high"=>50
			 ];
			 $default["humidity_range"]=[
			 		"low"=>60,
			 		"high"=>90
			 ];
			 
			 $level1=array_merge($default,$config);
			 return $level1;
				
	}
	
	private function updateDeviceConfiguration($model,$config){
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>$model->start_value,
				'end' =>$config['temperature_range']['low'],
				'level' => 'LOW TEMPERATURE',
				'condition_code' =>60,
				'color_code' => 'Blue',
		];
		
		
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>$config['temperature_range']['low'],
				'end' =>$config['temperature_range']['high'],
				'level' => 'NORMAL TEMPERATURE',
				'condition_code' =>60,
				'color_code' => 'Green',
		];
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>$config['temperature_range']['high'],
				'end' =>$model->end_value,
				'level' => 'HIGH TEMPERATURE',
				'condition_code' =>60,
				'color_code' => 'Red',
		];
		
		DeviceConfigTemperature::deleteAll(["device_id"=>$model->id]);
		$range = new DeviceConfigTemperature();
		$range->device_id=$model->id;
		$range->customloadmultiple($temp);
		$range->saveMultiple();
		
		$temp1=[];
		
		$temp1[]=[
				'device_id' =>$model->id,
				'start' =>0,
				'end' =>$config['humidity_range']['low'],
				'level' => 'LOW HUMIDITY',
				'condition_code' =>60,
				'color_code' => 'Blue',
		];
		
		
		
		$temp1[]=[
				'device_id' =>$model->id,
				'start' =>$config['humidity_range']['low'],
				'end' =>$config['humidity_range']['high'],
				'level' => 'NORMAL HUMIDITY',
				'condition_code' =>60,
				'color_code' => 'Green',
		];
		
		$temp1[]=[
				'device_id'=>$model->id,
				'start' =>$config['humidity_range']['high'],
				'end' =>100,
				'level' => 'HIGH HUMIDITY',
				'condition_code' =>60,
				'color_code' => 'Red',
		];
		
		DeviceConfigHumidity::deleteAll(["device_id"=>$model->id]);	
		$config = new DeviceConfigHumidity();
		$config->device_id=$model->id;
		$config->customloadmultiple($temp1);
		$config->saveMultiple();
	
		
	}
	
	
	public function actionTypes(){
		
		
		 return [
		 		[
		 			"id"=>1,
		 			"name"=>"Temperature"
		 		],
		 		[
		 				"id"=>3,
		 				"name"=>"Door"
		 		],
		 		[
		 				"id"=>2,
		 				"name"=>"Gps"
		 		],
		 ];
		
	}
	
	
	
}