<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
use backend\modules\api\models\DeviceGroups;
use common\models\CustomerSearch;
use common\models\Customer;
use backend\models\AdminUser;
use function yii\db\all;
use yii\filters\AccessControl;
use common\models\PasswordChange;

class UserController extends Controller{
	
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		
		return $behaviors;
	}
	
	public function actionIndex(){
		
		
		if(!$user->hasRole('super user'))
		{
			Yii::$app->getResponse()->setStatusCode(200);
			return [];
		}
		
		 return AdminUser::find()->all();
	}
	
	
	public function actionCreate(){
		
		
		if(!Yii::$app->user->identity->hasRole('super user') && !Yii::$app->user->identity->hasRole('Group Admin'))
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return [];
		}
		
		$model =new AdminUser();
		$model->setScenario('create');
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$model->loadDefaultValues();
		$model->load($rdata,"");
		$model->generateAuthKey();
		if($model->password_hash !="" )
		{
			$model->setPassword($model->password_hash);
		}
		
		$model->created_at=time();
		$model->updated_at=time();
		
		if($model->save())
		{
			
		
			$auth=Yii::$app->authManager;
			$role=$auth->getRole($rdata['role']);
			if(!is_null($role))
			{
				$auth->assign($role,$model->id);
			}

			return $model;
			 
		}else{
				
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
	
	}
	
	
	public function actionUpdate($id){
		
		if(!Yii::$app->user->identity->hasRole('super user') && !Yii::$app->user->identity->hasRole('Group Admin'))
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return [];
		}
		
		$model= AdminUser::find()->where(["id"=>$id])->one();
		
		if(is_null($model))
		{
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$model->setScenario("update");
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(empty($rdata))
		{
			throw new NotFoundHttpException("resource not found");
		}
		
		unset($rdata["id"]);
		$model->load($rdata,"");
		$model->updated_at=time();
		if(isset($rdata["password_hash"]) && $rdata["password_hash"] != "")
		{
			$model->setPassword($rdata["password_hash"]);
				
		}
			
		if($model->save())
		{
			
			
			
			$auth=Yii::$app->authManager;
			
			$auth->revokeAll($model->id);
			
			$role=$auth->getRole($rdata['role']);
			if(!is_null($role))
			{
				$auth->assign($role,$model->id);
			}
				
				
			return $model;
		
		}else{
		
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
	}
	
	public function actionDelete($id){
		
		if(!Yii::$app->user->identity->hasRole('super user') && !Yii::$app->user->identity->hasRole('Group Admin'))
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return [];
		}
		
		 if(Yii::$app->user->id==$id)
		 {
		 	throw new NotFoundHttpException("Object not found: $id");
		 	
		 }
		 
		
		$model=AdminUser::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
		
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$auth=Yii::$app->authManager;
			
		$auth->revokeAll($model->id);
		
		$model->delete();
			
		return $model;
	}
	
	
	public function actionChangePassword(){
		
			  $user_id=Yii::$app->user->id;
			  $rdata=Yii::$app->getRequest()->getBodyParams();
			  
			  $model = new PasswordChange();
			  $model->load($rdata,'');
			  $model->user_id=$user_id;
			  if($model->updatePassword())
			  {
			  	return ["ok"];
			  	
			  }else{
			  	Yii::$app->getResponse()->setStatusCode(422);
			  	 return $model->getErrors();
			  }
			 
	}
	
	
	
	public function actionRoles(){
		
		$user=Yii::$app->user->identity;
		
		if(!$user->hasRole('super user'))
		{
			return [["key"=>"Group Manager","value"=>"Manager"],["key"=>"Group User","value"=>"User"]];
		}
		return AdminUser::roles();
	}
	
}