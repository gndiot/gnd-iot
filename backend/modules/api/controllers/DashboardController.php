<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
use backend\modules\api\models\DeviceGroups;
use common\models\CustomerSearch;
use common\models\Customer;
use yii\helpers\ArrayHelper;
use common\models\Devices;
use common\models\DeviceAlert;
use yii\filters\AccessControl;
use yii\helpers\Json;
use common\models\DeviceTemperatureReading;
use common\models\Constants;

class DashboardController extends Controller{
	
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		
		
		return $behaviors;
	}
	
	
	public function actionIndex($group=1){
		
		
	
		
	}
	
	public function actionRecentNotification(){
		
		
		$user= Yii::$app->user->identity;
		
		$customer_id=$user->customer_id;
		
		$user_groups=explode("|",$user->group_ids);
		
		
		
		$alerts=DeviceAlert::find()->select(["devices.id","devices.display_name as name","device_alerts.alert_type","device_alerts.device_id","device_alerts.id as alert_id","device_alerts.payload","device_alerts.created_at"])->where(["device_alerts.group_id"=>$user_groups])->joinWith(['device'])->orderBy(["device_alerts.id"=>SORT_DESC])->limit(10)->asArray()->all();
		
		$count=count($alerts);
		
		  for ($i = 0; $i < $count; $i++) {
		  	
		  	   unset($alerts[$i]['device']);
		  	   
		  	   $date= new \DateTime();
		  	   $date->setTimestamp($alerts[$i]["created_at"]);
		  	   
		  	   $alerts[$i]["created_at"]=$date->format("Y-M-d H:i:s");
		  	   
		  	   $payload=Json::decode($alerts[$i]["payload"]);
		  	   $data=$this->alertMessage($alerts[$i]["alert_type"],$payload["value"],$alerts[$i]["name"]);
		  	   $temp=$alerts[$i];	  	   
		  	   $merge=array_merge($temp,$data);
		  	  $alerts[$i]=$merge;
		  	   unset($alerts[$i]['payload']);
		  	   
		  	
		  }
		
		
		return $alerts;
				
	}
	
	
	public function actionChartData(){
		
		$user= Yii::$app->user->identity;
		
		$customer_id=$user->customer_id;
		
		$user_groups=explode("|",$user->group_ids);
		
		
	
		
		$temperature= Temperature::find()->andWhere(['group_id'=>$user_groups,'status'=>10])->asArray()->all();
		$count=count($temperature);
		$temperature_high=[];
		$temperature_low=[];
		$temperature_normal=[];
		$humidity_high=[];
		$humidity_low=[];
		$humidity_normal=[];
		$no_communication=[];
		$lowbattery=[];
		$normalbattery=[];
		
		
		for ($i = 0; $i < $count; $i++) {
			
			$hasNoComm=false;
			
			//get Configurations
			$config=Json::decode($temperature[$i]["config_new"]);
			
			$readings=Json::decode($temperature[$i]["data"]);	
			//$readings=DeviceTemperatureReading::find()->where(["hardware_serial"=>$temperature[$i]["hardware_serial"]])->orderBy(["created_at"=>SORT_DESC])->asArray()->one();
			
			if(empty($readings))
			{
				continue;
			}
			
			//$dep=Temperature::find()->andWhere(['hardware_serial'=>$temperature[$i]["hardware_serial"]])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->one();
			$hasNoComm=$this->hasComminication($temperature[$i]["sync_time"],$config["reporting_interval"]);
			
			if($readings["battery"] < $config["battery_range"]["low"] )
			{
				
				$lowbattery[]=1;
				
			}else{
				
				$normalbattery[]=1;
			}
			
			if($hasNoComm)
			{
				$no_communication[]=1;
				$hasNoComm=true;
				
				continue;
			}

			
			if($config["temperature_range"]["high"] < $readings["temperature"])
			{
				
				if(!$hasNoComm)
				{
					$temperature_high[]=1;
				}
				
			}elseif ($config["temperature_range"]["low"] > $readings["temperature"])
			{
				if(!$hasNoComm)
				{
					$temperature_low[]=1;
				}
				
				
			}else{
				
				if(!$hasNoComm)
				{
					$temperature_normal[]=1;
				}
				
				
			}
			
			if($config["humidity_range"]["high"] < $readings["humidity"])
			{
				
				
				if(!$hasNoComm)
				{
					$humidity_high[]=1;
				}
				
				
			}elseif ($config["humidity_range"]["low"] > $readings["humidity"])
			{
				
				
				
				if(!$hasNoComm)
				{
					$humidity_low[]=1;
				}
				
				
			}else{
				
				if(!$hasNoComm)
				{
					$humidity_normal[]=1;
				}
				
				
			}
			
			
			
		}
		
		
		
		$patten_temperature=[
				[
						"name"=>'Normal Temperature',
						"y"=>count($temperature_normal),
						"color"=>"#43a047"
				],
				[
						"name"=>'High Temperature',
						"y"=>count($temperature_high),
						"color"=>"#e53935"
				],
				[
						"name"=>'Low Temperature',
						"y"=>count($temperature_low),
						"color"=>"#2196f3"
				],
				[
						"name"=>'No Comm',
						"y"=>count($no_communication),
						"color"=>"#bdbdbd"
				]
		];
		
		$patten_humidity=[
				[
						"name"=>'Normal Humidity',
						"y"=>count($humidity_normal),
						"color"=>"#43a047"
				],
				[
						"name"=>'High Humidity',
						"y"=>count($humidity_high),
						"color"=>"#e53935"
				],
				[
						"name"=>'Low Humidity',
						"y"=>count($humidity_low),
						"color"=>"#2196f3"
				],
				[
						"name"=>'No Comm',
						"y"=>count($no_communication),
						"color"=>"#bdbdbd"
				]
		];
		
		$patten_battery=[
				[
						"name"=>'Low Battery',
						"y"=>count($lowbattery),
						"color"=>"#e53935"
				],
				[
						"name"=>'Normal Battery',
						"y"=>count($normalbattery),
						"color"=>"#43a047"
				]
		];
		
		
		return [
				"temperature"=>$patten_temperature,
				"humidity"=>$patten_humidity,
				"battery"=>$patten_battery
		];
		
	}
	
	public function actionDevices(){
		
		$user= Yii::$app->user->identity;
		
		$customer_id=$user->customer_id;
		
		$groups=explode("|",$user->group_ids);
		
		
		
		$temperature= Temperature::find()->andWhere(['group_id'=>$groups,'status'=>10])->count();
		$door = Door::find()->andWhere(['group_id'=>$groups,'status'=>10])->count();
		$gps= Gps::find()->andWhere(['group_id'=>$groups,'status'=>10])->count();
		$customers=DeviceGroups::find()->where(["parent_id"=>0])->count();
		$no_comm_temperature=$this->getNoCommDevices(1,$groups);//Temperature::find()->andWhere(['group_id'=>$groups,'status'=>10])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
		$no_comm_door=$this->getNoCommDevices(3,$groups);//Door::find()->andWhere(['group_id'=>$groups,'status'=>10])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
		$no_comm_gps=$this->getNoCommDevices(2,$groups);//Gps::find()->andWhere(['group_id'=>$groups,'status'=>10])->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
	
		$alerts_ack=DeviceAlert::find()->where(["group_id"=>$groups,"status"=>DeviceAlert::STATUS_ACK])->count();
		$alerts_not_ack=DeviceAlert::find()->where(["group_id"=>$groups,"status"=>DeviceAlert::STATUS_NOT_ACK])->count();
		
		return [
				"temperature"=>$temperature,
				"door"=>$door,
				"gps"=>$gps,
				"customers"=>1,
				"humidity"=>$temperature,
				"no_com_temperature"=>$no_comm_temperature,
				"no_com_gps"=>$gps,
				"no_com_door"=>$door,
				"alert_ack"=>$alerts_ack,
				"alert_not_ack"=>$alerts_not_ack
		];
		
	}
	
	public function actionDeviceCommunication(){
		
				
	}
	
	public function actionAlertsCount(){
		
		//Yii::$app->db->createCommand("SELECT *,TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) as time_diff FROM `devices` HAVING TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->q;			
			  
		return Devices::find()->having("TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(`sync_time`),NOW()) > `response_intervel`")->count();
		   
	}
	
	
	private function hasNormal($value,$max,$min){
		
		
		if($value >= $max && $value <= $min)
		{
			return 1;
			
		}else{
			
			return 0;
		}
		
		
		
	}
	
	private function alertMessage($alert_type,$value,$device){
		
		  $content="";
		  $title="";
		  
		  switch ($alert_type) {
		  	case Constants::ALERT_HIGH_HUMIDITY:
		  	
		  		 $title=strtolower($alert_type)." from ".$device;
		  		// $content="current sensor value is ".$value."%";
		  		 $content=iconv("CP1257", "UTF-8","current sensor value is ".$value."%");
		  		
		  	break;
		  	case Constants::ALERT_LOW_HUMIDITY:
		  		
		  		$title=strtolower($alert_type)." from ".$device;
		  		$content=iconv("CP1257", "UTF-8","current sensor value is ".$value."%");
		  		
		  		break;
		  	case Constants::ALERT_HIGH_TEMPERATURE:
		  		
		  		$title=strtolower($alert_type)." from ".$device;
		  		$content= iconv("CP1257", "UTF-8", "current sensor value is ".$value."�C");
		  		
		  		break;
		  	case Constants::ALERT_LOW_TEMPERATURE:
		  		
		  		$title=strtolower($alert_type)." from ".$device;
		  		$content= iconv("CP1257", "UTF-8", "current sensor value is ".$value."�C");
		  		
		  		break;
		  	case Constants::ALERT_DOOR_CLOSE:
		  		
		  		$title=strtolower($alert_type)." from ".$device;
		  		$content="current door status is Close";
		  		
		  		break;
		  	case Constants::ALERT_DOOR_OPEN:
		  		
		  		$title=strtolower($alert_type)." from ".$device;
		  		$content="current door status is Open";
		  		
		  		break;
		  		
		  	case Constants::ALERT_LOW_BATTERY:
		  		
		  		$title=strtolower($alert_type)." from ".$device;
		  		
		  		
		  		$content=iconv("CP1257", "UTF-8","Low Battery ".$value."%");
		  		
		  		break;
		  	
		  	default:
		  		;
		  	break;
		  }
		  
		  
		 return [
		 		"title"=>$title,
		 		"body"=>$content
		 ];
		
		
		
	}
	
	public function actionTest(){
		
		//$result=$this->getNoCommDevices();
	
		return $this->getNoCommDevices(2);
	}
	
	public function getNoCommDevices($type,$groups){

		 $devList=Devices::find()->where(["type"=>$type,"status"=>10,"group_id"=>$groups])->asArray()->all();
		  $count=count($devList);
		  
		  $result=[];
		  
		 for ($i = 0; $i < $count; $i++) {
		 	
		 	  $config=Json::decode($devList[$i]["config_new"]);
		 	  
		 	  if(empty($config))
		 	  {
		 	  	 continue;
		 	  }
		 	  
		 	  $device=$devList[$i];
		 	  
		 	  $date1=new \DateTime();
		 	  $date2=new \DateTime();
		 	  $date2->setTimestamp($device['sync_time']);
		 	  
		 	  if($device['sync_time']=="")
		 	  {
		 	  	
		 	  	array_push($result,intval($device["type"]));
		 	  	
		 	  	continue;
		 	  }
		 	  
		 	  $diffInSeconds=$date2->getTimestamp()-$date1->getTimestamp();
		 	  
		 	  
		 	  $reporting_interval=intval($config["reporting_interval"])+Yii::$app->params["no_communication"];
		 	  
		 	  $convertMints=($reporting_interval*60);
		 	  
		 	  if(abs($diffInSeconds) > $convertMints)
		 	  {
		 	  	
		 	  	array_push($result,intval($device["type"]));
		 	  	continue;
		 	  	
		 	  }else{
		 	  	
		 	 
		 	  	
		 	  }
		 	  
		 	
		 }
		  
		  
		 return count($result);
		
		
	}
	
	
	public function hasComminication($sync_time='',$reporting_interval){
		
		
		$date1=new \DateTime();
		$date2=new \DateTime();
		$date2->setTimestamp($sync_time);
		
		if($sync_time=="")
		{
			return true;
		}
		
		$diffInSeconds=$date2->getTimestamp()-$date1->getTimestamp();
		
		
		$reporting_interval=intval($reporting_interval)+Yii::$app->params["no_communication"];
		
		$convertMints=($reporting_interval*60);
		
		if(abs($diffInSeconds) > $convertMints)
		{
			
			return true;
			
		}else{
			
			return false;
			
		}
		
	}
	
	
}