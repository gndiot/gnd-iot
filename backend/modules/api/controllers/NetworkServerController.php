<?php

namespace backend\modules\api\controllers;

use yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use backend\modules\api\models\NetworkProvider;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `api` module
 */
class NetworkServerController extends Controller
{
	public $enableCsrfValidation =false;
    /**
     * Renders the index view for the module
     * @return string
     */
	
	public function behaviors(){
		
			$behaviors=parent::behaviors();
			 $behaviors["access"]=[
			 		'class' => AccessControl::className(),
			 		'rules' => [
			 				[
			 						'allow' => true,
			 						'roles' => ['@'],
			 				],
			 		],
			 		'denyCallback' => function ($rule, $action) {
			 			
			 			throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
			 		}
			 ];
			
		   return $behaviors;
	}
	
	protected function verbs(){
		
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
		
	}
	
	
    public function actionIndex()
    {
    	
    	
    	return new ActiveDataProvider([
    	 		'query'=>NetworkProvider::find()
    	 ]);
    	
    }
    
    public function actionCreate(){
    	
    		$model = new NetworkProvider();
    		$rdata=Yii::$app->getRequest()->getBodyParams();
    		$model->load($rdata,"");
    	    $model->created_at=time();
    	    $model->updated_at=time();
    		if($model->save())
    		{
    			return $model;
    			
    		}else{
    			 
    			 Yii::$app->getResponse()->setStatusCode(422);
    			 return $model->getErrors();
    		}
    	
    }
    
    
    public function actionUpdate($id)
    {
    	
    		$model=NetworkProvider::find()->where(["id"=>$id])->one();
    		if(is_null($model))
    		{
    			
    			throw new NotFoundHttpException("Object not found: $id");
    		}
    		
    		$rdata=Yii::$app->getRequest()->getBodyParams();
    		unset($rdata["id"]);
    		$model->load($rdata,"");
    		$model->updated_at=time();
    		
    		if($model->save())
    		{
    			return $model;
    			 
    		}else{
    		
    			Yii::$app->getResponse()->setStatusCode(422);
    			return $model->getErrors();
    		}
    		
    }
    
    
    public function actionDelete($id){
    	
    	$model=NetworkProvider::find()->where(["id"=>$id])->one();
    	if(is_null($model))
    	{
    		 
    		throw new NotFoundHttpException("Object not found: $id");
    	}
    	
    	$model->delete();
    	
    	return $model;
    	
    	
    }
    
    
    
}
