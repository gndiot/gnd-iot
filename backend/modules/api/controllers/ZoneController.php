<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use common\models\Devices;
use yii\web\NotFoundHttpException;
use common\models\TimeZone;
use yii\filters\AccessControl;
//use backend\modules\api\models\Devices;


class ZoneController extends Controller{
	
	 
	public function behaviors(){
	
		$behaviors=parent::behaviors();
		$behaviors["access"]=[ 
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				 
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
	public function actionIndex()
	{
		 
		return new ActiveDataProvider([
				'query'=>TimeZone::find()
		]);
		 
	}
	
	public function actionTime(){
		
		
		return TimeZone::find()->all();
		
	}
	
	
	public function actionUser(){
		
		 $user=Yii::$app->user->identity;
		 
		 if(!is_null($user))
		 {
		 	
		 	 return $user->timezone;
		 	
		 }else{
		 	
		 	 return "No Time zone";
		 	 
		 }	
	}
	
	
}