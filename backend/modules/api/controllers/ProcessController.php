<?php

namespace backend\modules\api\controllers;

use yii\rest\Controller;
use yii;
use yii\filters\AccessControl;
use backend\models\AdminUser;
use common\models\MasterTag;
use yii\web\NotFoundHttpException;
use common\models\MasterProcess;
/**
 * Default controller for the `api` module
 */
class ProcessController extends Controller
{
	public $enableCsrfValidation =false;
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		return $behaviors;
	}
    /**
     * Renders the index view for the module
     * @return string
     */
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
    public function actionIndex()
    {
    	 
    	return MasterProcess::find()->all();
       
    }
    
    public function actionCreate(){
    	
    	 $rdata= Yii::$app->getRequest()->getBodyParams();
    	 $model = new MasterProcess();
    	 $model->load($rdata,"");
    	 
    	 if($model->save())
    	 {
    	 		
    	 	 return $model;
    	 	
    	 }else{
    	 	Yii::$app->getResponse()->setStatusCode(422);
    	 	 return $model->getErrors();
    	 }
    	 
    }
    
    public function actionUpdate($id){
    	
    	$model = MasterProcess::find()->where(["id"=>$id])->one();
    		 
    		 if(is_null($model))
    		 {
    		 	
    		 	throw new NotFoundHttpException();
    		 	
    		 }
    		 
    		 $rdata= Yii::$app->getRequest()->getBodyParams();
    		 
    		 $model->load($rdata,"");
    		 
    		 if($model->save())
    		 {
    		 	
    		 	return $model;
    		 	
    		 }else{
    		 	Yii::$app->getResponse()->setStatusCode(422);
    		 	 return $model->getErrors();
    		 }
    		 
    }
    
    public function actionDelete($id){
    	
    	$model = MasterProcess::find()->where(["id"=>$id])->one();
    	
    	if(is_null($model))
    	{
    		
    		throw new NotFoundHttpException();
    		
    	}
    	
           
    	if($model->delete())
    	{
    		
    		return $model;
    		
    	}else{
    		
    		return $model->getErrors();
    	}
    	
    }
    
}
