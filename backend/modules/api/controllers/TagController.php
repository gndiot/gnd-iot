<?php

namespace backend\modules\api\controllers;

use yii\rest\Controller;
use yii;
use yii\filters\AccessControl;
use backend\models\AdminUser;
use common\models\MasterTag;
use yii\web\NotFoundHttpException;
/**
 * Default controller for the `api` module
 */
class TagController extends Controller
{
	public $enableCsrfValidation =false;
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		return $behaviors;
	}
    /**
     * Renders the index view for the module
     * @return string
     */
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
    public function actionIndex()
    {
    	 
    	return MasterTag::find()->all();
       
    }
    
    public function actionCreate(){
    	
    	 $rdata= Yii::$app->getRequest()->getBodyParams();
    	 $model = new MasterTag();
    	 $model->load($rdata,"");
    	 
    	 if($model->save())
    	 {
    	 		
    	 	 return $model;
    	 	
    	 }else{
    	 	Yii::$app->getResponse()->setStatusCode(422);
    	 	 return $model->getErrors();
    	 }
    	 
    }
    
    public function actionUpdate($id){
    	
    		 $model = MasterTag::find()->where(["id"=>$id])->one();
    		 
    		 if(is_null($model))
    		 {
    		 	
    		 	throw new NotFoundHttpException();
    		 	
    		 }
    		 
    		 $rdata= Yii::$app->getRequest()->getBodyParams();
    		 
    		 $model->load($rdata,"");
    		 
    		 if($model->save())
    		 {
    		 	
    		 	return $model;
    		 	
    		 }else{
    		 	Yii::$app->getResponse()->setStatusCode(422);
    		 	 return $model->getErrors();
    		 }
    		 
    }
    
    public function actionDelete($id){
    	
    	$model = MasterTag::find()->where(["id"=>$id])->one();
    	
    	if(is_null($model))
    	{
    		
    		throw new NotFoundHttpException();
    		
    	}
    	
           
    	if($model->delete())
    	{
    		
    		return $model;
    		
    	}else{
    		Yii::$app->getResponse()->setStatusCode(422);
    		return $model->getErrors();
    	}
    	
    }
    
}
