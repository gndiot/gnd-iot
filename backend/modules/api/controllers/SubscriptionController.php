<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use common\models\DeviceSubscription;
use common\models\Constants;
use common\models\DeviceConfigTemperature;
use common\models\DeviceConfigHumidity;
use yii\web\NotFoundHttpException;
use common\models\Devices;
use yii\helpers\ArrayHelper;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\Json;

class SubscriptionController extends Controller{
	
	
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
					
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
			
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'add' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
	public function actionIndex(){
		
		
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			$list = DeviceSubscription::find()->all();
			
			return $list;
			
		}else{
			
			$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
			
			$gids=ArrayHelper::getColumn($groups, 'id');
			
			$model = DeviceSubscription::find()->andWhere(['group_id'=>$gids])->all();
			
			return $model;
			
		}
		
	}
	
	public function actionAdd(){
			 
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$subscription = new DeviceSubscription();
		$subscription->loadDefaultValues();
		$subscription->load($rdata,'');
		$subscription->created_at=time();
		$subscription->updated_at=time();
		
		 if($subscription->is_custom==0)
		 {
		 	   
		 	$device=Devices::find()->where(["id"=>$subscription->dev_id])->one();
		 	$config=Json::decode($device->config_new);
		 	
		 	 if($subscription->attribute==Constants::ALERT_HIGH_TEMPERATURE || $subscription->attribute==Constants::ALERT_LOW_TEMPERATURE )
		 	 {
		 	 	
		 	 	if($subscription->attribute==Constants::ALERT_HIGH_TEMPERATURE)
		 	 	{
		 	 		$subscription->condition_code=60;
		 	 		$subscription->value_one=$config["temperature_range"]["high"];
		 	 		$subscription->value_two=$config['end_value'];
		 	 		
		 	 	}elseif ($subscription->attribute==Constants::ALERT_LOW_TEMPERATURE)
		 	 	{
		 	 		
		 	 		$subscription->condition_code=60;
		 	 		$subscription->value_one=$config['start_value'];
		 	 		$subscription->value_two=$config["temperature_range"]["low"];
		 	 		
		 	 	}
		 	 	
		 	 }elseif ($subscription->attribute==Constants::ALERT_DOOR_OPEN || $subscription->attribute==Constants::ALERT_DOOR_CLOSE)
		 	 {
		 	 	
		 	 	$subscription->condition_code=Constants::CODE_EQUAL_TO;
		 	 	$subscription->value_one = Constants::ALERT_DOOR_OPEN==$subscription->attribute ? 16:1;
		 	 	
		 	 }elseif($subscription->attribute==Constants::ALERT_HIGH_HUMIDITY || $subscription->attribute==Constants::ALERT_LOW_HUMIDITY){
		 	 	 
		 	 	   if($subscription->attribute==Constants::ALERT_HIGH_HUMIDITY)
		 	 	   {
		 	 	   	$subscription->condition_code=60;
		 	 	   	$subscription->value_one=$config["humidity_range"]["high"];
		 	 	   	$subscription->value_two=100;
		 	 	   	
		 	 	   }elseif ($subscription->attribute==Constants::ALERT_LOW_HUMIDITY)
		 	 	   {
		 	 	   	
		 	 	   	$subscription->condition_code=60;
		 	 	   	$subscription->value_one=0;
		 	 	   	$subscription->value_two=$config["humidity_range"]["low"];
		 	 	   	
		 	 	   }
		 	 	   
		 	 	   
		 	 }elseif ($subscription->attribute==Constants::ALERT_LOW_BATTERY)
		 	 {
		 	 	
		 	 	 $subscription->condition_code=Constants::CODE_LESS_THAN_EQUAL;
		 	 	 $subscription->value_one=$config['battery_range']['low'];
		 	 	
		 	 }

		 }
		 
		 //get Device Group
		 
		  $deivce=Devices::find()->where(['id'=>$subscription->dev_id])->one();
		  
		  if(!is_null($deivce))
		  {
		  	
		  	$subscription->customer_id=$deivce->customer_id;
		  	$subscription->group_id=$deivce->group_id;
		  	
		  }
		 
		  if($subscription->save())
		  {
		  	
		  	return $subscription;
		  	
		  }else{
		
		  		Yii::$app->getResponse()->setStatusCode(422);
		  		
		  	 	return $subscription->getErrors();
		  	 
		  }
		

		 
	}
	
	public function actionUpdate($id){
		
		 $id=Yii::$app->utils->decrypt($id);
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			$subscription=DeviceSubscription::find()->where(["id"=>$id])->one();
			
		}else{
			$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
			$gids=ArrayHelper::getColumn($groups, 'id');
			$subscription=DeviceSubscription::find()->where(["id"=>$id,'group_id'=>$gids])->one();
		}
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		if(is_null($subscription))
		{
				
			throw new NotFoundHttpException("Object not found: $id");
		}
		$subscription->load($rdata,'');
		$subscription->updated_at=time();
		
		if($subscription->is_custom==0)
		{
			
			$device=Devices::find()->where(["id"=>$subscription->dev_id])->one();
			$config=Json::decode($device->config_new);
			
			if($subscription->attribute==Constants::ALERT_HIGH_TEMPERATURE || $subscription->attribute==Constants::ALERT_LOW_TEMPERATURE )
			{
				
				if($subscription->attribute==Constants::ALERT_HIGH_TEMPERATURE)
				{
					$subscription->condition_code=60;
					$subscription->value_one=$config["temperature_range"]["high"];
					$subscription->value_two=$config['end_value'];
					
				}elseif ($subscription->attribute==Constants::ALERT_LOW_TEMPERATURE)
				{
					
					$subscription->condition_code=60;
					$subscription->value_one=$config['start_value'];
					$subscription->value_two=$config["temperature_range"]["low"];
					
				}
				
			}elseif ($subscription->attribute==Constants::ALERT_DOOR_OPEN || $subscription->attribute==Constants::ALERT_DOOR_CLOSE)
			{
				
				$subscription->condition_code=Constants::CODE_EQUAL_TO;
				$subscription->value_one = Constants::ALERT_DOOR_OPEN==$subscription->attribute ? 16:1;
				
			}elseif($subscription->attribute==Constants::ALERT_HIGH_HUMIDITY || $subscription->attribute==Constants::ALERT_LOW_HUMIDITY){
				
				if($subscription->attribute==Constants::ALERT_HIGH_HUMIDITY)
				{
					$subscription->condition_code=60;
					$subscription->value_one=$config["humidity_range"]["high"];
					$subscription->value_two=$config['end_value'];
					
				}elseif ($subscription->attribute==Constants::ALERT_LOW_HUMIDITY)
				{
					
					$subscription->condition_code=60;
					$subscription->value_one=$config['start_value'];
					$subscription->value_two=$config["humidity_range"]["low"];
					
				}
				
				
			}elseif ($subscription->attribute==Constants::ALERT_LOW_BATTERY)
			{
				
				$subscription->condition_code=Constants::CODE_LESS_THAN_EQUAL;
				$subscription->value_one=$config['battery_range']['low'];
				
			}
			
		}
		
		if($subscription->save())
		{
			 
			return $subscription;
			 
		}else{
		
			Yii::$app->getResponse()->setStatusCode(422);
			
			return $subscription->getErrors();
		}
	}
	
	public function actionView($id){
		
		$id=Yii::$app->utils->decrypt($id);
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			$model=DeviceSubscription::find()->where(["id"=>$id])->one();
			
		}else{
			
			$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
			$gids=ArrayHelper::getColumn($groups, 'id');
			
			$model=DeviceSubscription::find()->where(["id"=>$id,'group_id'=>$gids])->one();
			
			
		}
		if(is_null($model))
		{
			  
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		return $model;
		 
		
	}
	
	public function actionDelete($id){
		$id=Yii::$app->utils->decrypt($id);
		
		$model=DeviceSubscription::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
				
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		 return $model->delete();
		
	}
	
	
	
}