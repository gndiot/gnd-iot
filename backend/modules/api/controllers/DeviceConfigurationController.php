<?php
namespace backend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use common\models\Devices;
use yii\web\NotFoundHttpException;
use common\models\DeviceConfigRange;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
use common\models\DeviceConfigTemperature;
use common\models\DeviceConfigBattery;
use common\models\DeviceConfigHumidity;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\ArrayHelper;
use common\models\DeviceTemperatureReading;
use common\models\DeviceGpsReading;
use common\models\DeviceDoorReading;
use common\models\DeviceAlert;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use console\tasks\DownlinkJob;
use common\models\TemperatureFilter;
use common\models\User;
//use backend\modules\api\models\Devices;


class DeviceConfigurationController extends Controller{
	
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
						[
								'allow' => true,
								'actions' => ['temperature'],
								'roles' => ['Devices']
						],
						[
								'allow' => true,
								'actions' => ['config'],
								'roles' => ['Device Config Update']
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'create' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'config' => ['POST','PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
	public function actionTemperature(){
		
	
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			$model =  TemperatureFilter::find()->all();
			
			return $model;
			
		}else{
			
			$user=Yii::$app->user->identity;
			$groups=explode("|",$user->group_ids);
			
			$model = TemperatureFilter::find()->andWhere(["group_id"=>$groups])->all();
			
			return $model;
			
			
		}
		
		
	}
	
	public function actionDoor(){
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			$model = Door::find()->all();
			
			return $model;
			
		}
		
		return [];
		
		
	}
	
	public function actionGps(){
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			$model = Gps::find()->all();
			return $model;
			
		}
		
	}
	
	public function actionTemperatureList(){
		
		if(!Yii::$app->user->identity->hasRole('super user'))
		{
			return [];
			
		}
		
		$model = Temperature::find()->andWhere(['config_status'=>[0,1]])->all();
		
		return $model;
		
	}
	
	public function actionFilterBy($group_id,$type){
		
		if(!Yii::$app->user->identity->hasRole('super user'))
		{
			return [];
			
		}
		
		$type=strtoupper($type);
		//	$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getAllChildren($group_id);
		
		if(empty($groups))
		{
			$groups=$group_id;
			
		}else{
			
			$groups= ArrayHelper::getColumn($groups, "id");
		}
		
		if($type==Devices::TEMPERATURE)
		{
			
			$model = Temperature::find()->andWhere(['group_id'=>$groups,'config_status'=>[0,1]])->all();
			
			return $model;
			
		}elseif($type ==Devices::GPS){
			
			$model = Gps::find()->andWhere(['group_id'=>$groups])->all();
			
			return $model;
			
		}elseif ($type==Devices::DOOR) {
			
			$model = Door::find()->andWhere(['group_id'=>$groups])->all();
			
			return $model;
		}
		
		
		return [];
		
	}
	
	
	public function actionConfig($id){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$device= Devices::find()->where(["id"=>$id,"config_status"=>"1"])->one();
		if(is_null($device))
		{
			throw new NotFoundHttpException("Object not found");
		}
		
		$oConfig=Json::decode($device->config_temp);
		$result=$this->array_merge($oConfig,$rdata);
			
		//$jobs = new DownlinkJob(["type"=>"Temperature","devices"=>[$id],"data"=>$result]);
		//$queue= Yii::$app->queue->push($jobs);
		
		$push_url=Url::to(["/service/cron/temperature"],true);
		
	 	$this->makeRequest([
				"delay"=>1000,
				"hostname"=>$push_url,
				"data"=>["type"=>"Temperature","devices"=>[$id],"data"=>$result],
				"method"=>"POST"
		]);
		
		$device->config_temp=Json::encode($result);
		$device->config_new=Json::encode($result);
		$device->config_status=10;
		$device->save(false);
		return $result;
	}
	
	
	public function actionTemperatureConfig(){
		
		 $rdata=Yii::$app->getRequest()->getBodyParams();	
		 
		 if(!array_key_exists("devices", $rdata) && !array_key_exists("config", $rdata)){
		 	
		 	return false;
		 }
		 
		 if(!empty($rdata["devices"]) && !empty($rdata["config"]))
		 {
		 	
		 	
		 	$push_url=Url::to(["/service/cron/temperature"],true);
		 	
		 	$this->makeRequest([
		 			"delay"=>1000,
		 			"hostname"=>$push_url,
		 			"data"=>["type"=>"Temperature","devices"=>$rdata["devices"],"data"=>$rdata["config"]],
		 			"method"=>"POST"
		 	]);
		 	
		 	
		 	/* $jobs = new DownlinkJob(["type"=>"Temperature","devices"=>$rdata["devices"],"data"=>$rdata["config"]]);
		 	$queue= Yii::$app->queue->push($jobs); */
		 
		 	$temp_config=Json::encode($rdata["config"]);
		 	Devices::updateAll(["config_status"=>10,"config_temp"=>$temp_config,"config_new"=>$temp_config],["id"=>$rdata["devices"]]);
		 	
		 	
		 	return ["ok"];
		 }
		 
		 Yii::$app->getResponse()->setStatusCode(422);
		 
		return false;
	}
	
	
	
	public function actionBatteryCalibration(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(!array_key_exists("devices", $rdata)){
			
			return false;
		}
		
		if(!empty($rdata["devices"]))
		{
			
			$push_url=Url::to(["/service/cron/battery"],true);

			$this->makeRequest([
				"delay"=>1000,
				"hostname"=>$push_url,
				"data"=>["type"=>"Battery","devices"=>$rdata["devices"],"data"=>null],
						"method"=>"POST"
				]);
			
			/* $jobs = new DownlinkJob(["type"=>"Battery","devices"=>$rdata["devices"],"data"=>null]);
			$queue= Yii::$app->queue->push($jobs); */
			
		}
		
		return ['ok'];
		
	}
	
	private function array_merge($oConfig,$rdata){
		
		foreach ($oConfig as $key=>$value)
		{
			if(is_array($oConfig[$key]))
			{
			
				if(array_key_exists($key,$rdata))
				{
					$oConfig[$key]=array_merge($oConfig[$key],$rdata[$key]);
					
				}
				
			}
			
		}
		
		return array_merge($oConfig,$rdata);
		
	}
	
	
	
	public function makeRequest($data){
		
		Yii::trace('post data -------');
		Yii::trace($data);
		
		$url=Yii::$app->params["task_scheduler_url"];
		
		$client = new Client();
		$response=$client->createRequest()->setFormat(Client::FORMAT_JSON)->setMethod('POST')->setUrl($url)->setData($data)->send();
		if($response->isOk){
			
		}
	}
	
	
	
}