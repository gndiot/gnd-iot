<?php

namespace backend\modules\v1\models;

use Yii;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\Json;

/**
 * This is the model class for table "devices".
 *
 * @property int $id
 * @property string $hardware_serial
 * @property int $customer_id
 * @property int $type
 * @property string $name
 * @property string $display_name
 * @property int $group_id
 * @property double $start_value
 * @property double $end_value
 * @property int $response_intervel
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $sync_time
 * @property int $sync_last_time
 * @property int $admin_id
 * @property string $config_new
 * @property string $config_temp
 * @property int $config_status
 * @property int $bc_req_status
 * @property int $bc_req_time
 * @property string $data
 * $property int $battery_level
 */
class Devices extends \yii\db\ActiveRecord
{
	
	 const TEMPERATURE='TEMPERATURE';
	 const GPS='GPS';
	 const DOOR='DOOR';
	 public $deviceRange=[];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['hardware_serial','name','response_intervel','start_value','end_value','status'],'required'],
            [['customer_id', 'type', 'group_id', 'response_intervel', 'status', 'created_at', 'updated_at', 'sync_time', 'sync_last_time', 'admin_id', 'customer_user_id','config_status','bc_req_time','bc_req_status'], 'integer'],
            [['start_value', 'end_value'], 'number'],
        	['start_value', 'default', 'value' => 0],
        	['end_value', 'default', 'value' => 80],
            [['hardware_serial', 'display_name'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 50],
        	[['config_new','config_temp'],'string'],
        	['data','string'],
        	['battery_level','integer'],
            [['hardware_serial'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hardware_serial' => 'Hardware Serial',
            'customer_id' => 'Customer ID',
            'type' => 'Type',
            'name' => 'Name',
            'display_name' => 'Display Name',
            'group_id' => 'Group ID',
            'start_value' => 'Start Value',
            'end_value' => 'End Value',
            'response_intervel' => 'Response Intervel',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sync_time' => 'Sync Time',
            'sync_last_time' => 'Sync Last Time',
            'admin_id' => 'Admin ID',
            'customer_user_id' => 'Customer User ID',
        	'config_new'=>'config_new',
        	'config_temp'=>'config_old',
        	'config_status'=>'config_status',
        	'bc_req_status'=>'bc_req_status',
        	'bc_req_time'=>'bc_req_time',
        	'data'=>'data',
        	'battery_level'=>'battery level'
        ];
    }
    
 
   public function fields(){
   	
   	      
   		return [
   				'value'=>'id',
   				'label'=>function($model){
   					
   					return $model->name." - ".$model->display_name;
   				},
   				'display_name'
   		];
   	
   	
   }
   
   
   
    
}
