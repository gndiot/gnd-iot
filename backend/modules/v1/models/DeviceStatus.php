<?php

namespace backend\modules\v1\models;

use Yii;
use backend\modules\api\models\DeviceGroups;

/**
 * This is the model class for table "device_status".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $device_ids
 * @property string $device_times
 * @property int $created_at
 */
class DeviceStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['customer_id', 'group_id','device_ids',"device_times"], 'required'],
        	['device_ids','each','rule'=>['integer']],
        	['device_times','onValidateTimes'],  
            [['customer_id', 'group_id', 'created_at'], 'integer'],
           
        ];
    }
    
    public function onValidateTimes($attributes,$params){
    	
    	 Yii::trace("OnValidateTimes");
    	 
    	 Yii::trace($attributes);
    	 Yii::trace($this->device_times);
    	 
    	 if(!is_array($this->device_times))
    	 {
    	 	
    	 	$this->addError($attributes,"{$attributes} must be array" );
    	 	return false;
    	 	
    	 }
    	 
    	 $count=count($this->device_times);
    	 
    	 for ($i = 0; $i < $count; $i++) {
    	 	
    	 	 if(!is_array($this->device_times[$i]))
    	 	 {
    	 	 	$this->addError($attributes,"{$attributes} {$i} must be array" );
    	 	 	continue;
    	 	 }

    	 	 if(!array_key_exists("from",$this->device_times[$i]))
    	 	 {
    	 	 	$this->addError($attributes,"{$attributes} {$i} invalid key" );
    	 	 	continue;
    	 	 }
    	 	 
    	 	 if(!array_key_exists("to",$this->device_times[$i]))
    	 	 {
    	 	 	$this->addError($attributes,"{$attributes} {$i} invalid key" );
    	 	 	continue;
    	 	 }
    	 	 
    	 	 if($this->device_times[$i]["from"] =="" || $this->device_times[$i]["to"] =="")
    	 	 {
    	 	 	$this->addError($attributes,"{$attributes} {$i} can't be blank!" );
    	 	 	continue;
    	 	 }
    	 	
    	 }

    	 return true;
    	 
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'group_id' => 'Group ID',
            'device_ids' => 'Device Ids',
            'device_times' => 'Device Times',
            'created_at' => 'Created At',
        ];
    }
    
    public function afterFind(){
    	
    	
    	$created_at=Yii::$app->utils->getDateTime($this->created_at);
    
    	
    	$this->created_at=$created_at->format('Y-m-d H:i:s');
    	
    	$this->device_ids=$this->device_ids!=""?explode("|", $this->device_ids):[];
    	
    	$this->device_times=json_decode($this->device_times,true);
    	
    	$this->created_at=$created_at->format('Y-m-d H:i:s');
    	
    	
    	
    }
    
    
    
    public function getCustomer(){
    	
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"customer_id"]);
    	
    }
    
    public function getGroup(){
    	
    	
    	
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"group_id"]);
    	
    }
    
    public function getDevices(){
    	
    	if($this->device_ids =="")
    	{
    		return [];
    	}
    	
    	$devices=implode("|",$this->device_ids);
    	Yii::trace($devices);
    	return Devices::find()->where(["id"=>$this->device_ids])->all();
    	//return $this->hasMany(Devices::className(), ['id'=>'device_ids'])->where(["id"=>$devices]);
    	
    }
    
    public function beforeSave($insert){
    	
    	
    	if (!parent::beforeSave($insert)) {
    		return false;
    	}
    	if($insert)
    	{
    		
    	}else{
    		
    		
    	}
    	
    	$this->device_times=json_encode($this->device_times);
    	$this->device_ids=implode("|", $this->device_ids);

      	return true;
    	
    }
    
    public function afterSave($insert, $changedAttributes){
    	
    	parent::afterFind();
    	
    	
    	$this->device_ids=$this->device_ids!=""?explode("|", $this->device_ids):[];
    	
    	$this->device_times=json_decode($this->device_times,true);
    }
    
    public function extraFields(){
    	
    	return ['customer','group','devices'];
    	
    }
    
}
