<?php
namespace backend\modules\v1\models;


use yii\data\ActiveDataProvider;
use yii\base\Model;
use common\models\Constants;

class SubscriptionSearch extends DeviceSubscription{
	
	
	public function rules(){
		
		return [
				
				['device_ids','each','rule'=>['integer']],
				['emails','each','rule'=>['email']],
				['numbers','each','rule'=>['integer']],

				[['value_one', 'value_two'], 'number'],
				[['attribute', 'email'], 'string', 'max' => 50],
				[['mobile_numbers'], 'string', 'max' => 20],
				[['intervel_type'], 'string', 'max' => 255],
				[['intervel_mins'],'default','value'=>10],
				//[['notification_status'],'default','value'=>self::STATUS_ACTIVE],
				[['intervel_type'], 'default', 'value' =>Constants::INTERVEL_TYPE_MINUTES],
				[['is_default_email','is_default_sms','is_push','is_email','is_sms'],'default','value'=>0]
				];
		
		
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	
	public function search($params){
		
		$query=DeviceSubscription::find();
	
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 50],
				],
		]);
			
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$query->andFilterWhere([
				'group_id'=>$this->group_id,
				'customer_id'=>$this->customer_id,
				'attribute'=>$this->attribute,
				'notification_status'=>$this->notification_status
		]);
		
		if($this->device_ids !="")
		{
			$export=implode("|", $this->device_ids);
			$query->andFilterWhere(['REGEXP','device_ids',$export]);
		}
		
		if($this->emails !="")
		{
			$export=implode("|", $this->emails);
			$query->andFilterWhere(['REGEXP','emails',$export]);
		}
		
		if($this->numbers !="")
		{
			$export=implode("|", $this->numbers);
			$query->andFilterWhere(['REGEXP','numbers',$export]);
		}
		
		
		  return $dataProvider;
	}
	
	public function searchGroups($params,$groups,$customer_id){
		
		
		$query=DeviceSubscription::find()->where(['group_id'=>$groups,'customer_id'=>$customer_id]);
	
		
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 50],
				],
		]);
		
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$query->andFilterWhere([
				'group_id'=>$this->group_id,
				//'customer_id'=>$this->customer_id,
				'attribute'=>$this->attribute,
				'notification_status'=>$this->notification_status
		]);
		
		if($this->device_ids !="")
		{
			$export=implode("|", $this->device_ids);
			$query->andFilterWhere(['REGEXP','device_ids',$export]);
		}
		
		if($this->emails !="")
		{
			$export=implode("|", $this->emails);
			$query->andFilterWhere(['REGEXP','emails',$export]);
		}
		
		if($this->numbers !="")
		{
			$export=implode("|", $this->numbers);
			$query->andFilterWhere(['REGEXP','numbers',$export]);
		}
		
		
		return $dataProvider;
			
		
		
	}
	
	
}