<?php

namespace backend\modules\v1\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "device_status".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $group_id
 * @property string $device_ids
 * @property string $device_times
 * @property int $created_at
 */
class DeviceStatusSearch extends DeviceStatus
{
  
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'group_id', 'created_at'], 'integer'],
            [['device_ids', 'device_times'], 'string'],
        ];
    }
    
    
    public function search($params){
    	
    	$query=DeviceStatus::find();
    	
    	$dataProvider=new ActiveDataProvider([
    			'query'=>$query,
    			'pagination' => [
    					'pageSizeLimit' => [0, 50],
    			],
    	]);
    	
    	$this->load($params,"");
    	
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    	
    	return $dataProvider;
    }
    
    

  
}
