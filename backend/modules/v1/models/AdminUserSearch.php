<?php
namespace backend\modules\v1\models;

use yii;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use common\models\Constants;

class AdminUserSearch extends AdminUser{
	
	
	public function rules(){
		
		return [
				[['customer_id'], 'string'],
				['group_ids','each','rule'=>['integer']],
				[['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
				[['auth_key'], 'string', 'max' => 32],
				[['first_name', 'last_name'], 'string', 'max' => 20],
				[['phone_number'], 'string', 'max' => 15],
				[['timezone'], 'string', 'max' => 10],
				[['status'], 'integer'],
				[['timezone_description'], 'string', 'max' => 50],
				
		];
	
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	
	public function search($params){
		
		
		
		$query=AdminUser::find();
		
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 50],
				],
		]);
		
		$this->load($params,"");
		
		Yii::trace($this->username);
		
		
		
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			
			Yii::trace($this->getErrors());
			
			return $dataProvider;
		}
		
		
		
		$query->andFilterWhere([
				'customer_id'=>$this->customer_id,
				'email'=>$this->email,
				'status'=>$this->status,
				'username'=>$this->username
		]);
		
		if($this->group_ids!="")
		{
			$export=implode("|", $this->group_ids);
			$query->andFilterWhere(['REGEXP','group_ids',$export]);
		}
		
		return $dataProvider;
	}
	
	public function searchGroups($params,$groups){
		
		
		$query=AdminUser::find()->where(['group_id'=>$groups]);
		
		
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 50],
				],
		]);
		
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$query->andFilterWhere([
				'group_id'=>$this->group_id,
				'customer_id'=>$this->customer_id,
				'email'=>$this->email,
				'status'=>$this->status,
				'username'=>$this->username
		]);
		
		if($this->group_ids!="")
		{
			$export=implode("|", $this->group_ids);
			$query->andFilterWhere(['REGEXP','group_ids',$export]);
		}
		
		
		
	}
	
	
	public function sGroups($params,$groups,$customer_id){
		
		
		$query=AdminUser::find()->where(["customer_id"=>$customer_id])->andWhere(["!=","username","admin"]);
		
		
		$dataProvider=new ActiveDataProvider([
				'query'=>$query,
				'pagination' => [
						'pageSizeLimit' => [0, 50],
				],
		]);
		
		$this->load($params,"");
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$query->andFilterWhere([
				//'group_id'=>$this->group_id,
				//'customer_id'=>$this->customer_id,
				'email'=>$this->email,
				'status'=>$this->status,
				'username'=>$this->username
		]);
		
		if($this->group_ids!="")
		{
			$export=implode("|", $this->group_ids);
			$query->andFilterWhere(['REGEXP','group_ids',$export]);
		}
		
		
		return $dataProvider;
	}
	
	
}