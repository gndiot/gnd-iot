<?php

namespace backend\modules\v1\models;

use Yii;
use backend\modules\api\models\DeviceGroups;

/**
 * This is the model class for table "admin_user".
 *
 * @property int $id
 * @property int $group_id
 * @property string $group_ids
 * @property string $customer_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $email
 * @property int $status
 * @property string $timezone
 * @property string $timezone_description
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class AdminUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['first_name','username','password_hash', 'email',], 'required','on'=>'create'],
        	[['first_name','username','password_hash', 'email',], 'required','on'=>'update'],
            [['customer_id'], 'string'],
        	['group_ids','each','rule'=>['integer']],
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['first_name', 'last_name'], 'string', 'max' => 20],
            [['phone_number'], 'string', 'max' => 15],
            [['timezone'], 'string', 'max' => 10],
            [['timezone_description'], 'string', 'max' => 50],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'group_ids' => 'Group Ids',
            'customer_id' => 'Customer ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'status' => 'Status',
            'timezone' => 'Timezone',
            'timezone_description' => 'Timezone Description',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    
    public function setPassword($password)
    {
    	$this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    public function generateAuthKey()
    {
    	$this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    public static function activeRoles(){
    	
    	
    	$roles=Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
    	
    	$result=[];
    	
    	foreach ($roles as $key=>$value)
    	{
    		array_push($result, $key);
    		
    	}
    	
    	return $result;
    	
    	
    }
    
    public static function roles(){
    	
    	$roles=Yii::$app->authManager->getRoles();
    	$result=[];
    	$is_admin=false;
    	$current_roles=self::activeRoles();
    	
    	if(in_array('admin', $current_roles) || in_array('super user', $current_roles))
    	{
    		$is_admin=true;
    		
    	}else{
    		
    		$is_admin=false;
    		
    	}
    	
    	foreach ($roles as $key=>$value)
    	{
    		
    		$temp=[];
    		
    		$temp["key"]=$key;
    		$temp["value"]=$key;
    		
    		if($is_admin)
    		{
    			 if($key!="super user")
    			 {
    			 	array_push($result, $temp);
    			 }
    			
    			
    		}else{
    			
    			
    			
    		}
    		
    		
    	}
    	
    	return $result;
    	
    }
    
   public function getRole(){
    	
    	
    	$roles=Yii::$app->authManager->getRolesByUser($this->id);
    	
    	$result=[];
    	
    	foreach ($roles as $key=>$value)
    	{
    		array_push($result, $key);
    		
    	}
    	
    	return $result;
    	
    }
    
    public function getGroups(){
    	
    	if($this->group_ids=="")
    	{
    		return [];
    	}
    	

    	
    	return DeviceGroups::find()->where(["id"=>$this->group_ids])->all();
    	
    	//return $this->hasMany(DeviceGroups::className(), ['id'=>'group_ids'])->where(["id"=>$devices]);
    	
    }
    
    public function toStringGroups($glue="|"){
    	
    	if($this->group_ids!=""){
    		
    		return implode($glue, $this->group_ids);	
    	}
    	
    	return "";
    	
    }
    
    public function toArrayGroups($glue="|"){
    		
    	if($this->group_ids !=""){
    		
    		$data=explode($glue, $this->group_ids);
    		
    		return array_map('intval',$data);
    	}
    	
    	return null;
    	
    	
    }
    
    public function afterFind(){
    	
    	 $this->group_ids=$this->toArrayGroups();
    	 $created_at=Yii::$app->utils->getDateTime($this->created_at);
    	 $updated_at=Yii::$app->utils->getDateTime($this->updated_at);
    	 $this->created_at=$created_at->format('Y-m-d H:i:s');
    	 $this->updated_at=$updated_at->format('Y-m-d H:i:s');
    	
    }
    
    
    
    public function beforeSave($insert){
    	
    	if (!parent::beforeSave($insert)) {
    		return false;
    	}
    	
    	$this->group_ids=$this->group_ids=$this->toStringGroups();
    	
     	return true;
    }
    
    
    public function afterSave($insert, $changedAttributes){
    	
    	
    	$this->group_ids=$this->toArrayGroups();
    	$created_at=Yii::$app->utils->getDateTime($this->created_at);
    	$updated_at=Yii::$app->utils->getDateTime($this->updated_at);
    	
    	$this->created_at=$created_at->format('Y-m-d H:i:s');
    	$this->updated_at=$updated_at->format('Y-m-d H:i:s');
    	
    	
    }
    
    public function fields(){
    	
    	$fields= parent::fields();
    	
    	unset($fields["auth_key"],$fields['password_hash'],$fields["password_reset_token"],$fields["group_ids"]);
    
    	return $fields;

    }
    
    public function extraFields(){
    	return ['groups','role'];
    	
    }
    
}
