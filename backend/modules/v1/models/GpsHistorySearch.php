<?php
namespace backend\modules\v1\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use common\models\Constants;
use backend\modules\gps\models\GpsHistory;

class GpsHistorySearch extends GpsHistory{
	
	
	public $from;
	public $to;
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
				[['device_id','from','to'], 'required'],
				[['device_id', 'humidity_value', 'movement', 'power_status', 'battery_status', 'battery_value', 'sensor_battery_value', 'created_at'], 'integer'],
				[['payload_raw'], 'string'],
				[['from','to'], 'date', 'format' => 'php:Y-m-d H:i:s'],
				[['temperature_value', 'latitude', 'longitude','speed'], 'number'],
				[['hardware_serial'], 'string', 'max' => 255],
				[['message_type'], 'string', 'max' => 10],
		];
	}
	
	public function getHistory(){
		
		$begin = new \DateTime($this->from);
		$end = new \DateTime($this->to);
		$data=GpsHistory::find()->andFilterWhere(["device_id"=>$this->device_id])->andFilterWhere([
         		'movement'=>$this->movement,
        ])->andFilterWhere(["between","created_at",$begin->getTimestamp(),$end->getTimestamp()])->asArray()->all();
	
        
        $count=count($data);
        
        for ($i = 0; $i < $count; $i++) {
        	$data[$i]["graph_datetime"]=intval($data[$i]["created_at"])*1000;
        	$begin = new \DateTime();
        	$begin->setTimestamp($data[$i]["created_at"]);
        	$data[$i]["temperature_value"]=number_format($data[$i]["temperature_value"],1,".",'.');
        	//$data[$i]["created_at"]=$begin->format("Y-m-d H:i:s");
        	$data[$i]["formatted_datetime"]=$begin->format("Y-m-d H:i:s");
        	
        }
        
        return $data; 
		
	}
	
}