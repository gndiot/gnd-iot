<?php
namespace backend\modules\v1\controllers;
use yii;
use yii\rest\Controller;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
use backend\modules\api\models\DeviceGroups;
use common\models\CustomerSearch;
use common\models\Customer;

use function yii\db\all;
use yii\filters\AccessControl;
use common\models\PasswordChange;
use backend\modules\v1\models\AdminUser;
use yii\data\ActiveDataProvider;
use backend\modules\v1\models\AdminUserSearch;
use yii\web\NotFoundHttpException;

class UserController extends Controller{
	
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'actions' => ['index'],
								'roles' => ['Users']
						],
						[
								'allow' => true,
								'actions' => ['create'],
								'roles' => ['User Add']
						],
						[
								'allow' => true,
								'actions' => ['roles'],
								'roles' => ['User Add']
						],
						[
								'allow' => true,
								'actions' => ['update'],
								'roles' => ['User Update']
						],
						[
								'allow' => true,
								'actions' => ['delete'],
								'roles' => ['User Delete']
						],
						[
								'allow' => true,
								'actions' => ['group-user'],
								'roles' => ['Group Users']
						],
						[
								'allow' => true,
								'actions' => ['group-add'],
								'roles' => ['Group User Add']
						],
						[
								'allow' => true,
								'actions' => ['group-update'],
								'roles' => ['Group User Update']
						],
						[
								'allow' => true,
								'actions' => ['group-delete'],
								'roles' => ['Group User Delete']
						],
						[
								'allow' => true,
								'actions' => ['group-roles'],
								'roles' => ['Group User Add','Group User Update']
						],
						[
								'allow' => true,
								'actions' => ['change-password'],
								'roles' => ['User Change Password']
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		
		return $behaviors;
	}
	
	public function actionIndex(){
		
		
		$filter=Yii::$app->getRequest()->getBodyParams();
		
		$dataProvider = new AdminUserSearch();
		
		return $dataProvider->search($filter);
		 
	}
	
	
	public function actionCreate(){
		
		$model =new AdminUser();
		$model->setScenario('create');
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$model->loadDefaultValues();
		$model->load($rdata,"");
		$model->generateAuthKey();
		if($model->password_hash !="" )
		{
			$model->setPassword($model->password_hash);
		}
		
		$model->created_at=time();
		$model->updated_at=time();
		
		if($model->save())
		{
			
		
			$auth=Yii::$app->authManager;
			$role=$auth->getRole($rdata['role']);
			if(!is_null($role))
			{
				$auth->assign($role,$model->id);
			}
			
		
			return $model;
			 
		}else{
				
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
	
	}
	
	
	public function actionUpdate($id){
		
		
		$model= AdminUser::find()->where(["id"=>$id])->one();
		
		if(is_null($model))
		{
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$model->setScenario("update");
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(empty($rdata))
		{
			throw new NotFoundHttpException("resource not found");
		}
		
		unset($rdata["id"]);
		$model->load($rdata,"");
		
		//
		$created=new \DateTime($model->created_at);
		$model->created_at=$created->getTimestamp();
		
		
		$model->updated_at=time();
		if(isset($rdata["password_hash"]) && $rdata["password_hash"] != "")
		{
			$model->setPassword($rdata["password_hash"]);
				
		}
			
		if($model->save())
		{
			
			$auth=Yii::$app->authManager;
			
			$auth->revokeAll($model->id);
			
			$role=$auth->getRole($rdata['role']);
			if(!is_null($role))
			{
				$auth->assign($role,$model->id);
			}
				
				
			return $model;
		
		}else{
		
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
	}
	
	public function actionDelete($id){
		
		
		 if(Yii::$app->user->id==$id)
		 {
		 	throw new NotFoundHttpException("Object not found: $id");
		 	
		 }
		 
		
		$model=AdminUser::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
		
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$auth=Yii::$app->authManager;
			
		$auth->revokeAll($model->id);
		
		$model->delete();
			
		return $model;
	}
	
	
	public function actionChangePassword(){
		
			  $user_id=Yii::$app->user->id;
			  $rdata=Yii::$app->getRequest()->getBodyParams();
			  
			  $model = new PasswordChange();
			  $model->load($rdata,'');
			  $model->user_id=$user_id;
			  if($model->updatePassword())
			  {
			  	return ["ok"];
			  	
			  }else{
			  	Yii::$app->getResponse()->setStatusCode(422);
			  	 return $model->getErrors();
			  }		 
	}
	
	public function actionRoles(){
		
		return AdminUser::roles();
	}
	
	public function actionGroupRoles(){
		
		return [["key"=>"Group Manager","value"=>"Manager"],["key"=>"Group User","value"=>"User"]];
		
	}
	
	public function actionGroupUser(){
		
		$user= Yii::$app->user->identity;
		
		$customer_id=$user->customer_id;
		
		$user_groups=explode("|",$user->group_ids);
		
		if(!$user->hasRole('Group Admin'))
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return [];
		}
		
		$filter=Yii::$app->getRequest()->getBodyParams();
		
		$dataProvider = new AdminUserSearch();
		
		return $dataProvider->sGroups($filter,$user_groups,$customer_id);
		
		
	}
	
	public function actionGroupAdd(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$user= Yii::$app->user->identity;
		
		$customer_id=$user->customer_id;
		
		$user_groups=explode("|",$user->group_ids);

		
		if(!in_array($rdata['role'], ["Group Manager","Group User"]))
		{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return [];
		}
		
		$model =new AdminUser();
		$model->setScenario('create');
		$model->customer_id=$customer_id;
		$model->loadDefaultValues();
		$model->load($rdata,"");
		$model->generateAuthKey();
		if($model->password_hash !="" )
		{
			$model->setPassword($model->password_hash);
		}
		
		$model->created_at=time();
		$model->updated_at=time();
		
		if($model->save())
		{
			
			$auth=Yii::$app->authManager;
			$role=$auth->getRole($rdata['role']);
			if(!is_null($role))
			{
				$auth->assign($role,$model->id);
			}

			return $model;
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
		
	}
	
	public function actionGroupUpdate($id){
		
		$user= Yii::$app->user->identity;
		
		$customer_id=$user->customer_id;
		
	
		$model= AdminUser::find()->where(["customer_id"=>$customer_id,"id"=>$id])->one();
		
		if(is_null($model))
		{
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$model->setScenario("update");
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(empty($rdata))
		{
			throw new NotFoundHttpException("resource not found");
		}
		
		if(!in_array($rdata['role'], ["Group Manager","Group User"]))
		{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return ["test"];
		}
		
		unset($rdata["id"]);
		$model->load($rdata,"");
		
		$datetime=new \DateTime($model->created_at);
		$model->created_at=$datetime->getTimestamp();
		
		$model->updated_at=time();
		if(isset($rdata["password_hash"]) && $rdata["password_hash"] != "")
		{
			$model->setPassword($rdata["password_hash"]);
			
		}
		
		if($model->save())
		{
			
			
			
			$auth=Yii::$app->authManager;
			
			$auth->revokeAll($model->id);
			
			$role=$auth->getRole($rdata['role']);
			if(!is_null($role))
			{
				$auth->assign($role,$model->id);
			}
			
			
			return $model;
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
	}
	
	public function actionGroupDelete($id){
		
		
		$user= Yii::$app->user->identity;
		
		$customer_id=$user->customer_id;
		
		if(Yii::$app->user->id==$id)
		{
			
			throw new NotFoundHttpException("Object not found: $id");
			
		}
		
		
		$model=AdminUser::find()->where(["customer_id"=>$customer_id,"id"=>$id])->one();
		if(is_null($model))
		{
			
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$auth=Yii::$app->authManager;
		
		$auth->revokeAll($model->id);
		
		$model->delete();
		
		return $model;
		
	}
	
}