<?php
namespace backend\modules\v1\controllers;
use yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\Devices;
use yii\db\Query;
use backend\modules\api\models\DailyReport;
use backend\modules\api\models\PeriodicReport;
use backend\modules\api\models\DailySummaryReport;
use backend\modules\api\models\DeviceGroups;
use backend\modules\gps\models\GpsHistory;

class GpsReportController extends Controller{
	
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	
	public function behaviors()
	{
		
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'allow' => true,
										'roles' => ['@'],
								],
						],
				],
		];
	}
	
	

	
	
	public function actionTemperatureDaily(){
		
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$data=[];
		
		$model= new DailyReport();
		$model->load($rdata,'');
		
		if(!$model->validate())
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
		
		$groups=DeviceGroups::getChildrenWithParent($model->group);
		$gids=ArrayHelper::getColumn($groups, 'id');
		
		
		$deivces = Devices::find()->select(["id"])->where(["group_id"=>$model->groups,"type"=>2])->asArray()->all();
		$d_ids=ArrayHelper::getColumn($deivces, "id");
		
		$type=$model->hours;
		
		$begin = new \DateTime($model->date);
		//$begin->setTimestamp("1495231200");
		$end = new \DateTime($model->date);
		//$end->setTimestamp("1495231200");
		$end = $end->modify( '+1 day' );
		$interval = new \DateInterval('P1D');
		$daterange = new \DatePeriod($begin, $interval ,$end);
		$colums=[];
		
		if($type=='two')
		{
			
			Yii::$app->db->createCommand('CREATE TEMPORARY TABLE temp_table (
			  `location` varchar(255) NULL,
			  `sensor_range` varchar(50) NULL,
			 `report_date` varchar(50) NULL,
			  `customer_id` varchar(50) NULL,
			  `group_id` varchar(50) NULL,
			  `00:00-02:00` varchar(50) NULL,
			  `02:00-04:00` varchar(50) NULL,
			  `04:00-06:00` varchar(50)  NULL,
			  `06:00-08:00` varchar(50)  NULL,
			  `08:00-10:00` varchar(50) NULL,
			  `10:00-12:00` varchar(50)  NULL,
			  `12:00-14:00` varchar(50)  NULL,
			  `14:00-16:00` varchar(50)  NULL,
			  `16:00-18:00` varchar(50)  NULL,
			  `18:00-20:00` varchar(50)  NULL,
			  `20:00-22:00` varchar(50)  NULL,
			  `22:00-00:00` varchar(50)  NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;')->execute();
			
			$colums=['location','sensor_range','report_date','customer_id','group_id','00:00-02:00','02:00-04:00','04:00-06:00','06:00-08:00','08:00-10:00','10:00-12:00','12:00-14:00','14:00-16:00','16:00-18:00','18:00-20:00','20:00-22:00','22:00-00:00'];
			
		}
		
		if($type=='four')
		{
			Yii::$app->db->createCommand('CREATE TEMPORARY TABLE temp_table (
			  `location` varchar(255) NULL,
			  `sensor_range` varchar(50) NULL,
			 `report_date` varchar(50) NULL,
			 `customer_id` varchar(50) NULL,
				`group_id` varchar(50) NULL,
			  `00:00-04:00` varchar(50) NULL,
			  `04:00-08:00` varchar(50) NULL,
			  `08:00-12:00` varchar(50)  NULL,
			  `12:00-16:00` varchar(50)  NULL,
			  `16:00-20:00` varchar(50) NULL,
			  `20:00-00:00` varchar(50)  NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;')->execute();
			
			$colums=['location','sensor_range','report_date','customer_id','group_id','00:00-04:00','04:00-08:00','08:00-12:00','12:00-16:00','16:00-20:00','20:00-00:00'];
			
		}
		
		$result=[];
		
		foreach($daterange as $date){
			
			$result[]=$this->fourHoursReport($date->format('Y-m-d'),$type,$d_ids,$colums,$model->sensor);
		}
		
		$results=Yii::$app->db->createCommand("select temp_table.*,device_groups.name as group_name,customer.name as customer_name from temp_table join device_groups on device_groups.id=temp_table.group_id join device_groups as customer on customer.id=temp_table.customer_id")->queryAll();
		return $results;
		
		
		
	}
	
	public function actionTemperatureDailySummary(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$data=[];
		$model= new DailySummaryReport();
		$model->load($rdata,'');
		if(!$model->validate())
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
		$begin = new \DateTime($model->from);
		//$begin->setTimestamp("1495231200");
		$end = new \DateTime($model->to);
		//$end->setTimestamp("1495231200");
		$end = $end->modify( '+1 day' );
		$interval = new \DateInterval('P1D');
		$daterange = new \DatePeriod($begin, $interval ,$end);
		$colums=[];
		Yii::$app->db->createCommand('CREATE TEMPORARY TABLE temp_table (
			  `date` varchar(255) NULL,
			  `sensor_name` varchar(50) NULL,
			 `limits` varchar(50) NULL,
			 `customer_id` varchar(50) NULL,
			  `group_id` varchar(50) NULL,
			  `0012_min` varchar(50) NULL,
			  `0012_max` varchar(50)  NULL,
			  `0012_avg` varchar(50) NULL,
			  `0012_total_samples` varchar(50)  NULL,
			  `1200_min` varchar(50) NULL,
			  `1200_max` varchar(50) NULL,
			  `1200_avg` varchar(50)  NULL,
			  `1200_total_samples` varchar(50)  NULL,
			  `e_min` varchar(50)  NULL,
			  `e_max` varchar(50) NULL,
			  `e_avg` varchar(50)  NULL,
			  `e_total_samples` varchar(50)  NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;')->execute();
		
		foreach($daterange as $date){
			
			$this->dailySummary($date->format('Y-m-d'),$model->devices,$model->sensor);
		}
		
		$results=Yii::$app->db->createCommand("select temp_table.*,device_groups.name as group_name,customer.name as customer_name from temp_table join device_groups on device_groups.id=temp_table.group_id join device_groups as customer on customer.id=temp_table.customer_id")->queryAll();
		
		return $results;
		
	}
	
	public function actionTemperatureMonthly(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$data=[];
		$model= new DailySummaryReport();
		$model->load($rdata,'');
		if(!$model->validate())
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
		$begin = new \DateTime($model->from);
		$end = new \DateTime($model->to);
		$interval = new \DateInterval('P1M');
		$daterange = new \DatePeriod($begin, $interval ,$end);
		$colums=[];
		
		Yii::$app->db->createCommand('CREATE TEMPORARY TABLE temp_table (
			  `sensor_name` varchar(255) NULL,
			  `limits` varchar(50) NULL,
			 `customer_id` varchar(50) NULL,
			 `group_id` varchar(50) NULL,
			  `low_reading` varchar(50) NULL,
			  `high_reading` varchar(50) NULL,
			  `total_avg` varchar(50)  NULL,
			  `total_samples` varchar(50) NULL,
			  `created_at` varchar(50) NULL,
			  `out_of_samples` varchar(50)  NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;')->execute();
		
		foreach($daterange as $date){
			
			$l_begin=$date->modify("first day of this month");
			$start=$l_begin->getTimestamp();
			$l_end=$l_begin->modify("last day of this month");
			$end=$l_begin->getTimestamp();
			$this->monthlyReport($start,$end,$model->devices,$model->sensor);
		}
		
		$results=Yii::$app->db->createCommand("select temp_table.*,device_groups.name as group_name,customer.name as customer_name from temp_table join device_groups on device_groups.id=temp_table.group_id join device_groups as customer on customer.id=temp_table.customer_id")->queryAll();
		return $results;
		
	}
	
	
	public function actionPeriodic(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$data=[];
		$model= new PeriodicReport();
		$model->load($rdata,'');
		if(!$model->validate())
		{
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
		}
		
		$begin = new \DateTime($model->from);
		$end = new \DateTime($model->to);
		$end->modify("tomorrow");
		$end->modify("-1 minute");
		
		$list=Devices::find()->select(["hardware_serial"])->where(["group_id"=>$model->groups,"type"=>[1,4],"id"=>$model->devices])->asArray()->all();
		
		$ids=ArrayHelper::getColumn($list, "hardware_serial");
		
		//$result=DeviceTemperatureReading::find()->select(["hardware_serial","temperature","humidity","created_at"])->where(["hardware_serial"=>$ids])->andWhere(["between","created_at",$begin->getTimestamp(),$end->getTimestamp()])->asArray()->all();
		
		$query= new Query();
		
		$result=$query->select(["device_gps_history.temperature_value","device_gps_history.sensor_battery_value","device_gps_history.humidity_value","devices.name","devices.display_name","devices.equipement_id","device_gps_history.created_at"])->from("device_gps_history")->leftJoin("devices","device_gps_history.hardware_serial=devices.hardware_serial")->where(["device_gps_history.hardware_serial"=>$ids])->andWhere(["between","device_gps_history.created_at",$begin->getTimestamp(),$end->getTimestamp()])->all();
		
		$count=count($result);
		
		for ($i = 0; $i < $count; $i++) {
			
			$begin = new \DateTime();
			$begin->setTimestamp($result[$i]["created_at"]);
			$result[$i]["created_at"]=$begin->format("Y-m-d H:i:s");
			
		}
		
		
		return $result;
		
	}
	
	public function actionHours(){
		
		$type='two';
		
		$begin = new \DateTime('2018-05-28');
		//$begin->modify("first day of this month");
		//$begin->setTimestamp("1495231200");
		$end = new \DateTime('2018-06-28');
		//$begin->modify("last day of this month");
		//$end->setTimestamp("1495231200");
		$end = $end->modify( '+1 day' );
		$interval = new \DateInterval('P1M');
		$daterange = new \DatePeriod($begin, $interval ,$end);
		$colums=[];
		
		Yii::$app->db->createCommand('CREATE TEMPORARY TABLE temp_table (
			  `sensor_name` varchar(255) NULL,
			  `limits` varchar(50) NULL,
			 `customer_id` varchar(50) NULL,
			 `group_id` varchar(50) NULL,
			  `low_reading` varchar(50) NULL,
			  `high_reading` varchar(50) NULL,
			  `total_avg` varchar(50)  NULL,
			  `total_samples` varchar(50) NULL,
			  `created_at` varchar(50) NULL,
			  `out_of_samples` varchar(50)  NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;')->execute();
		
		foreach($daterange as $date){
			//$this->dailySummary($date->format('Y-m-d'),[1]);
			$l_begin=$date->modify("first day of this month");
			$start=$l_begin->getTimestamp();
			$l_end=$l_begin->modify("last day of this month");
			$end=$l_begin->getTimestamp();
			$this->monthlyReport($start,$end,[1,2]);
			
		}
		
		$results=Yii::$app->db->createCommand("select temp_table.*,device_groups.name as group_name from temp_table join device_groups on device_groups.id=temp_table.group_id")->queryAll();
		return $results;
		//return $results;
		
	}
	
	
	
	public function getPeriodicReports(){
		
		
		
		
		
	}
	
	private function monthlyReport($from,$to,$device,$sensor_type="temperature"){
		
		$devices=Devices::find()->where(["id"=>$device,'type'=>2])->asArray()->all();
		$d_count=count($devices);
		
		for ($i = 0; $i < $d_count; $i++) {
			
			$dev_config=Json::decode($devices[$i]['config_new']);
			$d_customer_id=$devices[$i]['customer_id'];
			$d_group_id=$devices[$i]['group_id'];
			$d_name=$devices[$i]['display_name'];
			
			if($sensor_type=='temperature')
			{
				$data_high=60;
				$data_low=10;
				$outofsampling=GpsHistory::find()->where(["not between",'temperature_value',$data_low,$data_high])->andWhere(["hardware_serial"=>$devices[$i]["hardware_serial"]])->andWhere(["between","created_at",$from,$to])->count();
				$device_result=GpsHistory::find()->select("min(temperature_value) as low_reading,max(temperature_value) as high_reading,avg(temperature_value) as total_avg,count(temperature_value) as total_samples,created_at")->where(["hardware_serial"=>$devices[$i]["hardware_serial"]])->andWhere(["between","created_at",$from,$to])->asArray()->one();
				$device_result["out_of_samples"]=$outofsampling;
				if($device_result["created_at"] !="" && $device_result["created_at"] !=null)
				{
					$date =new \DateTime();
					$date->setTimestamp($device_result["created_at"]);
					$device_result["created_at"]=$date->format("Y-m-d");
					
				}else{
					
					continue;
					
				}
				
			}else{
				
				$data_high=90;
				$data_low=50;
				$outofsampling=GpsHistory::find()->where(["not between",'humidity_value',$data_low,$data_high])->andWhere(["between","created_at",$from,$to])->count();
				$device_result=GpsHistory::find()->select("min(humidity) as low_reading,max(humidity_value) as high_reading,avg(humidity_value) as total_avg,count(humidity_value) as total_samples,created_at")->where(["hardware_serial"=>$devices[$i]["hardware_serial"]])->andWhere(["between","created_at",$from,$to])->asArray()->one();
				$device_result["out_of_samples"]=$outofsampling;
				
				if($device_result["created_at"] !="" && $device_result["created_at"] !=null)
				{
					$date =new \DateTime();
					$date->setTimestamp($device_result["created_at"]);
					
					$device_result["created_at"]=$date->format("Y-m-d");
					
				}else{
					
					continue;
					
				}
				
				
			}
			
			$head=['sensor_name'=>$d_name,"limits"=>$data_low."-".$data_high,"customer_id"=>$d_customer_id,"group_id"=>$d_group_id];
			
			$devicelist=array_merge($head,$device_result);
			
			Yii::$app->db->createCommand()->insert("temp_table", $devicelist)->execute();
			
			
		}
		
		
		
		
		
	}
	private function dailySummary($date,$devices,$sensorType="temperature"){
		
		$daylist=[];
		$begin = new \DateTime($date);
		//$begin->setTimestamp("1495231200");
		$end = new \DateTime($date);
		//$end->setTimestamp("1495231200");
		$g_end = $end->modify( '+1 day' );
		
		$interval = new \DateInterval('PT12H');
		$daterange = new \DatePeriod($begin, $interval ,$g_end);
		$sensorlist=Devices::find()->select(['id','display_name as name','customer_id','group_id','config_new','hardware_serial'])->where(["id"=>$devices,'type'=>2])->asArray()->all();
		$_count=count($sensorlist);
		for ($i = 0; $i < $_count; $i++) {
			
			$result=[];
			$dev_config=[
					"temperature_range"=>[
							"low"=>10,
							"high"=>20
					],
					"humidity_range"=>[
							"low"=>10,
							"high"=>20
					]
			];	
			if($sensorType=="temperature")
			{
				$d_range=array_key_exists('temperature_range', $dev_config)?$dev_config['temperature_range']['low'].'-'.$dev_config['temperature_range']['high']:"";
				
			}else{
				
				$d_range=array_key_exists('humidity_range', $dev_config)?$dev_config['humidity_range']['low'].'-'.$dev_config['humidity_range']['high']:"";
			}
			
			$d_date=$begin->format("Y-m-d");
			$d_customer_id=$sensorlist[$i]['customer_id'];
			$d_group_id=$sensorlist[$i]['group_id'];
			$d_name=$sensorlist[$i]['name'];
			$result[]=["date"=>$d_date,'sensor_name'=>$d_name,"limits"=>$d_range,"customer_id"=>$d_customer_id,"group_id"=>$d_group_id];
			
			foreach($daterange as $date){
				
				$start=$date->getTimestamp();
				$suffix1=$date->format("H");
				$date->modify('+12 hour');
				$end=$date->getTimestamp();
				$suffix2=$suffix1.$date->format("H");
				if($sensorType=="temperature")
				{
					$mreuslt=GpsHistory::find()->select("MIN(temperature_value) as {$suffix2}_min,MAX(temperature_value) as {$suffix2}_max,AVG(temperature_value) as {$suffix2}_avg,count(id) as {$suffix2}_total_samples")->where(['between','created_at',$start,$end])->andWhere(["hardware_serial"=>$sensorlist[$i]["hardware_serial"]])->asArray()->all();
					
				}else{
					
					$mreuslt=GpsHistory::find()->select("MIN(humidity_value) as {$suffix2}_min,MAX(humidity_value) as {$suffix2}_max,AVG(humidity_value) as {$suffix2}_avg,count(id) as {$suffix2}_total_samples")->where(['between','created_at',$start,$end])->andWhere(["hardware_serial"=>$sensorlist[$i]["hardware_serial"]])->asArray()->all();
				}
				
				$result[]=$mreuslt[0];
				
			}
			
			if($sensorType=="temperature")
			{
				
				$lresult=GpsHistory::find()->select("MIN(humidity_value) as e_min,MAX(humidity_value) as e_max,AVG(humidity_value) as e_avg,count(id) as e_total_samples")->where(['between','created_at',$begin->getTimestamp(),$g_end->getTimestamp()])->asArray()->all();
				$result[]=$lresult[0];
				
			}else{
				
				$lresult=GpsHistory::find()->select("MIN(humidity_value) as e_min,MAX(humidity_value) as e_max,AVG(humidity_value) as e_avg,count(id) as e_total_samples")->where(['between','created_at',$begin->getTimestamp(),$g_end->getTimestamp()])->asArray()->all();
				$result[]=$lresult[0];
			}
			
			$final=call_user_func_array("array_merge", $result);
			$daylist[]=$final;
			
			Yii::$app->db->createCommand()->insert("temp_table", $final)->execute();
			
		}
		
		
		return $daylist;
		
	}
	
	private function fourHoursReport($date='2018-05-28',$type='two',$devices,$colums,$sensor_type='temperature'){
		
		$colums=$colums;
		
		$begin = new \DateTime($date);
		//$begin->setTimestamp("1495231200");
		$end = new \DateTime($date);
		//$end->setTimestamp("1495231200");
		$end = $end->modify( '+1 day' );
		if($type=='two')
		{
			$interval = new \DateInterval('PT2H');
			
		}else{
			
			$interval = new \DateInterval('PT4H');
		}
		
		$daterange = new \DatePeriod($begin, $interval ,$end);
		$resultdate=[];
		$list=Devices::find()->select(['id','display_name as name','customer_id','group_id','config_new','hardware_serial'])->where(["id"=>$devices,'type'=>2])->asArray()->all();
		$_count=count($list);
		for ($i = 0; $i < $_count; $i++) {
			
			$result=[];
			$dev_config=[
					 "temperature_range"=>[
					 		"low"=>10,
					 		 "high"=>20
					 ],
					"humidity_range"=>[
							"low"=>10,
							"high"=>20
					]
			];			//Json::decode($list[$i]['config_new']);
			$result[]=$list[$i]['name'];
			
			if($sensor_type=='temperature')
			{
				$result[]=array_key_exists('temperature_range', $dev_config)?$dev_config['temperature_range']['low'].'-'.$dev_config['temperature_range']['high']:"";
			}else{
				
				$result[]=array_key_exists('humidity_range', $dev_config)?$dev_config['humidity_range']['low'].'-'.$dev_config['humidity_range']['high']:"";
			}
			
			
			$result[]=$begin->format("Y-m-d");
			$result[]=$list[$i]['customer_id'];
			$result[]=$list[$i]['group_id'];
			foreach($daterange as $date){
				
				$start=$date->getTimestamp();
				//echo $date->format("H:i")."  ";
				if($type=='two')
				{
					$date->modify('+2 hour');
				}else{
					
					$date->modify('+4 hour');
				}
				
				
				$end=$date->getTimestamp();
				//echo "   ".$date->format("H:i") . "<br>";
				//$local=DeviceTemperatureReading::find()->select("avg(temperature) as temperature")->where(['between','created_at',$start,$end])->asArray()->all();
				$query=new Query();
				$query->from("device_gps_history");
				$query->where(['between','created_at',$start,$end]);
				$query->andWhere(["hardware_serial"=>$list[$i]["hardware_serial"]]);
				
				if($sensor_type=='temperature')
				{
					$avg=$query->average('temperature_value');
					
				}else{
					
					$avg=$query->average('humidity_value');
				}
				
				
				if(is_null($avg))
				{
					array_push($result, 0);
				}else{
					array_push($result, $avg);
				}
				
			}
			
			Yii::$app->db->createCommand()->batchInsert("temp_table",$colums, [$result])->execute();
			
		}
		
		//$results=Yii::$app->db->createCommand("select * from temp_table")->queryAll();
		
		//return $results;
		
		
	}
	
	
}