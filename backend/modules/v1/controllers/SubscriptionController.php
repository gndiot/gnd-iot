<?php
namespace backend\modules\v1\controllers;
use yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use common\models\Constants;
use common\models\DeviceConfigTemperature;
use common\models\DeviceConfigHumidity;
use yii\web\NotFoundHttpException;
use common\models\Devices;
use yii\helpers\ArrayHelper;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\Json;
use backend\modules\v1\models\DeviceSubscription;
use yii\data\ActiveDataProvider;
use backend\modules\v1\models\SubscriptionSearch;

class SubscriptionController extends Controller{
	
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
					
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
			
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				'index' => ['GET','POST'],
				'view' => ['GET', 'HEAD'],
				'add' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
	public function actionIndex(){
		
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			//$list = DeviceSubscription::find();
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new SubscriptionSearch();
			
			return $dataProvider->search($filter);
			
		}else{
		
			$user= Yii::$app->user->identity;
			
			$customer_id=$user->customer_id;
			
			$user_groups=explode("|",$user->group_ids);
			
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new SubscriptionSearch();
			
			return $dataProvider->searchGroups($filter,$user_groups,$customer_id);
		}
		
	}
	
	public function actionAdd(){
			 
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$subscription = new DeviceSubscription();
		
		$subscription->loadDefaultValues();
		$subscription->load($rdata,'');
		$subscription->created_at=time();
		$subscription->updated_at=time();
		
		$subscription->np_days=implode("|", $subscription->np_days);
		
		
		if($subscription->attribute==Constants::ALERT_DOOR_OPEN || $subscription->attribute==Constants::ALERT_DOOR_CLOSE)
		{
			$subscription->condition_code=Constants::CODE_EQUAL_TO;
			$subscription->value_one = Constants::ALERT_DOOR_OPEN==$subscription->attribute ? 16:1;
		}
		
		
		if($subscription->save()){
			
			Yii::$app->getResponse()->setStatusCode(201);
			
			return [];
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			
			 return $subscription->getErrors();
		}
		
		
		
		
		return $subscription;
		
	}
	
	public function actionUpdate($id){
		
		 $id=Yii::$app->utils->decrypt($id);
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			$subscription=DeviceSubscription::find()->where(["id"=>$id])->one();
			
		}else{
			
			$subscription=DeviceSubscription::find()->where(["id"=>$id])->one();
		}
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		if(is_null($subscription))
		{
			
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		$datetime=new \DateTime($subscription->created_at);
		$subscription->created_at=$datetime->getTimestamp();
		$subscription->load($rdata,'');
		
		if($subscription->attribute==Constants::ALERT_DOOR_OPEN || $subscription->attribute==Constants::ALERT_DOOR_CLOSE)
		{
			$subscription->condition_code=Constants::CODE_EQUAL_TO;
			$subscription->value_one = Constants::ALERT_DOOR_OPEN==$subscription->attribute ? 16:1;
		}
		$subscription->updated_at=time();

		$subscription->np_days=implode("|", $subscription->np_days);
		
		Yii::trace($subscription->np_days);
		
		if($subscription->save())
		{
			Yii::$app->getResponse()->setStatusCode(201);
			return $subscription;
			 
		}else{
		
			Yii::$app->getResponse()->setStatusCode(422);
			
			return $subscription->getErrors();
		}
	}
	
	public function actionView($id){
		
		$id=Yii::$app->utils->decrypt($id);
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			$model=DeviceSubscription::find()->where(["id"=>$id])->one();
			
		}else{
			
			$model=DeviceSubscription::find()->where(["id"=>$id])->one();
			
		}
		if(is_null($model))
		{
			  
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		return $model;
		 
		
	}
	
	public function actionDelete($id){
		$id=Yii::$app->utils->decrypt($id);
		
		$model=DeviceSubscription::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
				
			throw new NotFoundHttpException("Object not found: $id");
		}
		
		 return $model->delete();
		
	}
	
	
	
}