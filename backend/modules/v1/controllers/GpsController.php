<?php
namespace backend\modules\v1\controllers;
use yii;
use yii\rest\Controller;
use yii\filters\AccessControl;


use backend\modules\gps\models\GpsSearch;
use backend\modules\gps\models\GpsConfigurationsTemp;
use backend\modules\gps\models\GpsConfigurations;
use console\tasks\GpsDownlink;
use backend\modules\gps\models\GpsRecentHistory;
use backend\modules\v1\models\GpsHistorySearch;
use backend\modules\gps\models\GpsSubscriptionSearch;
use backend\modules\gps\models\GpsSubscriptions;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use backend\modules\api\models\DeviceGroups;
use backend\modules\gps\models\GpsZone;
use backend\modules\gps\models\GpsZoneSearch;
use backend\modules\gps\models\GpsZoneAlertSearch;
use backend\modules\gps\models\GpsZoneAlert;


class GpsController extends Controller{
	
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	
	public function behaviors()
	{
		
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'allow' => true,
										'roles' => ['@'],
								],
						],
				],
		];
	}
	

	public function actionIndex(){
		
		$filter=Yii::$app->getRequest()->getBodyParams();
		$dataProvider = new GpsSearch();
		
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			return $dataProvider->Adminsearch($filter);
			
		}else{
			
		
			$user= Yii::$app->user->identity;
			
			$customer_id=$user->customer_id;
			
			$user_groups=$user->group_ids;
		
			
			return $dataProvider->search($filter,$customer_id,$user_groups);
		}
		
	}
	
	
	public function actionConfiguration($id){
		
		$configurations=GpsConfigurationsTemp::find()->where(["device_id"=>$id])->one();
		if(is_null($configurations))
		{
					
			return "";
		}else{
			
			return $configurations;
		}
		
	}
	
	
	public function actionLive($id){
		
		    $live=GpsRecentHistory::find()->where(["device_id"=>$id])->one();
		    return $live;
	}
	
	
	public function actionConfigurationUpdate($id){
		
// 		  $configuration=
		$configurations=GpsConfigurationsTemp::find()->where(["device_id"=>$id])->one();
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(is_null($configurations))
		{
			
			$configurations=new GpsConfigurationsTemp();
			$configurations->device_id=$id;
			$configurations->load($rdata,"");
			$configurations->updated_at=time();
			if($configurations->save())
			{
				
				$jobs = new GpsDownlink(["type"=>"Temperature","devices"=>[$id],"data"=>ArrayHelper::toArray($configurations)]);
				$queue= Yii::$app->queue->push($jobs);
				
				return $configurations;
			
			}else{
				Yii::$app->getResponse()->setStatusCode(422);
				 return $configurations->getErrors();
			}
			
			
		}else{
			
			$configurations->load($rdata,"");
			$configurations->updated_at=time();
			if($configurations->save())
			{
				
				$jobs = new GpsDownlink(["type"=>"Temperature","devices"=>[$id],"data"=>ArrayHelper::toArray($configurations)]);
				$queue= Yii::$app->queue->push($jobs);
				
				return $configurations;
				
			}else{
				 Yii::$app->getResponse()->setStatusCode(422);
				return $configurations->getErrors();
			}
 			
		}
		
		
	}
	
	public function actionMultipleConfiguration(){
		
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		if(!array_key_exists("devices", $rdata) && !array_key_exists("config", $rdata)){
			
			return false;
		}
	
		if(!empty($rdata["devices"]) && !empty($rdata["config"]))
		{
			//$jobs = new DownlinkJob(["type"=>"Temperature","devices"=>$rdata["devices"],"data"=>$rdata["config"]]);
			//$queue= Yii::$app->queue->push($jobs);
			//Devices::updateAll(["config_status"=>10],["id"=>$rdata["devices"]]);
			
			return ["ok"];
		}
		
	}
	
	
	public function actionHistory($device_id,$from,$to){
		
		     $search=new GpsHistorySearch();
			 $search->device_id=$device_id;
			 $search->from=$from;
			 $search->to=$to;
		     if($search->validate())
		     {
		     	return $search->getHistory();
		     }else{
		     	
		     	  Yii::$app->getResponse()->setStatusCode(422);
		     	 return $search->getErrors();
		     	
		     }
		 
	}
	
	
	
	public function actionSubscriptions(){
		
		
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			//$list = DeviceSubscription::find();
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new GpsSubscriptionSearch();
			
			return $dataProvider->search($filter);
			
		}else{
			
			$user= Yii::$app->user->identity;
			
			$customer_id=$user->customer_id;
			
			$user_groups=explode("|",$user->group_ids);
			
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new GpsSubscriptionSearch();
			
			return $dataProvider->searchGroups($filter,$user_groups,$customer_id);
		}
		
	}
	
	public function actionSubscriptionAdd(){
		
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$subscription = new GpsSubscriptions();
		
		$subscription->loadDefaultValues();
		$subscription->load($rdata,'');
		$subscription->created_at=time();
		$subscription->updated_at=time();
		
		$subscription->np_days=implode("|", $subscription->np_days);
		if($subscription->save()){
			
			Yii::$app->getResponse()->setStatusCode(201);
			
			return [];
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			
			return $subscription->getErrors();
		}
		
		return $subscription;
		
	}
	
	
	public function actionSubscriptionUpdate($id){
		
		$id=Yii::$app->utils->decrypt($id);
		
		 $model= GpsSubscriptions::find()->where(["id"=>$id])->one();
		 if(is_null($model))
		 {
		 	 throw new NotFoundHttpException();
		 }
		 
		 $rdata=Yii::$app->getRequest()->getBodyParams();
		 $datetime=new \DateTime($model->created_at);
		 $model->created_at=$datetime->getTimestamp();
		 $model->load($rdata,"");
		 
		 $model->updated_at=time();
		 
		 $model->np_days=implode("|", $model->np_days);
		
		 
		 if($model->save())
		 {
		 	
		 	return $model;
		 	
		 }else{
		 	
		 	 Yii::$app->getResponse()->setStatusCode(422);
		 	 return $model->getErrors();
		 	 
		 }
		
	}
	
	public function actionSubscriptionView($id){
		
		$id=Yii::$app->utils->decrypt($id);
		
		$model= GpsSubscriptions::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			throw new NotFoundHttpException();
		}
		
		return $model;
		
		
	}
	
	public function actionSubscriptionDelete($id){
		
		$id=Yii::$app->utils->decrypt($id);
		$model= GpsSubscriptions::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			throw new NotFoundHttpException();
		}
		
		return $model->delete();
		
		
	}
	
	public function actionAlerts(){
		
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			//$list = DeviceSubscription::find();
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new GpsZoneAlertSearch();
			
			return $dataProvider->search($filter);
			
		}else{
			
			$user= Yii::$app->user->identity;
			
			$customer_id=$user->customer_id;
			
			$user_groups=explode("|",$user->group_ids);
			
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new GpsZoneAlertSearch();
			
			return $dataProvider->searchGroups($filter,$user_groups,$customer_id);
		}
		
	}
	
	
    public function actionZoneList($type){
    	
    	 return GpsZone::find()->select(["id","name"])->where(["zone_type"=>$type])->all();
    	
    }
	
	public function actionAlertAdd(){
		
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$model=new GpsZoneAlert();
		
		$model->loadDefaultValues();
		$model->load($rdata,'');
		$model->created_at=time();
		$model->updated_at=time();
		
	//	$subscription->np_days=implode("|", $subscription->np_days);
		if($model->save()){
			
			Yii::$app->getResponse()->setStatusCode(201);
			
			return [];
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			
			return $model->getErrors();
		}
		
		return $model;
		
		
	}
	
	public function actionAlertView($id){
		
		
		$model= GpsZoneAlert::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			throw new NotFoundHttpException();
		}
		
		return $model;
		
	}
	
	public function actionAlertDelete($id){
		
		$model= GpsZoneAlert::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			throw new NotFoundHttpException();
		}
		
		return $model->delete();
		
	}
	
	public function actionAlertUpdate($id){
	
		
		
		$model= GpsZoneAlert::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			throw new NotFoundHttpException();
		}
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$datetime=new \DateTime($model->created_at);
		$model->created_at=$datetime->getTimestamp();
		$model->load($rdata,"");
		
		$model->updated_at=time();
	
		
		if($model->save())
		{
			
			return $model;
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
			
		}
		
	}
	
	public function actionZones(){
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
			//$list = DeviceSubscription::find();
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new GpsZoneSearch();
			
			return $dataProvider->search($filter);
			
		}else{
			
			$user= Yii::$app->user->identity;
			
			$customer_id=$user->customer_id;
			
			$user_groups=explode("|",$user->group_ids);
			
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new GpsZoneSearch();
			
			return $dataProvider->searchGroups($filter,$user_groups,$customer_id);
		}
		
	}
	
	public function actionZoneAdd(){
		
		 $model= new GpsZone();
		 $rdata=Yii::$app->getRequest()->getBodyParams();
		 $model->load($rdata,"");
		 
		 $model->created_at=time();
		 if($model->save())
		 {
		 	return $model;
		 	
		 }else{
		 	Yii::$app->getResponse()->setStatusCode(422);
		 	 return $model->getErrors();	
		 }
		 
	}
	
	public function actionZoneUpdate($id){
		
		
		 $model= GpsZone::find()->where(["id"=>$id])->one();
		 
		 if(is_null($model))
		 {
		 	
		 	throw new NotFoundHttpException();
		 }
		 
		 $rdata=Yii::$app->getRequest()->getBodyParams();
		 
		 $model->load($rdata,"");
		 
		 $model->updated_at=time();
		 
		 if($model->save())
		 {
		 	return $model;
		 	
		 }else{
		 	
		 	Yii::$app->getResponse()->setStatusCode(422);
		 	return $model->getErrors();	
		 }
	
		
	}
	
	public function actionZoneView($id){
		
		$model= GpsZone::find()->where(["id"=>$id])->one();
		
		if(is_null($model))
		{
			
			throw new NotFoundHttpException();
		}
		
		return $model;
		
	}
	
	public function actionZoneDelete($id){
		
		$model= GpsZone::find()->where(["id"=>$id])->one();
		
		if(is_null($model))
		{
			
			throw new NotFoundHttpException();
		}
		
		return $model->delete();
		
	}
	
	
}