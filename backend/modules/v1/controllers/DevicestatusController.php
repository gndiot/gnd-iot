<?php

namespace backend\modules\v1\controllers;

use yii;
use backend\modules\v1\models\DeviceStatusSearch;
use backend\modules\v1\models\DeviceStatus;
use yii\web\NotFoundHttpException;

class DevicestatusController extends \yii\rest\Controller
{
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	
	protected function verbs(){
		
		return [
				'index' => ['GET','POST'],
				'view' => ['GET', 'HEAD'],
				'add' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
		
	}
	
	
    public function actionIndex()
    {
    	//$list = DeviceSubscription::find();
    	$filter=Yii::$app->getRequest()->getBodyParams();
    	
    	$dataProvider = new DeviceStatusSearch();
    	
    	return $dataProvider->search($filter);
    }
    
    public function actionCreate(){
    	
    	$rdata=Yii::$app->getRequest()->getBodyParams();
    
    	$model = new DeviceStatus();
    	$model->load($rdata,"");
    	$model->created_at=time();
    	if($model->save())
    	{
    		return $model;
    		
    	}else{
    		
    		 Yii::$app->getResponse()->setStatusCode(422);
    		return $model->getErrors();
    		
    	}

    }

    
    public function actionView($id){
    	
    	$rdata=Yii::$app->getRequest()->getBodyParams();
    	
    	$model=DeviceStatus::find()->where(["id"=>$id])->one();
    	if(is_null($model))
    	{
    		
    		throw new NotFoundHttpException("Object not found: $id");
    		
    	}
    	
    	return $model;
    	
    }
    
    public function actionUpdate($id){
    	
    	$rdata=Yii::$app->getRequest()->getBodyParams();
    	
    	 $model=DeviceStatus::find()->where(["id"=>$id])->one();
    	 if(is_null($model))
    	 {
    	 	
    	 	throw new NotFoundHttpException("Object not found: $id");
    	 	
    	 }
    	$model->load($rdata,"");
    	
    	$datetime=new \DateTime($model->created_at);
    	$model->created_at=$datetime->getTimestamp();
    	
    	
    	if($model->save())
    	{
    		return $model;
    		
    	}else{
    		
    		Yii::$app->getResponse()->setStatusCode(422);
    		return $model->getErrors();
    		
    	}
    	
    }
    
    public function actionDelete($id){
    	
    	$rdata=Yii::$app->getRequest()->getBodyParams();
    	
    	$model=DeviceStatus::find()->where(["id"=>$id])->one();
    	if(is_null($model))
    	{
    		
    		throw new NotFoundHttpException("Object not found: $id");
    		
    	}
    	return $model->delete();

    }
    
}
