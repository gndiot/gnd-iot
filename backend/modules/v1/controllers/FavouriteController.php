<?php

namespace backend\modules\v1\controllers;

use yii;
use backend\modules\gps\models\FavouriteLocationSearch;
use backend\modules\gps\models\FavouriteLocation;

class FavouriteController extends \yii\rest\Controller
{
   
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	
	protected function verbs(){
		
		return [
				'index' => ['GET','POST'],
				'view' => ['GET', 'HEAD'],
				'add' => ['POST'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
		
	}
	
	
	public function actionIndex()
	{
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			$dataProvider = new FavouriteLocationSearch();
			
			return $dataProvider->search($filter);
			
		}else{
			
			$user= Yii::$app->user->identity;
			
			$customer_id=$user->customer_id;
			
			$user_groups=explode("|",$user->group_ids);
			
			$filter=Yii::$app->getRequest()->getBodyParams();
			
			
			$dataProvider = new FavouriteLocationSearch();
			
			return $dataProvider->searchGroup($filter,$customer_id);
			
		}
		
		
	}
	
	public function actionCreate(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			$model = new FavouriteLocation();
			$model->load($rdata,"");
			$model->created_at=time();
			
		}else{
			
			$model = new FavouriteLocation();
			$model->load($rdata,"");
			$user= Yii::$app->user->identity;
			$customer_id=$user->customer_id;
			$model->customer_id=$customer_id;
			$model->created_at=time();
			
		}
		
		
		if($model->save())
		{
			return $model;
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
			
		}
		
	}
	
	
	public function actionView($id){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$model=FavouriteLocation::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			
			throw new NotFoundHttpException("Object not found: $id");
			
		}
		
		return $model;
		
	}
	
	public function actionUpdate($id){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$model=FavouriteLocation::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			
			throw new NotFoundHttpException("Object not found: $id");
			
		}
		$model->load($rdata,"");
		
		if(Yii::$app->user->identity->hasRole('super user'))
		{
			
		}else{
			
			$user= Yii::$app->user->identity;
			$customer_id=$user->customer_id;
			$model->customer_id=$customer_id;
		}
		
		
		if($model->save())
		{
			return $model;
			
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return $model->getErrors();
			
		}
		
	}
	
	public function actionDelete($id){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$model=FavouriteLocation::find()->where(["id"=>$id])->one();
		if(is_null($model))
		{
			
			throw new NotFoundHttpException("Object not found: $id");
			
		}
		return $model->delete();
		
	}

}
