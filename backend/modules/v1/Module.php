<?php

namespace backend\modules\v1;

use yii;
use backend\models\AdminUser;
use yii\web\Response;
/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\v1\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->registerComponent();
        
        
        if(Yii::$app->user->isGuest)
        {
        	if(!YII_ENV_DEV)
        	{
        		return ;
        	}
        	
        	Yii::$app->user->enableSession=false;
        	Yii::$app->user->enableAutoLogin=false;
        	Yii::$app->user->loginUrl=null;
        	$token=Yii::$app->request->get("token","");
        	$user=AdminUser::find()->where(["auth_key"=>$token])->one();
        	if(!is_null($user))
        	{
        		
        		
        		Yii::$app->user->login($user);
        	}
        	
        	
        }
        
        $user = Yii::$app->user->identity;
        if ($user && $user->timezone !='') {
        	
        	$offset=Yii::$app->utils->getTimezoneName($user->getTimezone());
        	
        	Yii::$app->setTimeZone($offset);
        }else{
        	
        	$offset=Yii::$app->utils->getTimezoneName('+05:30');
        	
        	Yii::$app->setTimeZone($offset);
        	
        }
        // custom initialization code goes here
    }
    
    
    
    private function registerComponent(){
    	
    	Yii::$app->setComponents([
    			'response' => [
    					'class' => 'yii\web\Response',
    					'format' => yii\web\Response::FORMAT_JSON,
    					'formatters' => [
    							'application/json' => yii\web\Response::FORMAT_JSON,
    							'application/xml' => yii\web\Response::FORMAT_XML,
    					],
    					'charset' => 'UTF-8',
    					'on beforeSend' => function ($event) {
    					$response = $event->sender;
    					if ($response->data !== null) {
    						if($response->isSuccessful)
    						{
    							$response->data = [
    									'success' => $response->isSuccessful,
    									'data' => $response->data,
    									'statusCode'=>$response->statusCode,
    									'error'=>null
    							];
    							
    						}else{
    							
    							$response->data = [
    									'success' => $response->isSuccessful,
    									'data' =>null,
    									'statusCode'=>$response->statusCode,
    									'error'=> $response->data,
    							];
    							
    							
    						}
    						$response->statusCode = 200;
    					}
    					},
    					]
    					]);
    	
    }
}
