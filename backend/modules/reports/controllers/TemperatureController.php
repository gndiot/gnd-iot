<?php

namespace backend\modules\reports\controllers;

use yii;
use yii\web\Controller;

/**
 * Default controller for the `reports` module
 */
class TemperatureController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	
    	 $path=Yii::getAlias("@backend")."/views/site";
    	 $this->viewPath=$path;
    	
    	return $this->render('index');
    }
}
