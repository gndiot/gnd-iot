<?php

namespace backend\modules\service\controllers;

use yii\rest\Controller;

/**
 * Default controller for the `service` module
 */
class TtnController extends Controller
{
	/**
	 * Renders the index view for the module
	 * @return string
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}
	
	
	
}
