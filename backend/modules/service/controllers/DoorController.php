<?php
namespace backend\modules\service\controllers;

use yii;
use yii\rest\Controller;
use common\models\DeviceDoorReading;
use console\tasks\DoorOpen;
use yii\helpers\Json;
use common\models\Constants;
use backend\modules\api\models\Devices;
use common\models\DeviceSubscription;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\filters\AccessControl;
use backend\models\AdminUser;
use common\models\TimeZone;
use common\models\DeviceAlert;
use common\models\NotificationTemplate;


class DoorController extends Controller{
	
	
	public function behaviors(){
		
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								//'ips' => YII_ENV_DEV ? ['127.0.0.1']:['166.62.10.190','192.185.129.139'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];

		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
					
				'push' => ['POST'],
				'test' => ['POST'],
		];
	
	}
		
   public function actionPush(){
   	
   	     $model = new DeviceDoorReading();
   	     $rdata=Yii::$app->getRequest()->getBodyParams();
   	     $model->load($rdata,"");
   	     self::customLog("Push Api Calling...");
   	     self::customLog(json_encode($rdata));
   	     if($model->save())
   	     {
   	     	self::customLog("Alert Checking Begin...");
   	     	 $this->checkingAlerts($model);
   	     	 //Yii::$app->queue->push(new DoorOpen(["id"=>1]));
   	     	 self::customLog("Alert Checking end...");
   	      	return "ok";
   	      	
   	     		
   	     }else{
   	     
   	     	return $model->getErrors();
   	     		
   	     }
   }
   
   public function actionHeartbeat(){
   	
   		$rdata=Yii::$app->getRequest()->getBodyParams();
   		return $rdata;
   	
   }
   
   public function actionNotification($s_id,$device_id){
   	
   		self::customLog("callback function called...");
   		$subscription= DeviceSubscription::find()->where(["id"=>$s_id])->one();
   		
   	 	 $status=0;
   	 	 
   	 	  if($subscription->attribute == Constants::ALERT_DOOR_CLOSE)
   	 	  {
   	 	  	
   	 	  	$status=$this->isDoorClose($device_id);
   	 	  
   	 	  }elseif ($subscription->attribute==Constants::ALERT_DOOR_OPEN)
   	 	  {
   	 	  	$status=$this->isDoorOpen($device_id);
   	 	  }
   	 	 
   	
   		if($status)
   		{
   			self::customLog("Alert still available");
   			$device=$device=Devices::find()->where(["id"=>$device_id])->one(); 
   			
   			$reading=DeviceDoorReading::find()->where(["device_id"=>$device_id])->orderBy(["id"=>SORT_DESC])->one();
   			$timezone=$this->getTimezone($device->group_id);
   			$datetime=Yii::$app->utils->getDateTime($reading->created_at,$timezone);
   			
   			
   			
   			if($subscription->emails!="")
   			{
   				$emails=explode("|", $subscription->emails);
   				
   			}else{
   				
   				$emails=[];
   			}
   			
   			
   			$email_content=[
   					 
   					"sensor_name"=>$device->display_name,
   					"sensor_display_name"=>$device->display_name,
   					"alert_type"=>$subscription->attribute,
   					"sensor_value"=>($reading->status==16)?'open':'close',
   					"alert_time"=>$datetime->format('Y-m-d h:i:s'),
   					"emails"=>$emails,
   					"mobile"=>$subscription->numbers
   			];
   			
   			$notification_content=[
   					"value"=>($reading->status==16)?'open':'close',
   					"battery"=>$reading->battery
   			];
   			
   			$this->saveNotification($device, $subscription->attribute, $notification_content);
   			
   			self::customLog("Preparing Email and sms alers");
   			
   			self::customLog(json_encode($email_content));
   			
   			 if($subscription->is_email==1)
   			 {
   			 	self::customLog(json_encode("Email Alert sending...."));
   			 	$this->dynamicEmail($email_content);
   			 	
   			 }
   			 
   			 if($subscription->is_sms==1)
   			 {
   			 	self::customLog(json_encode("SMS Alert sending...."));
   			 	$this->sendSMS($email_content);
   			 	
   			 }
   			 
   			 
   			 
   			if($subscription->intervel_mins > 0)
   			{
   				$push_url=Url::to(["notification",'s_id'=>$s_id,'device_id'=>$device_id],true);
   				$this->makeRequest([
   						"delay"=>$subscription->intervel_mins * 60000,
   						"hostname"=>$push_url,
   						"data"=>['company'=>""],
   						"method"=>"POST"
   				]);
   				
   				//$id=Yii::$app->queue->delay($subscription->intervel_mins * 60)->push(new DoorOpen(['push_url'=>$push_url]));
   				$subscription->last_run_time=time();
   				//$subscription->job_id=$id;
   			}
   			
   		}else{
   				
   			$subscription->job_id=0;
   				
   		}
   		
   		$subscription->save(false);

   }
   
   public function actionTest(){
   	
		   	$server=Yii::$app->request->getRawBody();
		   	$decode=Json::decode($server,true);
		   	$door= new DeviceDoorReading();
		   	$door->hardware_serial=$decode["hardware_serial"];
		   	$door->payload_raw=$decode["payload_raw"];
		   	
		   	
		   	if(array_key_exists('payload_fields', $decode))
		   	{
		   		$door->battery=$decode["payload_fields"]["battery"];
		   		$door->status=$decode["payload_fields"]["status"];
		   		$door->created_at=time();
		   		$door->save(false);
		   			
		   		return ["ok"];
		   		
		   	}else{
		   		
		   		 return ["invlid payloads"];
		   	}
		   	
   	
   }
   
   
   public function isDoorClose(){
   	
   	$reading=DeviceDoorReading::find()->where(["device_id"=>$device_id])->orderBy(["id"=>SORT_DESC])->one();
   	
   	if(is_null($reading))
   	{
   		 
   		return false;
   		 
   	}else{
   		 
   		if($reading->status==1)
   		{
   			return true;
   	
   		}else{
   			return false;
   		}
   		 
   	}
   			
   	
   }
   
   public function isDoorOpen($device_id){
   	
	   	
	   	$reading=DeviceDoorReading::find()->where(["device_id"=>$device_id])->orderBy(["id"=>SORT_DESC])->one();
	   	 
	   	if(is_null($reading))
	   	{
	   	
	   		return false;
	   	
	   	}else{
	   	
	   		if($reading->status==16)
	   		{
	   			return true;
	   			 
	   		}else{
	   			return false;
	   		}
	   	
	   	}
   	
   	
   }


   public function checkingAlerts($model){
   
   	$device=Devices::find()->where(["hardware_serial"=>$model->hardware_serial])->one();
   		
   	if(is_null($device))
   	{
   		self::customLog("Device Not Found... {$model->hardware_serial}");
   		return ;
   
   	}
   	
   	
   	$m_data=[
   			"status"=>floatval($model->status),
   			"battery"=>intval($model->battery)
   	];
   	
   	$device->data=Json::encode($m_data);
   	$device->battery_level=intval($model->battery);
   	$device->save(false);
   	
   	$subscriptions=DeviceSubscription::find()->where(["notification_status"=>DeviceSubscription::STATUS_ACTIVE])->andWhere(["REGEXP","device_ids",'[[:<:]]'.$device->id.'[[:>:]]'])->andWhere(["attribute"=>[Constants::ALERT_DOOR_CLOSE,Constants::ALERT_DOOR_OPEN,Constants::ALERT_BATTERY]])->all();
   	$count=count($subscriptions);   		
   	if($count > 0)
   	{
   		self::customLog("This {$model->hardware_serial} device has subscription.. ");
   
   	}else{
   
   		 
   	}
   		
   	foreach ($subscriptions as $subscription)
   	{	
   	
   		if($this->hasAlert($subscription->attribute, $subscription->condition_code, $model, $subscription->value_one, $subscription->value_two))
   		{
   			self::customLog("This device has Alert ");
   			
   			if($subscription->emails!="")
   			{
   				$emails=explode("|", $subscription->emails);
   				
   			}else{
   				
   				$emails=[];
   			}
   			
   			$email_content=[
   
   					"sensor_name"=>is_null($device->name)?"":$device->name,
   					"sensor_display_name"=>is_null($device->display_name)?"not set":$device->display_name,
   					"alert_type"=>$subscription->attribute,
   					"sensor_value"=>($model->status==16)?'open':'close',
   					"alert_time"=>date('Y-m-d h:i:s',$model->created_at),
   					"emails"=>$emails,
   					"mobile"=>$subscription->numbers
   			];
   			
   			self::customLog("Preparing Email and SMS Content ");
   			
   			self::customLog(json_encode($email_content));
   			
   			Yii::trace("email content");
   			Yii::trace($email_content);
   			
   			$notification_content=[
   					"value"=>($model->status==16)?'open':'close',
   					"battery"=>$model->battery
   			];
   			  			
   			if($subscription->intervel_mins > 0)
   			{
   				
   				self::customLog("device delay time update");
   				$push_url=Url::to(["notification",'s_id'=>$subscription->id,'device_id'=>$device->id],true);

   				$this->makeRequest([
   						"delay"=>$subscription->intervel_mins * 60000,
   						"hostname"=>$push_url,
   						"data"=>['company'=>""],
   						"method"=>"POST"
   				]);
   				
   				//$id=Yii::$app->queue->delay($subscription->intervel_mins * 60)->push(new DoorOpen(['push_url'=>$push_url]));
   				//$subscription->job_id=$id;
   				
   				// echo "job added";
   				
   			}else{
   				
   				if($subscription->is_email==1)
   				{
   					self::customLog(json_encode("Email Alert sending...."));
   					$this->dynamicEmail($email_content);
   					
   				}
   				
   				if($subscription->is_sms==1)
   				{
   					self::customLog(json_encode("SMS Alert sending...."));
   					$this->sendSMS($email_content);
   					
   				}
   				
   				$this->saveNotification($device, $subscription->attribute, $notification_content);
   				//$this->sendEmail($email_content,$subscription->attribute);
   				
   				//echo "Notification Send To their mail ids";
   			
   			}
   				
   			$subscription->last_run_time=time();
   			$subscription->save(false);
   			

   			 
   		}else{
   
   			//echo "Device Has no Alert \n";
   			 
   		}
   	}
   
   }
   
   
   public function hasAlert($alertType,$condition,$value,$from,$to){
   
   	switch ($alertType)
   	{
   		case Constants::ALERT_HIGH_TEMPERATURE:
   
   			return Constants::conditionChecking($condition,$value->temperature,$from,$to);
   
   			break;
   		case  Constants::ALERT_LOW_TEMPERATURE:
   
   			return Constants::conditionChecking($condition,$value->temperature,$from,$to);
   
   			break;
   		case Constants::ALERT_NORMAL_TEMPERATURE:
   			return Constants::conditionChecking($condition,$value->temperature,$from,$to);
   			break;
   				
   		case Constants::ALERT_HIGH_HUMIDITY:
   
   			return Constants::conditionChecking($condition,$value->humidity,$from,$to);
   
   			break;
   		case  Constants::ALERT_LOW_HUMIDITY:
   
   			return Constants::conditionChecking($condition,$value->humidity,$from,$to);
   
   			break;
   		case Constants::ALERT_NORMAL_HUMIDITY;
   			return Constants::conditionChecking($condition,$value->humidity,$from,$to);
   		break;
   		
   		case Constants::ALERT_DOOR_OPEN:
   			return Constants::conditionChecking($condition,$value->status,16);
   		break;
   		case Constants::ALERT_DOOR_CLOSE:
   			return Constants::conditionChecking($condition,$value->status,0);
   		break;
   			
   		default:
   
   			return false;
   
   			break;
   				
   				
   	}
   
   
   
   }
   
   
   public function sendEmail($email,$alert_type=""){
  
   	
   	  switch ($alert_type)
   	  {
   	  	
   	  	 case Constants::ALERT_DOOR_OPEN:
   	  	 	
	   	  	 	Yii::$app->mailer->compose('alert',['device_id'=>$email["sensor_name"],"notification_time"=>$email["alert_time"],"type"=>$email["alert_type"],"value"=>$email["sensor_value"]])
	   	  	 	->setFrom(Yii::$app->params["adminEmail"])
	   	  	 	->setTo($email["emails"])
	   	  	 	->setSubject("Device {$email["alert_type"]} alert {$email["sensor_name"]}")
	   	  	 	->send();
	   	  	 	
   	  	  break;
   	  	  
   	  	  case Constants::ALERT_DOOR_CLOSE:
   	  	  	
   	  	  		
	   	  	  	Yii::$app->mailer->compose('doorclose',['device_id'=>$email["sensor_name"],"notification_time"=>$email["alert_time"],"type"=>$email["alert_type"],"value"=>$email["sensor_value"]])
	   	  	  	->setFrom(Yii::$app->params["adminEmail"])
	   	  	  	->setTo($email["emails"])
	   	  	  	->setSubject("Device {$email["alert_type"]} alert {$email["sensor_name"]}")
	   	  	  	->send();
   	  	  	
   	  	  	
   	  	  	break;
   	  	  	
   	  	  case Constants::ALERT_LOW_BATTERY:
   	  	  	
   	  	  				
   	  	  	
   	  	  break;
   	  	
   	  	
   	  }
   	
   
   }
   
   public function sendSMS($alert){
   
	   	$template=NotificationTemplate::find()->where(["alert_type"=>$alert['alert_type'],"template_type"=>"sms"])->one();
	   	
	   	if(is_null($template))
	   	{
	   		//$this->sendEmail($email);
	   		return;
	   	}
	   	
	   	$html_content=$this->stringreplace($alert,$template->content_body);
	   	
	   	Yii::trace("Dynamic sms");
	   	self::customLog("Preparing Dynamic SMS");
	   	self::customLog($html_content);
	   	
	   	$mbarr=explode("|", $alert["mobile"]);
	   	
	   	$response=Yii::$app->sms->sendMultipleSms($mbarr,$html_content);
	   	
	   	self::customLog(json_encode($response));
	   	
	   	Yii::trace($response);
	   	Yii::trace("Dynamic sms respone data");
   
   }
   
   
   public function dynamicEmail($email){
   	
   	
   	$template=NotificationTemplate::find()->where(["alert_type"=>$email['alert_type'],"template_type"=>"email"])->one();
   	
   	if(is_null($template))
   	{
   		//$this->sendEmail($email,$email['alert_type']);
   		return;
   		
   	}
   	
   	$subject=$this->stringreplace($email,$template->subject);
   	$html_content=$this->stringreplace($email,$template->content_body);
   	
  
   	
   	
   	Yii::$app->mailer->compose()
   	->setFrom(Yii::$app->params["alertEmail"])
   	->setTo($email["emails"])
   	->setSubject($subject)
   	->setHtmlBody($html_content)
   	->send();
   	
   	
   }
   
   public function saveNotification($device,$alertType,$content="",$payload=""){
   	 
   	$alerts=new DeviceAlert();
   	$alerts->device_id=$device->id;
   	$alerts->group_id=$device->group_id;
   	$alerts->customer_id=$device->customer_id;
   	$alerts->alert_type=$alertType;
   	$alerts->sms=0;
   	$alerts->body=$payload;
   	$alerts->payload=json_encode($content);
   	$alerts->status=DeviceAlert::STATUS_NOT_ACK;
   	$alerts->created_at=time();
   	$alerts->save(false);
   
   }
	
   
   
   public function actionHttp(){
   	
   	 	
	   	$client= new Client();
	   	$response=$client->createRequest()->setMethod("GET")->setUrl("http://gndadmin.test/service/door/notification?s_id=3&device_id=3")->send();
	   	
	   	if($response->isOk)
	   	{
	   			
	   	   print_r($response);
	   			
	   	}else{
	   		
	   		
	   		 print_r($response);
	   	}
   	
   	
   }
   
   
   private function getTimezone($group_id){
   	
   		$user=AdminUser::find()->select(['timezone'])->where(['group_id'=>$group_id])->asArray()->one();
   		
   		 if(!empty($user))
   		 {
   		 	
   		 	 $offset=TimeZone::find()->where(['id'=>$user['timezone']])->asArray()->one();
   		 	  if(!empty($offset))
   		 	  {
   		 	  		
   		 	  	return Yii::$app->utils->getTimezoneName($offset['gmt']);
   		 	  	
   		 	  }
   		 	 
   		 }
   		 
   		 return Yii::$app->utils->getTimezoneName('');
   }
   
   public function stringreplace($variables,$content){
   	
   	$response = preg_replace_callback('/{(.+?)}/ix',function($match)use($variables){
   		return !empty($variables[$match[1]]) ? $variables[$match[1]] : $match[0];
   	},$content);
   		
   		return $response;
   }
   
   
   public function makeRequest($data){
   	
			 Yii::trace('post data -------');
			 Yii::trace($data);
			 $url=Yii::$app->params["task_scheduler_url"];
			 $client = new Client();
			 $response=$client->createRequest()->setFormat(Client::FORMAT_JSON)->setMethod('POST')->setUrl($url)->setData($data)->send();
			 if($response->isOk){
			 	
			 }
   }
   
   public static function customLog($log="",$type="door"){
   	
   	
   	return false;
   	if($log==""){
   		
   		return false;
   	}
   	
   	$path=Yii::getAlias('@runtime').'/gps';
   	\yii\helpers\FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
   	
   	//file_put_contents($path."/".$type.".log", $log."\n",FILE_APPEND);
   	
   }
	
}