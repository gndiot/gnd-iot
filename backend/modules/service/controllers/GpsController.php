<?php
namespace backend\modules\service\controllers;

use yii;
use yii\rest\Controller;
use common\models\DeviceGpsReading;
use yii\helpers\Json;
use yii\filters\AccessControl;
use backend\modules\gps\models\GpsHistory;
use backend\modules\gps\models\GpsRecentHistory;
use common\models\Devices;
use backend\modules\gps\models\GpsSubscriptions;
use common\models\Constants;
use backend\models\AdminUser;
use common\models\NotificationTemplate;
use common\models\DeviceAlert;
use backend\modules\gps\models\GpsZoneAlert;
use backend\modules\gps\models\GpsZone;
use backend\libs\Polygon;
use backend\modules\gps\models\GpsZoneNotification;


class GpsController extends Controller{
	
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								//'ips' => YII_ENV_DEV ? ['127.0.0.1']:['166.62.10.190','192.185.129.139'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		
		
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
					
				'push' => ['POST'],
				'test' => ['POST'],
		];
	
	}
	
		
   public function actionPush(){
   	
   	     $history=new GpsHistory();     
   	
   	     $rdata=Yii::$app->getRequest()->getBodyParams();
   	     
   	     $device=Devices::find()->where(['hardware_serial'=>$rdata["hardware_serial"],'status'=>10])->one();
   	     
   	     if(is_null($device))
   	     {
   	     	Yii::trace("Device not found.");
   	     	
   	     	return false;
   	     }
   	     
   	     
   	     //This is not good to restrict coordinate over here
   	     //this is only for rajtech
   	     if($rdata["latitude"] > 1 && $rdata["longitude"]  > 40)
   	     {
   	     	
   	     	
   	     }else{
   	     	
   	     	
   	     	$log="INVALID COORDINATES {$rdata["hardware_serial"]} {$rdata["latitude"]} {$rdata["longitude"]} TIME {$rdata["updated_at"]} ";
   	     	$this->testEmail($log,"GPS_LOG");
   	     	
   	     	return ["invalid coordinate"];
   	     }
   	     
   	     
   	     $history->load($rdata,"");
   	     $history->device_id=$device->id;
   	     $history->created_at=$rdata["updated_at"];
   	     $recentData=GpsRecentHistory::find()->where(["device_id"=>$device->id])->one();
   	     
   	     if(is_null($recentData))
   	     {
   	     	$recentData= new GpsRecentHistory();
   	     	$recentData->device_id=$device->id;
   	     	$recentData->load($rdata,"");
   	     	$recentData->save(false);
   	     	
   	     	if($rdata["temperature_value"] ==0 || $rdata["humidity_value"] ==0 || $rdata["longitude"] ==0 || $rdata["latitude"]==0)
   	     	{
   	     		return false;
   	     	}
   	     	
   	     	$device->sync_time=$history->created_at;
   	     	$device->save(false);
   	     	
   	     	$this->checkAlert($device,$recentData);
   	     	
   	     }else{
   	     	
   	     	
   	     	
   	     	$lastTemp=$recentData->temperature_value;
   	     	$lastHumidity=$recentData->humidity_value;
   	     	
   	     	
   	     	$recentData->load($rdata,"");
   	     
   	     	
   	     	if($rdata["temperature_value"]!=0)
   	     	{
   	     		if($recentData->humidity_value==0){
   	     			$recentData->humidity_value=$lastHumidity;
   	     		}
   	     		
   	     		$recentData->save(false);
   	     		$device->sync_time=$history->created_at;
   	     		$device->save(false);
   	     		
   	     	}else{
   	     		
   	     		
   	     		$recentData->temperature_value=$lastTemp;
   	     		if($recentData->humidity_value==0){
   	     			 $recentData->humidity_value=$lastHumidity;
   	     		}
   	     		
   	     		$history->temperature_value=$recentData->temperature_value;
   	     		$history->humidity_value=$recentData->humidity_value;
   	     		$recentData->save(false);
   	     		$device->sync_time=$history->created_at;
   	     		$device->save(false);
   	     		
   	     	
   	     	
   	     	}
   	     	
   	     	
   	     	
   	     }
   	    
   	     if($history->save())
   	     {
   	     	
   	      // $log=" {$history->temperature_value}:{$rdata["temperature_value"]} {$history->humidity_value}:{$rdata["humidity_value"]}";
   	    
   	      //$this->testEmail($log,"GPS_LOG");
   	     	
   	     	$this->checkAlert($device,$history);
   	     	$this->geoFenceAlert($device,$history);
   	     	return ["ok"];
   	     	
   	     }else{
   	     	
   	     	return $history->getErrors();
   	     	
   	     } 
   	    
   	     return $rdata; 	
   }
   
   
   public function geoFenceAlert($device,$history){
   	
   	  $currentDate=new \DateTime();
   	  
   	  $subscriptions=GpsZoneAlert::find()->andWhere(["REGEXP","device_ids",'[[:<:]]'.$device->id.'[[:>:]]'])->all();
   	  
   	  foreach ($subscriptions as $subscription)
   	  {
   	  	  
   	  	$zone=GpsZone::find()->where(["id"=>$subscription->zone])->one();
   	  	
   	  	if(is_null($zone))
   	  	{
   	  		Yii::trace("Zone Not Found For This Device");
   	  		continue;
   	  	}
   	  	
   	  	$locations=$zone->zone_data;
   	  	

   	  	if($zone->zone_type==1)
   	  	{
   	  		$time=time();
   	  		$alert_data=json_decode($device->data,true);
   	  		if(empty($alert_data))
   	  		{
   	  			Yii::trace("No Recent Data For This Device");

   	  		}
   	  		
   	  		$recentData=GpsZoneNotification::find()->where(["zone_alert_id"=>$subscription->id,"device_id"=>$device->id])->orderBy(["id"=>SORT_DESC])->one();
   	  		
   	  		if(is_null($recentData))
   	  		{
   	  			Yii::trace("No Recent Data For This Device");
   	  			
   	  		}
   	  		
   	  		$status=$this->onCircle($locations,$history);
   	  		
   	  		if($status["status"])
   	  		{
   	  			
   	  		  
   	  			 if(is_null($recentData))
   	  			 {
   	  			 	
   	  			 	$rzone["zone_id"]=$zone->id;
   	  			 	$rzone["zone_alert_type"]="ON_ENTER";
   	  			 	$rzone["zone_distance"]=$status["distance"];
   	  			 	$device->data=json_encode($rzone);
   	  			 	$device->save(false);
   	  			 	
   	  			 	$_sv_data=new GpsZoneNotification();
   	  			 	
   	  			 	$_sv_data->zone_alert_id=$subscription->id;
   	  			 	$_sv_data->device_id=$device->id;
   	  			 	$_sv_data->customer_id=$device->customer_id;
   	  			 	$_sv_data->group_id=$device->group_id;
   	  			 	$_sv_data->alert_type="ON_ENTER";
   	  			 	$_sv_data->data=json_encode($rzone);
   	  			 	$_sv_data->created_at=$time;
   	  			 	$_sv_data->save(false);
   	  			 	
   	  			   	  			 	
   	  			 	 
   	  			 	if($subscription->on_enter==1){
   	  			 		
   	  			 		$this->sendZoneAlert($_sv_data->alert_type, $device,$zone,$subscription,$time);
   	  			 		
   	  			 	}
   	  			 	
   	  			 	
   	  			 	
   	  			 	
   	  			 }else{
   	  			 	     
   	  			 	if($recentData->alert_type=="ON_ENTER")
   	  			 	{
   	  			 		
   	  			 		
   	  			 		
   	  			 		
   	  			 	}else{
   	  			 		
   	  			 		$rzone["zone_id"]=$zone->id;
   	  			 		$rzone["zone_alert_type"]="ON_ENTER";
   	  			 		$rzone["zone_distance"]=$status["distance"];
   	  			 		$device->data=json_encode($rzone);
   	  			 		$device->save(false);
   	  			 		
   	  			 		$_sv_data=new GpsZoneNotification();
   	  			 		$_sv_data->zone_alert_id=$subscription->id;
   	  			 		$_sv_data->device_id=$device->id;
   	  			 		$_sv_data->customer_id=$device->customer_id;
   	  			 		$_sv_data->group_id=$device->group_id;
   	  			 		$_sv_data->alert_type="ON_ENTER";
   	  			 		$_sv_data->data=json_encode($rzone);
   	  			 		$_sv_data->created_at=$time;
   	  			 		$_sv_data->save(false);
   	  			 		
   	  			 		
   	  			 		if($subscription->on_leave==1){
   	  			 			
   	  			 			$this->sendZoneAlert("ON_ENTER", $device,$zone,$subscription,$time);
   	  			 			
   	  			 		}
   	  			 		
   	  			 		
   	  			 	}
   	  			 	    
   	  			 	
   	  			 }
   	  			
   	  		}else{
   	  			
   	  			
   	  			if(is_null($recentData))
   	  			{
   	  				
   	  				continue;
   	  			}
   	  			
   	  			if($recentData->alert_type=="ON_ENTER")
   	  			{
   	  				$rzone["zone_id"]=$zone->id;
   	  				$rzone["zone_alert_type"]="ON_EXIT";
   	  				$rzone["zone_distance"]=$status["distance"];
   	  				$device->data=json_encode($rzone);
   	  				$device->save(false);
   	  				
   	  				
   	  				$_svr_data=new GpsZoneNotification();
   	  				
   	  				$_svr_data->zone_alert_id=$subscription->id;
   	  				$_svr_data->device_id=$device->id;
   	  				$_svr_data->alert_type="ON_EXIT";
   	  				$_svr_data->customer_id=$device->customer_id;
   	  				$_svr_data->group_id=$device->group_id;
   	  				$_svr_data->data=json_encode($rzone);
   	  				$_svr_data->created_at=$time;
   	  				$_svr_data->save(false);
   	  				
   	  				$this->sendZoneAlert("ON_EXIT", $device,$zone,$subscription,$time);
   	  							
   	  			}
   	  			
   	  			
   	  			
   	  		}
   	  		
   	  	}
   	  	
   	  	if($zone->zone_type==2)
   	  	{
   	  		
   	
   	  		$status=$this->OnPolygon($locations,$history,"REACTANGLE");
   	  		
   	  		$this->CheckingAlet($status,$subscription,$device,$zone);
   	  		
   	  	}
   	  	
   	  	if($zone->zone_type==3)
   	  	{
   	  		//$this->testEmail("Polygon Test Begin..","polygon");
   	  		
   	  		$status=$this->OnPolygon($locations,$history,"POLYGON");
   	  		 
   	  		$this->CheckingAlet($status,$subscription,$device,$zone);
   	 
   	  }
   	  	
   	  	 
  	 }
   
  }
   
   
  public function CheckingAlet($status,$subscription,$device,$zone){

   	
   	$recentData=GpsZoneNotification::find()->where(["zone_alert_id"=>$subscription->id,"device_id"=>$device->id])->orderBy(["id"=>SORT_DESC])->one();
   	
   	if(is_null($recentData))
   	{
   		Yii::trace("No Recent Data For This Device");
   		
   	}
   	
   	$time=time();
 	
   	if($status)
   	{
   		
   		
   		if(is_null($recentData))
   		{
   			
   			$rzone["zone_id"]=$zone->id;
   			$rzone["zone_alert_type"]="ON_ENTER";
   			$rzone["zone_distance"]=$status["distance"];
   			$device->data=json_encode($rzone);
   			$device->save(false);
   			
   			$_sv_data=new GpsZoneNotification();
   			
   			$_sv_data->zone_alert_id=$subscription->id;
   			$_sv_data->device_id=$device->id;
   			$_sv_data->customer_id=$device->customer_id;
   			$_sv_data->group_id=$device->group_id;
   			$_sv_data->alert_type="ON_ENTER";
   			$_sv_data->data=json_encode($rzone);
   			$_sv_data->created_at=$time;
   			$_sv_data->save(false);
   			
   			
   			
   			if($subscription->on_enter==1){
   				
   				$this->sendZoneAlert("ON_ENTER", $device,$zone,$subscription,$time);
   				
   			}
   			
   			
   			
   			
   		}else{
   			
   			if($recentData->alert_type=="ON_ENTER")
   			{
   				
   				
   				
   				
   			}else{
   				
   				$rzone["zone_id"]=$zone->id;
   				$rzone["zone_alert_type"]="ON_ENTER";
   				$rzone["zone_distance"]=$status["distance"];
   				$device->data=json_encode($rzone);
   				$device->save(false);
   				
   				$_sv_data=new GpsZoneNotification();
   				$_sv_data->zone_alert_id=$subscription->id;
   				$_sv_data->device_id=$device->id;
   				$_sv_data->customer_id=$device->customer_id;
   				$_sv_data->group_id=$device->group_id;
   				$_sv_data->alert_type="ON_ENTER";
   				$_sv_data->data=json_encode($rzone);
   				$_sv_data->created_at=$time;
   				$_sv_data->save(false);
   				
   				
   				
   				if($subscription->on_enter==1){
   					
   					$this->sendZoneAlert("ON_ENTER", $device,$zone,$subscription,$time);
   					
   				}
   				
   				$this->testEmail("On Exit","gps");
   				
   			}
   			
   			
   		}
   		
   	}else{
   		
   		if(is_null($recentData))
   		{
   			
   			return false;
   		}
   		
   		if($recentData->alert_type=="ON_ENTER")
   		{
   			$rzone["zone_id"]=$zone->id;
   			$rzone["zone_alert_type"]="ON_EXIT";
   			$rzone["zone_distance"]=$status["distance"];
   			$device->data=json_encode($rzone);
   			$device->save(false);
   			
   			
   			$_svr_data=new GpsZoneNotification();
   			
   			$_svr_data->zone_alert_id=$subscription->id;
   			$_svr_data->device_id=$device->id;
   			$_svr_data->alert_type="ON_EXIT";
   			$_svr_data->customer_id=$device->customer_id;
   			$_svr_data->group_id=$device->group_id;
   			$_svr_data->data=json_encode($rzone);
   			$_svr_data->created_at=$time;
   			$_svr_data->save(false);
   			
   			if($subscription->on_leave==1){
   				
   				$this->sendZoneAlert("ON_EXIT", $device,$zone,$subscription,$time);
   				
   			}
   			
   		
   			
   		}
   		

   		
   	}
   	
   	 
   }
   
   
   public function OnPolygon($location,$history,$type="POLYGON"){
   		
   	  $pointLocation =  new Polygon();
   	  $lat=$history->latitude;
   	  $lng=$history->longitude;
   	  
   	  $currentPoints=$lat.",".$lng;

   	   if($type=="POLYGON")
   	   {
   	   	$polygon=$this->convertLatlang2String($location[0]);
   	   }else{
   	   	Yii::trace("Rectangle Checking");
   	   	$polygon=$this->convertLatlang2String($location[0]["data"]);
   	   	
   	   }

   	  $result= $pointLocation->pointInPolygon($currentPoints, $polygon);
   	  
   	   if($result !="outside")
   	   {
   	   	
   		   	 return true;
   	   	
   	   }else{
   	   	
   		   	return false;
   	   	
   	   }
   		
   }
   
   public function convertLatlang2String($array){
   	
   	$result=[];
   	
   	
   	for ($i=0; $i < count($array); $i++) {
   		
   		$location=number_format(floatval($array[$i]["lat"]),4,".","").",".number_format((float)$array[$i]["lng"], 4, '.', '');
   		
   		$result[]=$location;
   		
   	}
   	
   	return $result;
   }
   
   
   
   public function onCircle($location,$history){
   	
   		    
         $lat1=$location[0]["center"]["lat"];
         $lng1=$location[0]["center"]["lng"];
         $lat2=$history->latitude;
         $lng2=$history->longitude;
         $km=$location[0]["radius"] / 1000;
         
         Yii::trace($lat1."-".$lng1."-".$lat2."-".$lng2." KM=".$km);
         $distance=$this->distance($lat1, $lng1, $lat2, $lng2,"K");
         
         Yii::trace($km." > ".$distance);
         
         if($km > $distance)
         {
           	
         	Yii::trace("In Circle");
         	
         	return ["status"=>true,"distance"=>$distance];
         	
         }else{
         	
         	Yii::trace("Not In Circle");
         	
         	return ["status"=>false,"distance"=>$distance];
         	
         }
      
   }
   
   public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
   	
   	$theta = $lon1 - $lon2;
   	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
   	$dist = acos($dist);
   	$dist = rad2deg($dist);
   	$miles = $dist * 60 * 1.1515;
   	$unit = strtoupper($unit);
   	
   	if ($unit == "K") {
   		return ($miles * 1.609344);
   	} else if ($unit == "N") {
   		return ($miles * 0.8684);
   	} else {
   		return $miles;
   	}
   }
   
   
   public function checkAlert($device,$data){
   	
   		$currentDate=new \DateTime();
   	
   		$m_data=[
   				"humidity"=>intval($data->humidity_value),
   				"temperature"=>floatval($data->temperature_value),
   				"battery"=>intval($data->battery_value),
   				"speed"=>intval($data->speed)
   		];
   		
   		
   		$subscriptions=GpsSubscriptions::find()->where(["notification_status"=>GpsSubscriptions::STATUS_ACTIVE])->andWhere(["REGEXP","device_ids","".$device->id])->all();
   	
   		$count=count($subscriptions);
   		
   		Yii::trace("Subscription Count ".$count);
   		
   		if($count > 0)
   		{
   			//echo "This device has {$count} ".($count > 1?"subscription":"subscriptions");
   			
   		}else{
   			
   			
   			//$this->CheckDeviceAlertWDS($device,$model);
   			
   			//echo "This device has no Subscription \n";
   			
   		}
   		
   		Yii::trace("looping Begin...");
   		
   		foreach ($subscriptions as $subscription)
   		{
   			 
   			if(!$this->validateTime($subscription->np_start_time, $subscription->np_end_time))
   			{
   				
   				continue;
   				
   			}
   			
   			if(!$this->hasAlert($subscription->attribute, $subscription->condition_code, $data, $subscription->value_one, $subscription->value_two))
   			{
   				Yii::trace("This device has No {$subscription->attribute} Alert",'custom_task');
   			   continue;	
   			}
   			
   			Yii::trace("This device has {$subscription->attribute} Alert",'custom_task');
   			
   			$timezone=$this->getTimezone($device->group_id);
   			$device_date=Yii::$app->utils->getDateTime($data->created_at,$timezone);
   			
   			
   			Yii::trace("This device has {$subscription->attribute} Alert",'custom_task');
   			
   			if($subscription->last_run_time == "" && is_null($subscription->last_run_time))
   			{
   				$subscription->last_run_time=time();
   				$subscription->started_at=time();
   				
   				Yii::trace("lastrun is empty or null",'custom_task');
   				
   			}else{
   				
   				$start = new \DateTime();
   				$start->setTimestamp($subscription->last_run_time);
   				$end   = new \DateTime(); // Current date time
   				$diff  = $start->diff($end);
   				
   				if($diff->i < ($subscription->intervel_mins)){
   					
   					Yii::trace("MINUTES interval is {$diff->i} > {$subscription->intervel_mins} no email send",'custom_task');
   					continue;
   				}else{
   					
   					if($diff->i < ($subscription->intervel_mins)){
   						
   						Yii::trace("MINUTES interval is {$diff->i} > {$subscription->intervel_mins} no email send",'custom_task');
   						continue;
   					}
   					
   				}
   				
   				
   			}
   			
   			$subscription->last_run_time=time();
   			//$subscription->save(false);
   			$sensor_value=$this->getSensorValue($subscription->attribute,$data);
   			
   			  			
   			if($subscription->emails !="")
   			{
   				$emails=$subscription->emails; //explode("|", $subscription->emails);
   				
   			}else{
   				
   				$emails=[];
   			}
   			if($subscription->mobile_numbers!="")
   			{
   				$mobile_numbers= $subscription->mobile_numbers;//explode("|", $subscription->mobile_numbers);
   				
   			}else{
   				
   				$mobile_numbers=[];
   			}
   			
   			$email_content=[
   					
   					"sensor_name"=>is_null($device->name)?"not set":$device->name,
   					"sensor_display_name"=>is_null($device->display_name)?"not set":$device->display_name,
   					"alert_type"=>$subscription->attribute,
   					"sensor_value"=>$sensor_value,
   					"alert_time"=>$device_date->format('Y-m-d H:i:s'),
   					"emails"=>$emails,
   					"mobile"=>$mobile_numbers
   			];
   			
   			
   			$notification_content=[
   					"temperature"=>floatval($data->temperature_value),
   					"humidity"=>floatval($data->humidity_value),
   					"battery"=>floatval($data->battery_value),
   					"speed"=>intval($data->speed),
   					"value"=>$sensor_value
   			];
   			
   			if($subscription->is_email==1)
   			{
   				Yii::trace("Sending email ...");
   				$this->dynamicEmail($email_content);
   				
   			}
   			
   			if($subscription->is_sms==1)
   			{
   				Yii::trace("Sending sms ...");
   				$this->sendSMS($email_content);
   				
   			}
   			
   			$this->saveNotification($device->id,$subscription->attribute,'',$notification_content,$device);
   			
   		}
   		
   		
   }
   
   public function getSensorValue($alertType,$value){
   	
   
  	
   	switch ($alertType)
   	{
   		case Constants::ALERT_HIGH_TEMPERATURE:
   			
   			return $value->temperature_value;
   			
   			break;
   		case  Constants::ALERT_LOW_TEMPERATURE:
   			
   			return $value->temperature_value;
   			
   			break;
   		case Constants::ALERT_NORMAL_TEMPERATURE:
   			
   			return $value->temperature_value;
   			break;
   			
   		case Constants::ALERT_HIGH_HUMIDITY:
   			
   			return $value->humidity_value;
   			
   			break;
   		case  Constants::ALERT_LOW_HUMIDITY:
   			
   			return $value->humidity_value;
   			
   			break;
   		case Constants::ALERT_NORMAL_HUMIDITY;
   		return $value->humidity_value;
   		break;
   		case Constants::ALERT_LOW_BATTERY;
   			return $value->battery_value;
   		break;
   		case Constants::ALERT_GPS_IDLE;
   			return $value->movement;
   		break;
   		case Constants::ALERT_GPS_OVERSPEED;
   		return $value->speed;
   		break;
   		
   		default:
   			
   			return "";
   			break;
   			
   	}
   	
   }
   private function getTimezone($group_id){
   	
   	$user=AdminUser::find()->select(['timezone'])->where(['group_id'=>$group_id])->asArray()->one();
   	
   	if(!empty($user))
   	{
   		
   		$offset=TimeZone::find()->where(['id'=>$user['timezone']])->asArray()->one();
   		if(!empty($offset))
   		{
   			
   			return Yii::$app->utils->getTimezoneName($offset['gmt']);
   			
   		}
   		
   	}
   	
   	return Yii::$app->utils->getTimezoneName('');
   }
   
   public function actionMtest(){
   	
   		
   	
   }
   
   public function actionTest(){
   	
   	$server=Yii::$app->request->getRawBody();
   	$decode=Json::decode($server,true);
   	$gps= new DeviceGpsReading();
   	
   	$gps->hardware_serial=$decode["hardware_serial"];
   	$gps->payload_raw=$decode["payload_raw"];
   	
   	if(array_key_exists('payload_fields', $decode))
   	{	
   		$gps->configuration_info=$decode["payload_fields"]["configuration_info"];
   		$gps->configuration_info_two=$decode["payload_fields"]["configuration_info_two"];
   		$gps->door=$decode["payload_fields"]["door"];
   		$gps->l_latitude=$decode["payload_fields"]["location"]["lat"];
   		$gps->l_longitude=$decode["payload_fields"]["location"]["lng"];
   		$gps->movement=$decode["payload_fields"]['movement'];
   		$gps->switch_two=$decode["payload_fields"]['switch_two'];
   		$gps->switchone=$decode["payload_fields"]['switchone'];
   		$gps->battery=$decode["payload_fields"]["battery"];
   		$gps->temperature=$decode["payload_fields"]["temperature"];
   		$gps->humidity=$decode["payload_fields"]["humidity"];
   		$gps->created_at=time();
   		$gps->save(false);
   		
   		return ["ok"];
   	}else{
   			
   		return ["invlid payloads"];
   	}
   	
   }
   
   public function validateTime($start,$end){
   	
   	$currentTime=new \DateTime();
   	$start_time=\DateTime::createFromFormat('H:i:s', $start);
   	$end_time=\DateTime::createFromFormat('H:i:s', $end);
   	Yii::trace($start." ".$end." Current Time".$currentTime->format("H:i:s"));
   	if ($currentTime> $start_time && $currentTime< $end_time)
   	{
   		
   		return true;
   		
   	}else{
   		
   		return false;
   	}
   	
   }
   
   public function hasAlert($alertType,$condition,$value,$from,$to){
   	
   	
   	Yii::trace(func_get_args(),'custom_task');
   	
   	switch ($alertType)
   	{
   		case Constants::ALERT_HIGH_TEMPERATURE:
   			
   			if(is_object($value))
   			{
   				return Constants::conditionChecking($condition,$value->temperature_value,$from,$to);
   			}else{
   				
   				return Constants::conditionChecking($condition,$value,$from,$to);
   			}
   			
   			
   			
   			break;
   		case  Constants::ALERT_LOW_TEMPERATURE:
   			
   			
   			if(is_object($value))
   			{
   				return Constants::conditionChecking($condition,$value->temperature_value,$from,$to);
   			}else{
   				
   				return Constants::conditionChecking($condition,$value,$from,$to);
   			}
   			
   			
   			break;
   		case Constants::ALERT_NORMAL_TEMPERATURE:
   			
   			
   			if(is_object($value))
   			{
   				return Constants::conditionChecking($condition,$value->temperature_value,$from,$to);
   			}else{
   				return Constants::conditionChecking($condition,$value,$from,$to);
   				
   			}
   			
   			break;
   			
   		case Constants::ALERT_HIGH_HUMIDITY:
   			
   			
   			if(is_object($value))
   			{
   				return Constants::conditionChecking($condition,$value->humidity_value,$from,$to);
   				
   			}else{
   				
   				return Constants::conditionChecking($condition,$value,$from,$to);
   				
   			}
   			
   			
   			break;
   		case  Constants::ALERT_LOW_HUMIDITY:
   			
   			
   			if(is_object($value))
   			{
   				return Constants::conditionChecking($condition,$value->humidity_value,$from,$to);
   			}else{
   				
   				return Constants::conditionChecking($condition,$value,$from,$to);
   			}
   			
   			
   			break;
   		case Constants::ALERT_NORMAL_HUMIDITY;
   		
   		if(is_object($value))
   		{
   			return Constants::conditionChecking($condition,$value->humidity_value,$from,$to);
   		}else{
   			
   			return Constants::conditionChecking($condition,$value,$from,$to);
   		}
   		
   		break;
   		case Constants::ALERT_LOW_BATTERY;
   		
   		if(is_object($value))
   		{
   			 
   			return Constants::conditionChecking($condition,$value->battery_value,$from,$to);
   			
   		}else{
   			
   			return Constants::conditionChecking($condition,$value,$from,$to);
   		}
   		
   		break;
   		
   		case Constants::ALERT_GPS_IDLE;
   			
   		if(is_object($value))
   		{
   			
   			return Constants::CheckGpsAlert($condition,$value->movement,$from,$to);
   			
   		}else{
   			
   			return Constants::CheckGpsAlert($condition,$value,$from,$to);
   		}
   		break;
   		case Constants::ALERT_GPS_OVERSPEED;
   		
   		if(is_object($value))
   		{
   			
   			return Constants::CheckGpsAlert($condition,$value->speed,$from,$to);
   			
   		}else{
   			
   			return Constants::CheckGpsAlert($condition,$value,$from,$to);
   		}
   		break;
   		default:
   			
   			return false;
   			
   			break;
   			
   	}
   	
   }
   
   public function dynamicEmail($email){
   	
   	
   	$template=NotificationTemplate::find()->where(["alert_type"=>$email['alert_type'],"template_type"=>"email"])->one();
   	
   	if(is_null($template))
   	{
   		Yii::trace("Dynamic Email Not found ...");
   		
//    		/$this->sendEmail($email);
   		return;
   		
   	}
   	
   	$subject=$this->stringreplace($email,$template->subject);
   	$html_content=$this->stringreplace($email,$template->content_body);
   	
   	if(empty($email["emails"]))
   	{
   		Yii::trace(" Emails Not found ...");
   		return false;
   	}
   	
   	Yii::$app->mailer->compose()
   	->setFrom(Yii::$app->params["alertEmail"])
   	->setTo($email["emails"])
   	->setSubject($subject)
   	->setHtmlBody($html_content)
   	->send();
   	
   	
   }
   

   
   
   public function sendZoneAlert($alertType,$device,$zone,$subscription,$alert_time){
   	
   			 
   			$datetime=new \DateTime();
   			$datetime->setTimestamp($alert_time);
   			
   		
   			
   			
   			
   			$zone_name=$zone->name !=""?$zone->name:" ";
   	 		$device_name=$device->name !=""?$device->name:" ";
   	 		$device_display_name=$device->display_name !=""?$device->display_name:" ";
   	 		$emails=empty($subscription->emails)?"":implode(",", $subscription->emails);
   	 		$mobile_numbers=empty($subscription->mobile_numbers)?"":implode(",", $subscription->mobile_numbers);
   	 		$alertType=$alertType;
   	 		
   	 		
			$content=[
					"alert_type"=>$alertType,
					"alert_name"=>str_replace("ON_","", $alertType),
					"zone_name"=>$zone_name,
					"sensor_name"=>$device_name,
					"sensor_display_name"=>$device_display_name,
					"emails"=>$emails,
					"mobile_numbers"=>$mobile_numbers,
					"alert_time"=>	$datetime->format("Y-m-d H:i:s")
					
			];   	 		
   	 		
			$this->dynamicEmail($content);
			
			$this->sendSMS($content);
			
			
   	 		//$log_msg=$device_name." On ".$alertType." at ".$zone_name." Time ".$emails." Mobile Number ".$mobile_numbers;
   	 		
   	 		//$this->testEmail($log_msg,"gps");

   }
   
   
   public function testEmail($log="",$type="circledata"){
   	
   	if($log==""){
   		 return false;
   	}
   	
   	$path=Yii::getAlias('@runtime').'/gps';
   	\yii\helpers\FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
   	
   	file_put_contents($path."/".$type.".log", $log."\n",FILE_APPEND);
   	 	
   }
   
   public function stringreplace($variables,$content){
   	
   	$response = preg_replace_callback('/{(.+?)}/ix',function($match)use($variables){
   		return !empty($variables[$match[1]]) ? $variables[$match[1]] : $match[0];
   	},$content);
   		
   		return $response;
   }
   
   public function sendSMS($alert){
   	
   	$template=NotificationTemplate::find()->where(["alert_type"=>$alert['alert_type'],"template_type"=>"sms"])->one();
   	
   	if(is_null($template))
   	{
   		//$this->sendEmail($email);
   		return;
   		
   	}
   	
   	$html_content=$this->stringreplace($alert,$template->content_body);
   	//Yii::$app->sms->sendSms($alert["mobile"],$html_content);
   	
   }
   
   public function saveNotification($id,$alertType,$content="",$payload,$device_ob){
     	
   	$alerts=new DeviceAlert();
   	$alerts->customer_id=$device_ob->customer_id;
   	$alerts->group_id=$device_ob->group_id;
   	$alerts->device_id=$id;
   	$alerts->alert_type=$alertType;
   	$alerts->sms=0;
   	$alerts->body=$content;
   	$alerts->payload=json_encode($payload);
   	$alerts->status=DeviceAlert::STATUS_NOT_ACK;
   	$alerts->created_at=time();
   	$alerts->save(false);
   	
   }
   
   
}