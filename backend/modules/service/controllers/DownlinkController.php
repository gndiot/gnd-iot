<?php

namespace backend\modules\service\controllers;

use yii;
use yii\rest\Controller;
use common\models\Devices;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\helpers\Json;

/**
 * Default controller for the `service` module
 */
class DownlinkController extends Controller
{
	
	public $down_base_url='http://manage.gndsolutions.in/service/downlink';
	//public $down_base_url='http://iotmanage.test/service/downlink';
	
	protected function verbs(){
		
		return [
				
				'push' => ['POST'],
				'test' => ['POST'],
				'temperature'=>['POST']
		];
		
	}
	
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        //return $this->render('index');
    }
    
    public function actionTemperature(){
    	
    	$rdata=Yii::$app->getRequest()->getBodyParams();
    	$deviceids=$rdata['ids'];
    	$dev_config=$rdata['configurations'];
    	$deviceslist=Devices::find()->where(['id'=>$deviceids,'config_status'=>[1,0]])->all();
    	$harwarelist=Devices::find()->select(['hardware_serial'])->where(['id'=>$deviceids,'config_status'=>[1,0]])->asArray()->all();
    	$harwareids=ArrayHelper::getColumn($harwarelist, 'hardware_serial');
    	$data=[
    			'hardware_ids'=>$harwareids,
    			'configurations'=>$this->device_configuration($dev_config)
    	];
    
    	$url = $this->down_base_url.'/temperature';
    	$this->makeHttpRequest($url, $data);
    	foreach ($deviceslist as $device)
    	{
    		
    		 $device->config_status=10;
    		 $device->config_temp=Json::encode($data["configurations"]);
    		 $device->save(false);
    	}
    	
    	return $data;
    	
    	//$this->makeHttpRequest($url, $data)
    }
    
    
    private function makeHttpRequest($url,$data){
    	
    	Yii::trace('post data -------');
    	Yii::trace($data);
    	Yii::trace('sending data to'.$url);
    	$client = new Client();
    	$response=$client->createRequest()->setFormat(Client::FORMAT_JSON)->setMethod('POST')->setUrl($url)->setData($data)->send();
    	if($response->isOk){
    		Yii::trace('response received from '.$url);
    		Yii::trace($response->content);
    	}
    	
    }
    
    private function device_configuration($config=[]){
    	
    	if(is_null($config))
    	{
    		$config=[];
    	}
    	$default=[];
    	$default['mode']=1;
    	$default['reporting_interval']=2;
    	$default['sampling_inteval']=30;
    	$default["temperature_range"]=[
    			"low"=>10,
    			"high"=>50
    	];
    	$default["humidity_range"]=[
    			"low"=>60,
    			"high"=>90
    	];
    	
    	$level1=array_merge($default,$config);
    	

    	$result=[];
    	
    	foreach ($level1 as $key=>$value)
    	  {
    	  	
    	  		if(is_array($value) && array_key_exists($key, $config))
    	  		{
    	  			
    	  			 $level1[$key]=array_merge($default[$key],$config[$key]);
    	  			 $result[$key]=array_merge($default[$key],$config[$key]);
    	  		}else{
    	  			
    	  			$result[$key]=$value;
    	  		}
    	  }
    	
    	  return $level1;
    	
    }
}
