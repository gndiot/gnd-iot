<?php
namespace backend\modules\service\controllers;

use yii;
use yii\rest\Controller;
use common\models\DeviceTemperatureReading;
use yii\helpers\Json;
use common\models\DeviceSubscription;
use common\models\Constants;
use common\models\Devices;
use common\models\DeviceAlert;
use common\models\DeviceConfigHumidity;
use common\models\DeviceConfigTemperature;
use yii\filters\AccessControl;
use backend\models\AdminUser;
use common\models\TimeZone;
use common\models\NotificationTemplate;
use backend\modules\v1\models\DeviceStatus;

class TemperatureController extends Controller{
	

	
	public function behaviors(){
	
		$behaviors=parent::behaviors();
		$behaviors["access"]=[
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								//'ips' => YII_ENV_DEV ? ['127.0.0.1']:['166.62.10.190','192.185.129.139'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
		
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				
				'push' => ['POST'],
				'test' => ['POST'],
				'periodic'=>['POST'],
				'alert'=>['POST']
		];
	
	}
	
	
	public function actionBca(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$device=Devices::find()->where(['hardware_serial'=>$rdata['hardware_serial']])->one();
		
		
		if(!is_null($device))
		{
			
			$device->bc_req_status=1;
			$device->save(false);
			
		}
		
		return ["Device not found"];
		
	}
	
	public function actionPush(){
		
		$model = new DeviceTemperatureReading();
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$model->load($rdata,"");
		$device_state=Devices::find()->where(['hardware_serial'=>$model->hardware_serial,'status'=>10])->one();
		
		 if(is_null($device_state))
		 {
		 	return ["Device inactive or not found!"];
		 }
		 
		
		 		
		 $md=DeviceStatus::find()->andWhere(["REGEXP","device_ids",'[[:<:]]'.$device_state->id.'[[:>:]]'])->one();
		 
		 if(!is_null($md))
		 {
		 	$status=false;
		 	$times=$md->device_times;
		 	$number=0;
		 	for ($i = 0; $i < count($times); $i++) {
		 		
		 		if($this->validateTimeAutomatic($times[$i]["from"],$times[$i]["to"]))
		 		{
		 			
		 			$status=false;
		 			break;
		 			
		 		}else{
		 			
		 		 $status=true;
		 			
		 			
		 		}
		 		
		 	}
		 	
		 	if($status)
		 	{
		 		Yii::trace("This Device Off Now");
		 		return ["This Device Off Now"];
		 	}
		 	
		 	
		 }
		 
		 return ["Every think is ok"];
		
		//Yii::trace($rdata,'custom_task');
		
		if($model->save())
		{
			 $this->checkingAlerts($model);
			return $model;
			
		}else{
				
			return $model->getErrors();
			
		}
		
	}
	
	
	/*
	 *  Periodic message will received here
	 *  */
	public function actionPeriodic(){
		
		$model = new DeviceTemperatureReading();
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$model->load($rdata,"");
		$device_state=Devices::find()->where(['hardware_serial'=>$model->hardware_serial])->one();
		
		if(is_null($device_state))
		{
			return ["Device inactive or not found!"];
		}
		
		$md=DeviceStatus::find()->andWhere(["REGEXP","device_ids", '[[:<:]]'.$device_state->id.'[[:>:]]'])->one();
		
		if(!is_null($md))
		{
			$status=false;
			$times=$md->device_times;
			$number=0;
			for ($i = 0; $i < count($times); $i++) {
				
				if($this->validateTimeAutomatic($times[$i]["from"],$times[$i]["to"]))
				{
					
					$status=false;
					break;
					
				}else{
					
					$status=true;
					
					
				}
				
			}
			
			if($status)
			{
				
				if(!is_null($device_state))
				 {
					$device_state->status=15;
					$device_state->save(false);
				 } 
				
				Yii::trace("This Device Off Now");
				return ["This Device Off Now"];
			}else{
				
				if(!is_null($device_state))
				{
					$device_state->status=10;
					$device_state->save(false);
				}
				 
			}
			
			
		}else{
			
			
			if($device_state->status!=10)
			{
				
				return ["Device May inactive"];
			}
			
		}
		
		//Yii::trace($rdata,'custom_task');
		if($model->save())
		{
			
			$this->checkingAlerts($model);
			
			return $model;
			
		}else{
			
			return $model->getErrors();
			
		}
		
	}
	
	
	public function actionAlert(){

		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		
		$model = new DeviceTemperatureReading();
		$rdata=Yii::$app->getRequest()->getBodyParams();
		$model->load($rdata,"");
		$device_state=Devices::find()->where(['hardware_serial'=>$model->hardware_serial])->one();
		
		if(is_null($device_state))
		{
			return ["Device inactive or not found!"];
		}
		
		$md=DeviceStatus::find()->andWhere(["REGEXP","device_ids", '[[:<:]]'.$device_state->id.'[[:>:]]'])->one();
		
		if(!is_null($md))
		{
			$status=false;
			$times=$md->device_times;
			$number=0;
			for ($i = 0; $i < count($times); $i++) {
				
				if($this->validateTimeAutomatic($times[$i]["from"],$times[$i]["to"]))
				{
					
					$status=false;
					break;
					
				}else{
					
					$status=true;
					
					
				}
				
			}
			
			if($status)
			{
				
				if(!is_null($device_state))
				{
					$device_state->status=15;
					$device_state->save(false);
				} 
				Yii::trace("This Device Off Now");
				return ["This Device Off Now"];
			}
			
			
		}else{
			
			
			if($device_state->status!=10)
			{
				
				return ["Device May inactive"];
			}
			
		}
		
		
		$temperature= new DeviceTemperatureReading();
		$temperature->hardware_serial=$rdata["hardware_serial"];
		$temperature->payload_raw=$rdata["payload_raw"];
		$temperature->battery=$rdata['battery'];
		$temperature->temperature=$rdata['temperature'];
		$temperature->humidity=$rdata['humidity'];
		$temperature->created_at=$rdata['created_at'];
		if($temperature->save(false))
		{
			
			$this->checkingAlerts($temperature);
			
			return $temperature;
			
		}
		
		return $temperature->getErrors();
		
	}
	
	
	public function actionHealthCheck(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$this->healthMessage($rdata,$rdata["hardware_serial"]);
		
		return $rdata;
	}
	
	public function actionTest(){
		
		$server=Yii::$app->request->getRawBody();
		$decode=Json::decode($server,true);
		
		$mtemp= Yii::$app->temperature;
		
		$mtemp->loadBase64Hex($decode["payload_raw"]);
		
		Yii::trace($decode["payload_raw"],'custom_task');
		Yii::trace($mtemp->isPeriodicMsg(),'custom_task');
		if($mtemp->isPeriodicMsg())
		{
			$data=$mtemp->periodicMessage();
			Yii::trace($data,'custom_task');
			$temperature= new DeviceTemperatureReading();
			$temperature->hardware_serial=$decode["hardware_serial"];
			$temperature->payload_raw=$decode["payload_raw"];
			$temperature->battery=$data['battery'];
			$temperature->temperature=$data['temperature'];
			$temperature->humidity=$data['humidity'];
			$temperature->created_at=time();
			$temperature->save(false);
			return $temperature;	
		}

		Yii::trace($mtemp->isAlertMsg(),'custom_task');
		
		if($mtemp->isAlertMsg())
		{
			$data=$mtemp->alertMessage();
			
			Yii::trace($data,'custom_task');
			
			
		}
		
		
		if($mtemp->isHealthMsg())
		{
			
			$data=$mtemp->getDecodeHealthCheckMsg($mtemp->input);
			
			Yii::trace($data,'custom_task');
			//$this->deviceAlertMessage($data,$decode["hardware_serial"]);
			$this->healthMessage($data,$decode["hardware_serial"]);

			
		}
		
		
		if($mtemp->isConfigurationRequest())
		{
			
			
			
			
		}
		
		
		
		
		/* if(array_key_exists('payload_fields', $decode))
		{
			
			$this->checkingAlerts($model);
			
			 return ["ok"];	
			 
		}else{
			
			 return ["invlid payloads"];
		} */
		
		return ["ok"];
	}
	
	
	public function checkingAlerts($model){
		
		 $currentDate=new \DateTime();
		 
		 $device=Devices::find()->where(["hardware_serial"=>$model->hardware_serial])->one();
		 
		 if(is_null($device))
		 {
		 	// echo "Device ID is not Matched";
		 	return ;
		 }
		 $m_data=[
		 		"humidity"=>intval($model->humidity),
		 		"temperature"=>floatval($model->temperature),
		 		"battery"=>intval($model->battery)
		 ];
		 
		  $device->data=Json::encode($m_data);
		  $device->battery_level=intval($model->battery);
		  $device->save(false);
		 $subscriptions=DeviceSubscription::find()->where(["dev_id"=>$device->id,"notification_status"=>DeviceSubscription::STATUS_ACTIVE])->all();
		 $count=count($subscriptions);
		 
		 if($count==0)
		 {
		 
		 	$subscriptions=DeviceSubscription::find()->where(["notification_status"=>DeviceSubscription::STATUS_ACTIVE])->andWhere(["REGEXP","device_ids","".$device->id])->all();
		 	
		 	Yii::trace("V1 Subcription Model Comming");
		 	
		 	Yii::trace($subscriptions);
		 }
		 
		 $count=count($subscriptions);
		 
		 Yii::trace("Subscription Count ".$count);
		 
		 if($count > 0)
		 {
		 	//echo "This device has {$count} ".($count > 1?"subscription":"subscriptions");
		 	
		 }else{
		 	
		 	
		 	$this->CheckDeviceAlertWDS($device,$model);
		 	
		 	//echo "This device has no Subscription \n";
		 	 
		 }
		 
		 Yii::trace("looping Begin...");
		 foreach ($subscriptions as $subscription)
		 {
		 	
		 	if(!$this->validateTime($subscription->np_start_time, $subscription->np_end_time))
		 	{
		 			
		 		continue;
		 	}
		 		
		 		if($this->hasAlert($subscription->attribute, $subscription->condition_code, $model, $subscription->value_one, $subscription->value_two))
		 		{
		 			Yii::trace("This device has {$subscription->attribute} Alert",'custom_task');
		 			
		 			$timezone=$this->getTimezone($device->group_id);
		 			$device_date=Yii::$app->utils->getDateTime($model->created_at,$timezone);
		 			
		 			
		 			if($subscription->last_run_time == "" && is_null($subscription->last_run_time))
		 			{
		 				$subscription->last_run_time=time();
		 				$subscription->started_at=time();
		 				
		 				Yii::trace("lastrun is empty or null",'custom_task');
		 				
		 			}else{
		 				
		 				$start = new \DateTime();
		 				$start->setTimestamp($subscription->last_run_time);
		 				$end   = new \DateTime(); // Current date time
		 				$diff  = $start->diff($end);
		 				
		 				 if($subscription->intervel_type=='MINUTES')
		 				 {
		 				 	if($diff->i < ($subscription->intervel_mins)){
		 				 		
		 				 		Yii::trace("MINUTES interval is {$diff->i} > {$subscription->intervel_mins} no email send",'custom_task');
		 				 		continue;
		 				 	}
		 				 	
		 				 }else{
		 				 	
		 				 	if($diff->i < ($subscription->intervel_mins)){
		 				 		
		 				 		Yii::trace("MINUTES interval is {$diff->i} > {$subscription->intervel_mins} no email send",'custom_task');
		 				 		continue;
		 				 	}
		 				 	 
		 				 }
		 				
		 			}

		 			$subscription->last_run_time=time();
		 			$subscription->save(false);
		 			
		 			$sensor_value=$this->getSensorValue($subscription->attribute,$model);
		 			 
		 			if($subscription->emails !="")
		 			{
		 				$emails=explode("|", $subscription->emails);
		 				
		 			}else{
		 				
		 				 $emails=[];
		 			}
		 			
		 			$email_content=[
		 					
		 					"sensor_name"=>is_null($device->name)?"":$device->name,
		 					"sensor_display_name"=>is_null($device->display_name)?"not set":$device->display_name,
		 					"alert_type"=>$subscription->attribute,
		 					"sensor_value"=>$sensor_value,
		 					"alert_time"=>$device_date->format('Y-m-d H:i:s'),
		 					"emails"=>$emails,
		 					"mobile"=>$subscription->numbers
		 			];
		 			
		
		 			$notification_content=[
		 					 "temperature"=>$model->temperature,
		 					 "humidity"=>$model->humidity,
		 					 "battery"=>$model->battery,
		 					"value"=>$sensor_value
		 			];
		 			
		 			
		 			if($subscription->is_email==1)
		 			{
		 				$this->dynamicEmail($email_content);
		 				
		 			}
		 			
		 			if($subscription->is_sms==1)
		 			{
		 				
		 				
		 				$this->sendSMS($email_content);
		 				
		 			}
		 			
		 		
		 			$this->saveNotification($device->id,$subscription->attribute,'',$notification_content,$device);
		 			
		 			//echo "Device Has Alert \n";
		 			
		 			
		 		
		 		}else{
		 			  
		 			Yii::trace("This device has No  {$subscription->attribute} Alert",'custom_task');
		 			$subscription->last_run_time=null;
		 			$subscription->started_at=null;
		 			$subscription->save(false);
		 			
		 			$this->CheckDeviceAlertWDS($device,$model);
		 			
		 			//echo "Device Has no Alert \n";
		 		
		 		}
		 }
		
	}
	
	
	public function getSensorValue($alertType,$value){
		
		
		switch ($alertType)
		{
			case Constants::ALERT_HIGH_TEMPERATURE:
				
				return $value->temperature;
				
				break;
			case  Constants::ALERT_LOW_TEMPERATURE:
				
				return $value->temperature;
				
				break;
			case Constants::ALERT_NORMAL_TEMPERATURE:
				
				return $value->temperature;
				break;
				
			case Constants::ALERT_HIGH_HUMIDITY:
				
				return $value->humidity;
				
				break;
			case  Constants::ALERT_LOW_HUMIDITY:
				
				return $value->humidity;
				
				break;
			case Constants::ALERT_NORMAL_HUMIDITY;
			return $value->humidity;
			break;
			case Constants::ALERT_LOW_BATTERY;
			return $value->battery;
			break;
			
			default:
				
				return "";
				break;
				
		}
		
	}
	
	public function hasAlert($alertType,$condition,$value,$from,$to){
		 

		      Yii::trace(func_get_args(),'custom_task');
		
			 switch ($alertType)
			 {
			 	case Constants::ALERT_HIGH_TEMPERATURE:
			 		
			 		  if(is_object($value))
			 		  {
			 		  	return Constants::conditionChecking($condition,$value->temperature,$from,$to);
			 		  }else{
			 		  	
			 		  	return Constants::conditionChecking($condition,$value,$from,$to);
			 		  }
			 		
			 		
			 		
			 	break;
			 	case  Constants::ALERT_LOW_TEMPERATURE:
			 		
			 		
			 		if(is_object($value))
			 		{
			 			return Constants::conditionChecking($condition,$value->temperature,$from,$to);
			 		}else{
			 			
			 			return Constants::conditionChecking($condition,$value,$from,$to);
			 		}
			 		
			 		
			 	break;
			 	case Constants::ALERT_NORMAL_TEMPERATURE:
			 		
			 		
			 		if(is_object($value))
			 		{
			 			return Constants::conditionChecking($condition,$value->temperature,$from,$to);
			 		}else{
			 			return Constants::conditionChecking($condition,$value,$from,$to);
			 			
			 		}
			 		
			 	break;
			 	
			 	case Constants::ALERT_HIGH_HUMIDITY:
			 		
			 		
			 		if(is_object($value))
			 		{
			 			return Constants::conditionChecking($condition,$value->humidity,$from,$to);
			 			
			 		}else{
			 			
			 			return Constants::conditionChecking($condition,$value,$from,$to);
			 			
			 		}
			 		
			 		
			 	break;
			 	case  Constants::ALERT_LOW_HUMIDITY:
			 		
			 		
			 		if(is_object($value))
			 		{
			 			return Constants::conditionChecking($condition,$value->humidity,$from,$to);
			 		}else{
			 			
			 			return Constants::conditionChecking($condition,$value,$from,$to);
			 		}
			 		
			 		
			 	break;
			 	case Constants::ALERT_NORMAL_HUMIDITY;
			 	
				 	if(is_object($value))
				 	{
				 		return Constants::conditionChecking($condition,$value->humidity,$from,$to);
				 	}else{
				 		
				 		return Constants::conditionChecking($condition,$value,$from,$to);
				 	}
			 	
			 	break;
			 	case Constants::ALERT_LOW_BATTERY;
			 	
			 	if(is_object($value))
			 	{
			 		return Constants::conditionChecking($condition,$value->battery,$from,$to);
			 		
			 	}else{
			 		
			 		return Constants::conditionChecking($condition,$value,$from,$to);
			 	}
			 	
			 	break;
			 	

			 	default:
			 		
			 		return false;
			 		
			 	break;
			 		
			 }
			 	
	}
	
	
	public function dynamicEmail($email){
		
		      
		$template=NotificationTemplate::find()->where(["alert_type"=>$email['alert_type'],"template_type"=>"email"])->one();
		
		if(is_null($template))
		{
			$this->sendEmail($email);
			return;
			
		}
		
		$subject=$this->stringreplace($email,$template->subject);
		$html_content=$this->stringreplace($email,$template->content_body);
		
		if(empty($email["emails"]))
		{
			return false;
		}
		
		Yii::$app->mailer->compose()
		->setFrom(Yii::$app->params["alertEmail"])
		->setTo($email["emails"])
		->setSubject($subject)
		->setHtmlBody($html_content)
		->send();
		
		
	}
	
	public function stringreplace($variables,$content){
		
		$response = preg_replace_callback('/{(.+?)}/ix',function($match)use($variables){
			return !empty($variables[$match[1]]) ? $variables[$match[1]] : $match[0];
		},$content);
			
			return $response;
	}
	
	public function sendEmail($email){
		
		$alertTemplate='alert';
		
		switch ($email['alert_type']) {
			case 'HIGH TEMPERATURE':
				$alertTemplate='highTemperature';
			break;
			case 'LOW TEMPERATURE':
				$alertTemplate='lowTemperature';
			break;
			case 'NORMAL TEMPERATURE':
				$alertTemplate='normalTemperature';
			break;
			
			case Constants::ALERT_HIGH_HUMIDITY:
				$alertTemplate='highHumidity';
				break;
			case Constants::ALERT_LOW_HUMIDITY:
				$alertTemplate='lowHumidity';
				break;
			case Constants::ALERT_NORMAL_HUMIDITY:
				$alertTemplate='normalHumidity';
				break;
			
			default:
				$alertTemplate='alert';
			break;
		}
		
		$email_content=[
				
				"sensor_name"=>$device->name,
				"sensor_display_name"=>$device->display_name,
				"alert_type"=>$subscription->attribute,
				"sensor_value"=>$sensor_value,
				"alert_time"=>$device_date->format('Y-m-d H:i:s'),
				"emails"=>$subscription->email,
				"mobile"=>$subscription->mobile_numbers
		];
			
		Yii::$app->mailer->compose($alertTemplate,['device_id'=>$email["sensor_name"],"notification_time"=>$email["alert_time"],"type"=>$email["alert_type"],"value"=>$email["sensor_value"]])
		->setFrom(Yii::$app->params["alertEmail"])
		->setTo($email["emails"])
		->setSubject("".strtolower($email["alert_type"])." alert from {$email["device_name"]}")
		->send();

	}
	
	public function sendSMS($alert){
		
		$template=NotificationTemplate::find()->where(["alert_type"=>$alert['alert_type'],"template_type"=>"sms"])->one();
		
		if(is_null($template))
		{
			//$this->sendEmail($email);
			return;
			
		}
		
		
		$html_content=$this->stringreplace($alert,$template->content_body);
		Yii::$app->sms->sendSms($alert["mobile"],$html_content);
		
	}
	
	public function saveNotification($id,$alertType,$content="",$payload,$device_ob){

			 $alerts=new DeviceAlert();
			 $alerts->customer_id=$device_ob->customer_id;
			 $alerts->group_id=$device_ob->group_id;
			 $alerts->device_id=$id;
			 $alerts->alert_type=$alertType;
			 $alerts->sms=0;
			 $alerts->body=$content;
			 $alerts->payload=json_encode($payload);
			 $alerts->status=DeviceAlert::STATUS_NOT_ACK;
			 $alerts->created_at=time();
			 $alerts->save(false);
			 		 
	}
	
	
	public function CheckDeviceAlertWDS($device,$model){
		
		 //Check Device alert without Subscriptions
		 
		$data=Json::decode($device->data);
		$config=Json::decode($device->config_new);
		$battery_level=$device->battery_level;
		
		$notification_content=[
				"temperature"=>$data["temperature"],
				"humidity"=>$data["humidity"],
				"battery"=>$data["battery"],
		];
		$sensor_value="";
		
		
		if($config["temperature_range"]["low"] > $data["temperature"])
		{
			
			$notification_content["value"]=$data["temperature"];
			
			$this->saveNotification($device->id,Constants::ALERT_LOW_TEMPERATURE,"",$notification_content,$device);
			
			Yii::trace("low Temperature");
			
			
			
		}elseif ($config["temperature_range"]["high"] < $data["temperature"])
		{
			$notification_content["value"]=$data["temperature"];
			$this->saveNotification($device->id,Constants::ALERT_HIGH_TEMPERATURE,"",$notification_content,$device);
			Yii::trace("heigh Temperature");
			
		}
		
		if($config["humidity_range"]["low"] > $data["humidity"])
		{
			
			if($data["humidity"] !=0)
			{
				$notification_content["value"]=$data["humidity"];
				$this->saveNotification($device->id,Constants::ALERT_LOW_HUMIDITY,"",$notification_content,$device);
				Yii::trace("low humidity");
				
			}else{
				
				Yii::trace("humidity Device Not Found..");
			}
			
			
		}elseif ($config["humidity_range"]["high"] < $data["humidity"])
		{
			$notification_content["value"]=$data["humidity"];
			$this->saveNotification($device->id,Constants::ALERT_HIGH_HUMIDITY,"",$notification_content,$device);
			Yii::trace("high humidity");
			
		}
		
		
		if($config["battery_range"]["low"] > $data["battery"]){
			$notification_content["value"]=$data["battery"];
			$this->saveNotification($device->id,Constants::ALERT_LOW_BATTERY,"",$notification_content,$device);
			Yii::trace("low battery");
			
		}
		
		
		
		
		
	}
	
	
	private function healthMessage($data ,$sensor_id){
		  
		  $status=true;
		 $device=Devices::find()->where(['hardware_serial'=>$sensor_id,'config_status'=>10])->one();
		 $d_config=Json::decode($device->config_temp);
		 
		 if($d_config['reporting_interval']!= $data['reporting_interval'])
		 {
		 	$status=false;
		 	
		 }
		 
		 if($d_config['sampling_inteval']!=$data['sesnsor_sampling_interval'])
		 {
		 	$status=false;
		 	
		 }
		 
		 if($d_config['temperature_range']['high']!=$data['temperature_high_threshold'])
		 {
		 	$status=false;
		 	
		 }
		 
		 if($d_config['temperature_range']['low']!=$data['temperature_low_threshold'])
		 {
		 	
		 	$status=false;
		 }
		 
		 if($d_config['humidity_range']['high']!=$data['humidity_high_threshold'])
		 {
		 	$status=false;
		 	
		 }
		 
		 if($d_config['humidity_range']['low']!=$data['humidity_low_threshold'])
		 {
		 	
		 	$status=false;
		 }
		 
		 if($status){
		 	
		 	if($device->type==1){
		 		
		 		$device->start_value=$d_config["start_value"];
		 		$device->end_value=$d_config["end_value"];	
		 	}
		 	$device->config_new=$device->config_temp;
		 	$device->config_status=1;
		 	$this->updateDeviceConfiguration($device, $d_config);
		 	$device->save(false);
		 }
		 
	}
	
	
	private function updateDeviceConfiguration($model,$config){
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>$model->start_value,
				'end' =>$config['temperature_range']['low'],
				'level' => 'LOW TEMPERATURE',
				'condition_code' =>60,
				'color_code' => 'Blue',
		];
		
		
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>$config['temperature_range']['low'],
				'end' =>$config['temperature_range']['high'],
				'level' => 'NORMAL TEMPERATURE',
				'condition_code' =>60,
				'color_code' => 'Green',
		];
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>$config['temperature_range']['high'],
				'end' =>$model->end_value,
				'level' => 'HIGH TEMPERATURE',
				'condition_code' =>60,
				'color_code' => 'Red',
		];
		
		DeviceConfigTemperature::deleteAll(["device_id"=>$model->id]);
		$range = new DeviceConfigTemperature();
		$range->device_id=$model->id;
		$range->customloadmultiple($temp);
		$range->saveMultiple();
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>0,
				'end' =>$config['humidity_range']['low'],
				'level' => 'LOW HUMIDITY',
				'condition_code' =>60,
				'color_code' => 'Blue',
		];
		
		
		
		$temp[]=[
				'device_id' =>$model->id,
				'start' =>$config['humidity_range']['low'],
				'end' =>$config['humidity_range']['high'],
				'level' => 'NORMAL HUMIDITY',
				'condition_code' =>60,
				'color_code' => 'Green',
		];
		
		$temp[]=[
				'device_id'=>$model->id,
				'start' =>$config['humidity_range']['high'],
				'end' =>100,
				'level' => 'HIGH HUMIDITY',
				'condition_code' =>60,
				'color_code' => 'Red',
		];
		
		
		DeviceConfigHumidity::deleteAll(["device_id"=>$model->id]);
		$config = new DeviceConfigHumidity();
		$config->device_id=$model->id;
		$config->customloadmultiple($temp);
		$config->saveMultiple();
		
		
	}
	private function getTimezone($group_id){
		
		$user=AdminUser::find()->select(['timezone'])->where(['group_id'=>$group_id])->asArray()->one();
		
		if(!empty($user))
		{
		 	
			$offset=TimeZone::find()->where(['id'=>$user['timezone']])->asArray()->one();
			if(!empty($offset))
			{
				
				return Yii::$app->utils->getTimezoneName($offset['gmt']);
				
			}
			
		}
		
		return Yii::$app->utils->getTimezoneName('');
	}
	
	public function validateTime($start,$end){
		
		$currentTime=new \DateTime();
		$start_time=\DateTime::createFromFormat('H:i:s', $start);
		$end_time=\DateTime::createFromFormat('H:i:s', $end);
		Yii::trace($start." ".$end." Current Time".$currentTime->format("H:i:s"));
		if ($currentTime > $start_time && $currentTime< $end_time)
		{
			Yii::trace(true);
			return true;
			
		}else{
			Yii::trace(false);
			 return false;
		}
		
	}
	
	public function validateTimeAutomatic($start,$end){
		
		$currentTime=new \DateTime();
		$start_time=\DateTime::createFromFormat('H:i', $start);
		$end_time=\DateTime::createFromFormat('H:i', $end);
		Yii::trace($start." ".$end." Current Time".$currentTime->format("H:i:s"));
		if ($currentTime >= $start_time && $currentTime< $end_time)
		{
			Yii::trace(true);
			return true;
			
		}else{
			Yii::trace(false);
			return false;
		}
		
	}
	
}