<?php
namespace api\models;


class Constants {
	
	
 	 const ALERT_HIGH_TEMPERATURE="HIGH TEMPERATURE"; 
 	 const ALERT_LOW_TEMPERATURE="LOW TEMPERATURE";
 	 const ALERT_NORMAL_TEMPERATURE="NORMAL TEMPERATURE";
 	 const ALERT_HIGH_HUMIDITY="HIGH HUMIDITY";
 	 const ALERT_LOW_HUMIDITY="LOW HUMIDITY";
 	 const ALERT_NORMAL_HUMIDITY="NORMAL HUMIDITY";
 	 const ALERT_DOOR_OPEN="DOOR OPEN";
 	 const ALERT_DOOR_CLOSE="DOOR CLOSE";
 	 const ALERT_BATTERY="BATTERY";
 	 const ALERT_OBEDIENCE="OBEDIENCE";
 	 const ALERT_LOW_BATTERY="LOW BATTERY";
 	 const ALERT_GPS_OVERSPEED="OVERSPEED";
 	 const ALERT_GPS_IDLE="IDLE";
 	 
 	 const INTERVEL_TYPE_MINUTES="MINUTES";
 	 const INTERVEL_TYPE_HOURS="HOURS";
 	 
 	 const CODE_GRATER_THAN=10;
 	 const CODE_LESS_THAN=20;
 	 const CODE_EQUAL_TO=30;
 	 const CODE_GREATER_THAN_EQUAL=40;
 	 const CODE_LESS_THAN_EQUAL=50;
 	 const CODE_BETWEEN=60;
 	 const CODE_NOT_BETWEEN=70;
 	 
 	 const GPS_CODE_IDLE=75;
 	 const GPS_CODE_OVERSPEED=80;
	
 	 
 	 public static function conditionChecking($condition,$valu1="",$value2="",$value3=""){
 	 	
 	 	$result=false;
 	 	 
 	 	switch ($condition) {
 	 		case self::CODE_GRATER_THAN:
 	 			 
 	 			if($valu1 > $value2)
 	 				$result=true;
 	 				 
 	 				break;
 	 		case self::CODE_LESS_THAN: //less then
 	 			 
 	 			if($valu1 < $value2)
 	 				$result=true;
 	 	
 	 				break;
 	 		case self::CODE_EQUAL_TO: //less then
 	 			 
 	 			if($valu1 == $value2)
 	 				$result=true;
 	 	
 	 				break;
 	 		case self::CODE_GREATER_THAN_EQUAL: 
 	 			 
 	 			//Greater than or equal to
 	 			 
 	 			if($valu1 <= $value2)
 	 				$result=true;
 	 	
 	 				break;
 	 		case self::CODE_LESS_THAN_EQUAL:
 	 			  
 	 			if($valu1 >= $value2)
 	 				$result=true;
 	 	
 	 				break;
 	 		case self::CODE_BETWEEN:
 	 			
 	 				if($valu1 > $value2 && $valu1 < $value3)
 	 				{
 	 					
 	 					$result=true;
 	 					
 	 				}else{
 	 					
 	 					 $result = false;
 	 					 
 	 				}
 	 			
 	 			break;
 	 		case self::CODE_NOT_BETWEEN:
 	 				
 	 			if($valu1 > $value2 && $valu1 < $value3)
 	 			{
 	 			
 	 				$result=false;
 	 			
 	 			}else{
 	 			
 	 				$result = true;
 	 					
 	 			}
 	 			
 	 			break;
 	 		default:
 	 			$result=false;
 	 			break;
 	 	}
 	 	 
 	 	return $result;
 	 	
 	 }
 	 
 	 public static function CheckGpsAlert($condition,$valu1="",$value2="",$value3=""){
 	 	
 	 	 $result=false;
 	 	 
 	 	 switch ($condition) {
 	 	 	case self::GPS_CODE_IDLE:
 	 	 		
 	 	 		if($valu1 ==1)
 	 	 		{
 	 	 			$result=true;
 	 	 		}
 	 	 		
 	 	 	  break;
 	 	 	case self::GPS_CODE_OVERSPEED: 
 	 	 		
 	 	 		if($valu1 < $value2)
 	 	 			$result=true;
 	 	 			break;
 	 	 	default:
 	 	 		$result=false;
 	 	 		break;
 	 	 }
 	 	 
 	 	return $result;
 	 	
 	 }
 	 
 	
	 
	
}

