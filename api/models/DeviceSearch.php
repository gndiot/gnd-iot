<?php

namespace api\models;

use Yii;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\Gps;
/**
 * This is the model class for table "devices".
 *
 * @property int $id
 * @property string $hardware_serial
 * @property int $customer_id
 * @property int $type
 * @property string $name
 * @property string $display_name
 * @property int $group_id
 * @property double $start_value
 * @property double $end_value
 * @property int $response_intervel
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $sync_time
 * @property int $sync_last_time
 * @property int $admin_id
 * @property string $config_new
 * @property string $config_temp
 * @property int $config_status
 * @property int $bc_req_status
 * @property int $bc_req_time
 * @property string $data
 * $property int $battery_level
 */
class DeviceSearch extends \yii\db\ActiveRecord
{
	
	 const TEMPERATURE='TEMPERATURE';
	 const GPS='GPS';
	 const DOOR='DOOR';
	 public $deviceRange=[];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['type'],'required'],
            [['customer_id', 'type', 'group_id', 'response_intervel', 'status', 'created_at', 'updated_at', 'sync_time', 'sync_last_time', 'admin_id', 'customer_user_id','config_status','bc_req_time','bc_req_status'], 'integer'],
            [['start_value', 'end_value'], 'number'],
        	['start_value', 'default', 'value' => 0],
        	['end_value', 'default', 'value' => 80],
            [['hardware_serial', 'display_name'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 50],
        	[['config_new','config_temp'],'string'],
        	['data','string'],
        	[['equipement_id','entity','department','process'],'string'],
        	[['battery_level','tag'],'integer'],
            [['hardware_serial'], 'unique'],
        ];
    }
    
    public function search($params){
    	
    	
    		$this->load($params,"");
    		
    		

    		
    		if($this->type==2)
    		{
    			
    			$query=Gps::find();
    	
    		
    		}else{
    			
    			
    			$query=Devices::find();
    		}
    	
		    	$dataProvider=new ActiveDataProvider([
		    			'query'=>$query,
		    			'pagination' => [
		    					'pageSizeLimit' => [0, 50],
		    			],
		    	]);
		    	
		    	
		    	
		    	if (!$this->validate()) {
		    		// uncomment the following line if you do not want to return any records when validation fails
		    		// $query->where('0=1');
		    		return $dataProvider;
		    	}
		    
		    	$query->andFilterWhere([
		    			'type'=>$this->type,
		    	]);
		    	return $dataProvider;
    	
    }
}
