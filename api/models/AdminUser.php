<?php

namespace api\models;

use Yii;
use yii\web\IdentityInterface;
use backend\modules\api\models\DeviceGroups;
use common\models\TimeZone;

/**
 * This is the model class for table "admin_user".
 *
 * @property int $id
 * @property int $group_id
 * @property string $group_ids
 * @property int $customer_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $email
 * @property int $status
 * @property string $timezone
 * @property string $timezone_description
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class AdminUser extends \yii\db\ActiveRecord implements IdentityInterface
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name','username','password_hash', 'email',], 'required','on'=>'create'],
        	[['first_name','username','password_hash', 'email',], 'required','on'=>'update'],
            [['status', 'created_by', 'created_at', 'updated_at','group_id','customer_id'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['first_name', 'last_name'], 'string', 'max' => 20],
            [['phone_number'], 'string', 'max' => 15],
        	[['group_ids'], 'string'],
            [['timezone'], 'string', 'max' => 10],
            [['timezone_description'], 'string', 'max' => 50],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
    	
        return [
            'id' => 'ID',
        	'group_id'=>'Group',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'status' => 'Status',
            'timezone' => 'Timezone',
            'timezone_description' => 'Timezone Description',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
    	return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    	
    	 $token=UserToken::find()->where(["access_token"=>$token])->one();
    	 if(!is_null($token))
    	 {
    	 	$token->created_at=time();
    	 	$token->save(false);
    	 	
    	 	return static::findOne(['id' => $token->user_id, 'status' => self::STATUS_ACTIVE]);
    	 }

    }
    
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
    	return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
    	if (!static::isPasswordResetTokenValid($token)) {
    		return null;
    	}
    
    	return static::findOne([
    			'password_reset_token' => $token,
    			'status' => self::STATUS_ACTIVE,
    	]);
    }
    
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
    	if (empty($token)) {
    		return false;
    	}
    
    	$timestamp = (int) substr($token, strrpos($token, '_') + 1);
    	$expire = Yii::$app->params['user.passwordResetTokenExpire'];
    	return $timestamp + $expire >= time();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
    	return $this->getPrimaryKey();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
    	return $this->auth_key;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
    	return $this->getAuthKey() === $authKey;
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
    	return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
    	$this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
    	$this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
    	$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    
    public static function activeRoles(){
    	
    
    	$roles=Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
    	
    	$result=[];
    	
    	foreach ($roles as $key=>$value)
    	{
    		array_push($result, $key);
    		 
    	}
    	
    	return $result;
    	
    	
    }
    
    public static function roles(){
    	 
    	$roles=Yii::$app->authManager->getRoles();
    	$result=[];
    	$is_admin=false;
    	 $current_roles=self::activeRoles();
  
    	if(in_array('admin', $current_roles) || in_array('super user', $current_roles))
    	{
    		$is_admin=true;
    		
    	}else{
    		
    		$is_admin=false;
    		
    	}
    
    	foreach ($roles as $key=>$value)
    	{
    		 
    		 $temp=[];
    		 
    		 $temp["key"]=$key;
    		 $temp["value"]=$key;
    		 
    		  if($is_admin)
    		  {
    		  	array_push($result, $temp);
    		  	
    		  }else{
    		  	
    		  	 
    		  	
    		  }
    		
    		 
    	}
    	
    	return $result;
    	 
    }
    
    public function getRoles(){
    	 
    	$roles=Yii::$app->authManager->getRolesByUser($this->id);
    	 
    	$result=[];
    	 
    	foreach ($roles as $key=>$value)
    	{
    		array_push($result, $key);
    		 
    	}
    	 
    	return $result;
    }
    
    public function getTimezone(){
    	
    	$timezone=TimeZone::find()->where(['id'=>$this->timezone])->asArray()->one();
    	if(!empty($timezone))
    	{
    		return $timezone['gmt'];
    	}else{
    		
    		 return '';
    	}
    	
    }
    
    
    public  function hasRole($role){
    	
    	$roles=Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
    	
    	
    	
    	foreach ($roles as $key=>$value)
    	{
    		if($role==$key)
    		{
    			return true;
    			break;
    		}
    		
    	}
    	 return false;
    	
    }
    
    public function getPermissions(){
    	 
    	$permissions=Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->id);
    	$result=[];
    	 
    	foreach ($permissions as $key=>$value)
    	{
    		array_push($result, $key);
    
    	}
    
    	return $permissions;
    
    }
    
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
    	$this->password_reset_token = null;
    }
    
    public function getGroup(){
    	
    	 
    	return $this->hasOne(DeviceGroups::className(), ["id"=>"group_id"]);
    	
    }
    
    
    public function getGroups(){
    	
    	  $groups=explode("|", $this->group_ids);
    	  
    	  return DeviceGroups::find()->select(["id","name"])->where(["id"=>$groups])->all();
    	
    }
    
    public function fields(){
    	
    	 $fields= parent::fields();
    	 
    	 unset($fields["auth_key"],$fields['password_hash']);
    	 
    	 $fields['role']=function($model){
    	 	  
    	 	 return $model->roles;
    	 };
    	     	 
    	 $fields["group"]=function($model){
    	 	
    	 		return $model->group;
    	 	
    	 };
    	 
    	 $fields['permissions']=function($model){
    	 		
    	 	return $model->permissions;
    	 };
    	 
    	 return $fields;
    	
    	
    }
    
}
