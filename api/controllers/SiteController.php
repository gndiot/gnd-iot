<?php

namespace api\controllers;

use yii;
use yii\rest\Controller;

class SiteController extends Controller
{
    public function actionIndex()
    {
        return ['Hello World!'];
    }

    protected function verbs(){
    	
    	return [
    			'index' => ['GET','HEAD'],
    			'view' => ['GET', 'HEAD'],
    			'create' => ['POST'],
    			'update' => ['PUT', 'PATCH'],
    			'delete' => ['DELETE'],
    	];
    	
    }
}
