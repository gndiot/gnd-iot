<?php

namespace api\modules\v1\controllers;

use yii;
use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use api\models\AdminUser;

class DashboardController extends Controller
{
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		
		$behaviors['authenticator']=[
				'class'=>HttpBasicAuth::className(),
				'auth'=>[$this,'auth'],
		];
		
		return $behaviors;

	}
	
	public function auth($key,$token){
		
		return AdminUser::findIdentityByAccessToken($token);	
	}
	
    public function actionIndex()
    {
        return ['Dashboard Controller'];
    }
    
    
    public function actionSensors(){
    	
    		

    	
    }
    
    protected function verbs(){
    	
    	return [
    			'index' => ['GET','HEAD'],
    			'view' => ['GET', 'HEAD'],
    			'create' => ['POST'],
    			'update' => ['PUT', 'PATCH'],
    			'delete' => ['DELETE'],
    	];
    	
    }
    
}
