<?php

namespace api\modules\v1\controllers;

use yii;
use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use api\models\AdminUser;
use api\models\Devices;
use api\modules\v1\models\Temperature;
use api\modules\v1\models\Door;
use api\modules\v1\models\Gps;
use api\models\DeviceSearch;
use api\modules\v1\models\DeviceGroups;
use yii\helpers\ArrayHelper;

class SensorsController extends Controller
{
	
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	public function behaviors(){
		
		$behaviors=parent::behaviors();
		
		$behaviors['authenticator']=[
				'class'=>HttpBasicAuth::className(),
				'auth'=>[$this,'auth'],
		];
		
		return $behaviors;
	}
	
	public function auth($key,$token){
		
		return AdminUser::findIdentityByAccessToken($token);	
	}
	
    public function actionIndex()
    {
    	
    	  $filter=Yii::$app->getRequest()->get();
          $search=new DeviceSearch();
          $dataProvider=$search->search($filter);
          
          return $dataProvider;
         
    }
    
    public function actionTypes(){
    	
    	
    	if(Yii::$app->user->can('super user'))
    	{
    		$connection=Yii::$app->db;
    		$command = $connection->createCommand('SELECT count(devices.id) as sensors_count,type FROM `devices` GROUP BY type');
    		
    		return $command->queryAll();
    		
    	}else{
    		
    		$groups=DeviceGroups::getChildrenWithParent(Yii::$app->user->identity->group_id);
    		
    		$gids=ArrayHelper::getColumn($groups, 'id');
    		
    		$ids=implode(",", $gids); //tostring ex 1,2,3
    		
    		$connection=Yii::$app->db;
    		$command = $connection->createCommand('SELECT count(devices.id) as sensors_count,type FROM `devices` WHERE group_id IN (:groupids) GROUP BY type',[':groupids'=>$ids]);
    		
    		return $command->queryAll();
    		
    	}
    	
    
    }
    
    public function actionView($id,$type){
    	
    			switch ($type)
    			{
    			  
    				case 1:
    					
    					return Temperature::find()->andWhere(["id"=>$id])->one();
    					
    				break;
    				case 2:
    					
    					return Gps::find()->andWhere(["id"=>$id])->one();
    					
    				break;
    				case 3:
    					
    					return Door::find()->andWhere(["id"=>$id])->one();
    					
    				 break;
    			}
    	
    			return [];
    }
    
    
    protected function verbs(){
    	
    	return [
    			'index' => ['GET','HEAD'],
    			'view' => ['GET', 'HEAD'],
    			'create' => ['POST'],
    			'update' => ['PUT', 'PATCH'],
    			'delete' => ['DELETE'],
    	];
    	
    }
    
}
