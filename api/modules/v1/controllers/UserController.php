<?php

namespace api\modules\v1\controllers;

use yii;
use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use api\models\AdminUser;
use api\models\LoginForm;
use api\models\UserToken;

class UserController extends Controller
{
	

	public function auth($key,$token){
		
		return AdminUser::findIdentityByAccessToken($token);	
	}
	
    public function actionIndex()
    {
        return ['Dashboard Controller'];
    }
    
    public function actionLogin(){
    	
    	$model = new LoginForm();
    	if ($model->load(Yii::$app->request->post(),"") && $model->login()) {
    		
    		$token=new UserToken();
    		$token->access_token=Yii::$app->security->generateRandomString();
    		$token->user_id=$model->accessToken["id"];
    		if($token->save(false))
    		{
    			return $token->access_token;
    			
    		}else{
    			
    			Yii::$app->getResponse()->setStatusCode(422);
    			return $model->getErrors();
    		}
    		
    	} else {
    		
    		Yii::$app->getResponse()->setStatusCode(422);
    		return $model->getErrors();
    	}
    	
    }
    
    public function actionToken($token){
    	
    		$model=new UserToken();
    		$model->load(Yii::$app->request->post(),"");
    		if($model->validate())
    		{
    			  $userToken=UserToken::find()->where(["access_token"=>$token])->one();
    			  
    			  if(!is_null($userToken))
    			  {
    			  		$userToken->load(Yii::$app->request->post(),"");
    			  		$userToken->save();
    			  		return "token has been updated ..";
    			  }else{
    			  	
    			  	$model->addError('access_token',"Invalid Access Token");
    			  	Yii::$app->getResponse()->setStatusCode(422);
    			  	return $model->getErrors();
    			  }
    			
    		}
    		
    	
    }
    
    protected function verbs(){
    	
    	return [
    			'index' => ['GET','HEAD'],
    			'view' => ['GET', 'HEAD'],
    			'token' => ['POST'],
    			'create' => ['POST'],
    			'update' => ['PUT', 'PATCH'],
    		    'login'=>['POST'],
    			'delete' => ['DELETE'],
    	];
    	
    }
    
}
