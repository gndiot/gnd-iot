<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "device_gps_history".
 *
 * @property string $id
 * @property int $device_id
 * @property string $hardware_serial
 * @property string $payload_raw
 * @property string $message_type
 * @property string $temperature_value
 * @property int $humidity_value
 * @property string $latitude
 * @property string $longitude
 * @property int $speed
 * @property int $movement
 * @property int $power_status
 * @property int $battery_status
 * @property int $battery_value
 * @property int $sensor_battery_value
 * @property int $created_at
 */
class GpsHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_gps_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id', 'humidity_value', 'movement', 'power_status', 'battery_status', 'battery_value', 'sensor_battery_value', 'created_at'], 'integer'],
            [['payload_raw'], 'string'],
        		[['temperature_value', 'latitude', 'longitude', 'speed'], 'number'],
            [['created_at'], 'required'],
            [['hardware_serial'], 'string', 'max' => 255],
            [['message_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'hardware_serial' => 'Hardware Serial',
            'payload_raw' => 'Payload Raw',
            'message_type' => 'Message Type',
            'temperature_value' => 'Temperature Value',
            'humidity_value' => 'Humidity Value',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'speed' => 'Speed',
            'movement' => 'Movement',
            'power_status' => 'Power Status',
            'battery_status' => 'Battery Status',
            'battery_value' => 'Battery Value',
            'sensor_battery_value' => 'Sensor Battery Value',
            'created_at' => 'Created At',
        ];
    }
}
