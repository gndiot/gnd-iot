<?php
namespace api\modules\v1\models;

use Yii;
use api\models\Devices;


class Gps extends Devices{
	
	public function getGps(){		 
		return $this->hasOne(DeviceGpsReading::className(),["device_id"=>"id"])->select(["hardware_serial","payload_raw","l_latitude","l_longitude"])->orderBy(["id"=>SORT_DESC]);
	}
	
	public static function find(){
		
		return parent::find()->where(["type"=>2]);
	}
	
	
	public function getConfiguration(){
		
		return $this->hasOne(GpsConfigurations::className(), ["device_id"=>"id"]);
		
	}
	
	public function getConfigurationTemp(){
		
		return $this->hasOne(GpsConfigurationsTemp::className(), ["device_id"=>"id"]);
	}
	
	public function getRecentData(){
	
		return $this->hasOne(GpsRecentHistory::className(), ["device_id"=>"id"]);
	}
	
	
	public function fields(){
		
		$fields=parent::fields();
		
		return $fields;

	}

	public function extraFields(){
		
		return ['configuration','configurationTemp','recentData'];
	}
	
	
}