<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "dev_config_humidity".
 *
 * @property int $id
 * @property int $device_id
 * @property double $start
 * @property double $end
 * @property string $level
 * @property int $condition_code
 * @property string $color_code
 */
class DeviceConfigHumidity extends \yii\db\ActiveRecord
{
	public $deviceRange=[];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dev_config_humidity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id', 'condition_code'], 'integer'],
            [['start', 'end'], 'number'],
            [['level'], 'string', 'max' => 255],
            [['color_code'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'start' => 'Start',
            'end' => 'End',
            'level' => 'Level',
            'condition_code' => 'Condition Code',
            'color_code' => 'Color Code',
        ];
    }
    

    public function customloadmultiple($data){
    
    	for ($i = 0; $i < count($data); $i++) {
    
    		$obj=new DeviceConfigHumidity();
    		$obj->loadDefaultValues();
    		$obj->load($data[$i],"");
    		$obj->device_id=$this->device_id;
    		array_push($this->deviceRange, $obj);
    	}
    
    }
    
    public function saveMultiple(){
    
    	$count=count($this->deviceRange);
    
    	if($count >0)
    		DeviceConfigHumidity::deleteAll(["device_id"=>$this->device_id]);
    		 
    		for ($i = 0; $i < $count; $i++) {
    			$this->deviceRange[$i]->device_id=$this->device_id;
    			$this->deviceRange[$i]->save(false);
    		}
    
    		return true;
    		 
    }
    
    public function customValidationMultiple()
    {
    
    	$status=true;
    
    	$count=count($this->deviceRange);
    
    	for ($i = 0; $i < $count; $i++) {
    		 
    		if(!$this->deviceRange[$i]->validate())
    		{
    			$status=false;
    		}
    		 
    	}
    	
    	return $status;
    
    }
    
    public function getMultipleValidation(){
    
    
    	$errors=[];
    
    	$count=count($this->deviceRange);
    	for ($i = 0; $i < $count; $i++) {
    		 
    		if(!$this->deviceRange[$i]->validate())
    		{
    			array_push($errors,$this->deviceRange[$i]->getErrors());
    
    		}else{
    
    			array_push($errors,[]);
    
    		}
    		 
    	}
    
    	return $errors;
    
    }
}
