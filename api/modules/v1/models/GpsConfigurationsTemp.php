<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "device_gps_configurations_temp".
 *
 * @property string $id
 * @property int $device_id
 * @property int $stauts
 * @property string $low_temperature
 * @property string $high_temperature
 * @property int $start_value
 * @property int $end_value
 * @property int $temperature_alert
 * @property int $humidity_alert
 * @property int $high_humidity
 * @property int $low_humidity
 * @property int $movement_alert
 * @property int $power_alert
 * @property int $battery_alert
 * @property int $battery_low_value
 * @property int $periodic_interval
 * @property int $updated_at
 */
class GpsConfigurationsTemp extends \yii\db\ActiveRecord
{
	const TASK_SCHEDULE=10;
	const TASK_PROCESSING=15;
	const TASK_COMPLETED=1;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_gps_configurations_temp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id', 'stauts', 'start_value', 'end_value', 'temperature_alert', 'humidity_alert', 'high_humidity', 'low_humidity', 'movement_alert', 'power_alert', 'battery_alert', 'battery_low_value', 'periodic_interval', 'updated_at'], 'integer'],
            [['low_temperature', 'high_temperature'], 'number'],
            [['updated_at'], 'required'],
        	[['high_humidity','low_humidity'],'integer','min'=>0,'max'=>100]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'stauts' => 'Stauts',
            'low_temperature' => 'Low Temperature',
            'high_temperature' => 'High Temperature',
            'start_value' => 'Start Value',
            'end_value' => 'End Value',
            'temperature_alert' => 'Temperature Alert',
            'humidity_alert' => 'Humidity Alert',
            'high_humidity' => 'High Humidity',
            'low_humidity' => 'Low Humidity',
            'movement_alert' => 'Movement Alert',
            'power_alert' => 'Power Alert',
            'battery_alert' => 'Battery Alert',
            'battery_low_value' => 'Battery Low Value',
            'periodic_interval' => 'Periodic Interval',
            'updated_at' => 'Updated At',
        ];
    }
}
