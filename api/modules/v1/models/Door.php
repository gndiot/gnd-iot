<?php

namespace api\modules\v1\models;

use Yii;
use api\models\Devices;

class Door extends Devices{
	
	public function getDoor(){		 
		return $this->hasOne(DeviceDoorReading::className(),["device_id"=>"id"])->select(["hardware_serial","payload_raw","battery","status"])->orderBy(["id"=>SORT_DESC]);
	}
	
	public static function find(){
	
		return parent::find()->where(["type"=>3]);
	
	}
	

	public function getBatteryConfig(){
	
		return $this->hasMany(DeviceConfigBattery::className(), ["device_id"=>"id"]);
	
	}

	public function fields(){
		
		$fields=parent::fields();
		
		$fields['data']=function($model){
				
			return $model->door;
				
		};
		
		$fields["config"]=function($model){
				
			return [
					"battery"=>$model->batteryConfig
			];
		
		};
		return $fields;
		
	}
	
}