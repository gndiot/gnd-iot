<?php
date_default_timezone_set('Asia/Dubai');
$timestamp = date("Y-m-d H:i:s");
define('FILE_SAVED_PATH',realpath(__DIR__.'/backend/web/ajic/'));
define('GROUP_ID',1);
defined('YII_ENV') or define('YII_ENV', 'dev');

$servername = "localhost";
$username = "thinxsan_iotpro";
$password = "admin@123";
$dbname = "thinxsan_rajtech";

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	//$stmt = $conn->prepare("SELECT id,name,customer_id,group_id,config_new,hardware_serial FROM devices where type=:type");
	//$stmt->execute(['type'=>1]);
	// set the resulting array to associative
	//$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
	//$data=$stmt->fetchAll();
	 
	 getReports($conn);
	 
	//print_r($data);
	
}
catch(PDOException $e) {
	echo "Error: " . $e->getMessage();
}
$conn = null;

function getReports($conn){

	$begin = new \DateTime();
	$begin->modify("today");
	$end = new \DateTime();
	$interval = new \DateInterval('PT4H');
	$daterange = new \DatePeriod($begin, $interval ,$end);
	foreach($daterange as $date){
		
		$f1=$date->format('Y-m-d-H-i');
		$start=$date->getTimestamp();
		$date->modify("+4 hour");
		$date->modify("-1 minute");
		$f2=$date->format('Y-m-d-H-i');
		$end=$date->getTimestamp();
		//echo $start."-".$end."\n";
		//echo $f1."-".$f2."\n";
		$filename=$f1."-".$f2.".xml";
		
		if(file_exists(FILE_SAVED_PATH."/".$filename))
		{
			echo "File already exiest \n";
			continue;
		}
		
		$stmt = $conn->prepare("SELECT devices.name,devices.display_name,devices.equipement_id,device_groups.name as location,master_entity.name as entity,master_department.name as department,master_process.name as process,temp.temperature,temp.humidity,temp.created_at FROM `device_temperature_readings` as temp LEFT JOIN devices ON devices.hardware_serial=temp.`hardware_serial` LEFT JOIN master_entity ON master_entity.id=devices.entity LEFT JOIN master_department ON master_department.id=devices.department LEFT JOIN master_process ON master_process.id=devices.process LEFT JOIN device_groups ON device_groups.id=devices.group_id WHERE devices.customer_id=:customer_id AND temp.created_at BETWEEN :start AND :end");
		$stmt->execute(['customer_id'=>GROUP_ID,'start'=>$start,'end'=>$end]);
		// set the resulting array to associative
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		$data=$stmt->fetchAll();
		$header=[
				'name'=>"name",
				'display_name'=>"display_name",
				'equipment_id'=>'Equipment Id',
				'location'=>'location',
				'entity'=>'entity',
				'department'=>'department',
				'process'=>'process',
				'temperature'=>'temperature',
				'humidity'=>'humidity',
				'created_at'=>'Time'
		];
		
		array_unshift($data,$header);
		//print_r($data);
		
		// function call to convert array to xml
		//$this->array_to_xml($data,$xml_data);
		
		//$folder=Yii::getAlias("@app")."/web/ajic";
		$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><rows></rows>');
		// function call to convert array to xml
		array_to_xml($data,$xml_data);
		
		print $xml_data->asXML(FILE_SAVED_PATH."/".$filename);
		
		
		
	}
	
}




function array_to_xml( $data, &$xml_data ) {
	foreach( $data as $key => $value ) {
		if( is_numeric($key) ){
			$key = 'row'; //dealing with <0/>..<n/> issues
		}
		if( is_array($value) ) {
			$subnode = $xml_data->addChild($key);
			array_to_xml($value, $subnode);
		} else {
			if($key=='created_at' && $value !="Time")
			{
				$date= new \DateTime();
				$date->setTimestamp(intval($value));
				$xml_data->addChild("$key",htmlspecialchars($date->format('Y-m-d H:i:s')));
				
				continue;
			}
			$xml_data->addChild("$key",htmlspecialchars("$value"));
		}
	}
}
