<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
	'name'=>"",
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
	'modules' => [
		'api' => [
				'class' => 'frontend\modules\api\Module',
		],	
	],
    'components' => [
        'request' => [
        	'parsers' => [
        		'application/json' => 'yii\web\JsonParser',
        	],
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\Customer',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
    	'authManager'  => [
    		 'class'=> 'yii\rbac\DbManager',
    		  'assignmentTable'=> '{{%customer_Auth_Assignment}}',
    		  'itemChildTable'=> '{{%customer_Auth_Item_Child}}',
    		  'itemTable'=> '{{%customer_Auth_Item}}',
    		  'ruleTable'=> '{{%customer_Auth_Rule}}',
    	],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            		'login' => 'site/login',
            		'logout' => 'site/logout',
            		'/api'=>'api',
            		'/' => 'site/index',
            		'/<url:[a-zA-Z0-9-_]+>' => 'site/index',
            ],
        ],
        
    ],
    'params' => $params,
	'timeZone'=>'Asia/Kolkata'
];
