<?php

namespace frontend\modules\api\controllers;


use yii;
use yii\rest\Controller;
use common\models\Customer;


/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{

    /**
     * Renders the index view for the module
     * @return string
     */
    protected function verbs(){
    
    	return [
    			'index' => ['GET','HEAD'],
    			'view' => ['GET', 'HEAD'],
    			'create' => ['POST'],
    			'update' => ['PUT', 'PATCH'],
    			'delete' => ['DELETE'],
    	];
    
    }
    
    
    public function actionIndex()
    {
    	$UTC_offset = '+05:30';
    	$date       = new \DateTime('now');
    	$timezone  = new \DateTimeZone(str_replace(':', '', $UTC_offset));
    	//$date->setTimezone($timezone);
    	
    	// echo $date->format("Y-m-d H:h:s");
    	 
    	 //echo strtotime(time(),'Y-m-d');
    	 
    	//var_dump($date);
    	
    //	die();
    	
    	return  Customer::find()->select(["auth_key AS token"])->asArray()->one();
    }
}
