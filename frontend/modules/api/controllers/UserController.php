<?php
namespace frontend\modules\api\controllers;

use yii;
use yii\rest\Controller;
use common\models\Customer;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\ArrayHelper;

class UserController extends Controller{
	
	 
	
	public function actionIndex(){
		
		
		
		$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getChildren($user->group_id);
		$groups= ArrayHelper::getColumn($groups, "parent_id");
		return Customer::find()->where(["customer_id"=>$user->customer_id,"group_id"=>$groups])->all();
	}

}