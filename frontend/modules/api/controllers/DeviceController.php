<?php
namespace frontend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use common\models\Devices;
use yii\web\NotFoundHttpException;
use common\models\DeviceConfigRange;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
//use backend\modules\api\models\Devices;


class DeviceController extends Controller{
	
	 
	public function behaviors(){
	
		$behaviors=parent::behaviors();
		$behaviors["access"]=[ 
				'class' => AccessControl::className(),
				'rules' => [
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
				'denyCallback' => function ($rule, $action) {
				 
				throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
				}
				];
			
		return $behaviors;
	}
	
	protected function verbs(){
	
		return [
				'index' => ['GET','HEAD'],
				'view' => ['GET', 'HEAD'],
				'update' => ['PUT', 'PATCH'],
				'delete' => ['DELETE'],
		];
	
	}
	
	
	public function actionIndex()
	{
		
		 $user=Yii::$app->user->identity;
		 $groups=DeviceGroups::getChildren($user->group_id);
		 $groups= ArrayHelper::getColumn($groups, "parent_id");
		return new ActiveDataProvider([
				'query'=>Devices::find()->where(["group_id"=>$groups,"customer_id"=>$user->customer_id])
		]);
		
	}
	
	public function actionGroup($id){
		
		$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getChildren($user->group_id);
		$groups= ArrayHelper::getColumn($groups, "parent_id");
		$list=Devices::find()->select(["id","name"])->where(["group_id"=>$groups,"customer_id"=>$user->customer_id])->all();
		
		return $list;
	}
	
	
	public function actionTemperature($id=""){
		
		$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getChildren($user->group_id);
		$groups= ArrayHelper::getColumn($groups, "parent_id");
		$list=Temperature::find()->andFilterWhere(["group_id"=>$groups,"customer_id"=>$user->customer_id])->all();
		
		return $list;
	}
	
	public function actionDoor($id=""){
		
		$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getChildren($user->group_id);
		$groups= ArrayHelper::getColumn($groups, "parent_id");
		$list=Door::find()->andFilterWhere(["group_id"=>$groups,"customer_id"=>$user->customer_id])->all();
		return $list;
	}
	
	public function actionGps($id=""){
		
		$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getChildren($user->group_id);
		$groups= ArrayHelper::getColumn($groups, "parent_id");
		$list=Gps::find()->andFilterWhere(["group_id"=>$groups,"customer_id"=>$user->customer_id])->all();
		return $list;
	}
	
	public function actionUpdate($id){
		
		    $model = Devices::find()->where(["id"=>$id])->one();
		   
		    if(is_null($model))
		    {
		    	
		    	throw new NotFoundHttpException("Object not found: $id");
		    }
		    
		    $rdata=Yii::$app->getRequest()->getBodyParams();
		    
		    if(empty($rdata))
		    {
		    	throw new NotFoundHttpException("resource not found");
		    }
		    
		    //unset($rdata["id"]);
		    
		    $model->load($rdata,"device");
		    $model->customloadMultiple($rdata, "range");
		    $model->updated_at=time();
		    
		    
		   
		    
		    if($model->save() && $model->loadMultipleValidation())
		    {
		    	
		    	if(array_key_exists("range", $rdata) && is_null($rdata["range"]))
		    	{
		    	
		    		DeviceConfigRange::deleteAll(["device_id"=>$id]);
		    	}else{
		    		
		    		$model->saveMultiple();
		    	}
		    	
		    	 
		    	 return $model;
		    	
		    	
		    }else{
		    	
		    	$error=[];
		    	if(isset($rdata["device"]))
		    	{
		    		$error["device"]=$model->getErrors();
		    		
		    	}
		    	
		    	if(isset($rdata["range"]))
		    	{
		    		$error["range"]=$model->getMultipleValidation();
		    		
		    	}
		    	
		    	Yii::$app->getResponse()->setStatusCode(422);
		    	return $error;
		    	
		    }
		
		    
		    
		    
		 
		
	}
	
	
	
}