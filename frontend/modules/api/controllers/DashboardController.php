<?php
namespace frontend\modules\api\controllers;
use yii;
use yii\rest\Controller;
use common\models\Temperature;
use common\models\Door;
use common\models\Gps;
use backend\modules\api\models\DeviceGroups;
use yii\helpers\ArrayHelper;
use common\models\Customer;

class DashboardController extends Controller{
	
	
	public function actionIndex(){
		
	
		 
		
	}
	
	public function actionDevices(){
		
		$user=Yii::$app->user->identity;
		$groups=DeviceGroups::getChildren($user->group_id);
		$groups= ArrayHelper::getColumn($groups, "parent_id");
		
		
		
		$temperature= Temperature::find()->andWhere(["group_id"=>$groups,"customer_id"=>$user->customer_id])->count();
		$door = Door::find()->andWhere(["group_id"=>$groups,"customer_id"=>$user->customer_id])->count();
		$gps= Gps::find()->andWhere(["group_id"=>$groups,"customer_id"=>$user->customer_id])->count();
		
		$customers =Customer::find()->where(["group_id"=>$groups,"customer_id"=>$user->customer_id])->count();
		
		
		return [
				"temperature"=>$temperature,
				"door"=>$door,
				"gps"=>$gps,
				"users"=>$customers
		];
		
		
		
		 
	}
	
}