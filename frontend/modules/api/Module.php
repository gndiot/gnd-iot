<?php

namespace frontend\modules\api;
use yii;
use common\models\Customer;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->registerComponent();
        
        if(Yii::$app->user->isGuest)
        {
        	Yii::$app->user->enableSession=false;
        	Yii::$app->user->enableAutoLogin=false;
        	Yii::$app->user->loginUrl=null;
        	$token=Yii::$app->request->get("token","");
        	$user=Customer::find()->where(["auth_key"=>$token])->one();
        	if(!is_null($user))
        	{
        		Yii::$app->user->login($user);
        	}
        }
         
        \Yii::configure($this, require __DIR__ . '/config/config.php');
    }
    
    
    
    private function registerComponent(){
    	 
    	Yii::$app->setComponents([
    			'response' => [
    					'class' => 'yii\web\Response',
    					'format' => yii\web\Response::FORMAT_JSON,
    					'formatters' => [
    							'application/json' => yii\web\Response::FORMAT_JSON,
    							'application/xml' => yii\web\Response::FORMAT_XML,
    					],
    					'charset' => 'UTF-8',
    					'on beforeSend' => function ($event) {
    					$response = $event->sender;
    					if ($response->data !== null) {
    						if($response->isSuccessful)
    						{
    							$response->data = [
    									'success' => $response->isSuccessful,
    									'data' => $response->data,
    									'statusCode'=>$response->statusCode,
    									'error'=>null
    							];
    
    						}else{
    
    							$response->data = [
    									'success' => $response->isSuccessful,
    									'data' =>null,
    									'statusCode'=>$response->statusCode,
    									'error'=> $response->data,
    							];
    
    
    						}
    						$response->statusCode = 200;
    					}
    					},
    					]
    					]);
    	 
    }
}
