<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    		'//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
    		'css/font-awesome.min.css',
    		'css/css/ionicons.min.css',
    		'//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
    		//'css/site.css',
    		'css/AdminLTE.min.css',
    		'css/_all-skins.css'
    ];
    public $js = [
    		'//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
    		'js/adminlte.js',
    		'js/main.959f8895.js'
    ];
}
