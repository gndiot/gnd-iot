<?php
namespace console\controllers;

use yii;
use yii\console\Controller;
use backend\models\AdminUser;
use backend\models\AdminSession;
use common\models\TimeZones;
use console\models\DownloadJob;

class CronController extends Controller{
	
	
 	public function actionIndex(){
 	
 		echo Yii::$app->queue->push(new DownloadJob([
 				'url' => 'http://omsdev.nekhop.com/image/75/75/1523443604808469480.jpg',
 				'file' => Yii::getAlias("@app")."/images/test4.jpg",
 		]));
 		
 		echo $fileinput=Yii::getAlias("@app")."/images/test.txt";
 	}
	
}