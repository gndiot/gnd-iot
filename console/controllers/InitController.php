<?php
namespace console\controllers;

use yii;
use yii\console\Controller;
use backend\models\AdminUser;
use backend\models\AdminSession;
use common\models\TimeZones;

class InitController extends Controller{
	
	
	 public function actionIndex(){
	 	
	 	
	 }
	 
	 public function actionApp(){
	 	     $this->truncateTables();
	 		 $this->updateTimezone();
	 		// $this->initPermissions();
	 		 //$this->DefaultRole();
	 		 $this->actionInstall();
	 		 $this->initadminusers();
	 }
	 
	 private function truncateTables(){
	 	
	 	 Yii::$app->db->createCommand()->truncateTable("time_zones")->execute();
	 	 Yii::$app->db->createCommand()->truncateTable("admin_session")->execute();
	 	 Yii::$app->db->createCommand()->truncateTable("admin_user")->execute();
	 	
	 	
	 }
	 
	 
	 private function DefaultRole(){
	 	
	 	$auth=Yii::$app->authManager;
	 	$superUser=$auth->createRole("super user");
	 	$admin = $auth->createRole("admin");
	 	$auth->add($superUser);
	 	$auth->add($admin);
	 	$auth->addChild($superUser, $admin);
	 	$permissions=$this->permissions();
	 	for ($i = 0; $i < count($permissions); $i++) {
	 		$manageProduct=$auth->getPermission($permissions[$i]);
	 		//$auth->add($manageProduct);
	 		$auth->addChild($admin, $manageProduct);
	 	}
	 	
	 	echo "Roles created successfully...".PHP_EOL;
	 }
	 
	 
	 private function initPermissions(){
	 	
	 	$auth=Yii::$app->authManager;
	 		
	 	$auth->removeAll();
	 		
	 	$permissions=$this->permissions();
	 	
	 	for ($i = 0; $i < count($permissions); $i++) {
	 	
	 		$mpermissions=$auth->createPermission($permissions[$i]);
	 		$auth->add($mpermissions);
	 	}
	 	echo "Permissions added successfully...".PHP_EOL;
	 }
	 
	 private function permissions(){
	 
	 	return [
	 			"Admin Dashboard",
	 			"Group Dashboard",
	 			'Temperature',
	 			'Door',
	 			'Gps',
	 			'Subscriptions',
	 			'Subscription Add',
	 			'Subscription Update',
	 			'Subscription Delete',
	 			'Groups',
	 			'Group Add',
	 			'Group Delete',
	 			'Group Update',
	 			'Devices',
	 			'Device Add',
	 			'Device Delete',
	 			'Device update',
	 			'Device Config Update',
	 			'Device Config Update Multiple',
	 			'Email Alert Template Update',
	 			'Sms Alert Template Update',
	 			'Change Own Password',
	 			'Users',
	 			'User Add',
	 			'User Update',
	 			'User Delete',
	 			'User Change Password',
	 			'Group Users',
	 			'Group User Add',
	 			'Group User Delete',
	 			'Group User Update',
	 			'Reports',
	 			'Report Temperature',
	 			'Report Temperature Corrective',
	 			'Report Door',
	 			'Report Gps',
	 			'Report Download',
	 	];
	 }
	 
	 
	 public function actionInstall(){
	 	
	 		$auth = Yii::$app->authManager;
	 		
	 		 $this->initPermissions();
	 		
	 		 $roles=$this->defaultRolePermissions();
	 		 
	 		 
	 		 for ($i = 0; $i < count($roles); $i++) {
	 		 	
	 		 		$role=$auth->createRole($roles[$i]['role']);
	 		 		
	 		 		 if($auth->add($role)){
	 		 		 	   
	 		 		 	
	 		 		 	foreach ($roles[$i]['permissions'] as $permissions)
	 		 		 	{
	 		 		 		 $pemission=$auth->getPermission($permissions);
	 		 		 		 
	 		 		 		  if(!is_null($pemission))
	 		 		 		  {
	 		 		 		  	  $auth->addChild($role, $pemission);
	 		 		 		  }
	 		 		 		 
	 		 		 		//echo $permissions." \n";
	 		 		 		 
	 		 		 		 
	 		 		 	}
	 		 		 	
	 		 		 	
	 		 		 }
	 		 		
	 		 		
	 		 		
	 		 	
	 		 }
	 		 
	 		 
	 		 
	 
	 	
	 	
	 }
	 
	 private function defaultRolePermissions(){
	 	
	 		   
	 	     return [
	 	     		
	 	     		[
	 	     			'role'=>'super user',
	 	     			'permissions'=>[
				 			"Admin Dashboard",
				 			'Groups',
	 	     				'Group Add',
	 	     				'Group Delete',
	 	     				'Group Update',
	 	     				'Device Delete',
	 	     				'Device Config Update',
	 	     				'Device Config Update Multiple',
	 	     				'Email Alert Template Update',
	 	     				'Sms Alert Template Update',
	 	     				'Users',
	 	     				'User Add',
	 	     				'User Update',
	 	     				'User Delete',
	 	     					
				 		]
	 	     		],
	 	     		[
	 	     		'role'=>'Group Admin',
	 	     		'permissions'=>[
	 	     				'Group Users',
	 	     				'Group User Add',
	 	     				'Group User Update',
	 	     				'Group User Delete',
	 	     				'Devices',
	 	     				'Device update',
	 	     				'Subscriptions',
	 	     				'Subscription Add',
	 	     				'Subscription Update',
	 	     				'Subscription Delete',
	 	     		]
	 	     		],
	 	     		[
	 	     		'role'=>'Group Manager',
	 	     		'permissions'=>[
	 	     				'Reports',
	 	     				'Report Temperature',
	 	     				'Report Door',
	 	     				'Report Gps',
	 	     				'Report Download'
	 	     			]
	 	     		],
	 	     		[
	 	     				'role'=>'Group User',
	 	     				'permissions'=>[
	 	     						'Group Dashboard',
	 	     						'Temperature',
	 	     						'Door',
	 	     						'Gps',
	 	     						'Report Temperature Corrective',
	 	     						'User Change Password'
	 	     				]
	 	     		]
	 	     		
	 	     ];
	 	
	 	
	 }
	 
	  private function initadminusers(){
	  		
	  		$user=new AdminUser();
	  		$user->username="Admin";
	  		$user->auth_key=Yii::$app->security->generateRandomString();
	  		$user->password_hash=Yii::$app->getSecurity()->generatePasswordHash("iot!@#root");
	  		$user->phone_number="";
	  		$user->first_name="Admin";
	  		$user->last_name="admin";
	  		$user->email="";
	  		$user->status=10; //Active
	  		$user->created_by=1;
	  		$user->created_at=time();
	  		$user->updated_at=time();
	  		
	  		if($user->save())
	  		{
	  			$auth=Yii::$app->authManager;
	  			$superUser=$auth->getRole("super user");
	  			$auth->assign($superUser,$user->id);
	  			echo "user created successfully...".PHP_EOL;
	  		}
	  		
	  }
	  
	  
	  private function updateTimezone()
	  {
	  	   $rootPath=Yii::getAlias("@root");
	  	   $file=$rootPath."/static_files/timezone_list.json";
	  	    if(file_exists($file))
	  	    {	 
	  	    	$data=json_decode(file_get_contents($file),true);
	  	    	$size=count($data);
	  	    	
	  	    	 for ($i = 0; $i < $size; $i++) {
	  	    	 	$zone=new TimeZones();
	  	    	 	$zone->gmt=$data[$i]["gmt"];
	  	    	 	$zone->zone=$data[$i]["zone"];
	  	    	 	$zone->save();
	  	    	 }
	  	    	 
	  	    	 echo "Timezones created successfully...".PHP_EOL;
	  	    }
	  	   
	  }
	  
	  
	  public function actionUpdateRoles(){
	  	
	  		//v1
	  		
	  	Yii::$app->db->createCommand()->truncateTable("admin_user")->execute();
	  	
	  	$this->actionInstall();
	  	$this->initadminusers();
	  	$this->actionRoleChild();
	  		  	
	  }
	  
	  public function actionRoleChild(){
	  	
	  	 echo "role Child Updatnig .. \n";
	  	$auth=Yii::$app->authManager;
	  	$superUser=$auth->getRole("super user");
	  	$group_admin=$auth->getRole("Group Admin");
	  	$group_manager=$auth->getRole("Group Manager");
	  	$group_user=$auth->getRole("Group User");
	  	
	  	 $auth->addChild($superUser, $group_admin);
	  	 $auth->addChild($group_admin, $group_manager);
	  	 $auth->addChild($group_manager, $group_user);
	  	 
	  	 echo "role Child Updatnig completed \n";
	  	
	  }
	  
	  
	  public function actionTest(){
	  	 
	  	    $last=1525780704;
	  	
	  	    $datetime=new \DateTime();
	  	   // $datetime->modify("+ 2 minute");
	  	    
	  	    $end= new \DateTime();
	  	    $end->setTimestamp($last);
	  	    
	  	    $diff=$datetime->diff($end);
	  	    
	  	    echo $diff->i;
	  	       
	  	   //echo  round(abs($last - $datetime->getTimestamp()) / 60 , 2);
	  	    //echo $datetime->getTimestamp();

	  }
	  
	  
	
}