<?php 
namespace console\controllers;

use yii;
use yii\console\Controller;
use console\tasks\DownlinkJob;


class TestController extends Controller{
	
	
	public function actionIndex(){
		
		echo "Hello \n";
		
		$temperature=Yii::$app->temperature;
		$temperature->loadBase64Hex('owEBARNaXw==');
		//$val=$temperature->_periodicMessage(25.5,72,60);
		//$val=$temperature->_alertMessage(1,1,5,95,95);
		//echo $val."\n";
		//echo $temperature->decodeDownlinkConfig('A5');
		
		//echo $temperature->getHextoString();
	  	//print_r($temperature->periodicMessage());
	  
	    $temp=$temperature->getHexDownlinkConfig(2,40,50,10,90,50,1);
	    echo $temp;
	   //echo $temperature->decodeDownlinkConfig('A1474E44534F4C0100030202000002002D01F400785A324B01');
	  //print_r($temperature->getDecodeHealthCheckMsg('A1474E44534F4C0100030202000002002801F48005A3C04BD0'));
	   
	  // $encode=base64_encode($temp);
	    
	   //echo $encode;

	   //A401393C4C
	   //A1474E44534F4C0100030202000002002801F400645A3C4C01

		//B10002002801F48005A3C0



	}
	
	public function actionQueue(){
		
		$jobs = new DownlinkJob(["type"=>"Temperature","devices"=>[13],"data"=>[]]);
		$queue= Yii::$app->queue->push($jobs);
		
	}
	
	
	public function actionMail(){
		
		var_dump(Yii::$app->mailer->compose()
		->setFrom('anand.kanagaraj@gndsolutions.in')
		->setTo('subramani.krishnan@gndsolutions.in')
		->setSubject('Message subject')
		->setTextBody("Plain text content ")
		->setHtmlBody("<b>HTML content </b>")
		->send());
		
	}
	
	
	
}