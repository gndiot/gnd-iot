<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','queue'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    	'migrate-rbac' => [
    				'class' => 'yii\console\controllers\MigrateController',
    				'migrationPath' => '@yii/rbac/migrations',
    				'migrationTable' => 'migration_rbac',
    	 ],
    	'migrate-queue' => [
    				'class' => 'yii\console\controllers\MigrateController',
    				'migrationPath' => null,
    				'migrationNamespaces' => [
    						'yii\queue\db\migrations',
    				],
    		],
    ],
    'components' => [
    		'user' =>[
    				'class' => 'backend\models\AdminUser',
    				
    		],
    	'utils'=>[
    				'class'=>'backend\components\UtilsComponent'
    		],
    	'authManager'  => [
    		 'class'=> 'yii\rbac\DbManager',
    	 ],
    	'temperature'  => [
    				'class'=> 'backend\components\TemperatureComponent',
    	],
    	'mailer' => [
    		'class' => 'yii\swiftmailer\Mailer',
  			'useFileTransport' => YII_ENV_DEV?true:false,
    	],
         /* 'queue' => [
            'class' => \yii\queue\amqp_interop\Queue::class,
            'host' => 'localhost',
            'port' => 5672,
            'user' => 'guest',
            'password' => 'guest',
            'queueName' => 'queue',
         //	'vhost'=>'backup',
         	'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
        ], */
    	'queue' => [
    				'class' => \yii\queue\db\Queue::class,
    				'db' => 'db', // DB connection component or its config
    				'tableName' => '{{%queue}}', // Table name
    				'channel' => 'default', // Queue channel key
    				'as log' => \yii\queue\LogBehavior::class,
    				'mutex' => \yii\mutex\MysqlMutex::class, // Mutex used to sync queries
    		],
    	'clientAuthManager'  => [
    		  'class'=> 'yii\rbac\DbManager',
    		  'assignmentTable'=> '{{%customer_Auth_Assignment}}',
    		  'itemChildTable'=> '{{%customer_Auth_Item_Child}}',
    		  'itemTable'=> '{{%customer_Auth_Item}}',
    		  'ruleTable'=> '{{%customer_Auth_Rule}}',
    	],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    	'mailer' => [
    				'class' => 'yii\swiftmailer\Mailer',
    				'useFileTransport' => false,
    	],
    ],
    'params' => $params,
];
