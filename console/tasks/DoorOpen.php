<?php
namespace console\tasks;

use yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\httpclient\Client;

class DoorOpen extends BaseObject implements JobInterface{
	
	public $id;
	public $device_id;
	public $subscription_id;
	public $delay;
	
	public $push_url;
	
	public function execute($queue)
	{
		$this->sendMail($this->push_url);
		$client= new Client();
		$response=$client->createRequest()->setMethod("GET")->setUrl($this->push_url)->send();
		
		if($response->isOk)
		{
			//Yii::error($response);
		}
		
	}

	public function sendMail($queue){
	
	
		Yii::$app->mailer->compose()
		->setFrom('from@domain.com')
		->setTo('to@domain.com')
		->setSubject('Message subject')
		->setTextBody("Plain text content ")
		->setHtmlBody("<b>HTML content </b>")
		->send();
	
	}
	
}