<?php
namespace console\tasks;
use yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use common\models\Devices;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\helpers\Json;
use common\models\UplinkRequestLog;

class DownlinkJob extends BaseObject implements JobInterface{
	
	public $down_base_url='http://manage.gndsolutions.in/service/downlink';
	//public $down_base_url='http://iotmanage.test/service/downlink';
	 public $type;
	 public $request_id=5;
	 public $devices;
	 public $data;
	 public $requestTime=1;
	 
	public function execute($queue)
	{
		 Yii::error("error em");
		 
		
		
		
		 if($this->type=="Temperature")
		 {
		 	try {
		 		
		 		$this->Temperature();
		 		
		 	} catch (\Exception $e) {
		 		
		 		$this->sendMail(Json::encode($e->getMessage()));
		 	}
		 	
		 	
		 }
		 
		 if($this->type=="Battery")
		 {
		 	
		  	$this->battery();
		 	
		 }
			
		
	}
	
	
	public function battery(){
		
		$deviceids=$this->devices;
		
		//$this->sendMail(Json::encode($deviceids));
		
		
		$deviceslist=Devices::find()->where(['id'=>$deviceids])->all();
		
		$harwarelist=Devices::find()->select(['hardware_serial'])->where(['id'=>$deviceids])->asArray()->all();
		
		$harwareids=ArrayHelper::getColumn($harwarelist, 'hardware_serial');
		
		if(empty($harwareids))
		{
			return;
		}
		
		$data=[
				'hardware_ids'=>$harwareids,
		];
		
		$url = $this->down_base_url.'/battery-calibration';
		$this->makeHttpRequest($url, $data);
		foreach ($deviceslist as $device)
		{
			
			$device->bc_req_status=10;
			$device->bc_req_time=time();
			$device->save(false);
		}
		
		return $data;
		
		
		
	}
	
	
	public function Temperature(){
		
			
		$deviceids=$this->devices;
		$dev_config=$this->data;
		
		//$this->sendMail(Json::encode($deviceids));
		
		
		$deviceslist=Devices::find()->where(['id'=>$deviceids])->all();
		
		$harwarelist=Devices::find()->select(['hardware_serial'])->where(['id'=>$deviceids,'config_status'=>[1,0]])->asArray()->all();
		
		$harwareids=ArrayHelper::getColumn($harwarelist, 'hardware_serial');
		
		if(empty($harwareids))
		{
			return;
		}
	
		$data=[
				'hardware_ids'=>$harwareids,
				'configurations'=>$this->device_configuration($dev_config)
		];
		
	
		$url = $this->down_base_url.'/temperature';
		$this->makeHttpRequest($url, $data);
		foreach ($deviceslist as $device)
		{
			
			$device->config_status=10;
			$device->config_temp=Json::encode($data["configurations"]);
			$device->save(false);
		}
		
		return $data;
		  
		
	}
	
	
	private function makeHttpRequest($url,$data){
		
		
		$client = new Client();
		$response=$client->createRequest()->setFormat(Client::FORMAT_JSON)->setMethod('POST')->setUrl($url)->setData($data)->send();
		if($response->isOk){
			$model = new UplinkRequestLog();
			$model->request_url=$url;
			$model->request_data=Json::encode($data);
			$model->response_data=Json::encode($response->data);
			$model->created_at=time();
			$model->save(false);
			
		}
		
	}
	
	private function device_configuration($config=[]){
		
		if(is_null($config))
		{
			$config=[];
		}
		$default=[];
		$default['mode']=1;
		$default['reporting_interval']=2;
		$default['sampling_inteval']=30;
		$default["temperature_range"]=[
				"low"=>10,
				"high"=>50
		];
		$default["humidity_range"]=[
				"low"=>60,
				"high"=>90
		];
		$default["battery_range"]=[
				"low"=>20,
			
		];
		
		$level1=array_merge($default,$config);
		
		
		$result=[];
		
		foreach ($level1 as $key=>$value)
		{
			
			
			
			if(is_array($value) && array_key_exists($key, $config))
			{
				
				$level1[$key]=array_merge($default[$key],$config[$key]);
				$result[$key]=array_merge($default[$key],$config[$key]);
			}else{
				
				$result[$key]=$value;
			}
		}
		
		return $level1;
		
	}
	
	public function sendMail($data){
		
		
		Yii::$app->mailer->compose()
		->setFrom('from@domain.com')
		->setTo('to@domain.com')
		->setSubject('Message subject')
		->setTextBody($data)
		->send();
		
	}
	
	
	
	
}