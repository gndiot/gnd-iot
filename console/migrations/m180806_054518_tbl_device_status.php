<?php

use yii\db\Migration;

/**
 * Class m180806_054518_tbl_device_status
 */
class m180806_054518_tbl_device_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{device_status}}',[
    			'id'=>$this->primaryKey(),
    			'customer_id'=>$this->integer(),
    			'group_id'=>$this->integer(),
    			'device_ids'=>$this->text(),
    			'device_times'=>$this->text(),
    			'created_at'=>$this->integer(),
    	]);
    	
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{%device_status}}');
    }

}
