<?php

use yii\db\Migration;

/**
 * Class m180605_091306_data_column_added
 */
class m180605_091306_data_column_added extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	
    	$this->addColumn('devices', 'data', $this->text()->after('bc_req_time')->null());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	
      $this->dropColumn('devices', 'data');
      
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_091306_data_column_added cannot be reverted.\n";

        return false;
    }
    */
}
