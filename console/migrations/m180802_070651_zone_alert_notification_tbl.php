<?php

use yii\db\Migration;

/**
 * Class m180802_070651_zone_alert_notification_tbl
 */
class m180802_070651_zone_alert_notification_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{device_gps_zone_notification}}',[
    			'id'=>$this->primaryKey(),
    			'zone_alert_id'=>$this->integer(),
    			'customer_id'=>$this->integer(),
    			'group_id'=>$this->integer(),
    			'device_id'=>$this->integer(),
    			'alert_type'=>$this->string(),
    			'data'=>$this->text(),
    			'created_at'=>$this->integer(),
    	]);
    	
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    	$this->dropTable('{{device_gps_zone_notification}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180802_070651_zone_alert_notification_tbl cannot be reverted.\n";

        return false;
    }
    */
}
