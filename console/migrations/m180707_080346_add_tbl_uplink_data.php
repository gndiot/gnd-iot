<?php

use yii\db\Migration;

/**
 * Class m180707_080346_add_tbl_uplink_data
 */
class m180707_080346_add_tbl_uplink_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{uplink_request_log}}',[
    			'id'=>$this->primaryKey(),
    			'request_url'=>$this->text(),
    			'request_data'=>$this->text(),
    			'response_data'=>$this->text(),
    			'created_at'=>$this->integer(),
    			'status'=>$this->integer()->defaultValue(5)
    	],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{%uplink_request_log}}');
    	
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180707_080346_add_tbl_uplink_data cannot be reverted.\n";

        return false;
    }
    */
}
