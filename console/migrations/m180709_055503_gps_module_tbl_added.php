<?php

use yii\db\Migration;

/**
 * Class m180709_055503_gps_module_tbl_added
 */
class m180709_055503_gps_module_tbl_added extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{%device_gps_history}}', [
    			'id' => $this->bigPrimaryKey(),
    			'device_id'=>$this->integer()->null(),
    			'hardware_serial' => $this->string(),
    			'payload_raw' => $this->text(),
    			'message_type'=>$this->string(10),
    			'temperature_value'=>$this->decimal(3,1),
    			'humidity_value'=>$this->integer(),
    			'latitude'=>$this->decimal(10,8),
    			'longitude'=>$this->decimal(11,8),
    			'speed'=>$this->decimal(3,1),
    			'movement'=>$this->integer(),
    			'power_status'=>$this->integer(),
    			'battery_status'=>$this->integer(),
    			'battery_value'=>$this->integer(),
    			'sensor_battery_value'=>$this->integer(),
    			'created_at' => $this->integer()->notNull(),
    	], $tableOptions);
    	
    	$this->createTable('{{%device_gps_configurations}}', [
    			'id' => $this->bigPrimaryKey(),
    			'device_id'=>$this->integer()->null(),
    			'stauts'=>$this->integer(),
    			'start_value'=>$this->integer(),
    			'end_value'=>$this->integer(),
    			'low_temperature'=>$this->decimal(3,1),
    			'high_temperature'=>$this->decimal(3,1),
    			'temperature_alert'=>$this->integer(),
    			'high_humidity'=>$this->integer(),
    			'low_humidity'=>$this->integer(),
    			'humidity_alert'=>$this->integer(),
    			'movement_alert'=>$this->integer(),
    			'power_alert'=>$this->integer(),
    			'battery_alert'=>$this->integer(),
    			'battery_low_value'=>$this->integer(),
    			'periodic_interval'=>$this->integer(),
    			'updated_at' => $this->integer()->notNull(),
    	], $tableOptions);
    	
    	$this->createTable('{{%device_gps_configurations_temp}}', [
    			'id' => $this->bigPrimaryKey(),
    			'device_id'=>$this->integer()->null(),
    			'stauts'=>$this->integer(),
    			'low_temperature'=>$this->decimal(3,1),
    			'high_temperature'=>$this->decimal(3,1),
    			'start_value'=>$this->integer(),
    			'end_value'=>$this->integer(),
    			'temperature_alert'=>$this->integer(),
    			'humidity_alert'=>$this->integer(),
    			'high_humidity'=>$this->integer(),
    			'low_humidity'=>$this->integer(),
    			'movement_alert'=>$this->integer(),
    			'power_alert'=>$this->integer(),
    			'battery_alert'=>$this->integer(),
    			'battery_low_value'=>$this->integer(),
    			'periodic_interval'=>$this->integer(),
    			'updated_at' => $this->integer()->notNull(),
    	], $tableOptions);
    	
    	$this->createTable('{{%device_gps_recent_history}}', [
    			'id' => $this->bigPrimaryKey(),
    			'device_id'=>$this->integer()->null(),
    			'message_type'=>$this->string(10),
    			'temperature_value'=>$this->decimal(3,1),
    			'humidity_value'=>$this->integer(),
    			'latitude'=>$this->decimal(10,8),
    			'longitude'=>$this->decimal(11,8),
    			'speed'=>$this->decimal(3,1),
    			'movement'=>$this->integer(),
    			'power_status'=>$this->integer(),
    			'battery_status'=>$this->integer(),
    			'battery_value'=>$this->integer(),
    			'sensor_battery_value'=>$this->integer(),
    			'updated_at' => $this->integer()->notNull(),
    	], $tableOptions);
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{%device_gps_history}}');
    	$this->dropTable('{{%device_gps_configurations}}');
    	$this->dropTable('{{%device_gps_configurations_temp}}');
    	$this->dropTable('{{%device_gps_recent_history}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180709_055503_gps_module_tbl_added cannot be reverted.\n";

        return false;
    }
    */
}
