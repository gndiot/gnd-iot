<?php

use yii\db\Migration;

/**
 * Class m180608_061456_battery_status_column_added
 */
class m180608_061456_battery_status_column_added extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('devices', 'battery_level', $this->integer()->after('data')->null());
    	
    	$file=Yii::getAlias("@app");
    	
    	$this->execute(file_get_contents($file."/sql/notification_template.sql"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn('devices', 'battery_level');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180608_061456_battery_status_column_added cannot be reverted.\n";

        return false;
    }
    */
}
