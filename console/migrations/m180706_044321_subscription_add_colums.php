<?php

use yii\db\Migration;

/**
 * Class m180706_044321_subscription_add_colums
 */
class m180706_044321_subscription_add_colums extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('device_subscriptions', 'sensor_type', $this->integer()->after('id')->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn('device_subscriptions', 'sensor_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180706_044321_subscription_add_colums cannot be reverted.\n";

        return false;
    }
    */
}
