<?php

use yii\db\Migration;

/**
 * Class m180614_095319_haccp_device_table
 */
class m180614_095319_haccp_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	$this->createTable('{{%haccp_device}}', [
    			'id' => $this->primaryKey(),
    			'customer_id' => $this->integer()->defaultValue(0),
    			'group_id' => $this->integer()->defaultValue(0),
    			'name'=>$this->string(150)->null(),
    	], $tableOptions);
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{%haccp_device}}');
    }

}
