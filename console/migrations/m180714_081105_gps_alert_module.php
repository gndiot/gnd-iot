<?php

use yii\db\Migration;

/**
 * Class m180714_081105_gps_alert_module
 */
class m180714_081105_gps_alert_module extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    	
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{device_gps_subscriptions}}',[
    			'id'=>$this->primaryKey(),
    			'sensor_type'=>$this->integer(),
    			'customer_id'=>$this->integer(),
    			'group_id'=>$this->integer(),
    			'device_ids'=>$this->text(),
    			'is_custom'=>$this->integer(),
    			'condition_code'=>$this->integer(),
    			'value_one'=>$this->float(),
    			'value_two'=>$this->float(),
    			'attribute'=>$this->string(50),
    			'is_email'=>$this->boolean(),
    			'is_push'=>$this->boolean(),
    			'is_sms'=>$this->boolean(),
    			'emails'=>$this->string(50),
    			'mobile_numbers'=>$this->text(),
    			'is_default_email'=>$this->boolean(),
    			'is_default_sms'=>$this->boolean(),
    			'is_repeat'=>$this->boolean(),
    			'intervel_type'=>$this->string(),
    			'intervel_mins'=>$this->integer(),
    			'last_run_time'=>$this->integer(),
    			'next_run_time'=>$this->integer(),
    			'started_at'=>$this->integer(),
    			'notify_first_time'=>$this->boolean(),
    			'notification_status'=>$this->integer(),
    			'np_days'=>$this->string(50)->defaultValue("Mon|Tue|Wed|Thu|Fri|Sat|Sun"),
    			'np_start_time'=>$this->time()->defaultValue("00:00:00"),
    			'np_end_time'=>$this->time()->defaultValue("23:59:00"),
    			'created_at'=>$this->integer(),
    			'updated_at'=>$this->integer(),
    			'job_id'=>$this->integer(),
    	]);
    	
    	$this->createTable('{{device_gps_zone_alert}}',[
    			'id'=>$this->primaryKey(),
    			'sensor_type'=>$this->integer(),
    			'customer_id'=>$this->integer(),
    			'group_id'=>$this->integer(),
    			'device_ids'=>$this->text(),
    			'zone_type'=>$this->bigInteger(),
    			'zone'=>$this->bigInteger(),
    			'on_leave'=>$this->integer()->defaultValue(0),
    			'on_enter'=>$this->integer()->defaultValue(0),
    			'emails'=>$this->text(),
    			'mobile_numbers'=>$this->text(),
    			'status'=>$this->integer(),
    			'created_at'=>$this->integer(),
    			'updated_at'=>$this->integer(),
    			'job_id'=>$this->integer(),
    	]);
    	
    	$this->createTable('{{device_gps_zones}}',[
    			'id'=>$this->primaryKey(),
    			'customer_id'=>$this->integer(),
    			'group_id'=>$this->integer(),
    			'name'=>$this->string(100),
    			'zone_type'=>$this->bigInteger(),
    			'zone_data'=>$this->text(),
    			'status'=>$this->integer(),
    			'created_at'=>$this->integer(),
    			'updated_at'=>$this->integer()
    	]);

    	
     }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    	$this->dropTable('{{%device_gps_zones}}');
    	$this->dropTable('{{%device_gps_zone_alert}}');
    	$this->dropTable('{{%device_gps_subscriptions}}');
    	
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180714_081105_gps_alert_module cannot be reverted.\n";

        return false;
    }
    */
}
