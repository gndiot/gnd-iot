<?php

use yii\db\Migration;

/**
 * Class m180614_071953_rajtech_masters_add_columns_in_devices_table
 */
class m180614_071953_rajtech_masters_add_columns_in_devices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('devices', 'entity', $this->string()->after('data')->null());
    	$this->addColumn('devices', 'department', $this->string()->after('entity')->null());
    	$this->addColumn('devices', 'process', $this->string()->after('department')->null());
    	$this->addColumn('devices', 'tag', $this->integer()->after('process')->null());
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn('devices', 'entity');
    	$this->dropColumn('devices', 'department');
    	$this->dropColumn('devices', 'process');
    	$this->dropColumn('devices', 'tag');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180614_071953_rajtech_masters_add_columns_in_devices_table cannot be reverted.\n";

        return false;
    }
    */
}
