<?php

use yii\db\Migration;

/**
 * Class m180703_122253_add_subscription_period
 */
class m180703_122253_add_subscription_period extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		
		$this->addColumn('device_subscriptions', 'np_days', $this->string(50)->defaultValue("Mon|Tue|Wed|Thu|Fri|Sat|Sun")->after('notification_status')->null());
		$this->addColumn('device_subscriptions', 'np_start_time', $this->time()->defaultValue("00:00:00")->after('np_days')->null());
		$this->addColumn('device_subscriptions', 'np_end_time', $this->time()->defaultValue("23:59:00")->after('np_start_time')->null());
		
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		
		$this->dropColumn('device_subscriptions', 'np_days');
		$this->dropColumn('device_subscriptions', 'np_start_time');
		$this->dropColumn('device_subscriptions', 'np_end_time');
		
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180703_122253_add_subscription_period cannot be reverted.\n";

        return false;
    }
    */
}
