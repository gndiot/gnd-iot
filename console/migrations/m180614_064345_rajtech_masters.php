<?php

use yii\db\Migration;

/**
 * Class m180614_064345_rajtech_masters
 */
class m180614_064345_rajtech_masters extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	
    	$this->createTable('{{%master_entity}}', [
    			'id' => $this->primaryKey(),
    			'group_id' => $this->integer()->defaultValue(0),
    			'name'=>$this->string(150)->null(),
    	], $tableOptions);
    	
    	
    	$this->createTable('{{%master_process}}', [
    			'id' => $this->primaryKey(),
    			'group_id' => $this->integer()->defaultValue(0),
    			'name'=>$this->string(150)->null(),
    	], $tableOptions);
    	
    	$this->createTable('{{%master_department}}', [
    			'id' => $this->primaryKey(),
    			'group_id' => $this->integer()->defaultValue(0),
    			'name'=>$this->string(150)->null(),
    	], $tableOptions);
    	
    	$this->createTable('{{%master_tag}}', [
    			'id' => $this->primaryKey(),
    			'group_id' => $this->integer()->defaultValue(0),
    			'name'=>$this->string(150)->null(),
    	], $tableOptions);
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{%master_tag}}');
    	$this->dropTable('{{%master_entity}}');
    	$this->dropTable('{{%master_process}}');
    	$this->dropTable('{{%master_department}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180614_064345_rajtech_masters cannot be reverted.\n";

        return false;
    }
    */
}
