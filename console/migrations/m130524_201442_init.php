<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%admin_user}}', [
        		'id' => $this->primaryKey(),
        		'group_id'=>$this->integer()->null(),
        		'username' => $this->string()->notNull()->unique(),
        		'auth_key' => $this->string(32)->notNull(),
        		'password_hash' => $this->string()->notNull(),
        		'password_reset_token' => $this->string()->unique(),
        		'first_name'=>$this->string(20),
        		'last_name'=>$this->string(20),
        		'phone_number'=>$this->string(15),
        		'email' => $this->string()->notNull()->unique(),
        		'status' => $this->smallInteger()->notNull()->defaultValue(10),
        		'timezone'=>$this->string(10),
        		'timezone_description'=>$this->string(50),
        		'created_by'=>$this->integer(),
        		'created_at' => $this->integer()->notNull(),
        		'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->createTable('{{%customer_login}}', [
        		'id' => $this->primaryKey(),
        		'customer_id'=>$this->integer()->notNull(),
        		'group_id'=>$this->integer()->notNull(),
        		'username' => $this->string()->notNull()->unique(),
        		'auth_key' => $this->string(32)->notNull(),
        		'password_hash' => $this->string()->notNull(),
        		'password_reset_token' => $this->string()->unique(),
        		'first_name'=>$this->string(20),
        		'last_name'=>$this->string(20),
        		'phone_number'=>$this->string(15),
        		'email' => $this->string()->notNull()->unique(),
        		'status' => $this->smallInteger()->notNull()->defaultValue(10),
        		'timezone'=>$this->string(10),
        		'timezone_description'=>$this->string(50),
        		'created_by'=>$this->integer(),
        		'created_at' => $this->integer()->notNull(),
        		'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
       
        $this->createTable('{{time_zones}}',[
        		'id'=>$this->primaryKey(),
        		'gmt'=>$this->string(),
        		'zone'=>$this->string()
        ],$tableOptions);
        
        
        
        $this->createTable('{{admin_session}}',[
        		'id'=>$this->primaryKey(64),
        		'expire'=>$this->integer(),
        		'data'=>$this->binary()
        ],$tableOptions);
       
        
        $this->createTable('{{customer_session}}',[
        		'id'=>$this->primaryKey(64),
        		'expire'=>$this->integer(),
        		'data'=>$this->binary()
        ],$tableOptions);
        
        
        
        $this->createTable('{{devices}}',[
        		'id'=>$this->primaryKey(),
        		'hardware_serial'=>$this->string(255)->unique(),
        		'customer_id'=>$this->integer(),
        		'type'=>$this->integer(),
        		'name'=>$this->string(50),
        		'display_name'=>$this->string(),
        		'group_id'=>$this->integer(),
        		'start_value'=>$this->float(),
        		'end_value'=>$this->float(),
        		'response_intervel'=>$this->integer(),
        		'status'=>$this->integer(),
        		'created_at'=>$this->integer(),
        		'updated_at'=>$this->integer(),
        		'sync_time'=>$this->integer()->null(),
        		'sync_last_time'=>$this->integer()->null(),
        		'admin_id'=>$this->integer()->null(),
        		'customer_user_id'=>$this->integer()->null(),
        		'config_new'=>$this->text()->null(),
        		'config_temp'=>$this->text()->null(),
        		'config_status'=>$this->integer()->defaultValue(0)->null(),
        		'bc_req_status'=>$this->integer()->null(),
        		'bc_req_time'=>$this->integer()->null()
        ]);
        
       	$this->createTable('{{device_config_temperature}}',[
       			  'id'=>$this->primaryKey(),
       			  'device_id'=>$this->integer(),
       			  'start'=>$this->float(),
       			  'end'=>$this->float(),
       			  'level'=>$this->string(),
       			  'condition_code'=>$this->integer()->defaultValue(60),
       			  'color_code'=>$this->string(20)
       	]);
       	
       	
       	$this->createTable('{{dev_config_battery}}',[
       			'id'=>$this->primaryKey(),
       			'device_id'=>$this->integer(),
       			'start'=>$this->float(),
       			'end'=>$this->float(),
       			'level'=>$this->string(),
       			'condition_code'=>$this->integer()->defaultValue(30),
       			'color_code'=>$this->string(20)
       	]);
       	
       	$this->createTable('{{dev_config_humidity}}',[
       			'id'=>$this->primaryKey(),
       			'device_id'=>$this->integer(),
       			'start'=>$this->float(),
       			'end'=>$this->float(),
       			'level'=>$this->string(),
       			'condition_code'=>$this->integer()->defaultValue(30),
       			'color_code'=>$this->string(20)
       	]);
        
       	$this->createTable('{{device_door_readings}}', [
       			'id'=>$this->primaryKey(),
       			'device_id'=>$this->string(11),
       			'hardware_serial'=>$this->string(),
       			'payload_raw'=>$this->text(),
       			'battery'=>$this->integer(),
       			'status'=>$this->integer(),
       			'created_at'=>$this->integer(),
       	]);
       	
       	$this->execute("CREATE TRIGGER `before_door_readings` BEFORE INSERT ON `device_door_readings`
 FOR EACH ROW SET NEW.device_id=CONVERT((SELECT id FROM devices WHERE devices.hardware_serial=NEW.hardware_serial),UNSIGNED INTEGER)");
       	
       	$this->execute("CREATE TRIGGER `after_door_readings` AFTER INSERT ON `device_door_readings`
 FOR EACH ROW UPDATE devices SET devices.sync_time=(@temp:=devices.sync_time), devices.sync_time =NEW.created_at, devices.sync_last_time = @temp WHERE devices.hardware_serial=NEW.hardware_serial");
       	
       	
       	
   
       	$this->createTable('{{device_temperature_readings}}', [
       			'id'=>$this->primaryKey(),
       			'device_id'=>$this->string(11),
       			'hardware_serial'=>$this->string(),
       			'payload_raw'=>$this->text(),
       			'battery'=>$this->integer(),
       			'temperature'=>$this->float(),
       			'humidity'=>$this->integer(),
       			'created_at'=>$this->integer(),
       	]);
       	
       	$this->execute("CREATE TRIGGER `before_temp_readings` BEFORE INSERT ON `device_temperature_readings`
 FOR EACH ROW SET NEW.device_id=CONVERT((SELECT id FROM devices WHERE devices.hardware_serial=NEW.hardware_serial),UNSIGNED INTEGER)");
       	
       	$this->execute("CREATE TRIGGER `after_temp_readings` AFTER INSERT ON `device_temperature_readings`
 FOR EACH ROW UPDATE devices SET devices.sync_time=(@temp:=devices.sync_time), devices.sync_time =NEW.created_at, devices.sync_last_time = @temp WHERE devices.hardware_serial=NEW.hardware_serial");
       	
       	
       	
       	$this->createTable('{{device_gps_readings}}', [
       			'id'=>$this->primaryKey(),
       			'device_id'=>$this->string(11),
       			'hardware_serial'=>$this->string(),
       			'payload_raw'=>$this->text(),
       			'battery'=>$this->integer(),
       			'configuration_info'=>$this->integer(),
       			'configuration_info_two'=>$this->integer(),
       			'door'=>$this->integer(),
       			'humidity'=>$this->integer(),
       			'l_latitude'=>$this->decimal(10,8),
       			'l_longitude'=>$this->decimal(11,8),
       			'movement'=>$this->integer(),
       			'switchone'=>$this->integer(),
       			'switch_two'=>$this->integer(),
       			'temperature'=>$this->float(),
       			'humidity'=>$this->integer(),
       			'created_at'=>$this->integer(),
       	]);
       	
       	$this->execute("CREATE TRIGGER `before_gps_readings` BEFORE INSERT ON `device_gps_readings`
 FOR EACH ROW SET NEW.device_id=CONVERT((SELECT id FROM devices WHERE devices.hardware_serial=NEW.hardware_serial),UNSIGNED INTEGER)");
       	
       	$this->execute("CREATE TRIGGER `after_gps_readings` AFTER INSERT ON `device_gps_readings`
 FOR EACH ROW UPDATE devices SET devices.sync_time=(@temp:=devices.sync_time), devices.sync_time =NEW.created_at, devices.sync_last_time = @temp WHERE devices.hardware_serial=NEW.hardware_serial");
       	
       	
       	
       	$this->createTable('{{device_type}}', [
       			 'id'=>$this->primaryKey(),
       			 'name'=>$this->string(),
       			 'parent_id'=>$this->integer(),
       	]);
        
        
        $this->createTable('{{device_groups}}',[
        		 'id'=>$this->primaryKey(),
        		 'parent_id'=>$this->integer(),
        		 'name'=>$this->string(),
        		 'description'=>$this->string(100),
        ]);
        
        
        $this->createTable('{{device_subscriptions}}',[
        		  'id'=>$this->primaryKey(),
        		  'customer_id'=>$this->integer(),
        		  'group_id'=>$this->integer(),
        		  'dev_id'=>$this->integer(),
        		  'is_custom'=>$this->integer(),
        		  'condition_code'=>$this->integer(),
        		  'value_one'=>$this->float(),
        		  'value_two'=>$this->float(),
        		  'attribute'=>$this->string(50),
        		  'is_email'=>$this->boolean(),
        		  'is_push'=>$this->boolean(),
        		  'is_sms'=>$this->boolean(),
        		  'email'=>$this->string(50),
        		  'mobile_numbers'=>$this->string(20),
        		  'is_default_email'=>$this->boolean(),
        		  'is_default_sms'=>$this->boolean(),
        		  'is_repeat'=>$this->boolean(),
        		  'intervel_type'=>$this->string(),
        		  'intervel_mins'=>$this->integer(),
        		  'last_run_time'=>$this->integer(),
        		  'next_run_time'=>$this->integer(),
        		  'started_at'=>$this->integer(),
        		  'notify_first_time'=>$this->boolean(),
        		  'notification_status'=>$this->integer(),
        		  'created_at'=>$this->integer(),
        		  'updated_at'=>$this->integer(),
        		  'job_id'=>$this->integer(),
        ]);
        
        
        $this->createTable('{{device_alerts}}', [
        		'id'=>$this->primaryKey(),
        		'customer_id'=>$this->integer()->null(),
        		'group_id'=>$this->integer()->null(),
        		'device_id'=>$this->integer(),
        		'alert_type'=>$this->string(),
        		'body'=>$this->text(),
        		'sms'=>$this->integer(),
        		'email'=>$this->integer(),
        		'push'=>$this->integer(),
        		'notes'=>$this->text(),
        		'payload'=>$this->string(),
        		'status'=>$this->integer(),
        		'created_at'=>$this->integer(),
        		'updated_at'=>$this->integer(),
        		'user_id'=>$this->integer()->null()
        ]);
        
       /*  $this->execute("CREATE VIEW device_temperature_reading_base AS SELECT device_temperature_readings.id,device_temperature_readings.device_id,device_temperature_readings.hardware_serial,device_temperature_readings.battery,device_temperature_readings.temperature,device_temperature_readings.humidity,HOUR(FROM_UNIXTIME(created_at)) as 'device_hour',DATE(FROM_UNIXTIME(created_at)) AS device_date,TIME(FROM_UNIXTIME(created_at)) as device_time,FROM_UNIXTIME(created_at) AS formated_date_time FROM `device_temperature_readings`");
        
        $this->execute("CREATE VIEW device_temperature_daily_summary AS SELECT dev_temp_base.device_id,dev_temp_base.device_date,(SELECT MIN(dev_am.temperature) as am_min FROM `device_temperature_reading_base` as dev_am WHERE  dev_am.device_hour BETWEEN  00 AND 11 AND dev_am.device_date=dev_temp_base.device_date AND dev_am.device_id=dev_temp_base.device_id) as am_min,(SELECT MAX(dev_am.temperature) as am_max FROM `device_temperature_reading_base` as dev_am WHERE  dev_am.device_hour BETWEEN  00 AND 11 AND dev_am.device_date=dev_temp_base.device_date AND dev_am.device_id=dev_temp_base.device_id) as am_max,(SELECT AVG(dev_am.temperature) as am_avg FROM `device_temperature_reading_base` as dev_am WHERE  dev_am.device_hour BETWEEN  00 AND 11 AND dev_am.device_date=dev_temp_base.device_date AND dev_am.device_id=dev_temp_base.device_id)as am_avg,(SELECT count(dev_am.temperature) as am_samples FROM `device_temperature_reading_base` as dev_am WHERE  dev_am.device_hour BETWEEN  00 AND 11 AND dev_am.device_date=dev_temp_base.device_date AND dev_am.device_id=dev_temp_base.device_id) as am_samples,(SELECT MIN(dev_pm.temperature) as pm_min FROM `device_temperature_reading_base` as dev_pm WHERE  dev_pm.device_hour BETWEEN  12 AND 23 AND dev_pm.device_date=dev_temp_base.device_date AND dev_pm.device_id=dev_temp_base.device_id) as pm_min,(SELECT MAX(dev_pm.temperature) as pm_max FROM `device_temperature_reading_base` as dev_pm WHERE  dev_pm.device_hour BETWEEN  12 AND 23 AND dev_pm.device_date=dev_temp_base.device_date AND dev_pm.device_id=dev_temp_base.device_id) as pm_max,(SELECT AVG(dev_pm.temperature) as pm_avg FROM `device_temperature_reading_base` as dev_pm WHERE  dev_pm.device_hour BETWEEN  12 AND 23 AND dev_pm.device_date=dev_temp_base.device_date AND dev_pm.device_id=dev_temp_base.device_id) as pm_avg,(SELECT count(dev_pm.temperature) as pm_samples FROM `device_temperature_reading_base` as dev_pm WHERE  dev_pm.device_hour BETWEEN  12 AND 23 AND dev_pm.device_date=dev_temp_base.device_date AND dev_pm.device_id=dev_temp_base.device_id) as pm_sample,(SELECT MIN(dev_total.temperature) as total_min FROM `device_temperature_reading_base` as dev_total WHERE dev_total.device_date=dev_temp_base.device_date AND dev_total.device_id=dev_temp_base.device_id) as total_min,(SELECT MAX(dev_total.temperature) as total_max FROM `device_temperature_reading_base` as dev_total WHERE dev_total.device_date=dev_temp_base.device_date AND dev_total.device_id=dev_temp_base.device_id) as total_max,(SELECT AVG(dev_total.temperature) as total_avg FROM `device_temperature_reading_base` as dev_total WHERE dev_total.device_date=dev_temp_base.device_date AND dev_total.device_id=dev_temp_base.device_id) as total_avg,(SELECT count(dev_total.temperature) as total_samples FROM `device_temperature_reading_base` as dev_total WHERE dev_total.device_date=dev_temp_base.device_date AND dev_total.device_id=dev_temp_base.device_id) as total_sample
 FROM device_temperature_reading_base as dev_temp_base GROUP BY DATE(dev_temp_base.device_date),dev_temp_base.device_id");
        
        $this->execute("CREATE VIEW device_temperature_daily_2hrs AS SELECT dev_temp_base.device_id,dev_temp_base.device_date,(SELECT AVG(dev_1.temperature) as am_avg FROM `device_temperature_reading_base` as dev_1 WHERE  dev_1.device_hour BETWEEN  00 AND 01 AND dev_1.device_date=dev_temp_base.device_date AND dev_1.device_id=dev_temp_base.device_id) as '00:00-2:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  02 AND 03 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '2:00-4:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  04 AND 05 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '4:00-6:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  06 AND 07 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '6:00-8:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  08 AND 09 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '8:00-10:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  10 AND 11 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '10:00-12:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  12 AND 13 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '12:00-14:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  14 AND 15 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '14:00-16:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  16 AND 17 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '16:00-18:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  18 AND 19 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '18:00-20:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  20 AND 21 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '20:00-22:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  22 AND 23 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '22:00-00:00' FROM `device_temperature_daily_summary` as dev_temp_base GROUP BY DATE(dev_temp_base.device_date),dev_temp_base.device_id");
        
        $this->execute("CREATE VIEW device_temperature_daily_4hrs AS SELECT dev_temp_base.device_id,dev_temp_base.device_date,(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  00 AND 03 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '00:00-04:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  04 AND 07 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '04:00-08:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  08 AND 11 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '08:00-12:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  12 AND 15 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '12:00-16:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  16 AND 19 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '16:00-20:00',(SELECT AVG(dev_2.temperature) as am_avg FROM `device_temperature_reading_base` as dev_2 WHERE  dev_2.device_hour BETWEEN  20 AND 23 AND dev_2.device_date=dev_temp_base.device_date AND dev_2.device_id=dev_temp_base.device_id) as '20:00-00:00' FROM `device_temperature_reading_base` as dev_temp_base GROUP BY DATE(dev_temp_base.device_date),dev_temp_base.device_id");
        
        $this->execute("CREATE VIEW device_temperature_month_report AS SELECT device_temperature_reading_base.device_id,device_temperature_reading_base.device_Date,MIN(`temperature`) as low_reading,MAX(`temperature`) AS high_reading,AVG(`temperature`) as total_avg,count(`id`) AS total_samples,(SELECT COUNT(*) FROM `device_temperature_reading_base` as dev_out_range WHERE dev_out_range.temperature NOT BETWEEN (SELECT device_config_temperature.start FROM device_config_temperature WHERE  device_config_temperature.device_id=dev_out_range.device_id AND device_config_temperature.color_code ='Green') AND (SELECT device_config_temperature.end FROM device_config_temperature WHERE  device_config_temperature.device_id=dev_out_range.device_id AND device_config_temperature.color_code ='Green') AND dev_out_range.device_id=device_temperature_reading_base.device_id) as out_of_samples FROM `device_temperature_reading_base` GROUP BY MONTH(device_date),device_id ");
           */
          
    }

    public function down()
    {
//     	$this->dropTable('{{%device_temperature_reading_base}}');
//     	$this->dropTable('{{%device_temperature_daily_summary}}');
//     	$this->dropTable('{{%device_temperature_daily_2hrs}}');
//     	$this->dropTable('{{%device_temperature_daily_4hrs}}');
//     	$this->dropTable('{{%device_temperature_month_report}}');
        $this->dropTable('{{%customer_login}}');
        $this->dropTable('{{%admin_user}}');
        $this->dropTable('{{%admin_session}}');
        $this->dropTable('{{%customer_session}}');
        $this->dropTable('{{%devices}}');
        $this->dropTable('{{%device_config_temperature}}');
        $this->dropTable('{{%dev_config_humidity}}');
        $this->dropTable('{{%dev_config_battery}}');
        
        $this->dropTable('{{%device_door_readings}}');
        $this->execute("DROP TRIGGER IF EXISTS `before_door_readings`");
        $this->execute("DROP TRIGGER IF EXISTS `after_door_readings`");
        
        $this->dropTable('{{%device_temperature_readings}}');
        $this->execute("DROP TRIGGER IF EXISTS `before_temp_readings`");
        $this->execute("DROP TRIGGER IF EXISTS `after_temp_readings`");
        
        $this->dropTable('{{%device_gps_readings}}');
        $this->execute("DROP TRIGGER IF EXISTS `before_gps_readings`");
        $this->execute("DROP TRIGGER IF EXISTS `before_gps_readings`");
       
        $this->dropTable('{{%device_type}}');
        $this->dropTable('{{%device_subscriptions}}');
        $this->dropTable('{{%device_groups}}');
        $this->dropTable('{{%time_zones}}');
        $this->dropTable('{{%device_alerts}}');
    }
}
