<?php

use yii\db\Migration;

/**
 * Class m180616_073607_update_colum_devices
 */
class m180616_073607_update_colum_devices extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		
    	$this->addColumn('devices', 'equipement_id', $this->string(150)->after('tag')->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn('devices', 'equipement_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_073607_update_colum_devices cannot be reverted.\n";

        return false;
    }
    */
}
