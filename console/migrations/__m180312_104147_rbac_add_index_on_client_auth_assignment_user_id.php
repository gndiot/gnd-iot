<?php

use yii\db\Migration;

/**
 * Class m180312_104147_rbac_add_index_on_client_auth_assignment_user_id
 */
class m180312_104147_rbac_add_index_on_client_auth_assignment_user_id extends Migration
{
	public $column = 'user_id';
	public $index = 'client_auth_assignment_user_id_idx';
	
	/**
	 * @throws yii\base\InvalidConfigException
	 * @return DbManager
	 */
	protected function getAuthManager()
	{
		$authManager = Yii::$app->clientAuthManager;
		if (!$authManager instanceof DbManager) {
			throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
		}
	
		return $authManager;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function up()
	{
		$authManager = Yii::$app->clientAuthManager;
		$this->createIndex($this->index, $authManager->assignmentTable, $this->column);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function down()
	{
		$authManager = Yii::$app->clientAuthManager;
		$this->dropIndex($this->index, $authManager->assignmentTable);
	}
}
