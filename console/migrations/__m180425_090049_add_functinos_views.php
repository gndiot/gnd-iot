<?php

use yii\db\Migration;

/**
 * Class m180425_090049_add_functinos_views
 */
class __m180425_090049_add_functinos_views extends Migration
{
   
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    	$this->execute("
CREATE FUNCTION `device_temp_alert`(mdevice INT,mvalue FLOAT,mlevel VARCHAR(50)) RETURNS tinyint(4)
BEGIN
DECLARE mstart_value FLOAT;
DECLARE mend_value FLOAT;
DECLARE mResult tinyint;
DECLARE creading INT;

(SELECT dev_config_temp_alert.start_value,dev_config_temp_alert.end_value FROM dev_config_temp_alert WHERE dev_config_temp_alert.device_id=mdevice AND dev_config_temp_alert.level=mlevel INTO mstart_value,mend_value);

IF (mvalue >= mstart_value AND mvalue <= mend_value) THEN 
    SET mResult=1;
ELSE 
    SET mResult=0;
END IF;

return mResult;
END");
    	
    $this->execute("CREATE VIEW device_temperature_alert_view AS 
    		SELECT  devices.id as device_id,device_temp_alert(devices.id,(SELECT device_temperature_readings.temperature FROM device_temperature_readings WHERE device_temperature_readings.device_id=devices.id ORDER BY device_temperature_readings.id DESC LIMIT 1),'low') AS low_alert,device_temp_alert(devices.id,(SELECT device_temperature_readings.temperature FROM device_temperature_readings WHERE device_temperature_readings.device_id=devices.id ORDER BY device_temperature_readings.id DESC LIMIT 1),'high') AS high_alert,device_temp_alert(devices.id,(SELECT device_temperature_readings.temperature FROM device_temperature_readings WHERE device_temperature_readings.device_id=devices.id ORDER BY device_temperature_readings.id DESC LIMIT 1),'mediam') AS mediam_alert FROM `devices`
    		");
    }

    public function down()
    {
       $this->execute("DROP FUNCTION IF EXISTS device_temp_alert");
       $this->execute("DROP VIEW IF EXISTS device_temperature_alert_view");
        
    }
}
