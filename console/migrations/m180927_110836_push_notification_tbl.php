<?php

use yii\db\Migration;

/**
 * Class m180927_110836_push_notification_tbl
 */
class m180927_110836_push_notification_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{user_tokens}}',[
    			'id'=>$this->primaryKey(),
    			'user_id'=>$this->integer(),
    			'source'=>$this->string(),
    			'access_token'=>$this->string(),
    			'fcm_token'=>$this->text(),
    			'device_unique'=>$this->string(),
    			'device_manufacture'=>$this->string(),
    			'device_model'=>$this->string(),
    			'device_version'=>$this->string(),
    			'app_version'=>$this->string(),
    			'created_at'=>$this->integer(),
    	]);
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{user_tokens}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180927_110836_push_notification_tbl cannot be reverted.\n";

        return false;
    }
    */
}
