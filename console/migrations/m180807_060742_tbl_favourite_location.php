<?php

use yii\db\Migration;

/**
 * Class m180807_060742_tbl_favourite_location
 */
class m180807_060742_tbl_favourite_location extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{favourite_location}}',[
    			'id'=>$this->primaryKey(),
    			'customer_id'=>$this->integer(),
    			'group_id'=>$this->integer(),
    			'name'=>$this->string(),
    			'data'=>$this->text(),
    			'created_at'=>$this->integer(),
    	]);
    	
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{favourite_location}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180807_060742_tbl_favourite_location cannot be reverted.\n";

        return false;
    }
    */
}
