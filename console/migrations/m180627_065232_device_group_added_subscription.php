<?php

use yii\db\Migration;

/**
 * Class m180627_065232_device_group_added_subscription
 */
class m180627_065232_device_group_added_subscription extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		
    	$this->addColumn('device_subscriptions', 'device_ids', $this->text()->after('dev_id')->null());
    	$this->addColumn('device_subscriptions', 'emails', $this->text()->after('email')->null());
    	$this->addColumn('device_subscriptions', 'numbers', $this->text()->after('mobile_numbers')->null());
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    	$this->dropColumn('device_subscriptions', 'device_ids');
    	$this->dropColumn('device_subscriptions', 'emails');
    	$this->dropColumn('device_subscriptions', 'numbers');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_065232_device_group_added_subscription cannot be reverted.\n";

        return false;
    }
    */
}
