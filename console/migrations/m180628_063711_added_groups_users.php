<?php

use yii\db\Migration;

/**
 * Class m180628_063711_added_groups_users
 */
class m180628_063711_added_groups_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		
    	$this->addColumn('admin_user', 'group_ids', $this->text()->after('group_id')->null());
    	$this->addColumn('admin_user', 'customer_id', $this->text()->after('group_ids')->null());
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn('admin_user', 'group_ids');
    	$this->dropColumn('admin_user', 'customer_id');
    	
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180628_063711_added_groups_users cannot be reverted.\n";

        return false;
    }
    */
    
}
