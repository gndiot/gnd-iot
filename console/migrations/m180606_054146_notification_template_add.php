<?php

use yii\db\Migration;

/**
 * Class m180606_054146_notification_template_add
 */
class m180606_054146_notification_template_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{%notification_template}}', [
    			'id' => $this->primaryKey(),
    			'alert_type' => $this->string(50)->notNull(),
    			'template_type' => $this->string(50)->notNull(),
    			'subject' => $this->string(),
    			'content_body'=>$this->text(),
    	], $tableOptions);
    	
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		
    	$this->dropTable('{{%notification_template}}');
    		
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180606_054146_notification_template_add cannot be reverted.\n";

        return false;
    }
    */
}
