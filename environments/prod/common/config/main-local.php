<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host={DB_HOST};dbname={DB_NAME}',
            'username' => '{DB_USERNAME}',
            'password' => '{DB_PASSWORD}',
            'charset' => 'utf8',
        	'on afterOpen'=>function($event){
        		
        	$user = Yii::$app->user;
        	if(!array_key_exists('identity', $user))
        	{
        		return false;
        	}
        	$user=$user->identity;
        	
        		if ($user && $user->timezone !='') {
        			
        			$offset=$user->getTimezone();
        			
        			$event->sender->createCommand("SET time_zone='{$offset}';")->execute();
        		}else{
        			
        			$offset='+05:30';
        			
        			$event->sender->createCommand("SET time_zone='{$offset}';")->execute();
        			
        		}
        		
        		}
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
